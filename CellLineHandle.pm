# Author: jkh1
# 2011-10-10
#

=head1 NAME

 Mitocheck::CellLineHandle

=head1 SYNOPSIS


=head1 DESCRIPTION

 Enables retrieval of Mitocheck cell line objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::CellLineHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::CellLine;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new cell line object handle
 Returntype: Mitocheck::CellLineHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: cell line id
 Description: Gets cell line with given id
 Returntype: Mitocheck::CellLine object

=cut

sub get_by_id {

  my ($self,$name) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};

  my $query = qq(SELECT cell_lineID FROM cell_line WHERE cell_lineID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($name);
  my ($cell_lineID) = $sth->fetchrow_array();
  if ($cell_lineID) {
    return Mitocheck::CellLine->new($dbc,$cell_lineID);
  }
  else {
    return undef;
  }
}

=head2 get_by_name

 Arg: cell line name
 Description: Gets cell line with given name
 Returntype: Mitocheck::CellLine object

=cut

sub get_by_name {

  my ($self,$name) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};

  my $query = qq(SELECT cell_lineID FROM cell_line WHERE name= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($name);
  my ($cell_lineID) = $sth->fetchrow_array();
  if ($cell_lineID) {
    return Mitocheck::CellLine->new($dbc,$cell_lineID);
  }
  else {
    return undef;
  }
}

=head2 get_all_by_image_set

 Arg: Mitocheck::ImageSet
 Description: Gets cell lines associated with given image set
 Returntype: list of Mitocheck::CellLine objects

=cut

sub get_all_by_image_set {

  my ($self,$set) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $setID = $set->ID();
  my $sth = $dbh->prepare("SELECT DISTINCT cell_lineID
                           FROM image
                           WHERE image_setID= ?");
  $sth->execute($setID);
  my @cell_lines;
  while (my ($id)=$sth->fetchrow_array()) {
    my $cell_line = $self->get_by_id($id);
    push (@cell_lines, $cell_line) if $cell_line;
  }
  $sth->finish();

  return @cell_lines;
}

=head2 store

 Arg: Mitocheck::CellLine
 Description: Enter cell line and associated data in Mitocheck database.
 Returntype: Mitocheck::CellLine, same as Arg

=cut

sub store {

  my ($self,$cell_line) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $qname = $dbh->quote($cell_line->name());
  my $desc = $cell_line->description() || "";
  my $qdesc = $dbh->quote($desc);
  my $species = $cell_line->species() || "";
  my $qspecies = $dbh->quote($species);

  # Check if already in database (by name)
  my $query =qq(SELECT lineID FROM cell_line WHERE name=$qname);
  my $sth = $dbh->prepare($query);
  $sth->execute();
  my ($lineID)=$sth->fetchrow_array();
  $sth->finish();
  if (!$lineID) {
    $lineID = $cell_line->ID();
    # Add to database
    $query=qq(INSERT INTO cell_line (lineID,name,description,species) VALUES ('$lineID',$qname,$qdesc,$qspecies));
    $dbh->do($query);
  }
  else { $cell_line->ID($lineID) }

  $cell_line->_decr_newID_count() if $cell_line->{'is_new'};
  $cell_line->{'is_new'} = undef;

  return $cell_line;
}

sub remove {

  my ($self,$cell_line) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $lineID = $cell_line->ID;
  my $query=qq(DELETE FROM cell_line WHERE lineID='$lineID');
  $dbh->do($query);

  return $cell_line->ID;
}

1;
