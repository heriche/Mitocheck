# Author: jkh1
# 2008-05-12
#

=head1 NAME

 Mitocheck::Peptide

=head1 SYNOPSIS


=head1 DESCRIPTION

 Representation of peptides obtained in mass spectrometry experiments.
 Here we define a peptide as a particular occurrence of a short amino-acid
 sequence.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Peptide;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_occ_count = 0;
  my $_newID_count = 0;

  sub get_occurrence_count {
    return $_occ_count;
  }
  sub _incr_occurrence_count {
    return ++$_occ_count;
  }
  sub _decr_occurrence_count {
    return --$_occ_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }

}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, occurrence ID
 Description: Creates a new peptide object.
 Returntype: Mitocheck::Peptide

=cut

sub new {

  my ($class,$dbc,$occurrenceID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbc->{'database_handle'};
  $class->_incr_occurrence_count();
  if (!defined $self->{'ID'} && !$occurrenceID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   occurrenceID
                 FROM     peptide_occurrence
                 ORDER BY occurrenceID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $occurrenceID = "PEP"."0"x(8-length $i).$i;;
    $self->{'is_new'} = 1;

  }
  $self->{'ID'} = $occurrenceID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, occurrenceID
 Description: Gets/sets occurrence ID
 Returntype: integer

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 sequence

 Arg: (optional) string
 Description: Gets/sets this peptide's sequence
 Returntype: string

=cut

sub sequence {

  my ($self,$seq) = @_;
  $self->{'sequence'} = $seq if $seq;
  if (!defined $self->{'sequence'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT p.sequence
                             FROM peptide p, peptide_occurrence po
                             WHERE po.occurrenceID = ?
                             AND po.peptideID = p.peptideID");
    $sth->execute($self->ID);
    ($self->{'sequence'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'sequence'};

}

=head2 score

 Arg: (optional) integer
 Description: Gets/sets this peptide's score
 Returntype: integer

=cut

sub score {

  my ($self,$score) = @_;
  $self->{'score'} = $score if $score;
  if (!defined $self->{'score'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT score
                             FROM peptide_occurrence
                             WHERE occurrenceID = ?");
    $sth->execute($self->ID);
    ($self->{'score'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'score'};

}

=head2 evalue

 Arg: (optional) double
 Description: Gets/sets this peptide's e-value
 Returntype: double

=cut

sub evalue {

  my ($self,$evalue) = @_;
  $self->{'evalue'} = $evalue if $evalue;
  if (!defined $self->{'evalue'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT evalue
                             FROM peptide_occurrence
                             WHERE occurrenceID = ?");
    $sth->execute($self->ID);
    ($self->{'evalue'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'evalue'};

}

=head2 bait

 Arg: (optional) Mitocheck::Protein
 Description: Gets/sets the bait this peptide instance was recovered with
 Returntype: Mitocheck::Protein

=cut

sub bait {

  my ($self,$bait) = @_;
  $self->{'bait'} = $bait if $bait;
  if (!defined $self->{'bait'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT baitID
                             FROM peptide_occurrence
                             WHERE occurrenceID = ?");
    $sth->execute($self->ID);
    my ($baitID) = $sth->fetchrow_array();
    $sth->finish();
    if ($baitID) {
      my $ph = $self->{'DBConnection'}->get_ProteinHandle();
      $self->{'bait'} = $ph->get_by_id($baitID);
    }
  }
  return $self->{'bait'};

}

=head2 experimentID

 Arg: (optional) string
 Description: Gets/sets the experiment in which this peptide was recovered
 Returntype: string

=cut

sub experimentID {

  my ($self,$expID) = @_;
  $self->{'expID'} = $expID if $expID;
  if (!defined $self->{'expID'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT experimentID
                             FROM peptide_occurrence
                             WHERE occurrenceID = ?");
    $sth->execute($self->ID);
    ($self->{'expID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'expID'};

}

=head2 get_proteins

 Arg: none
 Description: Gets the proteins this peptide maps to
 Returntype: list of Mitocheck::Protein objects

=cut

sub get_proteins {

  my $self = shift;
  if (!defined($self->{'proteins'})) {
    my $dbh = $self->{'database_handle'};
    my $ph = $self->{'DBConnection'}->get_ProteinHandle();
    my $sth = $dbh->prepare("SELECT DISTINCT pmi.proteinID
                             FROM peptide_map_info pmi, peptide_occurrence po
                             WHERE po.occurrenceID = ?
                             AND po.peptideID = pmi.peptideID");
    $sth->execute($self->ID);
    while (my ($protID) = $sth->fetchrow_array()) {
      if ($protID) {
	push @{$self->{'proteins'}},$ph->get_by_id($protID);
      }
    }
  }
  return @{$self->{'proteins'}} if (defined($self->{'proteins'}));

}

=head2 coordinates

 Arg: Mitocheck::Protein
 Description: Gets coordinates of this peptide on the given protein
 Returntype: list: start1, end1, start2, end2...
             (can get multiple pairs if peptide maps to several parts of the protein)

=cut

sub coordinates {

  my $self = shift;
  my $protein = shift if @_;
  unless (defined($protein)) {
    die "WARNING: Protein required";
  }
  @{$self->{'coordinates'}} = ();
  my $pept_occurrenceID = $self->ID();
  my $proteinID = $protein->ID();
  my $query = qq(SELECT DISTINCT pstart,pend
                 FROM peptide_map_info pmi, peptide_occurrence po
                 WHERE po.occurrenceID = '$pept_occurrenceID'
                 AND po.peptideID = pmi.peptideID
                 AND pmi.proteinID = '$proteinID');
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare($query);
  $sth->execute();
  while (my ($start,$end) = $sth->fetchrow_array()) {
    push @{$self->{'coordinates'}},($start,$end) if (defined($start) && defined($end));
  }
  $sth->finish();

  return @{$self->{'coordinates'}};
}

sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  return $self->{$attribute};
}

sub DESTROY {

  my $self = shift;
  $self->_decr_occurrence_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
