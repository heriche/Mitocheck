# Author: jkh1
# 2012-09-27
#

=head1 NAME

 Mitocheck::Measurement

=head1 SYNOPSIS



=head1 DESCRIPTION

 Representation of a Mitocheck measurement.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Measurement;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_measur_count = 0;
  my $_newID_count = 0;

  sub get_measur_count {
    return $_measur_count;
  }
  sub _incr_measur_count {
    return ++$_measur_count;
  }
  sub _decr_measur_count {
    return --$_measur_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, measurement ID
 Description: Creates a new measurement object.
 Returntype: Mitocheck::Measurement

=cut

sub new {

  my ($class,$dbc,$measurementID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_measur_count();
  if (!defined $self->{'ID'} && !$measurementID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   measurementID
                 FROM     measurement
                 ORDER BY measurementID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $measurementID = "MSR"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $measurementID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck measurement ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 name

 Arg: optional, measurement name
 Description: Gets/sets name of measurement
 Returntype: string

=cut

sub name {

  my $self = shift;
  $self->{'name'} = shift if @_;
  if (!defined $self->{'name'}) {
    my $measurementID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT name FROM measurement WHERE measurementID = ?");
    $sth->execute($measurementID);
    ($self->{'name'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'name'};

}

=head2 description

 Arg: optional, measurement description
 Description: Gets/sets description of measurement
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $measurementID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT description FROM measurement WHERE measurementID = ?");
    $sth->execute($measurementID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'description'};

}

=head2 statistics

 Arg: optional, hashref with mean and standard deviation as keys
 Description: Gets/sets mean and standard deviation values for this measurement
 Returntype: hashref

=cut

sub statistics {

  my $self = shift;
  $self->{'statistics'} = shift if @_;
  if (!defined $self->{'statistics'}) {
  	my $stats;
    my $measurementID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT mean,stddev
                             FROM statistics s, measurement m
                             WHERE m.measurementID = ?
                             AND m.statisticsID = s.statisticsID");
    $sth->execute($measurementID);
    my ($mean,$stddev) = $sth->fetchrow_array();
    if (defined($mean)) {
      $stats->{'mean'} = $mean;
      $stats->{'stddev'} = $stddev;
    }
    $sth->finish();
    $self->{'statistics'} = $stats;
  }

  return $self->{'statistics'};

}

=head2 unit

 Arg: optional, string
 Description: Gets/sets unit for this measurement.
 Returntype: string

=cut

sub unit {

  my $self = shift;
  $self->{'unit'} = shift if @_;
  if (!defined $self->{'unit'}) {
    my $measurementID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT unit FROM measurement WHERE measurementID = ?");
    $sth->execute($measurementID);
    ($self->{'unit'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'unit'};

}

=head2 component

 Arg: optional, component number
 Description: Gets/sets component or fraction ID of a protein this measurement corresponds to.
              This can be for example component 2 of a two-component FCS model.
 Returntype: integer

=cut

sub component {

  my $self = shift;
  $self->{'component'} = shift if @_;
  if (!defined $self->{'component'}) {
    my $measurementID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT component FROM measurement WHERE measurementID = ?");
    $sth->execute($measurementID);
    ($self->{'component'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'component'};

}

=head2 FCSexperiment

 Arg: optional, Mitocheck::FCSexperiment
 Description: Gets/sets FCS experiment in which the measurement was made
 Returntype: Mitocheck::FCSexperiment

=cut

sub FCSexperiment {

  my ($self,$experiment) = @_;
  $self->{'experiment'} = $experiment if defined($experiment);
  if(!defined($self->{'experiment'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT FCSexperimentID
                             FROM measurement
                             WHERE measurementID = ? ");
    $sth->execute($self->{'ID'});
    my ($expID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $FCSexph = $dbc->get_FCSexperimentHandle();
    $self->{'experiment'} = $FCSexph->get_by_id($expID);
  }
  return $self->{'experiment'};
}

=head2 localization

 Arg: optional, Mitocheck::Localization
 Description: Gets/sets localization occurrence of the measurement
 Returntype: Mitocheck::Localization

=cut

sub localization {

  my ($self,$loc) = @_;
  $self->{'localization'} = $loc if defined($loc);
  if(!defined($self->{'localization'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT localization_occurrenceID
                             FROM localization_occurrence_has_measurement
                             WHERE measurementID = ? ");
    $sth->execute($self->{'ID'});
    my ($locID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $loch = $dbc->get_LocalizationHandle();
    $self->{'localization'} = $loch->get_by_id($locID);
  }
  return $self->{'localization'};
}

=head2 interaction

 Arg: optional, Mitocheck::Interaction
 Description: Gets/sets interaction occurrence this measurement relates to
 Returntype: Mitocheck::Interaction

=cut

sub interaction {

  my ($self,$interaction) = @_;
  $self->{'interaction'} = $interaction if defined($interaction);
  if(!defined($self->{'interaction'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT interaction_occurrenceID
                             FROM interaction_occurrence_has_measurement
                             WHERE measurementID = ? ");
    $sth->execute($self->{'ID'});
    my ($interactionID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $ih = $dbc->get_InteractionHandle();
    $self->{'interaction'} = $ih->get_by_id($interactionID);
  }
  return $self->{'interaction'};
}

=head2 event

 Arg: optional, Mitocheck::Event
 Description: Gets/sets time information about the measurement
 Returntype: Mitocheck::Event

=cut

sub event {

  my ($self,$event) = @_;
  $self->{'event'} = $event if defined($event);
  if(!defined($self->{'event'})) {
    my $dbh = $self->{'database_handle'};
    my $eventID;
    # We need to distinguish between single protein measurement and interaction measurement
    if ($self->localization) {
      my $sth = $dbh->prepare("SELECT plo.eventID
                               FROM localization_occurrence_has_measurement lohm, protein_localization_occurrence plo
                               WHERE lohm.measurementID = ? 
                               AND lohm.localization_occurrenceID = plo.localization_occurrenceID");
      $sth->execute($self->{'ID'});
      ($eventID) = $sth->fetchrow_array();
      $sth->finish();
    }
    else {
      my $sth = $dbh->prepare("SELECT io.eventID
                               FROM interaction_occurrence_has_measurement iohm, interaction_occurrence io
                               WHERE iohm.measurementID = ? 
                               AND iohm.interaction_occurrenceID = io.interaction_occurrenceID");
      $sth->execute($self->{'ID'});
      ($eventID) = $sth->fetchrow_array();
      $sth->finish();
    }
    my $dbc = $self->{'DBConnection'};
    my $eh = $dbc->get_EventHandle();
    $self->{'event'} = $eh->get_by_id($eventID);
  }
  return $self->{'event'};
}

=head2 cellular_component

 Arg: optional, Mitocheck::CellularComponent
 Description: Gets/sets cellular component where the measurement was obtained
 Returntype: Mitocheck::CellularComponent

=cut

sub cellular_component {

  my ($self,$cellular_component) = @_;
  $self->{'cellular_component'} = $cellular_component if defined($cellular_component);
  if(!defined($self->{'cellular_component'})) {
    my $dbh = $self->{'database_handle'};
    my $ccID;
    # We need to distinguish between single protein measurement and interaction measurement
    if ($self->localization) {
      my $sth = $dbh->prepare("SELECT plo.cellular_componentID
                               FROM localization_occurrence_has_measurement lohm, protein_localization_occurrence plo
                               WHERE lohm.measurementID = ? 
                               AND lohm.localization_occurrenceID = plo.localization_occurrenceID");
      $sth->execute($self->{'ID'});
      ($ccID) = $sth->fetchrow_array();
      $sth->finish();
    }
    else {
      my $sth = $dbh->prepare("SELECT io.cellular_componentID
                               FROM interaction_occurrence_has_measurement iohm, interaction_occurrence io
                               WHERE iohm.measurementID = ? 
                               AND iohm.interaction_occurrenceID = io.interaction_occurrenceID");
      $sth->execute($self->{'ID'});
      ($ccID) = $sth->fetchrow_array();
      $sth->finish();
    }
    my $dbc = $self->{'DBConnection'};
    my $cch = $dbc->get_CellularComponentHandle();
    $self->{'cellular_component'} = $cch->get_by_id($ccID);
  }
  return $self->{'cellular_component'};
}

=head2 protein

 Arg: optional, Mitocheck::Protein
 Description: Gets/sets protein this measurement relates to
 Returntype: Mitocheck::Protein

=cut

sub protein {

  my ($self,$protein) = @_;
  $self->{'protein'} = $protein if defined($protein);
  if(!defined($self->{'protein'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT plo.proteinID
                             FROM localization_occurrence_has_measurement lohm, protein_localization_occurrence plo
                             WHERE lohm.measurementID = ? 
                             AND lohm.localization_occurrenceID = plo.localization_occurrenceID");
    $sth->execute($self->{'ID'});
    my ($proteinID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $proth = $dbc->get_ProteinHandle();
    $self->{'protein'} = $proth->get_by_id($proteinID);
  }
  return $self->{'protein'};
}

=head2 data_files

 Arg: optional,list of Mitocheck::DataFile objects
 Description: Gets/sets list of files associated with the FCS experiment
 Returntype: list of Mitocheck::DataFile

=cut

sub data_files {

  my ($self,@files) = @_;
  @{$self->{'data_files'}} = @files if @files;
  if (!defined $self->{'data_files'}) {
    my $dbc = $self->{'DBConnection'};
    my $dfh = $dbc->get_DataFileHandle();
    @{$self->{'data_files'}} = $dfh->get_all_by_measurement($self);
  }
  return @{$self->{'data_files'}} if (defined($self->{'data_files'}));
}

sub DESTROY {

  my $self = shift;
  $self->_decr_measur_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
