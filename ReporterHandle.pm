# Author: jkh1
# 2016-04-06
#

=head1 NAME

 Mitocheck::ReporterHandle

=head1 SYNOPSIS


=head1 DESCRIPTION

 Enables retrieval of Mitocheck reporter objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::ReporterHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Reporter;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new reporter object handle
 Returntype: Mitocheck::ReporterHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: reporter ID
 Description: Gets reporter with given ID
 Returntype: Mitocheck::Reporter object

=cut

sub get_by_id {

  my ($self,$reporterID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if reporter exists in database
  my $query = qq(SELECT COUNT(*) FROM reporter WHERE reporterID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($reporterID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }
  return new Mitocheck::Reporter($dbc,$reporterID);

}

=head2 get_by_externalID

 Arg1: string, ID in reference database
 Arg2: string, reference database
 Description: Gets reporter with given ID and reference database
 Returntype: Mitocheck::Reporter object

=cut

sub get_by_externalID {

  my ($self,$extID,$refDB) = @_;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT reporterID FROM reporter
                           WHERE externalID = ?
                           AND refDB = ?");
  $sth->execute($extID,$refDB);
  my ($reporterID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $reporterID );

  return $self->get_by_id($reporterID);

}

=head2 get_by_name

 Arg: string, reporter name
 Description: Gets reporter with given name
 Returntype: Mitocheck::Reporter object

=cut

sub get_by_name {

  my ($self,$name) = @_;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT reporterID FROM reporter
                            WHERE name=?");
  $sth->execute($name);
  my ($reporterID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $reporterID );

  return $self->get_by_id($reporterID);

}

=head2 get_all_by_image

 Arg: Mitocheck::Image or image ID
 Description: Gets all reporters associated with the given image
 Returntype: list of Mitocheck::Reporter objects

=cut

sub get_all_by_image {

  my ($self,$image) = @_;
  my $imageID = ref($image) ? $image->ID(): $image;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $sth = $dbh->prepare("SELECT reporterID, channel
                           FROM image_has_reporter
                           WHERE imageID = ? ");
  $sth->execute($imageID);
  my @reporters;
  while (my ($id,$channel)=$sth->fetchrow_array()) {
    my $reporter = $self->get_by_id($id);
    $reporter->channel($channel);
    push (@reporters,$reporter);
  }
  $sth->finish();

  return @reporters;

}

=head2 new_reporter

 Arg: key => value pairs
 Description: creates a new reporter object
 Returntype: Mitocheck::Reporter

=cut

sub new_reporter {

  my $self = shift;
  my %reporter = @_;
  my $dbc = $self->{'DBConnection'};
  unless ($reporter{'-name'} && $reporter{'-type'}) {
    croak "Reporter must have a name and type";
  }
  my $reporter = new Mitocheck::Reporter($dbc);
  $reporter->name($reporter{'-name'});
  $reporter->type($reporter{'-type'});
  $reporter->externalID($reporter{'-externalID'}) if ($reporter{'-externalID'});
  $reporter->reference_database($reporter{'-reference_database'}) if ($reporter{'-reference_database'});
  $reporter->description($reporter{'-description'}) if ($reporter{'-description'});
  $reporter->EFOID($reporter{'-EFOID'}) if ($reporter{'-EFOID'});
  $reporter->EFOterm($reporter{'-EFOterm'}) if ($reporter{'-EFOterm'});
  $reporter->url($reporter{'-url'}) if ($reporter{'-url'});

  return $reporter;

}

=head2 store

 Arg: Mitocheck::Reporter
 Description: Enters reporter in Mitocheck database.
 Returntype: Mitocheck::Reporter, same as Arg

=cut

sub store {

  my ($self,$reporter) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $reporterID;
  my $externalID = $reporter->externalID || "";
  my $refDB = $reporter->reference_database || "";
  # check if reporter already in database using externalID
  # and reference database
  if ($externalID && $reporter->reference_database) {
    my $query=qq(SELECT reporterID
                 FROM   reporter
                 WHERE  externalID='$externalID'
                 AND refDB='$refDB');
    my $sth= $dbh->prepare($query);
    $sth->execute();
    ($reporterID) = $sth->fetchrow_array();
    $sth->finish();
  }
  if (!$reporterID) {
    $reporterID = $reporter->ID();
    my $type = $reporter->type;
    my $name = $reporter->name;
    my $description = $reporter->description || "";
    my $EFOID = $reporter->EFOID || "";
    my $EFOterm = $reporter->EFOterm || "";
    my $url = $reporter->url || "";
    my $sourceID = $reporter->source->ID if ($reporter->source);
    # add to database
    my $query=qq(INSERT INTO reporter (reporterID,externalID,refDB,type,name,description,EFOID,EFOterm,URL,sourceID) VALUES ('$reporterID','$externalID','$refDB','$type','$name','$description','$EFOID','$EFOterm','$url','$sourceID'));
    my $rows= $dbh->do ($query);
  }
  else {
    carp "WARNING: reporter (with ID $externalID in $refDB) already in database with ID $reporterID\n";
    $reporter->ID($reporterID);
  }
  $reporter->_decr_newID_count() if $reporter->{'is_new'};
  $reporter->{'is_new'} = undef;

  return $reporter;

}

1;
