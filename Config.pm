# Author: jkh1

=head1 NAME

 Mitocheck::Config

=head1 DESCRIPTION

 This modules contains default configuration settings, mostly
 database connection details. It only exports variables.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Config;

use strict;
use warnings;
use Exporter;

our @ISA = ('Exporter');

our @EXPORT = qw($APIVERSION $MTCDBHOST $MTCDBNAME $MTCDBUSER $MTCDBPASS $MTCDBPORT);

our $APIVERSION = 2.0;

# Mitocheck database
our $MTCDBNAME='mitocheck2_1';
our $MTCDBUSER='anonymous';
our $MTCDBHOST='localhost';
our $MTCDBPORT='3306';

# Phenotype ontology file in OBO format
#our $CMPOFILE = "$ENV{'DOCUMENT_ROOT'}/data/cmpo.obo";

1;
