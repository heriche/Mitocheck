# Author: jkh1
# 2008-09-03
#

=head1 NAME

 Mitocheck::ImageSet

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $image_set_handle = $dbc->get_ImageSetHandle();

 my $image_set = $image_set_handle->get_by_id('IMG00000001');

 my $comments = $image_set->comments();
 my @dsRNAs = $image_set->dsRNAs();
 my @proteins = $image_set->proteins();
 my $source = $image_set->source();

=head1 DESCRIPTION

 Representation of Mitocheck images.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::ImageSet;


use strict;
use warnings;
use Carp;
use Scalar::Util qw(weaken);


{
  my $_set_count = 0;
  my $_newID_count = 0;

  sub get_set_count {
    return $_set_count;
  }
  sub _incr_set_count {
    return ++$_set_count;
  }
  sub _decr_set_count {
    return --$_set_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, image set ID
 Description: Creates a new image set object.
 Returntype: Mitocheck::ImageSet

=cut

sub new {

  my ($class,$dbc,$setID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_set_count();
  if (!defined $self->{'ID'} && !$setID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   i.image_setID
                 FROM     image_set i
                 ORDER BY i.image_setID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $setID = "IMS"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $setID;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck image set ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 images

 Arg: optional, list of Mitocheck::Image objects
 Description: Gets/sets the list of images forming this image set
 Returntype: list of Mitocheck::Image objects

=cut

sub images {

  my ($self,@images) = @_;

  if (@images) {
    @{$self->{'images'}} = @images;
  }

  if (!defined $self->{'images'}) {
    my $setID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $ih = $dbc->get_ImageHandle();
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT imageID
                             FROM image
                             WHERE image_setID = ?");
    $sth->execute($setID);
    while (my ($imageID)=$sth->fetchrow_array()) {
      my $image = $ih->get_by_id($imageID);
      push @{$self->{'images'}},$image;
    }
    $sth->finish();
  }
  return @{$self->{'images'}};

}

=head2 dsRNA

 Arg: optional, Mitocheck::dsRNA object
 Description: DEPRECATED, replaced by dsRNAs method
 Returntype: Mitocheck::dsRNA object

=cut

sub dsRNA {

  my ($self,$dsRNA) = @_;
  carp "WARNING: ImageSet method dsRNA is deprecated, use ImageSet->dsRNAs instead.\n";
  if (defined $dsRNA) {
    if (ref($dsRNA) ne 'Mitocheck::dsRNA') {
      croak "Argument must be a Mitocheck::dsRNA object";
    }
    else { push @{$self->{'dsRNAs'}},$dsRNA }
  }
  if (!defined $self->{'dsRNAs'}) {
    $self->dsRNAs();
  }
  return $self->{'dsRNAs'}->[0] if (defined($self->{'dsRNAs'}));
}

=head2 dsRNAs

 Arg: optional, list of Mitocheck::dsRNA objects
 Description: Gets/sets set of dsRNAs used to obtain the image set
 Returntype: list of Mitocheck::dsRNA object

=cut

sub dsRNAs {

  my ($self,@dsRNAs) = @_;

  if (@dsRNAs) {
    if (ref($dsRNAs[0]) ne 'Mitocheck::dsRNA') {
      croak "Argument must be a list of Mitocheck::dsRNA objects";
    }
    else { @{$self->{'dsRNAs'}} = @dsRNAs }
  }

  if (!defined $self->{'dsRNAs'}) {
    my $setID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $dbc = $self->{'DBConnection'};
    my $dsrh = $dbc->get_dsRNAHandle();
    my $sth = $dbh->prepare("SELECT oligo_pairID
                             FROM dsRNA_has_image_set
                             WHERE image_setID = ?");
    $sth->execute($setID);
    while (my ($dsRNAID) = $sth->fetchrow_array()) {
      my $dsRNA = $dsrh->get_by_id($dsRNAID);
      push @{$self->{'dsRNAs'}}, $dsRNA if $dsRNA;
    }
    $sth->finish();
  }
  return @{$self->{'dsRNAs'}} if (defined($self->{'dsRNAs'}));

}

=head2 proteins

 Arg: optional, list of Mitocheck::Protein objects
 Description: Gets/sets proteins whose localization is shown in the image set
 Returntype: list of Mitocheck::Protein objects

=cut

sub proteins {

  my ($self,@proteins) = @_;

  if (@proteins) {
    if (ref($proteins[0]) ne 'Mitocheck::Protein') {
      croak "Argument must be a list of Mitocheck::Protein objects";
    }
    else { @{$self->{'proteins'}} = @proteins }
  }

  if (!defined $self->{'proteins'}) {
    my $setID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $ph = $dbc->get_ProteinHandle();
    my $sth = $dbh->prepare("SELECT DISTINCT plo.proteinID
                             FROM protein_localization_occurrence plo,
                                  localization_occurrence_has_image_set lohims
                             WHERE lohims.image_setID = ?
                             AND lohims.localization_occurrenceID = plo.localization_occurrenceID");
    $sth->execute($setID);
    while (my ($proteinID) = $sth->fetchrow_array()) {
      push @{$self->{'proteins'}}, $ph->get_by_id($proteinID);
    }
  }
  return @{$self->{'proteins'}} if (defined ($self->{'proteins'}));
}

=head2 source

 Arg: optional, Mitocheck::Source
 Description: Gets/sets origin of the image set
 Returntype: Mitocheck::Source object

=cut

sub source {

  my ($self,$source) = @_;

  if (defined $source) {
    if (ref($source) ne 'Mitocheck::Source') {
      croak "Argument must be a Mitocheck::Source object";
    }
    else { $self->{'source'} = $source }
  }

  if (!defined $self->{'source'}) {
    my $setID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sourceID
                             FROM image_set
                             WHERE image_setID = ? ");
    $sth->execute($setID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};

}

=head2 control

 Arg: optional, Mitocheck::ImageSet
 Description: Gets/sets negative control for the image set
 Returntype: Mitocheck::ImageSet object

=cut

sub control {

  my ($self,$control) = @_;
  if (defined $control) {
    if (ref($control) ne 'Mitocheck::Image') {
      croak "Argument must be a Mitocheck::ImageSet object";
    }
    else { $self->{'control'} = $control }
  }
  if (!defined $self->{'source'}) {
    my $setID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT controlID
                             FROM image_set
                             WHERE image_setID = ? ");
    $sth->execute($setID);
    my ($controlID) = $sth->fetchrow_array();
    $sth->finish();
    if ($controlID) {
      my $dbc = $self->{'DBConnection'};
      my $ch = $dbc->get_ImageSetHandle();
      $self->{'control'} = $ch->get_by_id($controlID);
    }
  }
  return $self->{'control'};
}

=head2 phenotypes

 Arg: none
 Description: Gets all phenotypes associated with image set
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub phenotypes {

  my $self = shift;
  @{$self->{'phenotypes'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $ph = $dbc->get_PhenotypeHandle();
  @{$self->{'phenotypes'}} = $ph->get_all_by_image_set($self);

  return @{$self->{'phenotypes'}};
}

=head2 cell_lines

 Arg: none
 Description: Gets all cell lines associated with image set
 Returntype: list of Mitocheck::CellLine objects

=cut

sub cell_lines {

  my $self = shift;
  @{$self->{'cell_lines'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $clh = $dbc->get_CellLineHandle();
  @{$self->{'cell_lines'}} = $clh->get_all_by_image_set($self);

  return @{$self->{'cell_lines'}};
}


=head2 spot_location

 Arg: (optional) string
 Description: Gets/sets the position of the spot used to generate
              the image set
 Returntype: string

=cut

sub spot_location {

  my ($self,$sploc) = @_;
  $self->{'spot_location'} = $sploc if (defined $sploc);
  my $dbc = $self->{'DBConnection'};
  if (!defined $self->{'spot_location'}) {
    my $setID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT spot_location
                             FROM image_set
                             WHERE image_setID = ? ");
    $sth->execute($setID);
    ($self->{'spot_location'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'spot_location'};
}

=head2 comments

 Arg: (optional) string
 Description: Gets/sets comments for this image set
 Returntype: string

=cut

sub comments {

  my ($self,$sploc) = @_;
  $self->{'comments'} = $sploc if (defined $sploc);
  my $dbc = $self->{'DBConnection'};
  if (!defined $self->{'comments'}) {
    my $setID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT comments
                             FROM image_set
                             WHERE image_setID = ? ");
    $sth->execute($setID);
    ($self->{'comments'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'comments'};
}


=head2 store_attributes

 Args: key=>value pairs
 Description: Sets arbitrary attributes for image set and stores them
              in the database.
              One of the attributes must be a source=>source object pair.
 Returntype: 1

=cut

sub store_attributes {

  my $self = shift;
  my %attribute = @_;
  my $source = $attribute{'source'} || $attribute{'-source'};
  if (!defined($source) || ref($source) ne 'Mitocheck::Source') {
    croak " Source object required.";
  }
  my $sourceID = $source->ID;
  delete($attribute{'source'}) if (defined($attribute{'source'}));
  delete($attribute{'-source'}) if (defined($attribute{'-source'}));
  my $dbh = $self->{'database_handle'};
  foreach my $key(keys %attribute) {
    $key =~s/^-//; # remove leading - if any
    $self->{$key} = $attribute{$key};
    my $query = "SELECT attributeID FROM attribute WHERE name= ? AND sourceID= ? AND value= ?";
    my $sth= $dbh->prepare($query);
    $sth->execute($key,$sourceID,$attribute{$key});
    my ($ID) = $sth->fetchrow_array();
    if (!$ID) {
      # find last ID used
      my $query=qq(SELECT attributeID
                   FROM attribute
                   ORDER BY attributeID
                   DESC
                   LIMIT 1);
      my $sth= $dbh->prepare($query);
      $sth->execute();
      my ($lastID) = $sth->fetchrow_array();
      $ID = ++$lastID;
      $query = "INSERT INTO attribute (attributeID,name,value,sourceID) VALUES('$ID','$key','$attribute{$key}','$sourceID')";
      my $rows= $dbh->do($query);
    }
    my $setID = $self->ID;
    $query = "INSERT IGNORE INTO image_set_has_attribute (image_setID,attributeID) VALUES('$setID','$ID')";
    my $rows= $dbh->do($query);
  }
  return 1;
}

=head2 AUTOLOAD

 Arg: string, attribute name as stored in the database
 Description: Retrieves value of any attribute stored in the database
 Returntype: any

=cut

sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  if (!defined $self->{$attribute}) {
    my $setID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT a.value
                             FROM image_set_has_attribute isha, attribute a
                             WHERE isha.image_setID = ?
                             AND isha.attributeID = a.attributeID
                             AND a.name = ? ");
    $sth->execute($setID,$attribute);
    ($self->{$attribute})=$sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{$attribute};

}

sub DESTROY {

  my $self = shift;
  $self->_decr_set_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
