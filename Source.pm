# Author: jkh1
# 2005-06-13, last modified 2011-01-14
#

=head1 NAME

 Mitocheck::Source

=head1 SYNOPSIS



=head1 DESCRIPTION

 Representation of source of data.


=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Source;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_source_count = 0;
  my $_newID_count = 0;

  sub get_source_count {
    return $_source_count;
  }
  sub _incr_source_count {
    return ++$_source_count;
  }
  sub _decr_source_count {
    return --$_source_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, source ID
 Description: Creates a new source object.
 Returntype: Mitocheck::Source

=cut

sub new {

  my ($class,$dbc,$sourceID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_source_count();
  if (!defined $self->{'ID'} && !$sourceID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   sourceID
                 FROM     source
                 ORDER BY sourceID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $sourceID = "SRC"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $sourceID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck source ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 name

 Arg:  optional, source's name
 Description: Gets/sets source's name
 Returntype: string

=cut

sub name {

  my $self = shift;
  $self->{'name'} = shift if @_;
  if (!defined $self->{'name'}) {
    my $sourceID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT s.name FROM source s WHERE s.sourceID = ? ");
    $sth->execute($sourceID);
    ($self->{'name'}) = $sth->fetchrow_array() || "";
    $sth->finish();
  }
  return $self->{'name'};

}

=head2 description

 Arg: optional, text description of source
 Description Gets/sets description of source
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $sourceID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT s.description FROM source s WHERE s.sourceID = ? ");
    $sth->execute($sourceID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 reference

 Arg: optional, reference for source
 Description Gets/sets reference for source
 Returntype: string

=cut

sub reference {

  my $self = shift;
  $self->{'reference'} = shift if @_;
  if (!defined $self->{'reference'}) {
    my $sourceID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT s.reference FROM source s WHERE s.sourceID = ? ");
    $sth->execute($sourceID);
    ($self->{'reference'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'reference'};

}

=head2 PMID

 Arg: optional, PubMed ID of source
 Description Gets/sets PubMed ID for source
 Returntype: string

=cut

sub PMID {

  my $self = shift;
  $self->{'PMID'} = shift if @_;
  if (!defined $self->{'PMID'}) {
    my $sourceID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT s.PMID FROM source s WHERE s.sourceID = ? ");
    $sth->execute($sourceID);
    ($self->{'PMID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'PMID'};

}

=head2 URL

 Arg: optional, URL of source
 Description: Gets/sets URL for source
 Returntype: string

=cut

sub URL {

  my $self = shift;
  $self->{'URL'} = shift if @_;
  if (!defined $self->{'URL'}) {
    my $sourceID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT s.URL FROM source s WHERE s.sourceID = ? ");
    $sth->execute($sourceID);
    ($self->{'URL'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'URL'};

}

=head2 userID

 Arg: optional, string representing a user ID
 Description: Gets/sets the ID of the user authorised to view data from
              this source
 Returntype: string

=cut

sub userID {

  my $self = shift;
  $self->{'userID'} = shift if @_;
  if (!defined $self->{'userID'}) {
    my $sourceID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT s.userID FROM source s WHERE s.sourceID = ? ");
    $sth->execute($sourceID);
    ($self->{'userID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'userID'};

}

sub DESTROY {

  my $self = shift;
  $self->_decr_source_count();
  $self->_decr_newID_count() if $self->{'is_new'};

}



1;
