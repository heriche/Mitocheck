# Author: jkh1
# 2011-10-10
#

=head1 NAME

 Mitocheck::CellLine

=head1 SYNOPSIS



=head1 DESCRIPTION

 Representation of a cell line.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::CellLine;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_line_count = 0;
  my $_newID_count = 0;

  sub get_line_count {
    return $_line_count;
  }
  sub _incr_line_count {
    return ++$_line_count;
  }
  sub _decr_line_count {
    return --$_line_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, cell line ID
 Description: Creates a new cell line object.
 Returntype: Mitocheck::CellLine

=cut

sub new {

  my ($class,$dbc,$lineID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $class->_incr_line_count();
  if (!defined $self->{'ID'} && !$lineID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   cell_lineID
                 FROM     cell_line
                 ORDER BY cell_lineID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $lineID = "CLL"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $lineID;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck cell line ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 name

 Arg: optional, cell line name
 Description: Gets/sets name of the cell line
 Returntype: string

=cut

sub name {

  my $self = shift;
  $self->{'name'} = shift if @_;
  if (!defined $self->{'name'}) {
    my $lineID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT name FROM cell_line WHERE cell_lineID = ?");
    $sth->execute($lineID);
    ($self->{'name'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'name'};

}

=head2 description

 Arg: optional, text description of cell line
 Description Gets/sets description of cell line
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT description FROM cell_line WHERE cell_lineID = ? ");
    $sth->execute($geneID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 species

 Arg: optional, species name
 Description Gets/sets species this cell line comes from
 Returntype: string

=cut

sub species {

  my $self = shift;
  $self->{'species'} = shift if @_;
  if (!defined $self->{'species'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT species FROM cell_line WHERE cell_lineID = ? ");
    $sth->execute($geneID);
    ($self->{'species'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'species'};

}


sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  return $self->{$attribute};
}


sub DESTROY {

  my $self = shift;
  $self->_decr_line_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}


1;
