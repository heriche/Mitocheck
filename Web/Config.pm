# Author: jkh1

=head1 NAME

 Mitocheck::Web::Config

=head1 DESCRIPTION

 This modules contains default configuration settings, mostly
 database connection details. It only exports variables.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Web::Config;

use strict;
use warnings;
use Exporter;

our @ISA = ('Exporter');

our @EXPORT = qw($ENSDBHOST $ENSDBPORT $ENSDBNAME $ENSDBUSER $ENSDBPASS $FASTA_LEN $baseURL %PLOTS_DIR $PROTEIN_INTERACTIONS);

# Ensembl database
our $ENSDBHOST = "bluegecko";
our $ENSDBNAME = "homo_sapiens_core_87_38";
our $ENSDBUSER = "anonymous";
our $ENSDBPASS = undef;
our $ENSDBPORT = 3306;

# for FASTA sequence formating
our $FASTA_LEN = 60;

# Where plots are (currently only for Mitocheck primary screen)
our %PLOTS_DIR;
$PLOTS_DIR{'Mitocheck primary screen'} = 'http://www.mitocheck.org/data/mitocheck_screen/plots';
$PLOTS_DIR{'Mitocheck validation screen'} = 'http://www.mitocheck.org/data/mitocheck_screen/plots';

# File with protein interaction graph
our $PROTEIN_INTERACTIONS = "$ENV{'DOCUMENT_ROOT'}/data/protein_interactions/all_protein_interactions.txt";

1;
