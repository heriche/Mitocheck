# Author: jkh1
# 2016-03-10
#

=head1 NAME

 Mitocheck::Web::GenePage

=head1 SYNOPSIS




=head1 DESCRIPTION

 Methods to generate content for sections of the gene page.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Web::GenePage;

use strict;
use warnings;
use vars qw($XREFS);

$XREFS      = {
	       'wormbase' => "http://www.wormbase.org/db/gene/gene?name=%s;class=Gene",
	       'flybase'  => "http://flybase.org/reports/%s",
               'mgi'      => "http://www.informatics.jax.org/marker/%s",
               'sgd'      => "http://db.yeastgenome.org/cgi-bin/locus.pl?locus=%s",
	       'ensembl/danio rerio'  => "http://www.ensembl.org/Danio_rerio/geneview?gene=%s",
	       'ensembl/gallus gallus'=> "http://www.ensembl.org/Gallus_gallus/geneview?gene=%s",
	       'genedb/pombe'=>"http://www.genedb.org/genedb/Search?submit=Search+for&name=%s&organism=pombe&desc=yes&wildcard=yes"
	      };

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, gene ID
 Description: Creates a GenePage object.
 Returntype: Mitocheck::GenePage

=cut

sub new {

  my $class = shift;
  my $dbc = shift;
  my $geneID = shift if @_;
  my $self = {};
  $self->{'dbc'} = $dbc;
  $self->{'geneID'} = $geneID if $geneID;
  bless ($self, $class);

  return $self;
}

=head2 identifiers

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the identifiers section of the gene page.
 Returntype: string

=cut

sub identifiers {

  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my $identifiers = "";
  my $symbol = $gene->symbol();
  my @names = $gene->names();
  if ($format eq 'text') {
    $identifiers = join("\n",($geneID,$symbol,@names))."\n";
  }
  elsif ($format eq 'json') {
    my $names = join(",", map {qq/"$_"/} @names);
    $identifiers = qq(
                       {
                         "ID": "$geneID",
                         "symbol": "$symbol",
                         "names": [$names]
                       }
                     );
  }
  else { # default to html
    my $names = join("; ", @names);
    $identifiers = qq(
                      <div class="header">Identifiers</div>
                      <div class="identifier emphasis">$symbol</div>
                      <div class="identifier">$names</div>
                      <div class="identifier">Ensembl ID: <a class="genelink" href="http://www.ensembl.org/Homo_sapiens/geneview?gene=$geneID" target="_blank" title="Go to Ensembl">$geneID</a></div>
    );
  }
  return $identifiers;
}

=head2 phenotypes

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the phenotypes section of the gene page.
 Returntype: string

=cut

sub phenotypes {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my @phenotypes = $gene->phenotypes();
  my $phenotypes = "";
  if ($format eq 'text') {
    if (@phenotypes && $phenotypes[0]) {
      # Tab-delimited table
      $phenotypes .= "description\tCMPO term\tCMPO ID\tdsRNA index\tannotation type\tsource\n";
      foreach my $phenotype(@phenotypes) {
	my $row = ucfirst($phenotype->description)."\t".$phenotype->CMPOterm."\t".$phenotype->CMPOID."\t".$phenotype->dsRNA_index($gene)."\t".$phenotype->annotation_type()."\t".$phenotype->source()->name()."\n";
	$phenotypes .= $row;
      }
    }
  }
  elsif ($format eq 'json') {
    if (@phenotypes && $phenotypes[0]) {
      my @array;
      foreach my $phenotype(@phenotypes) {
	my $desc = ucfirst($phenotype->description());
	my $CMPOterm = $phenotype->CMPOterm();
	my $CMPOID = $phenotype->CMPOID();
	my $dsRNAindex = $phenotype->dsRNA_index($gene);
	my $annotation_type = $phenotype->annotation_type();
	my $source_name = $phenotype->source()->name();
	my $element = qq(
                        {
                          "description": "$desc",
                          "CMPO": {"term": "$CMPOterm", "id": "$CMPOID"},
                          "dsRNA_index": "$dsRNAindex",
                          "annotation_type": "$annotation_type",
                          "source": "$source_name"
                        }
        );
	push @array,$element;
      }
      $phenotypes = "[".join(",",@array)."]";
    }
  }
  else { # default to html
    if (@phenotypes && $phenotypes[0]) {
      $phenotypes = qq(
             <div class="header">Phenotypes</div>
             <table width="100%">
               <thead>
               <tr>
                 <th width="40%" style="text-align:left">Description</th>
                 <th width="15%" style="text-align:center"><a title="Reproducible dsRNAs giving the phenotype/dsRNAs specifically targeting this gene" style="text-align:center">Nb of reproducible dsRNAs</a></th>
                 <th width="10%" style="text-align:center"><a title="M: manual, A: automatic">Annotation type</a></th>
                 <th width="35%" style="text-align:center">Source</th>
               </tr>
               </thead>
               <tbody>
      );
      foreach my $phenotype(@phenotypes) {
	my $desc = ucfirst($phenotype->description());
	my $CMPOterm = $phenotype->CMPOterm();
	my $CMPOID = $phenotype->CMPOID();
	my $dsRNAindex = $phenotype->dsRNA_index($gene);
	my $annotation_type = $phenotype->annotation_type() eq 'manual'? 'M':'A';
	my $source = $phenotype->source();
	my $source_name = $source->name();
	my $sourceID = $source->ID();
	$phenotypes .= qq(
               <tr>
                 <td>$desc</td>
                 <td style="text-align:center;">$dsRNAindex</td>
                 <td style="text-align:center;">$annotation_type</td>
                 <td style="text-align:center;"><a class="link" href="javascript:void(0)"onclick="window.open('cgi-bin/mtc?action=get_source_info;query=$sourceID','sourceinfo','height=500, width=400,scrollbars=yes')">$source_name</a></td>
               </tr>
        );
      }
      $phenotypes .=qq(</tbody></table>);
    }
    else {
      $phenotypes = qq(
             <div class="header">Phenotypes</div>
             <div style="text-indent: 10px">No data</div>
      );
    }
  }
  return $phenotypes;
}

=head2 phenotypes_by_dsRNA

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Show details of phenotypes by dsRNA in phenotype section
              of the gene page.
 Returntype: string

=cut

sub phenotypes_by_dsRNA {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my @dsRNAs = grep { defined($_) && ($_->mismatch($gene)==0 || scalar($_->target_genes())==1) } $gene->dsRNAs();
  @dsRNAs = grep {$_->phenotypes} @dsRNAs;
  my $ncols = scalar(@dsRNAs);
  my @phenotypes = map { $_->phenotypes } @dsRNAs;
  my %seen;
  @phenotypes = sort {$a->source->name cmp $b->source->name } grep { !$seen{$_->ID}++ } @phenotypes;
  my $phenotypes = "";
  if ($format eq 'text') {
    if (@phenotypes && $phenotypes[0]) {
      # Tab-delimited table
      $phenotypes .= "Description\tCMPO term\tCMPO ID";
      foreach my $dsRNA(@dsRNAs) {
	$phenotypes .= "\t".$dsRNA->ID;
      }
      $phenotypes .= "\tSource\n";
      # Info on targets and mismatches
      $phenotypes .= "\t\t";
      foreach my $dsRNA(@dsRNAs) {
	my $mm = $dsRNA->mismatch($gene);
	$phenotypes .= "\tmismatches: $mm";
	my @target_genes = $dsRNA->target_genes(0);
	# If 2 genes share the same symbol, consider them as one gene
	my @symbols = map {$_->symbol} @target_genes;
	my %seen_genes;
	@seen_genes{@symbols} = (1) x @symbols;
	if (scalar(keys %seen_genes)>1) {
	  $phenotypes .= "|other targeted genes: ".scalar(keys %seen_genes)-1;
	}
      }
      $phenotypes .= "\t\n";
      foreach my $phenotype(@phenotypes) {
	my $desc = ucfirst($phenotype->description());
	my $CMPOterm = $phenotype->CMPOterm();
	my $CMPOID = $phenotype->CMPOID();
	$phenotypes .= $desc."\t".$CMPOterm."\t".$CMPOID;
	my $screen = $phenotype->source;
	my $sourceID = $screen->ID;
	my $annotation_type = $phenotype->annotation_type;
	foreach my $dsRNA(@dsRNAs) {
	  my ($p) = grep {ucfirst($_->description) eq $desc && $_->source->ID eq $sourceID && $_->annotation_type eq $annotation_type} $dsRNA->phenotypes;
	  if ($p) {
	    my $reproducibility = $p->reproducibility($dsRNA);
	    $phenotypes .= "\t$reproducibility";
	  }
	  else {
	    my $n = $dsRNA->replicates($screen);
	    if ($n) {
	      $phenotypes .= "\t0/$n";
	    }
	    else {
	      $phenotypes .= "\tNA";
	    }
	  }
	}
	$phenotypes .= "\t".$screen->name."\n";
      }
    }
  }
  elsif ($format eq 'json') {
    if (@phenotypes && $phenotypes[0]) {
      $phenotypes = '{';
      foreach my $i(0..$#dsRNAs) {
	my $dsRNA = $dsRNAs[$i];
	$phenotypes .= q(").$dsRNA->ID.q(":);
	my @phens = $dsRNA->phenotypes();
	my @array;
	foreach my $phenotype(@phens) {
	  my $desc = ucfirst($phenotype->description());
	  my $CMPOterm = $phenotype->CMPOterm();
	  my $CMPOID = $phenotype->CMPOID();
	  my $reproducibility = $phenotype->reproducibility($dsRNA);
	  my $annotation_type = $phenotype->annotation_type();
	  my $source_name = $phenotype->source()->name();
	  my $element = qq(
                        {
                          "description": "$desc",
                          "CMPO": {"term": "$CMPOterm", "id": "$CMPOID"},
                          "reproducibility": "$reproducibility",
                          "annotation_type": "$annotation_type",
                          "source": "$source_name"
                        }
        );
	  push @array,$element;
	}
	$phenotypes .= "[".join(",",@array)."]";
	unless ($i == $#dsRNAs) {
	  $phenotypes .= ',';
	}
      }
      $phenotypes .= '}';
    }
  }
  else {
    if (@phenotypes && $phenotypes[0]) {
      $phenotypes = qq(
             <div class="header">Phenotypes by dsRNA</div>
             <table width="100%">
               <thead>
      );
      $phenotypes .= "<tr><th>Phenotype</th>";
      foreach my $dsRNA(@dsRNAs) {
	my $id = $dsRNA->ID;
	$phenotypes .= qq(<th style="text-align:center;"><a class="genelink" href=dsRNA.shtml?dsRNA=$id>$id</a>
        );
	my $mm = $dsRNA->mismatch($gene);
	$phenotypes .= qq(<br><span style="font-weight: normal">mismatches: $mm);
	my @target_genes = $dsRNA->target_genes(0);
	# If 2 genes share the same symbol, consider them as one gene
	my @symbols = map {$_->symbol} @target_genes;
	my %seen_genes;
	@seen_genes{@symbols} = (1) x @symbols;
	my $ng = scalar(keys %seen_genes) -1;
	if ($ng>0) {
	  $phenotypes .= ",<br>other targeted genes: $ng</span></th>";
	}
	else {
	  $phenotypes .= "</span></th>";
	}
      }
      $phenotypes .= qq(<th>Source</th></tr>
               </thead>
               <tbody>);
      foreach my $phenotype(@phenotypes) {
	my $desc = ucfirst($phenotype->description());
	my $CMPOterm = $phenotype->CMPOterm();
	my $CMPOID = $phenotype->CMPOID();
	$phenotypes .= qq(
                 <tr><td style="text-align:left;">$desc</td>
        );
	my $screen = $phenotype->source;
	my $sourceID = $screen->ID;
	my $annotation_type = $phenotype->annotation_type;
	foreach my $dsRNA(@dsRNAs) {
	  my ($p) = grep {ucfirst($_->description) eq $desc && $_->source->ID eq $sourceID && $_->annotation_type eq $annotation_type} $dsRNA->phenotypes;
	  if ($p) {
	    my $reproducibility = $p->reproducibility($dsRNA);
	    $phenotypes .= qq(<td style="text-align:center;">$reproducibility</td>);
	  }
	  else {
	    my $n = $dsRNA->replicates($screen);
	    if ($n) {
	      $phenotypes .= qq(<td style="text-align:center;">0/$n</td>);
	    }
	    else {
	      $phenotypes .= qq(<td style="text-align:center;">N/A</td>);
	    }
	  }
	}
	$phenotypes .= qq(<td><a class="link" href="javascript:void(0)"onclick="window.open('cgi-bin/mtc?action=get_source_info;query=$sourceID','sourceinfo','height=500, width=400,scrollbars=yes')">).$screen->name.qq(</td></tr>);
      }
      $phenotypes .= "</tbody></table>";
    }
    else {
      $phenotypes = qq(
             <div class="header">Phenotypes by dsRNA</div>
             <div style="text-indent: 10px">No data</div>
      );
    }
  }
  return $phenotypes;
}

=head2 protein_interactions

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the protein interaction section of the gene page.
 Returntype: string

=cut

sub protein_interactions {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my $dbc = $self->{'dbc'};
  my $inth = $dbc->get_InteractionHandle();
  my @interactions = $inth->get_all_by_gene($gene);
  my $query_is_contaminant = $gene->is_contaminant();
  my $protein_interactions = "";
  if ($format eq 'text') {
    if (@interactions && $interactions[0]) {
      # Tab-delimited table of gene A, gene B, source,
      # subcellular localization, localization GO ID,
      # event description, event GO ID
      $protein_interactions = "gene A\tgene B\tweight (0 for contaminants)\tsource\tsubcellular localization\tlocalization GO ID\tevent description\tevent GO ID\tinteraction type\n";
      foreach my $int(@interactions) {
	my @partners = map {$_ && $_->gene} $int->partners();
	my $source = $int->source();
	my $source_name = $source->name() if ($source);
	my $weight = 1;
	# For Mitocheck MS experiments, if gene is contaminant and
	# not used as bait, set weight to 0
	if ($source_name=~/mito(check|sys)/i && $query_is_contaminant && $partners[1]->ID eq $geneID) {
	  $weight = 0;
	}
	my $cc = $int->cellular_component();
	my $event = $int->event();
	my $type = $int->type() || "";
	$protein_interactions .= $partners[0]->symbol."\t".$partners[1]->symbol."\t$weight\t$source_name\t".$cc->description()."\t".$cc->GOID()."\t".$event->description()."\t".$event->GOID."\t$type\n";
      }
    }
  }
  elsif ($format eq 'json') {
    if (@interactions && $interactions[0]) {
      my @array;
      foreach my $int(@interactions) {
	my @partners = map {$_ && $_->gene} $int->partners();
	my $A = $partners[0]->symbol;
	my $B = $partners[1]->symbol;
	my $source = $int->source();
	my $source_name = $source->name() if ($source);
	my $weight = 1;
	# For Mitocheck MS experiments, if gene is contaminant and
	# not used as bait, set weight to 0
	if ($source_name=~/mito(check|sys)/i && $query_is_contaminant && $partners[1]->ID eq $geneID) {
	  $weight = 0;
	}
	my $cc = $int->cellular_component();
	my $cc_desc = $cc->description();
	my $ccID = $cc->GOID();
	my $event = $int->event();
	my $event_desc = $event->description();
	my $eventID = $event->GOID();
	my $type = $int->type() || "";
	my $element = qq(
                        {
                          "interactors": ["$A", "$B"],
                          "weight": "$weight",
                          "cellular_component": { "description": "$cc_desc",
                                                  "GOID": "$ccID"
                                                },
                          "event": { "description": "$event_desc",
                                     "GOID": "$eventID"
                                   },
                          "interaction_type": "$type",
                          "source": "$source_name"
                        }
        );
	push @array,$element;
      }
      $protein_interactions = "[".join(",",@array)."]";
    }
  }
  else { # html
    my $OK = 1;
    # Check if interaction with self is the only interaction
    if (scalar(@interactions) == 1) {
      my @partners = map {$_ && $_->gene} $interactions[0]->partners();
      if ($partners[0]->symbol eq $partners[1]->symbol) {
	$OK = 0;
      }
    }
    if (@interactions && $interactions[0] && $OK) {
      $protein_interactions = qq(
             <div class="header">Protein interactions</div>
             <TABLE width="100%">
               <thead>
               <tr>
                 <th width="20%" style="text-align:center">Interaction</th>
                 <th width="10%" style="text-align:center">Experiment type</th>
                 <th width="20%" style="text-align:center">Cellular component</th>
                 <th width="20%" style="text-align:center">Event</th>
                 <th width="30%" style="text-align:center">Source</th>
               </tr>
               </thead>
               <tbody>
      );
      foreach my $int(@interactions) {
	my @partners = map {$_ && $_->gene} $int->partners();
	my $A = $partners[0]->symbol;
	my $idA = $partners[0]->EnsemblID();
	my $B = $partners[1]->symbol;
	my $idB = $partners[1]->EnsemblID();
	next if ($A eq $B); # Don't show interaction with self
	# Emphasize and link query's interaction partner
	if ($A eq $gene->symbol) {
	  $B = qq(<a href="?gene=$idB" class="genelink">$B</a>);
	}
	if ($B eq $gene->symbol) {
	  $A = qq(<a href="?gene=$idA" class="genelink">$A</a>);
	}
	my $source = $int->source();
	my $source_name = $source->name() if ($source);
	my $sourceID = $source->ID() if ($source);
	# For Mitocheck MS experiments, don't show interaction
	# if gene is contaminant and not used as bait
	if ($source_name=~/mito(check|sys)/i && $query_is_contaminant && $partners[1]->ID eq $geneID) {
	  next;
	}
	my $cc = $int->cellular_component();
	my $cc_desc = $cc->description();
	my $event = $int->event();
	my $event_name = $event->name();
	my $type = $int->type() || "";
	$protein_interactions .= qq(
               <tr>
                 <td style="text-align:left;">$A - $B</td>
                 <td style="text-align:center;">$type</td>
                 <td style="text-align:center;">$cc_desc</td>
                 <td style="text-align:center;">$event_name</td>
                 <td style="text-align:center;"><a class="link" href="javascript:void(0)"onclick="window.open('cgi-bin/mtc?action=get_source_info;query=$sourceID','sourceinfo','height=500, width=400,scrollbars=yes')">$source_name</a></td>
               </tr>
        );
      }
      $protein_interactions .=qq(</tbody></TABLE>);
    }
    else {
      $protein_interactions = qq(
             <div class="header">Protein interactions</div>
             <div style="text-indent: 10px">No data</div>
      );
    }
  }
  return $protein_interactions;
}

=head2 ms_experiments

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the protein interaction section of the gene page.
 Returntype: string

=cut

sub ms_experiments {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my $dbc = $self->{'dbc'};
  my $exph = $dbc->get_MSexperimentHandle();
  my @MSexperiments = grep {defined($_) && $_} $exph->get_all_by_gene($gene);
  my $MSexperiments = "";
  if ($format eq 'text') {
    $MSexperiments = qq(Experiment ID\tExperiment name\tBait symbol\tBait ID\tBait species\tCellular component\tEvent\n);
    foreach my $exp(@MSexperiments) {
      next unless $exp;
      my $bait = $exp->bait;
      next unless $bait;
      $bait = $bait->gene;
      my $bait_symbol = $bait->symbol;
      next if ($gene->is_contaminant() && $gene->symbol ne $bait_symbol);
      my $baitID = $bait->EnsemblID;
      my ($actual_bait,$species) = @{$exp->actual_bait};
      my $cc = $exp->cellular_component;
      my $cc_name = $cc->description || "";
      my $event = $exp->event;
      my $event_name = $event->name || "";
      my $exp_name = $exp->name;
      my $expID = $exp->ID;
      $MSexperiments .= qq($expID\t$exp_name\t$bait_symbol\t$actual_bait\t$species\t$cc_name\t$event_name\n);
    }
  }
  elsif ($format eq 'json') {
    $MSexperiments = '[';
    foreach my $i(0.. $#MSexperiments) {
      my $exp = $MSexperiments[$i];
      next unless $exp;
      my $bait = $exp->bait;
      next unless $bait;
      $bait = $bait->gene;
      my $bait_symbol = $bait->symbol;
      next if ($gene->is_contaminant() && $gene->symbol ne $bait_symbol);
      my $baitID = $bait->EnsemblID;
      my ($actual_bait,$species) = @{$exp->actual_bait};
      my $cc = $exp->cellular_component;
      my $cc_name = $cc->description || "";
      my $event = $exp->event;
      my $event_name = $event->name || "";
      my $exp_name = $exp->name;
      my $expID = $exp->ID;
      $MSexperiments .= qq(
                              { "id": "$expID",
                                "name": "$exp_name",
                                "bait_symbol": "$bait_symbol",
                                "actual_bait": "$actual_bait",
                                "bait_species": "$species",
                                "cellular_component": "$cc_name",
                                "event": "$event_name"
                              }
      );
      unless ($i == $#MSexperiments) {
	$MSexperiments .= q(,);
      }
    }
    $MSexperiments .= ']';
  }
  else {
    $MSexperiments = qq(
      <div class="header">Mass spectrometry experiments</div>
    );
    if ($gene->is_contaminant()) {
      # Remove experiments in which it is not the bait
      my @keep;
      foreach my $exp(@MSexperiments) {
	next unless $exp;
	my $bait = $exp->bait;
	next unless $bait;
	$bait = $bait->gene;
	my $bait_symbol = $bait->symbol;
	if ($gene->symbol eq $bait_symbol) {
	  push @keep, $exp;
	}
      }
      @MSexperiments = @keep;
      $MSexperiments .= qq(
      <div style="text-indent: 10px"><p>This gene is considered a contaminant, only showing experiments where it is the bait if any.</p></div>
       );
    }
    if (@MSexperiments && $MSexperiments[0]) {
      $MSexperiments .= qq(
      <TABLE width="100%">
          <thead>
            <tr>
                <th style="width:10%;text-align:center">Bait</th>
                <th style="width:20%;text-align:center">Bait species</th>
                <th style="width:30%;text-align:center">Cellular component</th>
                <th style="width:25%;text-align:center">Event</th>
                <th style="width:15%;text-align:center">Experiment name</th>
            </tr>
         </thead>
         <tbody>
      );
      foreach my $exp(@MSexperiments) {
	next unless $exp;
	my $bait = $exp->bait;
	next unless $bait;
	$bait = $bait->gene;
	my $bait_symbol = $bait->symbol;
	next if ($gene->is_contaminant() && $gene->symbol ne $bait_symbol);
	my $baitID = $bait->EnsemblID;
	my $species = $exp->actual_bait_species;
	my $cc = $exp->cellular_component;
	my $cc_name = $cc->description || "";
	my $event = $exp->event;
	my $event_name = $event->name || "";
	my $exp_name = $exp->name;
	my $expID = $exp->ID;
	$MSexperiments .= qq(
            <tr>
                <td style="text-align:center">
                    <a class="genelink" href="gene.shtml?gene=$baitID">$bait_symbol</a>
                </td>
                <td style="text-align:center">$species</td>
                <td style="text-align:center">$cc_name</td>
                <td style="text-align:center">$event_name</td>
                <td style="text-align:center"><a class="link" href="MSexperiment.shtml?experiment=$expID">$exp_name</a></td>
            </tr>
        );
      }
      $MSexperiments .= qq(</tbody></table>);
    }
    else {
      unless ($gene->is_contaminant) {
	$MSexperiments .= qq(
             <div style="text-indent: 10px">No data</div>
      );
      }
    }
  }
  return $MSexperiments;
}

=head2 complexes

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the protein complexes section of the gene page.
 Returntype: string

=cut

sub complexes {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my @complexes = $gene->get_complexes;
  my $complexes = "";
  if ($format eq 'text') {
    if (@complexes && $complexes[0]) {
      $complexes = qq(Complex ID\tSubcellular localization\tLocalization GO ID\tEvent description\tEvent GO ID\tMembers\tMembers Ensembl IDs\n);
      foreach my $complex(@complexes) {
	my $complexID = $complex->ID;
        my $cc = $complex->cellular_component;
	my $ccID = $cc->GOID;
        my $cc_desc = $cc->description || '';
        my $event = $complex->event;
	my $eventID = $event->GOID;
        my $event_name = $event->name || "";
	my @members = sort {$a->gene->symbol cmp $b->gene->symbol} $complex->members;
	my $members_symbols = join(", ", map {$_->gene->symbol} @members);
	my $members_IDs = join(", ", map {$_->gene->EnsemblID} @members);
	$complexes .= qq($complexID\t$cc_desc\t$ccID\t$event_name\t$eventID\t$members_symbols\t$members_IDs\n);
      }
    }
  }
  elsif ($format eq 'json') {
    if (@complexes && $complexes[0]) {
      my @array;
      foreach my $complex(@complexes) {
	my $complexID = $complex->ID;
        my $cc = $complex->cellular_component;
	my $ccID = $cc->GOID;
        my $cc_desc = $cc->description || '';
        my $event = $complex->event;
	my $eventID = $event->GOID;
        my $event_name = $event->name || "";
	my @members = $complex->members;
	my $members_symbols = '"'.join('", "', map {$_->gene->symbol} @members).'"';
	my $members_IDs = '"'.join('", "', map {$_->gene->EnsemblID} @members).'"';
	my $element = qq(
                   {
                     "complex": "$complexID",
                     "cellular_component": { "description": "$cc_desc",
                                              "GOID": "$ccID"
                                           },
                     "event": { "description": "$event_name",
                                "GOID": "$eventID"
                              },
                     "members": { "symbols": [$members_symbols],
                                  "IDs": [$members_IDs]
                                }
                   }
        );
	push @array,$element;
      }
      $complexes = "[".join(",",@array)."]";
    }
  }
  else {
    if (@complexes && $complexes[0]) {
      $complexes = qq(
             <div class="header">Predicted protein complexes</div>
             <TABLE width="100%">
               <thead>
               <tr>
                 <th width="20%" style="text-align:center">Complex</th>
                 <th width="20%" style="text-align:center">Cellular component</th>
                 <th width="20%" style="text-align:center">Event</th>
                 <th width="40%" style="text-align:center">Members</th>
               </tr>
               </thead>
               <tbody>
      );
      foreach my $complex(@complexes) {
	my $complexID = $complex->ID;
        my $cc = $complex->cellular_component;
        my $cc_desc = $cc->description || '';
        my $event = $complex->event;
        my $event_name = $event->name || "";
	my @members = $complex->members;
	my $members;
	my $n = scalar(@members);
        if ($n>50) {
          $members = qq{<a onclick="window.open('cgi-bin/mtc?action=complex_members&complex=$complexID','Complex members','height=500, width=400,scrollbars=yes')" href="javascript:void(0)" class="link" title="Click to list complex members">Large complex ($n members)</a>};
        }
	else {
	  my @html_members;
	  foreach my $member(sort {$a->gene->symbol cmp $b->gene->symbol} @members) {
	    my $ID = $member->gene->EnsemblID;
	    my $symbol = $member->gene->symbol;
	    push @html_members, qq(<a class="genelink" href="gene.shtml?gene=$ID">$symbol</a>);
	  }
	  $members = join(", ",@html_members);
	}
	$complexes .= qq(
               <tr>
                 <td style="text-align:center;">$complexID</td>
                 <td style="text-align:center;">$cc_desc</td>
                 <td style="text-align:center;">$event_name</td>
                 <td style="text-align:center;">$members</td>
               </tr>
        );
      }
      $complexes .=qq(</tbody></TABLE>);
    }
    else {
      $complexes = qq(
             <div class="header">Predicted protein complexes</div>
             <div style="text-indent: 10px">No data</div>
      );
    }
  }
  return $complexes;
}

=head2 localizations

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the subcellular localization section of the gene page.
 Returntype: string

=cut

sub localizations {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my $symbol = $gene->symbol();
  my @proteins = $gene->proteins();
  my @localizations = map { $_->localizations() } @proteins;
  my $localizations = "";
  if ($format eq 'text') {
    $localizations = qq(Cellular component GO ID\tCellular component term\tEvent term\tEvent GO ID\tSource name\n);
     foreach my $loc(@localizations) {
	my $GOID = $loc->cellular_component->GOID();
	my $desc = $loc->cellular_component->description();
	my $event = $loc->event();
	my $eventID = $event->GOID();
	my $event_name = $event->name();
	my $source = $loc->source();
	my $source_name = $source->name() if ($source);
	$localizations .= qq($GOID\t$desc\t$event_name\t$eventID\t$source_name\n);
      }
  }
  elsif ($format eq 'json') {
    if (@localizations && $localizations[0]) {
      my @array;
      foreach my $loc(@localizations) {
	my $GOID = $loc->cellular_component->GOID();
	my $desc = $loc->cellular_component->description();
	my $event = $loc->event();
	my $event_name = $event->name();
	my $eventID = $event->GOID();
	my $source = $loc->source();
	my $source_name = $source->name() if ($source);
	my @measurements = $loc->measurements();
	my $element = qq(
                        {
                          "cellular_component": { "description": "$desc",
                                                  "GOID": "$GOID"
                                                },
                          "event": { "description": "$event_name",
                                     "GOID": "$eventID"
                                   },
                          "source": "$source_name"
        );
	if (@measurements && $measurements[0]) {
	  $element .= qq(, "measurements": [ );
	  foreach my $measurement(@measurements) {
	    my $name = $measurement->name;
	    my $stats = $measurement->statistics;
	    my $mean = $stats->{'mean'};
	    my $stddev = $stats->{'stddev'};
	    my $unit = $measurement->unit || "";
	    $element .= qq(, { "name": "$name",
                               "unit": "$unit",
                               "mean": "$mean",
                               "standard_deviation": "$stddev"
                            } );
	  }
	  $element .= qq( ] );
	}
	$element .= qq( } );
	push @array,$element;
      }
      $localizations = "[".join(",",@array)."]";
    }
  }
  else { # html
    if (@localizations && $localizations[0]) {
      $localizations = qq(
             <div class="header">Subcellular localizations</div>
             <TABLE width="100%">
               <thead>
               <tr>
                 <th width="25%" style="text-align:center">Subcellular localization</th>
                 <th width="20%" style="text-align:center">Event</th>
                 <th width="30%" style="text-align:center">Source</th>
               </tr>
               </thead>
               </tbody>
      );
      foreach my $loc(@localizations) {
	my $GOID = $loc->cellular_component->GOID();
	my $desc = $loc->cellular_component->description();
	my $event = $loc->event();
	my $event_name = $event->name();
	my $source = $loc->source();
	my $source_name = $source->name() if ($source);
	$localizations .= qq(
                <tr>
                 <td style="text-align:center;">$desc</td>
                 <td style="text-align:center;">$event_name</td>
                 <td style="text-align:center;">$source_name</td>
               </tr>
        );
      }
      $localizations .=qq(</tbody></TABLE>);
    }
    else {
      $localizations = qq(
             <div class="header">Subcellular localizations</div>
             <div style="text-indent: 10px"><p>No data</p></div>
      );
    }
    # my $bace = qq(<a class="genelink" href="http://hymanlab.mpi-cbg.de:8080/bac_viewer/search.action?ensembl=$geneID" target=_blank>BaCe</a>);
    my $mitotic_cell_atlas = qq(the <a class="genelink" href="http://www.mitocheck.org/mitotic_cell_atlas/index.html?genes%5B%5D=$symbol" target="_blank">mitotic cell atlas</a>);
    my $hpa = qq(the <a class="genelink" href="http://www.proteinatlas.org/$geneID/cell" target="_blank">Human Protein Atlas</a>);
    $localizations .= qq(<p>See also: $mitotic_cell_atlas and $hpa</p>);
  }
  return $localizations;
}

=head2 phylogeny

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the phylogeny section of the gene page.
 Returntype: string

=cut

sub phylogeny {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my @orthologs = $gene->orthologs();
  my @paralogs = $gene->paralogs();
  my $phylogeny = "";
  if ($format eq 'text') {
    if ((@orthologs && $orthologs[0]) || (@paralogs && $paralogs[0])) {
      # Tab-delimited table
      $phylogeny .= "symbol\tID\tspecies\tdatabase\trelationship\n";
      foreach my $ortholog(@orthologs) {
	my $db = lc($ortholog->database());
	my $species = $ortholog->species();
	my $desc = $ortholog->description();
	my $id = $ortholog->dbID();
	my $symbol = $ortholog->symbol();
	$phylogeny .= "$symbol\t$id\t$species\t$db\tortholog\n";
      }
      foreach my $paralog(@paralogs) {
	my $id = $paralog->EnsemblID();
	my $symbol = $paralog->symbol();
	$phylogeny .= "$symbol\t$id\tHomo sapiens\tEnsEMBL\tparalog\n";
      }
    }
  }
  elsif ($format eq 'json') {
    if ((@orthologs && $orthologs[0]) || (@paralogs && $paralogs[0])) {
      my @array;
      foreach my $ortholog(@orthologs) {
	my $db = lc($ortholog->database());
	my $species = $ortholog->species();
	my $desc = $ortholog->description();
	my $id = $ortholog->dbID();
	my $symbol = $ortholog->symbol();
	my $element = qq(
                        {
                          "id": "$id",
                          "symbol": "$symbol",
                          "species": "$species",
                          "database": "$db"
                        }
        );
	push @array,$element;
      }
      $phylogeny = qq( { "orthologs": [ ).join(",",@array).qq(],);
      @array = ();
      foreach my $paralog(@paralogs) {
	my $id = $paralog->EnsemblID();
	my $symbol = $paralog->symbol();
	my $element = qq(
                        {
                          "id": "$id",
                          "symbol": "$symbol"
                        }
         );
      }
      $phylogeny .= qq( "paralogs": [ ).join(",",@array).qq(] });
    }
  }
  else { # html
    if ((@orthologs && $orthologs[0]) || (@paralogs && $paralogs[0])) {
      $phylogeny = qq(
           <div class="header">Phylogeny</div>
               <div class="grid">
                 <div class="cell">
                   <p class="column_header">Orthologs</p>
      );
      foreach my $ortholog(@orthologs) {
	my $db = lc($ortholog->database());
	my $species = ucfirst($ortholog->species());
	$species=~s/_/ /g;
	my $id = $ortholog->dbID();
	my $symbol = $ortholog->symbol();
	my $altID = $ortholog->altID();
	my $url = sprintf $XREFS->{$db}, $id;
	my $extralink='';
	if ($altID && $db eq 'wormbase') {
	  $extralink=qq((see also <a class="genelink" href="http://worm.mpi-cbg.de/phenobank/cgi-bin/GenePage.py?Gene=$altID" target="_blank">PhenoBank</a>));
	}
	$phylogeny .= sprintf(qq{<B>%s</B>: <a class="genelink" href="%s" target="_blank" title="Go to $db">%s</a> %s<br>\n}, $species, $url, $symbol, $extralink);
      }
      $phylogeny .= qq(
                 </div>
                 <div class="cell">
                   <p class="column_header">Paralogs</p>
      );
      foreach my $paralog(@paralogs) {
	my $id = $paralog->EnsemblID();
	my $symbol = $paralog->symbol();
	$phylogeny .= qq(<a class="genelink" href="?gene=$id">$symbol</a><br />\n);
      }
      $phylogeny .= qq(
                   </div>
               </div>
           </div>
      );
    }
    else {
      $phylogeny = qq(
           <div class="header">Phylogeny</div>
           <div class="grid">
               <div class="column">No known homologs</div>
           </div>
      );
    }
  }

  return $phylogeny;
}

=head2 dsRNAs

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the dsRNAs section of the gene page.
 Returntype: string

=cut

sub dsRNAs {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my @dsRNAs = grep { defined($_) && ($_->mismatch($gene)==0 || scalar($_->target_genes())==1) } $gene->dsRNAs();
  my $dsRNAs = "";
  if ($format eq 'text') {
    if (@dsRNAs && $dsRNAs[0]) {
      $dsRNAs = qq(ID\tSupplier\tCat#\t#mismatches\tOther targets\n);
      foreach my $siRNA(@dsRNAs) {
	my $ID = $siRNA->ID();
	my $ext_ID = $siRNA->external_ID();
	my $mismatch = $siRNA->mismatch($gene);
	my @targets = $siRNA->target_genes();
	# Remove current gene from targets list
	my @other_targets = grep { $_->EnsemblID() ne $geneID } @targets;
	my @list;
	foreach my $other_target(@other_targets) {
	  my $symbol = $other_target->symbol();
	  my $id = $other_target->ID();
	  my $mm = $siRNA->mismatch($other_target);
	  push @list,qq{$symbol ($id,mismatch: $mm)};
	}
	my $targets = "";
	if (@list) {
	  $targets = join("; ",@list);
	}
	else {
	  $targets = "none";
	}
	my $supplier = $siRNA->supplier();
	my $supplier_name = $supplier->name;
	next if ($supplier_name eq 'CellBASE');
	$dsRNAs .= qq($ID\t$supplier_name\t$ext_ID\t$mismatch\t$targets\n);
      }
    }
  }
  elsif ($format eq 'json') {
    if (@dsRNAs && $dsRNAs[0]) {
      $dsRNAs = q([);
      foreach my $i(0..$#dsRNAs) {
	my $siRNA = $dsRNAs[$i];
	my $ID = $siRNA->ID();
	my $ext_ID = $siRNA->external_ID();
	my $mismatch = $siRNA->mismatch($gene);
	my @targets = $siRNA->target_genes();
	# Remove current gene from targets list
	my @other_targets = grep { $_->EnsemblID() ne $geneID } @targets;
	my @list;
	foreach my $other_target(@other_targets) {
	  my $symbol = $other_target->symbol();
	  my $id = $other_target->ID();
	  my $mm = $siRNA->mismatch($other_target);
	  push @list,qq{$symbol ($id,mismatch: $mm)};
	}
	my $targets = "";
	if (@list) {
	  $targets = join("; ",@list);
	}
	else {
	  $targets = "none";
	}
	my $supplier = $siRNA->supplier();
	my $supplier_name = $supplier->name;
	next if ($supplier_name eq 'CellBASE');
	$dsRNAs .= qq(
                  { "id": "$ID",
                    "catalog_number": "$ext_ID",
                    "supplier": "$supplier_name",
                    "mismatch": "$mismatch",
                    "other_targets": "$targets"
                  }
        );
	unless ($i == $#dsRNAs) {
	  $dsRNAs .= ',';
	}
      }
      $dsRNAs .= ']';
    }
  }
  else { # html
    if (@dsRNAs && $dsRNAs[0]) {
      $dsRNAs = qq(
           <div class="header">dsRNAs</div>
             <table width="100%">
               <thead>
               <tr>
                 <th width="20%" style="text-align:center">ID</th>
                 <th width="20%" style="text-align:center">Supplier</th>
                 <th width="10%" style="text-align:center">Cat#</th>
                 <th width="10%" style="text-align:center">#mismatches</th>
                 <th width="40%" style="text-align:center">Other targets</th>
               </tr>
               </thead>
               <tbody>
      );
      foreach my $siRNA(@dsRNAs) {
	my $ID = $siRNA->ID();
	my $ext_ID = $siRNA->external_ID();
	my $mismatch = $siRNA->mismatch($gene);
	my @targets = $siRNA->target_genes();
	# Remove current gene from targets list
	my @other_targets = grep { $_->EnsemblID() ne $geneID } @targets;
	my @list;
	foreach my $other_target(@other_targets) {
	  my $symbol = $other_target->symbol();
	  my $id = $other_target->ID();
	  my $mm = $siRNA->mismatch($other_target);
	  push @list,qq(<a class="genelink" href="?gene=$id">$symbol</a> (mismatch: $mm));
	}
	my $targets = "";
	if (@list) {
	  $targets = join("; ",@list);
	}
	else {
	  $targets = "none";
	}
	my $supplier = $siRNA->supplier();
	my $supplier_name = $supplier->name;
	next if ($supplier_name eq 'CellBASE');
	my $supplier_URL = $supplier->URL;
	$dsRNAs .= qq(
               <tr>
                 <td style="text-align:center"><a class="genelink" href="dsRNA.shtml?dsRNA=$ID">$ID</td>
                 <td style="text-align:center">$supplier_name</td>
                 <td style="text-align:center">$ext_ID</td>
                 <td style="text-align:center">$mismatch</td>
                 <td style="text-align:center">$targets</td>
               </tr>
	);
      }
      $dsRNAs .= qq(</tbody></table>);
    }
    else {
      $dsRNAs = qq(
             <div class="header">dsRNAs</div>
             <div style="text-indent: 10px">No dsRNAs</div>
      );
    }
  }
  return $dsRNAs;
}

=head2 sequences

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the sequences section of the gene page.
 Returntype: string

=cut

sub sequences {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my @transcripts = $gene->transcripts();
  my $sequences = "";
  if ($format eq 'text') {
    # Tab-delimited list of transcripts and corresponding proteins
    $sequences = qq(transcript ID\tprotein ID\tUniProt accession\n);
    foreach my $transcript(@transcripts) {
      my $transid = $transcript->EnsemblID() ||"";
      $sequences .= "$transid";
      my $prot = $transcript->protein();
      my ($uniprot,$Ensprot);
      if ($prot) {
	$uniprot = $prot->uniprot_acc() || "";
	$Ensprot = $prot->EnsemblID();
	$sequences .= "\t$Ensprot\t$uniprot";
      }
      $sequences .= "\n";
    }
  }
  elsif ($format eq 'json') {
    $sequences = q([);
    foreach my $i(0..$#transcripts) {
      my $transcript = $transcripts[$i];
      my $transid = $transcript->EnsemblID() ||"";
      $sequences .= qq({ "transcript_id": "$transid");
      my $prot = $transcript->protein();
      my ($uniprot,$Ensprot);
      if ($prot) {
	$uniprot = $prot->uniprot_acc() || "";
	$Ensprot = $prot->EnsemblID();
	$sequences .= qq(, "protein_id": "$Ensprot",
                           "uniprot_acc": "$uniprot");
      }
      $sequences .= q(});
      unless ($i == $#transcripts) {
	$sequences .= ',';
      }
    }
    $sequences .= q(]);
  }
  else { # html
    $sequences = qq(
           <div class="header">Sequences</div>
             <div class="grid">
               <div class="table_head">
                 <div class="row">
                   <p class="column_header" style="padding: 10px;">Proteins</p>
                   <p class="column_header" style="padding: 10px;">Transcripts</p>
                 </div>
               </div>
    );
    foreach my $transcript(@transcripts) {
      my $transid = $transcript->EnsemblID() ||"";
      my $prot = $transcript->protein();
      my ($protid,$uniprot,$Ensprot);
      if ($prot) {
	$protid = $prot->ID();
	$uniprot = $prot->uniprot_acc();
	$Ensprot = $prot->EnsemblID();
      }
      $sequences .= qq(
                   <div class="row">
      );
      if ($uniprot) {
	my $root=substr($uniprot,0,4);
	$sequences .= qq(
                     <div class="cell">
                       <a class="genelink" href="http://www.uniprot.org/uniprot/$uniprot" target="_blank" title="Go to Uniprot">$uniprot</a>
                       (<a class="link" href="cgi-bin/mtc?action=get_sequence;query=$Ensprot" title="Get sequence in Fasta format" target="_blank">Fasta</a>)
                     </div>
        );
      }
      elsif ($protid) {
	$sequences .= qq(
                     <div class="cell">
                       <a class="genelink" href="http://www.ensembl.org/Homo_sapiens/protview?peptide=$Ensprot" target="_blank" title="Go to Ensembl">$Ensprot</a>

                       (<a class="link" href="cgi-bin/mtc?action=get_sequence;query=$Ensprot" title="Get sequence in Fasta format" target="_blank">Fasta</a>)
                      </div>
        );
      }
      else {
	$sequences .= qq(
                      <div class="cell">
                        No protein
                      </div>
        );
      }
      $sequences .= qq(
                     <div class="cell">
                       <a class="genelink" href="http://www.ensembl.org/Homo_sapiens/transview?transcript=$transid" target="_blank" title="Go to Ensembl">$transid</a>

                        (<a class="link" href="cgi-bin/mtc?action=get_sequence;query=$transid" title="Get sequence in Fasta format" target="_blank">Fasta</a>)
                      </div>
                   </div>
      );
    }
  }
  return $sequences;
}

=head2 images

 Arg1: (optional) string, Ensembl or Mitocheck gene ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the images section of the gene page.
 Returntype: string

=cut

sub images {
  my ($self,$geneID,$format) = @_;
  my $gene = $self->_get_gene($geneID);
  if (!$gene) {
    return undef;
  }
  $geneID = $gene->EnsemblID();
  my @imageSets = sort {$a->source->ID cmp $b->source->ID} $gene->image_sets();
  my $images = "";
  if ($format eq 'text') {
    $images = qq(ID\tSource\tdsRNA\tLocation\tPhenotypes\n);
    for my $imageSet(@imageSets) {
      my $id = $imageSet->ID();
      my $source = $imageSet->source();
      my $source_name = $source->name()if $source;
      my $sourceID = $source->ID() if $source;
      my @dsRNAs = $imageSet->dsRNAs();
      my $dsRNA = $dsRNAs[0] if @dsRNAs;
      my $dsRNAID = $dsRNA->ID() if $dsRNA;
      my @phenotypes = $imageSet->phenotypes();
      my $phenotypes = "";
      if (@phenotypes && $phenotypes[0]) {
	$phenotypes .= join("; ", map {ucfirst($_->description)} @phenotypes);
      }
      my $spot_location = $imageSet->spot_location();
      $images .= qq($id\t$source_name\t$dsRNAID\t$spot_location\t$phenotypes\n);
    }
  }
  elsif ($format eq 'json') {
    $images = q([);
    for my $i(0..$#imageSets) {
      my $imageSet = $imageSets[$i];
      my $id = $imageSet->ID();
      my $source = $imageSet->source();
      my $source_name = $source->name()if $source;
      my $sourceID = $source->ID() if $source;
      my @dsRNAs = $imageSet->dsRNAs();
      my $dsRNA = $dsRNAs[0] if @dsRNAs;
      my $dsRNAID = $dsRNA->ID() if $dsRNA;
      my @phenotypes = $imageSet->phenotypes();
      my $phenotypes = "";
      if (@phenotypes && $phenotypes[0]) {
	$phenotypes .= join("; ", map {ucfirst($_->description)} @phenotypes);
      }
      my $spot_location = $imageSet->spot_location();
      $images .= qq( {
                       "id": "$id",
                       "source": "$source_name",
                       "dsRNA": "$dsRNAID",
                       "location": "$spot_location",
                       "phenotypes": "$phenotypes"
                     });
      unless ($i == $#imageSets) {
	$images .= ',';
      }
    }
    $images .= q(]);
  }
  else { # html
    if (@imageSets && $imageSets[0]) {
      $images = qq(
           <div class="header">Images/movies</div>
           <TABLE width="100%">
               <thead>
               <tr>
                 <th width="5%" style="text-align:center">ID</th>
                 <th width="20%" style="text-align:center">Source</th>
                 <th width="10%" style="text-align:center">dsRNA</th>
                 <th width="65%" style="text-align:center">Phenotypes</th>
               </tr>
               </thead>
               <tbody>
      );
      for my $imageSet(@imageSets) {
	my $id = $imageSet->ID();
	my $source = $imageSet->source();
	my $source_name = $source->name()if $source;
	my $sourceID = $source->ID() if $source;
	my @dsRNAs = $imageSet->dsRNAs();
	my $dsRNA = $dsRNAs[0] if @dsRNAs;
	my $dsRNAID = $dsRNA->ID() if $dsRNA;
	my @phenotypes = $imageSet->phenotypes();
	my $phenotypes = "";
	if (@phenotypes && $phenotypes[0]) {
	  $phenotypes .= join("; ", map {ucfirst($_->description)} @phenotypes);
	}
	$images .= qq(
               <tr>
                 <td style="text-align:center"><a class="genelink" href="images.shtml?images=$id" target=_blank>$id</a></td>
                 <td style="text-align:center"><a class="link" href="javascript:void(0)"onclick="window.open('cgi-bin/mtc?action=get_source_info;query=$sourceID','sourceinfo','height=500, width=400,scrollbars=yes')">$source_name</a></td>
                 <td style="text-align:center"><a class="genelink" href="dsRNA.shtml?dsRNA=$dsRNAID">$dsRNAID</a></td>
                 <td style="text-align:center">$phenotypes</td>
               </tr>
        );
      }
      $images .= qq(</tbody></TABLE>);
    }
    else {
      $images = qq(
           <div class="header">Images/movies</div>
           <div style="text-indent: 10px">No images</div>
      );
    }
  }
  return $images;
}

=head2 _get_gene

 Arg: (optional) string, Ensembl or Mitocheck gene ID
 Description: Get the gene with the given ID
 Returntype: Mitocheck::Gene object

=cut

sub _get_gene {
  my ($self,$geneID) = @_;
  $geneID ||= $self->{'geneID'};
  my $gh = $self->{'dbc'}->get_GeneHandle();
  my $gene;
  if ($geneID=~/^GEN/i) {
    $gene = $gh->get_by_id($geneID);
    $geneID = $gene->EnsemblID();
  }
  else {
    $gene = $gh->get_by_EnsemblID($geneID);
  }
  return $gene;
}

1;
