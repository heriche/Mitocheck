# Author: jkh1
# 2016-04-12
#

=head1 NAME

 Mitocheck::Web::MSexperimentPage

=head1 SYNOPSIS




=head1 DESCRIPTION

 Methods to generate content for the MS experiment page.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Web::MSexperimentPage;

use strict;
use warnings;

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, MSexperiment ID
 Description: Creates a MSexperimentPage object.
 Returntype: Mitocheck::MSexperimentPage

=cut

sub new {

  my $class = shift;
  my $dbc = shift;
  my $ID = shift if @_;
  my $self = {};
  $self->{'dbc'} = $dbc;
  $self->{'MSexperimentID'} = $ID if $ID;
  bless ($self, $class);

  return $self;
}

=head2 summary_data

 Arg1: (optional) string, Mitocheck MS experiment ID
 Arg2: (optional) string, format (default to html)
 Description: Creates content of the MS experiment page.
 Returntype: string

=cut

sub summary_data {
  my ($self,$ID,$format) = @_;
  my $MSexperiment = $self->_get_MSexperiment($ID);
  if (!$MSexperiment) {
    return undef;
  }
  my $experiment = "";
  my $name = $MSexperiment->name();
  my ($actual_baitID,$bait_species) = @{$MSexperiment->actual_bait};
  $bait_species =~tr/ /_/;
  my $bait = $MSexperiment->bait;
  unless ($bait) {
    # No bait or non-human bait couldn't be assigned to human gene
    $experiment = qq(<div class="header">Summary data for MS experiment $ID</div>
                     <div style="text-indent: 10px">No bait or non-human bait has no human ortholog.</div>
    );
    return $experiment;
  }
  # Use precomputed HTML summary for the web
  # because computation below is slow
  unless ($format eq 'html') {
    my $baitID = $bait->gene->EnsemblID;
    my $bait_symbol = $bait->gene->symbol;
    my $actual_bait = $bait_symbol;
    if ($bait_species eq 'Mus musculus') {
      $actual_bait = "mouse $bait_symbol";
    }
    my $bait_coverage = $MSexperiment->actual_bait_coverage;
    my $bait_score = $MSexperiment->actual_bait_score;
    my $bait_len = $MSexperiment->actual_bait_length;
    my $bait_MW = $MSexperiment->actual_bait_mass() || "";
    my $cellular_component_term = $MSexperiment->cellular_component->description;
    my $event_name = $MSexperiment->event->name;
    my @summary = $MSexperiment->get_summary_table(1);
    my @genes;
    my %score;
    my %coverage;
    my %MW;
    my %group;
    my %remove;
    my $gh = $self->{'dbc'}->get_GeneHandle();
    foreach my $row(@summary) {
      my ($gene,$protein) = @{$row};
      next unless ($gene);
      if ($gene->is_contaminant) {
	$remove{$gene->ID}++;
      }
      next if ($remove{$gene->ID});
      push @genes,$gene;
      my $symbol = $gene->symbol;
      $score{$symbol} = $protein->score($MSexperiment);
      $coverage{$symbol} = $protein->coverage($MSexperiment);
      $MW{$symbol} = $protein->mass;
      # Group genes that have same peptides
      my @group = $gh->get_all_by_shared_peptides($gene,$MSexperiment);
      foreach my $g(@group) {
	push @{$group{$gene->ID}}, $g;
	next if ($g->ID eq $gene->ID);
	$remove{$g->ID}++;
      }
    }
    @genes = sort {$score{$b->symbol}<=>$score{$a->symbol}} grep {defined($_) && !$remove{$_->ID}} @genes;
    if ($format eq 'text') {
      $experiment = qq(Bait\tPrey\tScore\tCoverage\tMW (kDa)\tCellular component\tEvent\tTagged gene\tExperiment name\n);
      foreach my $gene(@genes) {
	next unless ($gene);
	my $ID = $gene->EnsemblID;
	my $symbol = $gene->symbol;
	my $score = $score{$symbol};
	my $coverage = $coverage{$symbol};
	my $mw = $MW{$symbol};
	if (defined($group{$gene->ID}) && scalar(@{$group{$gene->ID}}>1)) {
	  $symbol = join(" or ",map { $_->symbol } @{$group{$gene->ID}});
	}
	$experiment .= qq($bait_symbol\t$symbol\t$score\t$coverage\t$cellular_component_term\t$event_name\t$actual_baitID\t$name\n);
      }
    }
    elsif ($format eq 'json') {
      my $ccID = $MSexperiment->cellular_component->GOID;
      my $eventID = $MSexperiment->event->GOID;
      $experiment = qq( { "experiment_name": "$name",
                          "cellular_component": { "description": "$cellular_component_term",
                                                  "GOID": "$ccID"
                                                },
                          "event": { "description": "$event_name",
                                     "GOID": "$eventID"
                                   },
                          "tagged gene": "$actual_baitID",
                          "bait": { "symbol": "$bait_symbol",
                                    "ID": "$baitID"
                                  },
                          "preys":
      );
      my @array;
      foreach my $gene(@genes) {
	next unless ($gene);
	my $ID = $gene->EnsemblID;
	my $symbol = $gene->symbol;
	my $score = $score{$symbol};
	my $coverage = $coverage{$symbol};
	my $mw = $MW{$symbol};
	if (defined($group{$gene->ID}) && scalar(@{$group{$gene->ID}}>1)) {
	  $ID = join(" or ",map { $_->ID } @{$group{$gene->ID}});
	  $symbol = join(" or ",map { $_->symbol } @{$group{$gene->ID}});
	}
	my $element = qq( { "ID": "$ID",
                            "symbol": "$symbol",
                            "score": "$score",
                            "coverage": "$coverage",
                            "mw": "$mw"
                          }
        );
	push @array,$element;
      }
      $experiment .= " [".join(",",@array)."]";
      $experiment .= qq(});
    }
  }
  else { # html
    $experiment = qq(<div class="header">Summary data for MS experiment $ID</div>);
    $experiment .= $self->_get_html_summary($ID);
  }
  return $experiment;
}

=head2 _get_MSexperiment

 Arg: (optional) string, Ensembl or Mitocheck MSexperiment ID
 Description: Get the mass spectrometry experiment with the given ID
 Returntype: Mitocheck::MSexperiment object

=cut

sub _get_MSexperiment {
  my ($self,$ID) = @_;
  $ID ||= $self->{'MSexperimentID'};
  my $MSeh = $self->{'dbc'}->get_MSexperimentHandle();
  my $MSexperiment = $MSeh->get_by_id($ID);
  return $MSexperiment;
}

sub _get_html_summary {
  my $self = shift;
  my $ID = shift;
  my $dbh = $self->{'dbc'}->get_DatabaseHandle();
  my $sth = $dbh->prepare("SELECT html_summary FROM MSexperiment WHERE experimentID = ?");
  $sth->execute($ID);
  my ($html) = $sth->fetchrow_array();
  $sth->finish();
  return $html;
}

1;
