# Author: jkh1
# 2016-03-31
#

=head1 NAME

 Mitocheck::Web::dsRNAPage

=head1 SYNOPSIS




=head1 DESCRIPTION

 Methods to generate content for sections of the dsRNA page.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Web::dsRNAPage;

use strict;
use warnings;

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, dsRNA ID
 Description: Creates a dsRNAPage object.
 Returntype: Mitocheck::dsRNAPage

=cut

sub new {

  my $class = shift;
  my $dbc = shift;
  my $dsRNAID = shift if @_;
  my $self = {};
  $self->{'dbc'} = $dbc;
  $self->{'dsRNAID'} = $dsRNAID if $dsRNAID;
  bless ($self, $class);

  return $self;
}

=head2 identifiers

 Arg1: (optional) string, Mitocheck dsRNA ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the identifiers section of the dsRNA page.
 Returntype: string

=cut

sub identifiers {

  my ($self,$dsRNAID,$format) = @_;
  my $dsRNA = $self->_get_dsRNA($dsRNAID);
  if (!$dsRNA) {
    return undef;
  }
  my $intended_targetID = $dsRNA->intended_target_ID();
  my $extID = $dsRNA->external_ID();
  my $supplier = $dsRNA->supplier();
  my ($supplier_name, $product ,$supplierURL) = ("","","");
  if ($supplier) {
    $supplier_name = $supplier->name;
    $product = $supplier->description;
    $supplierURL = $supplier->URL;
  }
  my $identifiers = "";
  if ($format eq 'text') {
    $identifiers = qq(ID\tCatalog_number\tSupplier\tIntended target gene\n);
    $identifiers .= qq($dsRNAID\t$extID\t$supplier_name\t$intended_targetID\n);
  }
  elsif ($format eq 'json') {
    $identifiers = qq( { "id": "$dsRNAID",
                         "catalog_number": "$extID",
                         "supplier": "$supplier_name",
                         "intended_target": "$intended_targetID"
                       });
  }
  else {
    $identifiers = qq(
                      <div class="header">Identifiers</div>
                      <div class="identifier emphasis">$dsRNAID</div>
                      <div class="identifier">From $supplier_name: $product</div>
                      <div class="identifier">Cat# $extID</div>
                      <div class="identifier">Intended target gene: $intended_targetID</div>
    );
  }
  return $identifiers;
}

=head2 sequences

 Arg1: (optional) string, Mitocheck dsRNA ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the sequences section of the dsRNA page.
 Returntype: string

=cut

sub sequences {

  my ($self,$dsRNAID,$format) = @_;
  my $dsRNA = $self->_get_dsRNA($dsRNAID);
  if (!$dsRNA) {
    return undef;
  }
  my $sseq = $dsRNA->sense_sequence();
  my $asseq = reverse($dsRNA->antisense_sequence());
  my $seq;
  # So far we only have siRNAs
  if (length($asseq) == 25) {  # We should rely on a type attribute instead
    # Assume it's a blunt-end siRNA
    $seq = $sseq."\n".$asseq;
  }
  else {
    # Assume a 2-nt overhang
    $seq = "  ".$sseq."\n".$asseq;
  }
  my $sequences = "";
  if ($format eq 'text') {
    $sequences = qq(Sense\tAntisense\n);
    $sequences .= qq($sseq\t$asseq\n);
  }
  elsif ($format eq 'json') {
    $sequences = qq({ "sense": "$sseq", "antisense": "$asseq" });
  }
  else {
   $sequences = qq(
           <div class="header">Sequences</div>
           <div class="cell"><pre>$seq</pre></div>
   );
 }
  return $sequences;
}

=head2 phenotypes

 Arg1: (optional) string, Mitocheck dsRNA ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the phenotypes section of the dsRNA page.
 Returntype: string

=cut

sub phenotypes {
  my ($self,$dsRNAID,$format) = @_;
  my $dsRNA = $self->_get_dsRNA($dsRNAID);
  if (!$dsRNA) {
    return undef;
  }
  my @phenotypes = $dsRNA->phenotypes();
  my $phenotypes = "";
  if ($format eq 'text') {
    if (@phenotypes && $phenotypes[0]) {
      # Tab-delimited table
      $phenotypes .= "description\tCMPO term\tCMPO ID\treproducibility\tannotation type\tsource\n";
      foreach my $phenotype(@phenotypes) {
	my $row = ucfirst($phenotype->description)."\t".$phenotype->CMPOterm."\t".$phenotype->CMPOID."\t".$phenotype->reproducibility($dsRNA)."\t".$phenotype->annotation_type()."\t".$phenotype->source()->name()."\n";
	$phenotypes .= $row;
      }
    }
  }
  elsif ($format eq 'json') {
    if (@phenotypes && $phenotypes[0]) {
      my @array;
      foreach my $phenotype(@phenotypes) {
	my $desc = ucfirst($phenotype->description());
	my $CMPOterm = $phenotype->CMPOterm();
	my $CMPOID = $phenotype->CMPOID();
	my $reproducibility = $phenotype->reproducibility($dsRNA);
	my $annotation_type = $phenotype->annotation_type();
	my $source_name = $phenotype->source()->name();
	my $element = qq(
                        {
                          "description": "$desc",
                          "CMPO": {"term": "$CMPOterm", "id": "$CMPOID"},
                          "reproducibility": "$reproducibility",
                          "annotation_type": "$annotation_type",
                          "source": "$source_name"
                        }
        );
	push @array,$element;
      }
      $phenotypes = "[".join(",",@array)."]";
    }
  }
  else { # default to html
    if (@phenotypes && $phenotypes[0]) {
      $phenotypes = qq(
             <div class="header">Phenotypes</div>
             <table width="100%">
               <thead>
               <tr>
                 <th width="40%" style="text-align:left">Description</th>
                 <th width="15%" style="text-align:center"><a style="text-align:center">Reproducibility</a></th>
                 <th width="10%" style="text-align:center"><a title="M: manual, A: automatic">Annotation type</a></th>
                 <th width="35%" style="text-align:center">Source</th>
               </tr>
               </thead>
               <tbody>
      );
      foreach my $phenotype(@phenotypes) {
	my $desc = ucfirst($phenotype->description());
	my $CMPOterm = $phenotype->CMPOterm();
	my $CMPOID = $phenotype->CMPOID();
	my $reproducibility = $phenotype->reproducibility($dsRNA);
	my $annotation_type = $phenotype->annotation_type() eq 'manual'? 'M':'A';
	my $source = $phenotype->source();
	my $source_name = $source->name();
	my $sourceID = $source->ID();
	$phenotypes .= qq(
               <tr>
                 <td>$desc</td>
                 <td style="text-align:center;">$reproducibility</td>
                 <td style="text-align:center;">$annotation_type</td>
                 <td style="text-align:center;"><a class="link" href="javascript:void(0)"onclick="window.open('cgi-bin/mtc?action=get_source_info;query=$sourceID','sourceinfo','height=380, width=300,scrollbars=yes')">$source_name</a></td>
               </tr>
        );
      }
      $phenotypes .=qq(</tbody></table>);
    }
    else {
      $phenotypes = qq(
             <div class="header">Phenotypes</div>
             <div style="text-indent: 10px">No phenotype</div>
      );
    }
  }
  return $phenotypes;
}

=head2 images

 Arg1: (optional) string, Mitocheck dsRNA ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the images section of the dsRNA page.
 Returntype: string

=cut

sub images {
  my ($self,$dsRNAID,$format) = @_;
  my $dsRNA = $self->_get_dsRNA($dsRNAID);
  if (!$dsRNA) {
    return undef;
  }
  my @imageSets = sort {$a->source->ID cmp $b->source->ID} $dsRNA->image_sets();
  my $images = "";
  if ($format eq 'text') {
    $images = qq(ID\tSource\tdsRNA\tLocation\tPhenotypes\n);
    for my $imageSet(@imageSets) {
      my $id = $imageSet->ID();
      my $source = $imageSet->source();
      my $source_name = $source->name()if $source;
      my $sourceID = $source->ID() if $source;
      my @dsRNAs = $imageSet->dsRNAs();
      my $dsRNA = $dsRNAs[0] if @dsRNAs;
      my $dsRNAID = $dsRNA->ID() if $dsRNA;
      my @phenotypes = $imageSet->phenotypes();
      my $phenotypes = "";
      if (@phenotypes && $phenotypes[0]) {
	$phenotypes .= join("; ", map {ucfirst($_->description)} @phenotypes);
      }
      my $spot_location = $imageSet->spot_location();
      $images .= qq($id\t$source_name\t$dsRNAID\t$spot_location\t$phenotypes\n);
    }
  }
  elsif ($format eq 'json') {
    $images = q([);
    for my $i(0..$#imageSets) {
      my $imageSet = $imageSets[$i];
      my $id = $imageSet->ID();
      my $source = $imageSet->source();
      my $source_name = $source->name()if $source;
      my $sourceID = $source->ID() if $source;
      my @dsRNAs = $imageSet->dsRNAs();
      my $dsRNA = $dsRNAs[0] if @dsRNAs;
      my $dsRNAID = $dsRNA->ID() if $dsRNA;
      my @phenotypes = $imageSet->phenotypes();
      my $phenotypes = "";
      if (@phenotypes && $phenotypes[0]) {
	$phenotypes .= join("; ", map {ucfirst($_->description)} @phenotypes);
      }
      my $spot_location = $imageSet->spot_location();
      $images .= qq( {
                       "id": "$id",
                       "source": "$source_name",
                       "dsRNA": "$dsRNAID",
                       "location": "$spot_location",
                       "phenotypes": "$phenotypes"
                     });
      unless ($i == $#imageSets) {
	$images .= ',';
      }
    }
    $images .= q(]);
  }
  else { # html
    if (@imageSets && $imageSets[0]) {
      $images = qq(
           <div class="header">Images/movies</div>
           <table width="100%">
               <thead>
               <tr>
                 <th width="5%" style="text-align:center">ID</th>
                 <th width="25%" style="text-align:center">Source</th>
                 <th width="60%" style="text-align:center">Phenotypes</th>
               </tr>
               </thead>
               <tbody>
      );
      for my $imageSet(@imageSets) {
	my $id = $imageSet->ID();
	my $source = $imageSet->source();
	my $source_name = $source->name()if $source;
	my $sourceID = $source->ID() if $source;
	my @phenotypes = $imageSet->phenotypes();
	my $phenotypes = "";
	if (@phenotypes && $phenotypes[0]) {
	  $phenotypes .= join("; ", map {ucfirst($_->description)} @phenotypes);
	}
	$images .= qq(
               <tr>
                 <td style="text-align:center"><a class="genelink" href="images.shtml?images=$id" target=_blank>$id</a></td>
                 <td style="text-align:center"><a class="link" href="javascript:void(0)"onclick="window.open('cgi-bin/mtc?action=get_source_info;query=$sourceID','sourceinfo','height=380, width=300,scrollbars=yes')">$source_name</a></td>
                 <td style="text-align:center">$phenotypes</td>
               </tr>
        );
      }
      $images .= qq(</tbody></table>);
    }
    else {
      $images = qq(
             <div class="header">Images/movies</div>
             <div style="text-indent: 10px">No image</div>
      );
    }
  }
  return $images;
}

=head2 targets

 Arg1: (optional) string, Mitocheck dsRNA ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the targets section of the dsRNA page.
 Returntype: string

=cut

sub targets {
  my ($self,$dsRNAID,$format) = @_;
  my $dsRNA = $self->_get_dsRNA($dsRNAID);
  if (!$dsRNA) {
    return undef;
  }
  my @target_genes = sort { $dsRNA->mismatch($a) <=> $dsRNA->mismatch($b) } $dsRNA->target_genes();
  my $targets = "";
  if ($format eq 'text') {
    $targets = qq(ID\tSymbol\tMismatches\ttranscripts hit/total transcripts\n);
    foreach my $target(@target_genes) {
      my $mismatch = $dsRNA->mismatch($target);
      my $geneID = $target->EnsemblID();
      my $target_symbol = $target->symbol();
      my ($chr,$start1,$end1,$start2,$end2) = $dsRNA->coordinates($target);
      my $start = $start1;
      my $end = $end2 || $end1;
      my @transcripts = $target->transcripts();
      my $n = scalar(@transcripts);
      my $hit = 0;
      foreach my $transcript(@transcripts) {
	my $mm = $dsRNA->mismatch($transcript);
	$hit++ if (defined($mm) && $mm < 1);
      }
      $targets .= qq($geneID\t$target_symbol\t$mismatch\t$hit/$n\n);
    }
  }
  elsif ($format eq 'json') {
    $targets = q([);
    foreach my $i(0..$#target_genes) {
      my $target = $target_genes[$i];
      my $mismatch = $dsRNA->mismatch($target);
      my $geneID = $target->EnsemblID();
      my $target_symbol = $target->symbol();
      my ($chr,$start1,$end1,$start2,$end2) = $dsRNA->coordinates($target);
      my $start = $start1;
      my $end = $end2 || $end1;
      my @transcripts = $target->transcripts();
      my $n = scalar(@transcripts);
      my $hit = 0;
      foreach my $transcript(@transcripts) {
	my $mm = $dsRNA->mismatch($transcript);
	$hit++ if (defined($mm) && $mm < 1);
      }
      $targets .= qq( { "id": "$geneID",
                        "symbol": "$target_symbol",
                        "mismatch": "$mismatch",
                         "transcripts hit": "$hit/$n"
                       } );
      unless ($i == $#target_genes) {
	$targets .= ',';
      }
    }
    $targets .= q(]);
  }
  else {
    if (@target_genes && $target_genes[0]) {
      $targets .= qq(
             <div class="header">Targets</div>
             <table width="100%">
               <thead>
                 <tr>
                   <th width="10%" style="text-align:center">Target gene</th>
                   <th width="10%" style="text-align:center">Mismatches</th>
                   <th width="20%" style="text-align:center">transcripts hit/total transcripts</th>
                   <th width="20%" style="text-align:center">Genome context</th>
                 </tr>
               </thead>
               <tbody>
      );
      foreach my $target(@target_genes) {
	my $mismatch = $dsRNA->mismatch($target);
	my $geneID = $target->EnsemblID();
	my $target_gene = $target->symbol();
	my ($chr,$start1,$end1,$start2,$end2) = $dsRNA->coordinates($target);
	my $start = $start1;
	my $end = $end2 || $end1;
	my @transcripts = $target->transcripts();
	my $n = scalar(@transcripts);
	my $hit = 0;
	foreach my $transcript(@transcripts) {
	  my $mm = $dsRNA->mismatch($transcript);
	  $hit++ if (defined($mm) && $mm < 1);
	}
	$targets .= qq(
                 <tr>
                   <td style="text-align:center"><a class="genelink" href="gene.shtml?gene=$geneID">$target_gene</a></td>
                   <td style="text-align:center">$mismatch</td>
                   <td style="text-align:center">$hit/$n</td>
                   <td style="text-align:center"><a class="link" href="http://www.ensembl.org/Homo_sapiens/contigview?chr=$chr;vc_start=$start;vc_end=$end" TITLE="View in Ensembl" target=_blank>Ensembl</a> or <a class="link" href="http://genome.ucsc.edu/cgi-bin/hgTracks?hgsid=132015460&clade=mammal&org=Human&db=hg38&position=chr$chr%3A$start-$end&pix=620&Submit=submit" TITLE="View in UCSC genome browser" target=_blank>UCSC</a></td>
                 </tr>
        );
      }
      $targets .= qq(</tbody></table>);
    }
    else {
      $targets = qq(
             <div class="header">Targets</div>
             <div style="text-indent: 10px">No target</div>
      );
    }
  }
  return $targets;
}

=head2 alignments

 Arg1: (optional) string, Mitocheck dsRNA ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the alignments section of the dsRNA page.
 Returntype: string

=cut

sub alignments {
  my ($self,$dsRNAID,$format) = @_;
  my $dsRNA = $self->_get_dsRNA($dsRNAID);
  my @target_transcripts =  sort {$dsRNA->mismatch($a->gene())<=>$dsRNA->mismatch($b->gene())} $dsRNA->target_transcripts();
  if (!$dsRNA) {
    return undef;
  }
  my $alignments = "";
  if ($format eq 'text') {
    $alignments = qq(Gene\tTranscript\tMismatches\tAlignment\n);
    foreach my $transcript(@target_transcripts) {
      my $chr             = $transcript->chromosome();
      my $target_geneID   = $transcript->gene()->EnsemblID();
      my $target_gene     = $transcript->gene()->symbol();
      my $transcriptID    = $transcript->EnsemblID();
      my $mismatch        = $dsRNA->mismatch($transcript);
      my $alignment       = $dsRNA->alignment($transcript);
      $alignments .= qq($target_geneID\t$transcriptID\t$mismatch\t$alignment\n);
    }
  }
  elsif ($format eq 'json') {
    $alignments = q([);
    foreach my $i(0..$#target_transcripts) {
      my $transcript = $target_transcripts[$i];
      my $chr             = $transcript->chromosome();
      my $target_geneID   = $transcript->gene()->EnsemblID();
      my $target_gene     = $transcript->gene()->symbol();
      my $transcriptID    = $transcript->EnsemblID();
      my $mismatch        = $dsRNA->mismatch($transcript);
      my $alignment       = $dsRNA->alignment($transcript);
      $alignments .= qq( { "gene": "$target_geneID",
                           "transcript": "$transcriptID",
                           "mismatch": "$mismatch",
                           "alignment": "$alignment"
                          });
      unless ($i == $#target_transcripts) {
	$alignments .= ',';
      }
    }
    $alignments .= q(]);
  }
  else {
    $alignments .= qq(
            <div class="header">Alignments</div>
            <table width="100%">
              <thead>
                <tr>
                  <th width="40%" style="text-align:center">Target transcript</th>
                  <th width="10%" style="text-align:center">Mismatches</th>
                  <th width="50%" style="text-align:center">Alignment</th>
                </tr>
              </thead>
              <tbody>
    );
    foreach my $transcript(@target_transcripts) {
      my $chr             = $transcript->chromosome();
      my $target_geneID   = $transcript->gene()->EnsemblID();
      my $target_gene     = $transcript->gene()->symbol();
      my $transcriptID    = $transcript->EnsemblID();
      my $mismatch        = $dsRNA->mismatch($transcript);
      my $alignment       = $dsRNA->alignment($transcript);
      $alignment=~s/\n/<br>/g;
      $alignments .= qq(
                <tr>
	          <td style="text-align:center"><br><br><a class="genelink" href="gene.shtml?gene=$target_geneID">$target_gene</a>, transcript: <a class="genelink" href="http://www.ensembl.org/Homo_sapiens/transview?transcript=$transcriptID" target="_blank">$transcriptID</a>, chr $chr</td>
	          <td style="text-align:center">$mismatch</td>
                  <td style="text-align:center"><pre>$dsRNAID<br>$alignment$transcriptID</pre></td>
	        </tr>
      );
    }
    $alignments .= qq(</tbody></table>);
  }
  return $alignments;
}

=head2 graphic_maps

 Arg1: (optional) string, Mitocheck dsRNA ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the graphic map section of the dsRNA page.
 Returntype: string

=cut

sub graphic_maps {
  my ($self,$dsRNAID,$format) = @_;
  my $dsRNA = $self->_get_dsRNA($dsRNAID);
  if (!$dsRNA) {
    return undef;
  }
  my @target_genes = sort { $dsRNA->mismatch($a) <=> $dsRNA->mismatch($b) } $dsRNA->target_genes();
  my $maps = "";
  if ($format eq 'text') {

  }
  elsif ($format eq 'json') {

  }
  else {
    $maps .= qq(
            <div class="header">Maps</div>
    );
    foreach my $gene(@target_genes) {
      my $geneID= $gene->ID;
      my $symbol= $gene->symbol;
      $maps .= qq(
           <div class="grid">
             <div class="cell">
               $symbol<img src=cgi-bin/utilities/transview.pl?gene=$geneID;dsRNA=$dsRNAID; style="vertical-align:middle">
             </div>
           </div>
      );
    }
  }
  return $maps;
}

=head2 _get_dsRNA

 Arg: (optional) string, Mitocheck dsRNA ID
 Description: Get the dsRNA with the given ID
 Returntype: Mitocheck::dsRNA object

=cut

sub _get_dsRNA {
  my ($self,$dsRNAID) = @_;
  $dsRNAID ||= $self->{'dsRNAID'};
  my $dsRNAh = $self->{'dbc'}->get_dsRNAHandle();
  my $dsRNA;
  if ($dsRNAID=~/^DSR/i) {
    $dsRNA = $dsRNAh->get_by_id($dsRNAID);
  }
  # else {
  #   $dsRNA = $dsRNAh->get_by_external_ID($dsRNAID);
  # }
  return $dsRNA;
}

1;
