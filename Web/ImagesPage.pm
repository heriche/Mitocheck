# Author: jkh1
# 2016-04-05
#

=head1 NAME

 Mitocheck::Web::ImagesPage

=head1 SYNOPSIS




=head1 DESCRIPTION

 Methods to generate content for sections of the images page.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Web::ImagesPage;

use strict;
use warnings;
use Mitocheck::Web::Config;

my $imagepath = "http://www.mitocheck.org/data";

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, image set ID
 Description: Creates an ImagesPage object.
 Returntype: Mitocheck::ImagesPage

=cut

sub new {

  my $class = shift;
  my $dbc = shift;
  my $ID = shift if @_;
  my $self = {};
  $self->{'dbc'} = $dbc;
  $self->{'ID'} = $ID if $ID;
  bless ($self, $class);

  return $self;
}

=head2 metadata

 Arg1: (optional) string, Mitocheck image set ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the metadata section of the images page.
 Returntype: string

=cut

sub metadata {
  my ($self,$ID,$format) = @_;
  my $imageSet = $self->_get_image_set($ID);
  if (!$imageSet) {
    return undef;
  }
  my @dsRNAs = $imageSet->dsRNAs();
  my $dsRNAID = $dsRNAs[0]->ID();
  my @genes = $dsRNAs[0]->target_genes(0);
  my $spot_location = $imageSet->spot_location();
  my $source = $imageSet->source();
  my $source_name = $source->name();
  my $metadata = "";
  if ($format eq 'text') {
    my $target_genes = join(", ",map { my $symbol = $_->symbol();
				       my $id = $_->EnsemblID();
				       qq($id|$symbol)
				     } @genes);
    $metadata = qq(ID\tdsRNA\tTargeted gene(s)\tLocation\tSource\n);
    $metadata .= qq($ID\t$dsRNAID\t$target_genes\t$spot_location\t$source_name\n);
  }
  elsif ($format eq 'json') {
    my $target_genes = join(", ",map { my $symbol = $_->symbol();
				       my $id = $_->EnsemblID();
				       qq($id|$symbol)
				     } @genes);
    $metadata = qq( {  "id": "$ID",
                       "dsRNA": "$dsRNAID",
                       "target_genes": "$target_genes",
                       "location": "$spot_location",
                       "source": "$source_name" } );
  }
  else { # html
    my $s = scalar (@genes)>1? "s":"";
    my $target_genes = join(", ",map { my $symbol = $_->symbol();
				       my $id = $_->EnsemblID();
				       qq(<a class="genelink" href="gene.shtml?gene=$id">$symbol</a>)
				     } @genes);
    $metadata .= qq(
           <div class="header">Metadata</div>
           <div class="identifier emphasis">Targeted gene$s: $target_genes</div>
           <div class="identifier emphasis">dsRNA: <a class="genelink" href="dsRNA.shtml?dsRNA=$dsRNAID">$dsRNAID</a></div>
         </div>
    );
  }
  return $metadata;
}

=head2 images

 Arg1: (optional) string, Mitocheck image set ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the images section of the images page.
 Returntype: string

=cut

sub images {
  my ($self,$ID,$format) = @_;
  my $imageSet = $self->_get_image_set($ID);
  if (!$imageSet) {
    return undef;
  }
    my @images = sort {$a->display_order<=>$b->display_order} $imageSet->images();
  my @dsRNAs = $imageSet->dsRNAs();
  my @genes = $dsRNAs[0]->target_genes(0);
  my $neg_control = $imageSet->control() || '';
  my $neg_controlID = "";
  if ($neg_control) {
    $neg_controlID = $neg_control->ID;
  }
  my $is_ctrl = $neg_controlID eq $ID ? 1 : 0;
  my @cell_lines = $imageSet->cell_lines();
  my @phenotypes = $imageSet->phenotypes();
  my $source = $imageSet->source;
  my @labels;
  my %seen;
  foreach my $image(@images) {
    my $labels = join(", ", sort map {$_->name} $image->reporters);
    push @labels, $labels unless ($seen{$labels}++);
  }
  my $images = "";
  if ($format eq 'text') {
    $images = qq(Cell line\tLabels\tFile\n);
    foreach my $image(@images) {
      my $cell_line = $image->cell_line();
      my $cell_line_name = $cell_line->name();
      my $labels = join(", ", sort map {$_->name} $image->reporters);
      my $filename = $image->filename();
      my $type = $image->file_type();
      $images .= qq($cell_line_name\t$labels\t$filename\n);
    }
  }
  elsif ($format eq 'json') {
    $images = q([);
    foreach my $i(0..$#images) {
      my $image = $images[$i];
      my $cell_line = $image->cell_line();
      my $cell_line_name = $cell_line->name();
      my $labels = join(", ", sort map {$_->name} $image->reporters);
      my $filename = $image->filename();
      my $type = $image->file_type();
      $images .= qq( { "file": "$filename",
                       "cell_line": "$cell_line_name",
                       "labels": "$labels"
                     } );
      unless ($i == $#images) {
	$images .= ',';
      }
    }
    $images .= q(]);
  }
  else { # html
   $images .= qq(
           <div class="header">Images/movies</div>
           <div class="grid">
    );
    foreach my $cell_line(@cell_lines) {
      my $cell_line_name = $cell_line->name();
      $images .= qq(<div class="row">
                      <div class="cell"></div>
      );
      foreach my $label(@labels) {
	$images .= qq(<div class="column_header" style="text-align: center;">$label</div>);
      }
      $images .= qq(</div>
                    <div class="row">
                      <div class="column_header" style="vertical-align: middle; text-align: center; width: 60px;">$cell_line_name</div>
      );
      foreach my $i(0..$#labels) {
	foreach my $image(@images) {
	  next if ($image->cell_line->name ne $cell_line_name);
	  my $labels = join(", ", sort map {$_->name} $image->reporters);
	  next if ($labels ne $labels[$i]);
	  my $filename = $image->filename();
	  my $url = $imagepath.$filename;
	  if ($image->file_type eq 'movie') {
	    $images .= qq(
                      <div class="cell" style="width: 650px;">
                        <video src="$url" controls width="640" height="496">
                        Your browser does not support the <code>video</code> element.
                        </video>
                      </div>
            );
	  }
	  else {
	    $images .= qq(
                      <div class="cell" style="width: 220px;">
                        <img style="display: block; margin: 0 auto;" src="$url" width="210">
                      </div>
            );
	  }
	}
      }
      my $fillers = 3 - scalar(@labels);
      if ($fillers>0) {
	foreach my $i(1..$fillers) {
	  $images .= qq(<div class="cell"></div>);
	}
      }
      $images .= qq(</div>); # close cell line row
    }
    $images .= qq(</div>); # close grid
    if ($neg_control) {
      $images .= qq(<p style="text-align: center;"><a class="link" href="images.shtml?images=$neg_controlID" target=_blank>Show negative control</a></p>) unless ($is_ctrl);
    }
  }
  return $images;
}

=head2 phenotypes

 Arg1: (optional) string, Mitocheck image set ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the phenotypes section of the images page.
 Returntype: string

=cut

sub phenotypes {
  my ($self,$ID,$format) = @_;
  my $imageSet = $self->_get_image_set($ID);
  if (!$imageSet) {
    return undef;
  }
  my @phenotypes = $imageSet->phenotypes();
  my $phenotypes = "";
  if ($format eq 'text') {
    $phenotypes = qq(Description\tCMPO term\tCMPO ID\tAnnotation type\n);
    foreach my $phenotype(@phenotypes) {
      my $desc = ucfirst($phenotype->description());
      my $CMPOterm = $phenotype->CMPOterm();
      my $CMPOID = $phenotype->CMPOID();
      my $annotation_type = $phenotype->annotation_type();
      $phenotypes .= qq($desc\t$CMPOterm\t$CMPOID\t$annotation_type\n);
    }
  }
  elsif ($format eq 'json') {
    $phenotypes = q([);
    foreach my $i(0..$#phenotypes) {
      my $phenotype = $phenotypes[$i];
      my $desc = ucfirst($phenotype->description());
      my $CMPOterm = $phenotype->CMPOterm();
      my $CMPOID = $phenotype->CMPOID();
      my $annotation_type = $phenotype->annotation_type();
      $phenotypes .= qq(
                        {
                          "description": "$desc",
                          "CMPO": {"term": "$CMPOterm", "id": "$CMPOID"},
                          "annotation_type": "$annotation_type"
                        } );
      unless ($i == $#phenotypes) {
	$phenotypes .= ',';
      }
    }
    $phenotypes .= q(]);
  }
  else { # html
    if (@phenotypes && $phenotypes[0]) {
      $phenotypes .= qq(
             <div class="header">Phenotypes</div>
             <table width="100%">
               <thead>
               <tr>
                 <th width="40%" style="text-align:left">Description</th>
                 <th width="10%" style="text-align:center"><a title="M: manual, A: automatic">Annotation type</a></th>
                 <th width="35%" style="text-align:center">Source</th>
               </tr>
               </thead>
               <tbody>
      );
      foreach my $phenotype(@phenotypes) {
	my $desc = ucfirst($phenotype->description());
	my $CMPOterm = $phenotype->CMPOterm();
	my $CMPOID = $phenotype->CMPOID();
	my $annotation_type = $phenotype->annotation_type() eq 'manual'? 'M':'A';
	my $source = $phenotype->source();
	my $source_name = $source->name();
	my $sourceID = $source->ID();
	$phenotypes .= qq(
               <tr>
                 <td>$desc</td>
                 <td style="text-align:center;">$annotation_type</td>
                 <td style="text-align:center;"><a class="link" href="javascript:void(0)"onclick="window.open('cgi-bin/mtc?action=get_source_info;query=$sourceID','sourceinfo','height=380, width=300,scrollbars=yes')">$source_name</a></td>
               </tr>
        );
      }
      $phenotypes .=qq(</tbody></table>);
    }
    else {
      $phenotypes .= qq(
             <div class="header">Phenotypes</div>
             <div style="text-indent: 10px">No phenotype</div>
      );
    }
  }
  return $phenotypes;
}

=head2 plots

 Arg1: (optional) string, Mitocheck image set ID
 Arg2: (optional) string, format (default to html)
 Description: Creates the plots section of the images page.
 Returntype: string

=cut

sub plots {
  my ($self,$ID,$format) = @_;
  my $imageSet = $self->_get_image_set($ID);
  if (!$imageSet) {
    return undef;
  }
  my $spot_location = $imageSet->spot_location;
  my ($plate,$spot) = split(/\//,$spot_location);
  my $neg_control = $imageSet->control();
  my ($neg_controlID,$ctrlspot,$is_ctrl);
  if ($neg_control) {
    $neg_controlID = $neg_control->ID() if $neg_control;
    my $ctrl_location = $neg_control->spot_location;
    (undef,$ctrlspot) = split(/\//,$ctrl_location);
    $is_ctrl = $neg_controlID eq $ID ? 1 : 0;
  }
  my $source = $imageSet->source();
  my $plotdir = $PLOTS_DIR{$source->name()};
  unless ($plotdir) {
    return "";
  }
  # html only
  my $plots = q(<div class="header">Plots</div>);
  unless ($is_ctrl) {
    $plots .= qq(<TABLE RULES=cols WIDTH="100%"><TR><TH>Control</TH><TH>This movie</TH></TR>);
  }
  else {
    $plots .= qq(<TABLE WIDTH="100%"><TR><TH>This movie</TH><TH></TH></TR>);
  }
  my $title = $is_ctrl? 'Plot of proliferation index for this movie' : 'Plot of proliferation index for this movie\'s negative control';
  $plots .= qq(<TR><TD style="text-align: center;"><A HREF="$plotdir/$plate/ProliferationBestNegativeControl_$plate.png"><IMG SRC="$plotdir/$plate/ProliferationBestNegativeControl_$plate.png" WIDTH=333 HEIGHT=267 ALT="Plot of proliferation index for control movie" TITLE="$title"></A></TD>);
  if (!$is_ctrl) {
    $plots .= qq(<TD style="text-align: center;"><A HREF="$plotdir/$plate/$spot/Proliferation_$plate--$spot.png"><IMG SRC="$plotdir/$plate/$spot/Proliferation_$plate--$spot.png" WIDTH=333 HEIGHT=267 ALT="Plot of proliferation index for this movie" TITLE="Plot of this movie's proliferation index"></A></TD></TR>);
  }
  $title = $is_ctrl? "Plot of this movie's features" : 'Plot of all features for this movie\'s negative control';
  $plots .= qq(<TR><TD style="text-align: center;"><A HREF="$plotdir/$plate/PhenoBestNegativeControl_$plate.png"><IMG SRC="$plotdir/$plate/PhenoBestNegativeControl_$plate.png" WIDTH=333 HEIGHT=267 ALT="Plot of features for control movie" TITLE="$title"></A></TD>);
  if (!$is_ctrl) {
    $plots .= qq(<TD style="text-align: center;"><A HREF="$plotdir/$plate/$spot/Pheno_$plate--$spot.png"><IMG SRC="$plotdir/$plate/$spot/Pheno_$plate--$spot.png" WIDTH=333 HEIGHT=267 ALT="Plot of features for this movie" TITLE="Plot of this movie's features that differ from control"></A></TD></TR>);
  }
  $plots .= "</TABLE>";

  return $plots;
}

=head2 _get_image_set

 Arg: (optional) string, Mitocheck image set ID
 Description: Get the image set with the given ID
 Returntype: Mitocheck::ImageSet object

=cut

sub _get_image_set {
  my ($self,$ID) = @_;
  $ID ||= $self->{'ID'};
  my $ish = $self->{'dbc'}->get_ImageSetHandle();
  my $set = $ish->get_by_id($ID);

  return $set;
}

1;
