# Author: jkh1
# 2016-03-18
#

=head1 NAME

 Mitocheck::Web::Tools

=head1 SYNOPSIS




=head1 DESCRIPTION

 Tools to process a gene list.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Web::Tools;

use strict;
use warnings;
use Carp;
use Mitocheck::Web::Config;
use Algorithms::Graph;

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, arrayref of IDs
 Description: Creates a Tools object.
 Returntype: Mitocheck::Tools

=cut

sub new {

  my $class = shift;
  my $dbc = shift;
  my $IDs = shift if @_;
  my $self = {};
  $self->{'dbc'} = $dbc;
  $self->{'IDs'} = $IDs if $IDs;
  bless ($self, $class);

  return $self;
}

=head2 phenomap

 Arg1: optional, arrayref of gene IDs
 Arg2: (optional) string, format (default to json)
 Description: Gets phenotype profiles of the query genes.
 Returntype: string

=cut

sub phenomap {

  my ($self, $IDs, $format) = @_;
  $IDs ||= $self->{'IDs'};
  my $phenomap;
  my $dbh = $self->{'dbc'}->get_DatabaseHandle();
  my @phenotypes;
  # Get all phenotypes
  my $query = qq( SELECT DISTINCT description FROM phenotype
                  ORDER BY sourceID,phenotypeID );
  my $sth = $dbh->prepare($query);
  $sth->execute();
  my %index;
  my $i = 0;
  my $max_text_length = 0;
  while (my ($desc) = $sth->fetchrow_array()) {
    $desc=~s/\/[a-np-z].+$//;
    push @phenotypes,qq(<span title='$desc'>$desc<\/span>);
    $index{$desc} = $i;
    $i++;
  }
  my @profiles;
  my @IDs;
  my %seen;
  foreach my $ID(@{$IDs}) {
    my @profile = (0) x scalar(@phenotypes);
    my ($gene,$dsRNA);
    if ($ID=~/^DSR\d+/i) {
      my $dsRNAh =  $self->{'dbc'}->get_dsRNAHandle();
      $dsRNA = $dsRNAh->get_by_id($ID);
      next unless $dsRNA;
      next if ($seen{$ID}++);
      my $extID = $dsRNA->external_ID();
      push @IDs, qq(<a href='dsRNA.shtml?dsRNA=$ID'>$extID<\/a>);
      foreach my $phen($dsRNA->phenotypes()) {
	my $desc = $phen->description;
	$desc=~s/\/[a-np-z].+$//;
	my $r = $phen->reproducibility($dsRNA);
	my ($n,undef) = split(/\//,$r);
	$profile[$index{$desc}] = $n if ($index{$desc});
      }
      push @profiles, \@profile;
    }
    else { # assume it's a gene
      $gene = $self->_get_gene($ID);
      next unless $gene;
      my $EnsID = $gene->EnsemblID;
      next if ($seen{$EnsID}++);
      my $symbol = $gene->symbol || $ID;
      push @IDs, qq(<a href='gene.shtml?gene=$EnsID'>$symbol<\/a>);
      foreach my $phen($gene->phenotypes()) {
	my $desc = $phen->description;
	$desc=~s/\/[a-np-z].+$//;
	my $idx = $phen->dsRNA_index($gene);
	my ($n,undef) = split(/\//,$idx);
	$profile[$index{$desc}] = $n if ($index{$desc});
      }
      push @profiles, \@profile;
    }
  }
  if ($format eq 'text') {

  }
  else {
    my $scale = qq([[0,],[],[],[],[],[],[],[]]);
    my $x = '["'.join('","',@phenotypes).'"]';
    my $y = '["'.join('","',@IDs).'"]';
    my $z = '[';
    foreach my $i(0..$#profiles) {
      $z .= '["'.join('","',@{$profiles[$i]}).'"]';
      unless ($i == $#profiles) {
	$z .= ',';
      }
    }
    $z .= ']';
    $phenomap = qq({"x": $x, "y": $y, "z": $z, "type": "heatmap", "colorscale": "Reds"});
  }
  return $phenomap;
}

=head2 siRNAs_selection

 Arg1: optional, arrayref of gene IDs
 Arg2: (optional) string, format (default to html)
 Description: Gets dsRNAs targeting the query genes
 Returntype: string

=cut

sub siRNAs_selection {
  my ($self, $IDs, $format) = @_;
  $IDs ||= $self->{'IDs'};
  my $dsRNAh =  $self->{'dbc'}->get_dsRNAHandle();
  my $dsRNAs;
  if ($format eq 'text') {

  }
  else {
    $dsRNAs = qq(
        <table id="siRNAs">
          <thead>
            <tr>
              <th class="details-control"> </th>
              <th>Gene</th>
              <th>Number of dsRNAs</th>
              <th> </th>
            </tr>
          </thead>
          <tbody>
    );
    my %seen;
    foreach my $geneID(@{$IDs}) {
      my $gene = $self->_get_gene($geneID);
      next unless $gene;
      my @dsRNAs = grep { $_->mismatch($gene)==0 } $dsRNAh->get_all_by_gene($gene);
      my $n = scalar(@dsRNAs);
      my $EnsID = $gene->EnsemblID;
      next if ($seen{$EnsID}++);
      my $symbol = $gene->symbol;
      $dsRNAs .= qq(
            <tr id="$EnsID">
              <td class="details-control"> </td>
              <td style="text-align:center;"><a class="genelink" href='gene.shtml?gene=$EnsID'>$symbol<\/a></td>
              <td style="text-align:center;">$n</td>
              <td class="child_row">
      );
      $dsRNAs .= qq(
                <table>
                  <thead>
                    <tr>
                      <th width="20%" style="text-align:center">dsRNA ID</th>
                      <th width="20%" style="text-align:center">Supplier</th>
                      <th width="10%" style="text-align:center">Cat#</th>
                      <th width="10%" style="text-align:center">#mismatches</th>
                      <th width="40%" style="text-align:center">Other targets</th>
                    </tr>
                  </thead>
                  <tbody>
      );
      foreach my $siRNA(@dsRNAs) {
    	my $ID = $siRNA->ID();
    	my $ext_ID = $siRNA->external_ID();
    	my $mismatch = $siRNA->mismatch($gene);
    	my @targets = $siRNA->target_genes();
    	# Remove current gene from targets list
    	my @other_targets = grep { $_->EnsemblID() ne $EnsID } @targets;
    	my @list;
    	foreach my $other_target(@other_targets) {
    	  my $symbol = $other_target->symbol();
    	  my $id = $other_target->EnsemblID();
    	  my $mm = $siRNA->mismatch($other_target);
    	  push @list,qq(<a class="genelink" href="?gene=$id">$symbol</a> (mismatch: $mm));
    	}
    	my $targets = "";
    	if (@list) {
    	  $targets = join("; ",@list);
    	}
    	else {
    	  $targets = "none";
    	}
    	my $supplier = $siRNA->supplier();
    	my $supplier_name = $supplier->name;
    	next if ($supplier_name eq 'CellBASE');
    	my $supplier_URL = $supplier->URL;
    	$dsRNAs .= qq(
               <tr>
                 <td style="text-align:center"><a class="genelink" href="dsRNA.shtml?dsRNA=$ID">$ID</a></td>
                 <td style="text-align:center">$supplier_name</td>
                 <td style="text-align:center">$ext_ID</td>
                 <td style="text-align:center">$mismatch</td>
                 <td style="text-align:center">$targets</td>
               </tr>
    	);
      }
      $dsRNAs .= qq(</tbody>
                  </table>
                </td>
              </tr>
      );
    }
    $dsRNAs .= qq(</tbody></table>);
  }
  return $dsRNAs;
}

=head2 interaction_graph

 Arg1: optional, arrayref of gene IDs
 Arg2: (optional) string, format (default to html)
 Description: Gets relevant protein interaction graph using a limited k-walk
              on the whole interaction graph.
              See: Faust K, Dupont P, Callut J, van Helden J. Pathway discovery
              in metabolic networks by subgraph extraction. Bioinformatics.
              2010 May 1;26(9): 1211-8.
 Returntype: string

=cut

sub interaction_graph {
  my ($self, $IDs, $format) = @_;
  $IDs ||= $self->{'IDs'};

  my $EnsIDs;
  unless ($IDs->[0] =~/ENSG/i) { # Assume all genes use the same type
    # Convert query from gene symbols to EnsEMBL IDs
    my $gh = $self->{'dbc'}->get_GeneHandle();
    my @EnsIDs;
    foreach my $id(@$IDs) {
      my $gene = $gh->get_by_symbol($id);
      if ($gene) {
	push @$EnsIDs, $gene->EnsemblID;
      }
    }
  }
  else {
    $EnsIDs = $IDs;
  }
  my %is_query;
  @is_query{@{$EnsIDs}} = map '1', @{$EnsIDs};

  my $graph = "";

  if (scalar(keys %is_query <2)) {
    my @IDs = keys %is_query;
    if (@IDs && defined($IDs[0])) {
      my $gh = $self->{'dbc'}->get_GeneHandle();
      my $symbol = $gh->get_by_EnsemblID($IDs[0])->symbol();
      $graph .= qq( { "data": { "id": "$IDs[0]", "name": "$symbol", "is_query": true} });
    }
    else {
      $graph .= qq( { "data": { "id": "Unknown", "name": "Unknown" } });
    }
    return $graph;
  }

  # Reading graph from file is faster than building it from database queries
  my $G = Algorithms::Graph->read_from_file($PROTEIN_INTERACTIONS);

  # Extract relevant subgraph
  my $subgraph = $G->get_relevant_subgraph($EnsIDs, 0.0001, 50, 0);
  my @nodes = $subgraph->vertices();
  my @edges = $subgraph->edges();
  # Export in selected format
  if ($format eq 'text') {
    foreach my $edge(@edges) {
      my $a = $edge->[0];
      my $b = $edge->[1];
      $graph .= qq($a\t$b\n);
    }
  }
  elsif ($format eq 'json') {
    my $gh = $self->{'dbc'}->get_GeneHandle();
    $graph .= q([);
    foreach my $node(@nodes) {
      my $is_query = q();
      if ($is_query{$node}) {
	$is_query = qq(, "is_query": true);
      }
      my $symbol = $gh->get_by_EnsemblID($node)->symbol();
      $graph .= qq( { "data": { "id": "$node", "name": "$symbol" $is_query } },);
    }
    my @tmp;
    foreach my $edge(@edges) {
      my $a = $edge->[0];
      my $b = $edge->[1];
      push @tmp, qq( { "data": {
                              "id": "$a-$b",
                              "source": "$a",
                              "target": "$b"
                      }
                    }
      );
    }
    $graph .= join(", ", @tmp);
    $graph .= qq( ] );
  }
  else {
    # Format not supported
  }

  return $graph;
}

=head2 browse_screens

 Description: Browse screens by phenotype
 Returntype: string

=cut

sub browse_screens {
  my ($self,undef) = @_;
  my $dbc = $self->{'dbc'};
  my $sh = $dbc->get_SourceHandle();
  my @screens = sort {$a->name cmp $b->name } $sh->get_all_screens();
  # Reorder to put validation screen before secondary screen
  foreach my $i(1..$#screens-1) {
    if ($screens[$i]->name=~/secondary/ && $screens[$i+1]->name=~/validation/) {
      ($screens[$i],$screens[$i+1]) = ($screens[$i+1],$screens[$i]);
      $i += 2;
    }
  }
  my $ph = $dbc->get_PhenotypeHandle();

  my $html = qq(
    <form id="browse">
      <p><B>Use this page to get a list of genes with some phenotype.</B>
      <p><B>Select a screen:</B><BR>);
  foreach my $screen(@screens) {
    my $name = $screen->name;
    my $id = $screen->ID;
    my $divid = 'div'.$id;
    $html .= qq(
      <INPUT TYPE="radio" NAME="source" VALUE="$id" onclick="switchDiv('$id');">$name<BR>);
    }
  $html .= qq(
      <P><input type="checkbox" name="dsRNA_index" value="1">For each gene, consider only phenotypes shown by more than 1 dsRNA (in the selected screen)<br>);
  $html .=qq(
      <INPUT TYPE="hidden" NAME="query_type" VALUE="phenotypes">
      <INPUT TYPE="hidden" NAME="action" VALUE="search">
    );
  foreach my $screen(@screens) {
    my $id = $screen->ID;
    my $divid = 'div'.$id;
    my @phenotypes = $ph->get_all_by_screen($screen,1);

    $html .= qq(<DIV id="$divid" style="position: absolute; display: none;">
      <B>Select a phenotype:</B><BR>M: manual annotation, A: automatic annotation<BR><BR>
      <SELECT id="$id" NAME="query" SIZE="1" disabled="disabled">
        <OPTION VALUE="">-------</option>
	<OPTION VALUE="any">any phenotype</option>);
    if ($screen->name eq 'Mitocheck primary screen' || $screen->name eq 'Mitocheck validation screen') {
      $html .= qq(
        <OPTION VALUE="mitotic">any mitotic phenotype (A+M)</option>
        <OPTION VALUE="mitotic (M)">any mitotic phenotype (M)</option>
	<OPTION VALUE="mitotic (A)">any mitotic phenotype (A)</option>);
      if ($screen->name eq 'Mitocheck primary screen') {
	$html .= qq(<OPTION VALUE="motility">any motility phenotype</option>);
      }
    }
    foreach my $phenotype(@phenotypes) {
      my $desc = $phenotype->description;
      my $phID = $phenotype->ID;
      my $annot = $phenotype->annotation_type eq 'manual' ?' (M)' : ' (A)';
      $html .= qq(<OPTION VALUE="$phID"> $desc $annot<BR>);
    }
    $html .= qq(</SELECT> </DIV>);
  }

  $html .= qq(</form>
         <P><BR><BR><BR><BR>);

  return $html;
}

=head2 _get_gene

 Arg: (optional) string, Ensembl or Mitocheck gene ID
 Description: Get the gene with the given ID
 Returntype: Mitocheck::Gene object

=cut

sub _get_gene {
  my ($self,$geneID) = @_;
  $geneID ||= $self->{'geneID'};
  my $gh = $self->{'dbc'}->get_GeneHandle();
  my $gene;
  if ($geneID=~/^MCG_/i) {
    $gene = $gh->get_by_id($geneID);
    $geneID = $gene->EnsemblID();
  }
  elsif ($geneID=~/ENSG/i) {
    $gene = $gh->get_by_EnsemblID($geneID);
  }
  else { # Assume gene symbol
    $gene = $gh->get_by_symbol($geneID);
  }
  return $gene;
}

1;
