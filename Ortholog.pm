# Author: jkh1
# 2005-06-13
#

=head1 NAME

 Mitocheck::Ortholog

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $ortholog_handle = $dbc->get_OrthologHandle();

 my $ortholog = $ortholog_handle->get_by_id('OTG00000001');

 my $symbol = $ortholog->symbol();
 my $species = $ortholog->species();




=head1 DESCRIPTION

 Representation of a Mitocheck ortholog.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Ortholog;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_orth_count = 0;
  my $_newID_count = 0;

  sub get_ortholog_count {
    return $_orth_count;
  }
  sub _incr_orth_count {
    return ++$_orth_count;
  }
  sub _decr_orth_count {
    return --$_orth_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, ortholog ID
 Description: Creates a new ortholog object.
 Returntype: Mitocheck::Ortholog

=cut

sub new {

  my ($class,$dbc,$orthologID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_orth_count();
  if (!defined $self->{'ID'} && !$orthologID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   orthologID
                 FROM     ortholog
                 ORDER BY orthologID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $orthologID = "OTG"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $orthologID;
  bless ($self, $class);
  return $self;

}

=head2 ID

 Arg: optional, Mitocheck ortholog ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 human_genes

 Arg: optional, list of Mitocheck::Genes
 Description: Gets/sets the human gene(s) this is the ortholog of.
 Returntype: list of Mitocheck::Genes

=cut

sub human_genes {

  my $self = shift;
  @{$self->{'human_genes'}} = @_ if @_;
  if (!defined $self->{'human_genes'}) {
    my $orthologID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT DISTINCT o.geneID FROM ortholog o WHERE o.orthologID = ?");
    $sth->execute($orthologID);
    while (my ($geneID) = $sth->fetchrow_array()) {
      my $dbc = $self->{'DBConnection'};
      my $gh = $dbc->get_GeneHandle();
      push @{$self->{'human_genes'}},$gh->get_by_id($geneID);
    }
  }
  return @{$self->{'human_genes'}};
}

=head2 symbol

 Arg: optional, gene symbol
 Description: Gets/sets ortholog's symbol
 Returntype: string

=cut

sub symbol {

  my $self = shift;
  $self->{'symbol'} = shift if @_;
  if (!defined $self->{'symbol'}) {
    my $orthologID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT o.symbol FROM ortholog o WHERE o.orthologID = ?");
    $sth->execute($orthologID);
    ($self->{'symbol'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'symbol'};

}

=head2 database

 Arg: optional, organism database
 Description: Gets/sets ortholog's organism database
 Returntype: string

=cut

sub database {

  my $self = shift;
  $self->{'database'} = shift if @_;
  if (!defined $self->{'database'}) {
    my $orthologID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT o.db FROM ortholog o WHERE o.orthologID = ? ");
    $sth->execute($orthologID);
    ($self->{'database'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'database'};


}

=head2 dbID

 Arg: optional, ID in organism database
 Description: Gets/sets ortholog's organism database ID
 Returntype: string

=cut

sub dbID {

  my $self = shift;
  $self->{'dbID'} = shift if @_;
  if (!defined $self->{'dbID'}) {
    my $orthologID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT o.dbID FROM ortholog o WHERE o.orthologID = ? ");
    $sth->execute($orthologID);
    ($self->{'dbID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'dbID'};

}

=head2 altID

 Arg: optional, alternate gene ID of ortholog
 Description: Gets/sets ortholog's alternate ID
 Returntype: string

=cut

sub altID {

  my $self = shift;
  $self->{'altID'} = shift if @_;
  if (!defined $self->{'altID'}) {
    my $orthologID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT o.altID FROM ortholog o WHERE o.orthologID = ? ");
    $sth->execute($orthologID);
    ($self->{'altID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'altID'};

}



=head2 description

 Arg: optional, text description of ortholog function
 Description Gets/sets description of ortholog function
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $geneID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT o.description FROM ortholog o WHERE o.orthologID = ? ");
    $sth->execute($geneID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 species

 Arg: none
 Description: Gets/sets ortholog's species
 Returntype: string

=cut

sub species {

  my $self = shift;
  $self->{'species'} = shift if @_;
  if (!defined $self->{'species'}) {
    my $orthologID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT o.species
                             FROM ortholog o
                             WHERE o.orthologID = ? ");
    $sth->execute($orthologID);
    ($self->{'species'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'species'};

}

sub DESTROY {

  my $self = shift;
  $self->_decr_orth_count();
  $self->_decr_newID_count() if $self->{'is_new'};

}


1;
