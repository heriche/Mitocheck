# Author: jkh1
# 2005-06-13
#

=head1 NAME

 Mitocheck::OrthologHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $ortholog_handle = $dbc->get_OrthologHandle();

 my @orthologs = $ortholog_handle->get_all_by_gene('GEN00000001');

=head1 DESCRIPTION

 Enables retrieval of Mitocheck ortholog objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::OrthologHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Ortholog;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new ortholog object handle
 Returntype: Mitocheck::OrthologHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;
}

=head2 get_by_id

 Arg: gene ID
 Description: Gets ortholog with given ID
 Returntype: Mitocheck::Ortholog object

=cut

sub get_by_id {

  my ($self,$orthologID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if ortholog exists in database
  my $query = qq(SELECT COUNT(*) FROM ortholog WHERE orthologID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($orthologID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::Ortholog($dbc,$orthologID);
}

=head2 get_by_symbol

 Arg1: species Latin name, e.g. Mus musculus
 Arg2: gene symbol
 Description: Gets ortholog with given symbol in given species
 Returntype: Mitocheck::Ortholog object

=cut

sub get_by_symbol {

  my ($self,$species,$symbol) = @_;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT orthologID FROM ortholog
                            WHERE species=?
                            AND   symbol=?");
  $sth->execute($species,$symbol);
  my ($ID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $ID );

  return $self->get_by_id($ID);

}

=head2 get_by_dbID

 Arg1: model organism database
 Arg2: model organism database ID
 Description: Gets ortholog with given database ID
 Returntype: Mitocheck::Ortholog object

=cut

sub get_by_dbID {

  my ($self,$db,$dbID) = @_;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT orthologID FROM ortholog
                            WHERE db=?
                            AND   dbID=?");
  $sth->execute($db,$dbID);
  my ($ID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $ID );

  return $self->get_by_id($ID);

}

=head2 get_by_altID

 Arg1: species Latin name
 Arg2: ortholog alternate ID
 Description: Gets ortholog with given ID
 Returntype: Mitocheck::Ortholog object

=cut

sub get_by_altID {

  my ($self,$species,$altID) = @_;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT orthologID FROM ortholog
                            WHERE species=?
                            AND   altID=?");
  $sth->execute($species,$altID);
  my ($ID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $ID );

  return $self->get_by_id($ID);

}



=head2 get_all_by_gene

 Arg1: Mitocheck::Gene or gene ID
 Arg2: optional, species name
 Description: Gets orthologs of given gene, limit to given species if any
 Returntype: list of Mitocheck::Ortholog objects

=cut

sub get_all_by_gene {

  my ($self,$gene,$species) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbh = $self->{'database_handle'};
  my $sth;
  if (defined $species) {
    $sth = $dbh->prepare("SELECT DISTINCT o.orthologID
                          FROM ortholog o
                          WHERE o.geneID = ?
                          AND o.species = ? ");
    $sth->execute($geneID,$species);
  }
  else {
    $sth = $dbh->prepare("SELECT DISTINCT o.orthologID
                          FROM ortholog o
                          WHERE o.geneID = ?
                          ORDER BY species ");
    $sth->execute($geneID);
  }
  my @orthologs;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@orthologs,$self->get_by_id($id));
  }
  $sth->finish();

  return @orthologs;
}

=head2 new_ortholog

 Arg: ortholog attributes
 Description: creates a new ortholog object
 Returntype: Mitocheck::Ortholog

=cut

sub new_ortholog {

  my $self = shift;
  my %ortholog = @_;
  unless (defined($ortholog{'-human_gene'})) {
    croak "Error: ortholog requires a human gene";
  }
  my $dbc = $self->{'DBConnection'};
  my $ortholog = new Mitocheck::Ortholog($dbc);
  $ortholog->human_genes($ortholog{'-human_gene'});
  $ortholog->species($ortholog{'-species'});
  $ortholog->database($ortholog{'-database'});
  $ortholog->dbID($ortholog{'-dbID'});
  $ortholog->altID($ortholog{'-altID'});
  $ortholog->symbol($ortholog{'-symbol'});
  $ortholog->description($ortholog{'-description'}) if $ortholog{'-description'};
  return $ortholog;

}

=head2 store

 Arg: Mitocheck::Ortholog
 Description: Enters ortholog and associated data
              in Mitocheck database
 Returntype: Mitocheck::Ortholog, same as Arg

=cut

sub store {

  my ($self,$ortholog) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my @genes = $ortholog->human_genes();
  croak "Human gene required. Can't store ortholog in database" unless ($genes[0]);
  foreach my $gene(@genes) {
    my $geneID = $gene->ID();
    my $species = $ortholog->species() || croak "Species required. Can't store ortholog in database";
    my $db = $ortholog->database() || croak "Database name required. Can't store ortholog in database";
    my $dbID = $ortholog->dbID() || croak "dbID required. Can't store ortholog in database";
    my $altID = $ortholog->altID() || '';
    my $symbol = $ortholog->symbol() || croak "Symbol required. Can't store ortholog in database";
    #check if already in database
    my $query=qq(SELECT orthologID
                 FROM ortholog
                 WHERE geneID='$geneID'
                 AND db='$db'
                 AND dbID='$dbID');
    my $sth= $dbh->prepare($query);
    $sth->execute();
    my $orthologID = $sth->fetchrow_array();
    $sth->finish();
    if (!$orthologID) {
      $orthologID = $ortholog->ID();
      my $qsymbol = $dbh->quote($symbol);
      # add to database
      $query = qq(INSERT INTO ortholog (orthologID,geneID,species,db,symbol,dbID,altID) VALUES ('$orthologID','$geneID','$species','$db',$qsymbol,'$dbID','$altID'));
      my $rows= $dbh->do ($query);
    }
    else { $ortholog->ID($orthologID) }
  }
  $ortholog->_decr_newID_count() if $ortholog->{'is_new'};
  $ortholog->{'is_new'} = undef;

  return $ortholog;
}

1;
