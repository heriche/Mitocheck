# Author: jkh1
# 2008-06-16
#

=head1 NAME

 Mitocheck::MSexperimentHandle

=head1 SYNOPSIS



=head1 DESCRIPTION

 Enables retrieval of Mitocheck mass spectrometry experiment objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::MSexperimentHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::MSexperiment;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new MS experiment object handle
 Returntype: Mitocheck::MSexperimentHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: experiment ID
 Description: Gets MS experiment with given ID
 Returntype: Mitocheck::MSexperiment object

=cut

sub get_by_id {

  my ($self,$ID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # check if experiment exists in database
  my $query = qq(SELECT COUNT(*) FROM MSexperiment WHERE experimentID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($ID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::MSexperiment($dbc,$ID);

}

=head2 get_by_name

 Arg: string, experiment name
 Description: Gets MS experiment with given name
 Returntype: Mitocheck::MSexperiment object

=cut

sub get_by_name {

  my ($self,$name) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $query = qq(SELECT experimentID FROM MSexperiment WHERE name = ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($name);
  my ($expID) = $sth->fetchrow_array();
  if (defined($expID)) {
    return new Mitocheck::MSexperiment($dbc,$expID);
  }
  else {
    return undef;
  }

}

=head2 get_all_experiments

 Arg: (optional) Mitocheck::Source or source ID
 Description: Gets all MS experiments (from given source if any)
 Returntype: list of Mitocheck::MSexperiment objects

=cut

sub get_all_experiments {

  my $self = shift;
  my $source = shift if @_;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my @experiments;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $query;
  if ($sourceID) {
    $query = qq(SELECT DISTINCT experimentID
                FROM MSexperiment
                WHERE sourceID='$sourceID');
  }
  else {
    $query = qq(SELECT DISTINCT experimentID FROM MSexperiment);
  }
  my $sth= $dbh->prepare($query);
  $sth->execute();
  while (my ($id) = $sth->fetchrow_array()) {
    push (@experiments,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @experiments;
}

=head2 get_all_by_source

 Arg: Mitocheck::Source or source ID
 Description: Gets all MS experiments from given source
 Returntype: list of Mitocheck::MSexperiment objects

=cut

sub get_all_by_source {

  my $self = shift;
  my $source = shift if @_;

  return $self->get_all_experiments($source);
}


=head2 get_all_by_protein

 Arg1: Mitocheck::Protein or protein ID
 Arg2: (optional) bait as Mitocheck::Protein or protein ID
 Description: Gets all MS experiments where the given protein was found,
              optionally limited to experiments using the given bait
 Returntype: list of Mitocheck::MSexperiment objects

=cut

sub get_all_by_protein {

  my ($self,$protein,$bait) = @_;
  $protein ||= "";
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  $bait ||= "";
  my $baitID = ref($bait) ? $bait->ID() : $bait;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  if ($baitID) {
    $sth = $dbh->prepare("SELECT DISTINCT po.experimentID
                          FROM peptide_occurrence po,peptide_map_info pmi
                          WHERE pmi.proteinID = ?
                          AND pmi.peptideID = po.peptideID
                          AND po.baitID = ?");
    $sth->execute($proteinID,$baitID);
  }
  else {
    $sth = $dbh->prepare("SELECT DISTINCT po.experimentID
                          FROM peptide_occurrence po,peptide_map_info pmi
                          WHERE pmi.proteinID = ?
                          AND pmi.peptideID = po.peptideID");
    $sth->execute($proteinID);
  }
  my @experiments;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@experiments,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @experiments;
}

=head2 get_all_by_bait

 Arg1: Mitocheck::Protein or protein ID
 Description: Gets all MS experiments that use the given bait protein
 Returntype: list of Mitocheck::MSexperiment objects

=cut

sub get_all_by_bait {

  my ($self,$bait) = @_;
  $bait ||= "";
  my $baitID = ref($bait) ? $bait->ID() : $bait;

  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $sth = $dbh->prepare("SELECT DISTINCT po.experimentID
                           FROM peptide_occurrence po
                           WHERE po.baitID = ?");
  $sth->execute($baitID);
  my @experiments;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@experiments,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @experiments;
}

=head2 get_all_by_gene

 Arg1: Mitocheck::Gene or gene ID
 Arg2: (optional) bait as Mitocheck::Protein or protein ID
 Description: Gets all MS experiments where the given gene was found by
              parsimonious assignment of peptides (plus experiments where
              the gene is used as bait),
              optionally limited to experiments using the given bait
 Returntype: list of Mitocheck::MSexperiment objects

=cut

sub get_all_by_gene {

  my ($self,$gene,$bait) = @_;
  $gene ||= "";
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  $bait ||= "";
  my $baitID = ref($bait) ? $bait->ID() : $bait;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  my %seen;
  # Get MS experiments where the given gene was found by
  # parsimonious assignment of peptides
  if ($baitID) {
    $sth = $dbh->prepare("SELECT DISTINCT pga.experimentID
                          FROM peptide_gene_assignment pga, peptide_occurrence po
                          WHERE pga.geneID = ?
                          AND pga.experimentID = po.experimentID
                          AND po.baitID = ?");
    $sth->execute($geneID,$baitID);
  }
  else {
    $sth = $dbh->prepare("SELECT DISTINCT experimentID
                          FROM peptide_gene_assignment
                          WHERE geneID = ?");
    $sth->execute($geneID);
  }
  my @experiments;
  while (my ($id) = $sth->fetchrow_array()) {
    if ($id) {
      push (@experiments,$self->get_by_id($id));
      $seen{$id}++;
    }
  }
  $sth->finish();

  # Add experiments where the gene is used as bait
  $sth = $dbh->prepare("SELECT DISTINCT experimentID
                        FROM peptide_occurrence po, transcript t
                        WHERE t.geneID = ?
                        AND t.proteinID = po.baitID");
  $sth->execute($geneID);
  while (my ($id) = $sth->fetchrow_array()) {
    if ($id && !$seen{$id}++) {
      push (@experiments,$self->get_by_id($id));
    }
  }
  $sth->finish();

  return @experiments;
}

=head2 get_all_by_cellular_component

 Arg1: Mitocheck::CellularComponent or cellular component ID
 Description: Gets all MS experiments from the given component
 Returntype: list of Mitocheck::MSexperiment objects

=cut

sub get_all_by_cellular_component {

  my ($self,$cc) = @_;
  $cc ||= "";
  my $ccID = ref($cc) ? $cc->ID() : $cc;

  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $sth = $dbh->prepare("SELECT DISTINCT experimentID
                           FROM MSexperiment
                           WHERE cellular_componentID = ?");
  $sth->execute($ccID);
  my @experiments;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@experiments,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @experiments;
}

1;
