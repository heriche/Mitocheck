# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::DBConnection

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 or to read configuration file:
 $dbc = Mitocheck::DBConnection->new(-file => config_file.txt);

 my $gene_handle = $dbc->get_GeneHandle();

 my $gene = $gene_handle->get_by_EnsemblID('ENSG00000178999');

=head1 DESCRIPTION

 Provides database connection and handles for Mitocheck objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::DBConnection;

use 5.8.0; # needs Perl version 5.8.0 or higher
use strict;
use warnings;
use Carp;
use DBI;
use Mitocheck::GeneHandle;
use Mitocheck::TranscriptHandle;
use Mitocheck::ProteinHandle;
use Mitocheck::PhenotypeHandle;
use Mitocheck::LocalizationHandle;
use Mitocheck::EventHandle;
use Mitocheck::dsRNAHandle;
use Mitocheck::ImageSetHandle;
use Mitocheck::ImageHandle;
use Mitocheck::OrthologHandle;
use Mitocheck::SourceHandle;
use Mitocheck::InteractionHandle;
use Mitocheck::PeptideHandle;
use Mitocheck::MSexperimentHandle;
use Mitocheck::ComplexHandle;
use Mitocheck::CellLineHandle;
use Mitocheck::SequenceTagHandle;
use Mitocheck::CellularComponentHandle;
use Mitocheck::FCSexperimentHandle;
use Mitocheck::MeasurementHandle;
use Mitocheck::DataFileHandle;
use Mitocheck::ReporterHandle;
use Mitocheck::Config;

=head2 new

 Arg: (optional) hash with database connection details using the following keys:
      -database, -host, -user, -password, -port, -file
 Description: Creates a new database connection object. If no arg is provided,
              uses defaults from Mitocheck::Config
 Returntype: Mitocheck::DBConnection object

=cut

sub new {

  my $class = shift;

  my %dbc = @_;

  if (!defined($dbc{'-database'})) {
    if (defined($dbc{'-file'}) && -e $dbc{'-file'}) {
      # use configuration file if provided
      unless (my $return = do $dbc{'-file'}) {
	croak "couldn't parse $dbc{'-file'}: $@" if $@;
	croak "couldn't do $dbc{'-file'}: $!"    unless defined $return;
	croak "couldn't run $dbc{'-file'}"       unless $return;
      }
    }
    $dbc{'-database'} = $MTCDBNAME;
    $dbc{'-host'}     = $MTCDBHOST;
    $dbc{'-user'}     = $MTCDBUSER;
    $dbc{'-password'} = $MTCDBPASS;
    $dbc{'-port'}     = $MTCDBPORT;
  }

  my $db = $dbc{'-database'};
  die "ERROR: No database selected\n" unless ($db);

  my $host = $dbc{'-host'};
  my $user_name = $dbc{'-user'};
  my $password = $dbc{'-password'} || "";
  my $port = $dbc{'-port'} || "";
  my $dsn="DBI:mysql:$db:$host:$port";

  my $self = {};
  bless ($self, $class);

  my $dbh;
  $SIG{ALRM} = sub { die "timeout"; };
  alarm(10); # time in seconds to wait
  eval {
    $dbh= DBI->connect ($dsn, $user_name, $password, {RaiseError=> 1, PrintError=> 0});
  };
  if (!$dbh || $@) {
    croak "Could not connect to database $db on host $host: $DBI::errstr\n";
  }
  alarm(0);
  $dbh->{'mysql_auto_reconnect'} = 1 if ($dbc{'-autoreconnect'});
  $self->{'database_handle'} = $dbh;
  $self->{'database'} = $db;
  # check that API and database versions match
  my $query = qq(SELECT * FROM dbinfo);
  my $sth = $dbh->prepare($query);
  $sth->execute();
  my ($version,$Ensembl,$date)=$sth->fetchrow_array();
  $sth->finish();
  $self->{'dbversion'} = $version if $version;
  unless ($version == $APIVERSION) {
    warn "\nWARNING: API version doesn't match database version for $db. Some things won't work.\n\n";
  }

  return $self;

}

sub get_API_version {

  my $self = shift;

  return $APIVERSION;
}

sub get_db_name {

  my $self = shift;

  return $self->{'database'};

}

sub get_db_version {

  my $self = shift;
  if (!defined($self->{'dbversion'})) {
    my $sth = $self->{'database_handle'}->prepare("SELECT version FROM dbinfo");
    $sth->execute();
    my ($version) = $sth->fetchrow_array();
    $self->{'dbversion'} = $version if $version;
  }

  return $self->{'dbversion'};
}

sub get_Ensembl_version {

  my $self = shift;
  if (!defined($self->{'Ensversion'})) {
    my $sth = $self->{'database_handle'}->prepare("SELECT Ensembl FROM dbinfo");
    $sth->execute();
    my ($version) = $sth->fetchrow_array();
    $self->{'Ensversion'} = $version if $version;
  }

  return $self->{'Ensversion'};
}

sub get_Ensembl_URL {

  my $self = shift;
  if (!defined($self->{'EnsURL'})) {
    my $sth = $self->{'database_handle'}->prepare("SELECT Ensembl_URL FROM dbinfo");
    $sth->execute();
    my ($url) = $sth->fetchrow_array();
    $self->{'EnsURL'} = $url if $url;
  }

  return $self->{'EnsURL'};
}

sub get_news {
  my $self = shift;
  my @news;
  my $sth = $self->{'database_handle'}->prepare("SELECT creation_date, title, content FROM news WHERE expired != 1");
  $sth->execute();
  while (my $item = $sth->fetchrow_arrayref()) {
    push @news,$item;
  }
  return @news;
}

=head2 get_DatabaseHandle

 Description: Gets the underlying DBI database handle object
 Returntype: DBI database handle object

=cut

sub get_DatabaseHandle {

  my $self = shift;
  $self->{'database_handle'} = shift if @_;
  return $self->{'database_handle'};

}

=head2 disconnect

 Description: Disconnects from the database
 Returntype: success/failure

=cut

sub disconnect {

  my $self = shift;
  $self->{'database_handle'}->disconnect() if(defined $self->{'database_handle'});
}

=head2 get_GeneHandle

 Description: Gets a handle for creating Gene objects
 Returntype: Mitocheck::GeneHandle

=cut

sub get_GeneHandle {

  my $self = shift;
  return new Mitocheck::GeneHandle($self);

}

=head2 get_TranscriptHandle

 Description: Gets a handle for creating Transcript objects
 Returntype: Mitocheck::TranscriptHandle

=cut

sub get_TranscriptHandle {

  my $self = shift;
  return new Mitocheck::TranscriptHandle($self);

}

=head2 get_ProteinHandle

 Description: Gets a handle for creating Protein objects
 Returntype: Mitocheck::ProteinHandle

=cut

sub get_ProteinHandle {

  my $self = shift;
  return new Mitocheck::ProteinHandle($self);

}

=head2 get_PhenotypeHandle

 Description: Gets a handle for creating Phenotype objects
 Returntype: Mitocheck::PhenotypeHandle

=cut

sub get_PhenotypeHandle {

  my $self = shift;
  return new Mitocheck::PhenotypeHandle($self);

}

=head2 get_LocalizationHandle

 Description: Gets a handle for creating Localization objects
 Returntype: Mitocheck::LocalizationHandle

=cut

sub get_LocalizationHandle {

  my $self = shift;
  return new Mitocheck::LocalizationHandle($self);

}

=head2 get_dsRNAHandle

 Description: Gets a handle for creating dsRNA objects
 Returntype: Mitocheck::dsRNAHandle

=cut

sub get_dsRNAHandle {

  my $self = shift;
  return new Mitocheck::dsRNAHandle($self);

}

=head2 get_ImageSetHandle

 Description: Gets a handle for creating ImageSet objects
 Returntype: Mitocheck::ImageSetHandle

=cut

sub get_ImageSetHandle {

  my $self = shift;
  return new Mitocheck::ImageSetHandle($self);

}

=head2 get_ImageHandle

 Description: Gets a handle for creating Image objects
 Returntype: Mitocheck::ImageHandle

=cut

sub get_ImageHandle {

  my $self = shift;
  return new Mitocheck::ImageHandle($self);

}

=head2 get_OrthologHandle

 Description: Gets a handle for creating Ortholog objects
 Returntype: Mitocheck::OrthologHandle

=cut

sub get_OrthologHandle {

  my $self = shift;
  return new Mitocheck::OrthologHandle($self);

}

=head2 get_SourceHandle

 Description: Gets a handle for creating Source objects
 Returntype: Mitocheck::SourceHandle

=cut

sub get_SourceHandle {

  my $self = shift;
  return new Mitocheck::SourceHandle($self);

}

=head2 get_InteractionHandle

 Description: Gets a handle for creating Interaction objects
 Returntype: Mitocheck::InteractionHandle

=cut

sub get_InteractionHandle {

  my $self = shift;
  return new Mitocheck::InteractionHandle($self);

}

=head2 get_PeptideHandle

 Description: Gets a handle for creating Peptide objects
 Returntype: Mitocheck::PeptideHandle

=cut

sub get_PeptideHandle {

  my $self = shift;
  return new Mitocheck::PeptideHandle($self);

}

=head2 get_MSexperimentHandle

 Description: Gets a handle for creating Interaction objects
 Returntype: Mitocheck::MSexperimentHandle

=cut

sub get_MSexperimentHandle {

  my $self = shift;
  return new Mitocheck::MSexperimentHandle($self);

}

=head2 get_ComplexHandle

 Description: Gets a handle for creating Complex objects
 Returntype: Mitocheck::ComplexHandle

=cut

sub get_ComplexHandle {

  my $self = shift;
  return new Mitocheck::ComplexHandle($self);

}

=head2 get_CellLineHandle

 Description: Gets a handle for creating CellLine objects
 Returntype: Mitocheck::CellLineHandle

=cut

sub get_CellLineHandle {

  my $self = shift;
  return new Mitocheck::CellLineHandle($self);

}

=head2 get_SequenceTagHandle

 Description: Gets a handle for creating SequenceTag objects
 Returntype: Mitocheck::SequenceTagHandle

=cut

sub get_SequenceTagHandle {

  my $self = shift;
  return new Mitocheck::SequenceTagHandle($self);

}

=head2 get_EventHandle

 Description: Gets a handle for creating Event objects
 Returntype: Mitocheck::EventHandle

=cut

sub get_EventHandle {

  my $self = shift;
  return new Mitocheck::EventHandle($self);

}

=head2 get_CellularComponentHandle

 Description: Gets a handle for creating CellularComponent objects
 Returntype: Mitocheck::CellularComponentHandle

=cut

sub get_CellularComponentHandle {

  my $self = shift;
  return new Mitocheck::CellularComponentHandle($self);

}

=head2 get_FCSexperimentHandle

 Description: Gets a handle for creating FCSexperiment objects
 Returntype: Mitocheck::FCSexperimentHandle

=cut

sub get_FCSexperimentHandle {

  my $self = shift;
  return new Mitocheck::FCSexperimentHandle($self);

}

=head2 get_MeasurementHandle

 Description: Gets a handle for creating Measurement objects
 Returntype: Mitocheck::MeasurementHandle

=cut

sub get_MeasurementHandle {

  my $self = shift;
  return new Mitocheck::MeasurementHandle($self);

}

=head2 get_DataFileHandle

 Description: Gets a handle for creating DataFile objects
 Returntype: Mitocheck::DataFileHandle

=cut

sub get_DataFileHandle {

  my $self = shift;
  return new Mitocheck::DataFileHandle($self);

}

=head2 get_ReporterHandle

 Description: Gets a handle for creating Reporter objects
 Returntype: Mitocheck::ReporterHandle

=cut

sub get_ReporterHandle {

  my $self = shift;
  return new Mitocheck::ReporterHandle($self);

}

sub DESTROY {
  my $self = shift;
  $self->{'database_handle'}->disconnect() if (defined $self->{'database_handle'});
}



1;
