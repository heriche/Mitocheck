# Author: jkh1
# 2005-06-13
#

=head1 NAME

 Mitocheck::SourceHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $source_handle = $dbc->get_SourceHandle();

 my $source = $source_handle->get_by_id('SRC00000001');

=head1 DESCRIPTION

 Enables retrieval of Mitocheck source objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::SourceHandle;


use strict;
use warnings;
use Mitocheck::Source;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new source object handle
 Returntype: Mitocheck::SourceHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: source ID
 Description: Gets source with given ID
 Returntype: Mitocheck::Source object

=cut

sub get_by_id {

  my ($self,$sourceID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if source exists in database
  my $query = qq(SELECT COUNT(*) FROM source WHERE sourceID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($sourceID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }
  return new Mitocheck::Source($dbc,$sourceID);

}

=head2 get_by_name

 Arg: source's name
 Description: Gets source with given name
 Returntype: Mitocheck::Source object

=cut

sub get_by_name {

  my ($self,$name) = @_;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT s.sourceID FROM source s
                           WHERE s.name=?");
  $sth->execute($name);
  my ($sourceID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $sourceID );

  return $self->get_by_id($sourceID);

}

=head2 new_source

 Arg: none
 Description: creates a new source object
 Returntype: Mitocheck::Source

=cut

sub new_source {

  my $self = shift;
  my $dbc = $self->{'DBConnection'};

  return new Mitocheck::Source($dbc);

}

=head2 get_all_screens

 Arg: none
 Description: geta a list of all screens stored in the database
 Returntype: list of Mitocheck::Source objects

=cut

sub get_all_screens {

  my $self = shift;
  my $dbh = $self->{'database_handle'};
  my @screens;
  # Currently the only way to identify a screen is by name
  my $query = qq(SELECT sourceID FROM source WHERE name LIKE '%screen%');
  my $sth= $dbh->prepare($query);
  $sth->execute();
  while (my $id=$sth->fetchrow_array()) {
    push (@screens,$self->get_by_id($id));
  }
  $sth->finish();

  return @screens;

}


1;
