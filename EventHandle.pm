# Author: jkh1
# 2011-05-10
#

=head1 NAME

 Mitocheck::EventHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $event_handle = $dbc->get_EventHandle();

 my $event = $event_handle->get_by_id('EV00000034');


=head1 DESCRIPTION

 Enables retrieval of Mitocheck event objects. An event represents any
 kind of time information e.g. anaphase or 30 h post-transfection

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::EventHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Event;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new Event object handle.
 Returntype: Mitocheck::EventHandle

=cut

sub new {

  my ($class,$dbc) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;

  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: event ID
 Description: Gets event with given ID
 Returntype: Mitocheck::Event object

=cut

sub get_by_id {

  my ($self,$eventID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if event exists in database
  my $query = qq(SELECT COUNT(*) FROM event WHERE eventID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($eventID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::Event->new($dbc,$eventID);

}

=head2 get_by_description

 Arg: string, event description
 Description: Gets event with given description
 Returntype: Mitocheck::Event object

=cut

sub get_by_description {

  my ($self,$description) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT eventID FROM event
                 WHERE description = ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($description);
  my ($eventID) = $sth->fetchrow_array();
  return undef unless $eventID;

  return Mitocheck::Event->new($dbc,$eventID);

}

=head2 get_by_GOID

 Arg: event GO ID
 Description: Gets cellular component with given GO ID
 Returntype: Mitocheck::Event object

=cut

sub get_by_GOID {

  my ($self,$ID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT eventID FROM event WHERE GOID= ?);
  my $sth= $dbh->prepare($query);
  $sth->execute($ID);
  my ($eID) = $sth->fetchrow_array();

  return undef if (!defined $eID);

  return $self->get_by_id($eID);

}
=head2 new_event

 Arg (required): -description => text
 Description: creates a new event object
 Returntype: Mitocheck::Event

=cut

sub new_event {

  my $self = shift;
  my %event = @_;
  # required attributes = ('-description');
  unless (defined($event{'-description'})) {
    croak "WARNING: Event requires at least the following attributes: -description";
  }
  my $dbc = $self->{'DBConnection'};

  my $event = Mitocheck::Event->new($dbc);
  $event->description($event{'-description'});
  $event->sequence_name($event{'-sequence_name'});
  $event->order_in_sequence($event{'-order_in_sequence'});
  $event->GOID($event{'-GOID'});
  $event->GOterm($event{'-GOterm'});

  return $event;

}

=head2 store

 Arg: Mitocheck::Event
 Description: Enters event and associated data in Mitocheck
              database
 Returntype: Mitocheck::Event, normally same as Arg

=cut

sub store {

  my ($self,$event) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  # Check if event already in database
  # two events are the same if they have
  # same description
  my $query = qq(SELECT eventID
                 FROM event
                 WHERE description= ?);
  my $sth = $dbh->prepare($query);
  $sth->execute($event->description);
  my ($evID) = $sth->fetchrow_array();
  $sth->finish();
  if ($evID) {
    $event->ID($evID);
  }
  else {
    my $evID = $event->ID;
    my $desc = $event->description;
    my $seq_name = $event->sequence_name || '';
    my $order = $event->order_in_sequence || '';
    my $GOID = $event->GOID || '';
    my $GOterm = $event->GOterm || '';
    my $query = qq{INSERT INTO event
           (eventID,description,sequence_name,order_in_sequence,GOID,GOterm)
    VALUES ('$evID','$desc',$seq_name','$order','$GOID','$GOterm')};
    my $rows = $dbh->do($query);
  }
  $event->_decr_newID_count() if $event->{'is_new'};
  $event->{'is_new'} = undef;

  return $event;
}

1;

