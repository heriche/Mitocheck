# Author: jkh1
# 2012-09-21
#

=head1 NAME

 Mitocheck::FCSexperiment

=head1 SYNOPSIS


=head1 DESCRIPTION

 Representation of a Mitocheck fluorescence spectroscopy experiment

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::FCSexperiment;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_exp_count = 0;
  my $_newID_count = 0;

  sub get_exp_count {
    return $_exp_count;
  }
  sub _incr_exp_count {
    return ++$_exp_count;
  }
  sub _decr_exp_count {
    return --$_exp_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }

}



=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, FCS experiment ID
 Description: Creates a new FCS experiment object.
 Returntype: Mitocheck::FCSexperiment

=cut

sub new {

  my ($class,$dbc,$ID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbc->{'database_handle'};
  $class->_incr_exp_count();
  if (!defined $self->{'ID'} && !$ID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   experimentID
                 FROM     FCSexperiment
                 ORDER BY experimentID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $ID = "FCS"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;

  }
  $self->{'ID'} = $ID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, FCS experiment ID
 Description: Gets/sets FCS experiment ID
 Returntype: integer

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 image_sets

 Arg: optional,list of Mitocheck::ImageSet objects
 Description: Gets/sets list of example images for the FCS experiment
 Returntype: list of Mitocheck::ImageSet

=cut

sub image_sets {

  my ($self,@sets) = @_;
  @{$self->{'image_sets'}} = @sets if @sets;
  if (!defined $self->{'image_sets'}) {
    my $dbc = $self->{'DBConnection'};
    my $ish = $dbc->get_ImageSetHandle();
    @{$self->{'image_sets'}} = $ish->get_all_by_FCSexperiment($self);
  }
  return @{$self->{'image_sets'}} if (defined($self->{'image_sets'}));
}

=head2 data_files

 Arg: optional,list of Mitocheck::DataFile objects
 Description: Gets/sets list of files associated with the FCS experiment
 Returntype: list of Mitocheck::DataFile

=cut

sub data_files {

  my ($self,@files) = @_;
  @{$self->{'data_files'}} = @files if @files;
  if (!defined $self->{'data_files'}) {
    my $dbc = $self->{'DBConnection'};
    my $dfh = $dbc->get_DataFileHandle();
    @{$self->{'data_files'}} = $dfh->get_all_by_FCSexperiment($self);
  }
  return @{$self->{'data_files'}} if (defined($self->{'data_files'}));
}

=head2 cell_line

 Arg: optional, Mitocheck::CellLine
 Description: Gets/sets cell line used in the experiment
 Returntype: Mitocheck::CellLine

=cut

sub cell_line {

  my ($self,$cell_line) = @_;
  $self->{'cell_line'} = $cell_line if defined($cell_line);
  if(!defined($self->{'cell_line'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT cell_lineID
                             FROM FCSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->{'ID'});
    my ($cell_lineID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $clh = $dbc->get_CellLineHandle();
    $self->{'cell_line'} = $clh->get_by_id($cell_lineID);
  }
  return $self->{'cell_line'};
}

=head2 source

 Arg: optional, Mitocheck::Source
 Description: Gets/sets source of the experiment
 Returntype: list of Mitocheck::Source

=cut

sub source {

  my ($self,$source) = @_;
  $self->{'source'} = $source if defined($source);
  if(!defined($self->{'source'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sourceID
                             FROM FCSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->{'ID'});
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};
}

=head2 sequence_tags

 Arg: optional, list of Mitocheck::SequenceTag
 Description: Gets/sets sequence tags identifying proteins studied in the FCS experiment
 Returntype: list of Mitocheck::SequenceTag

=cut

sub sequence_tags {

  my ($self,@seq_tags) = @_;
  @{$self->{'sequence_tags'}} = @seq_tags if @seq_tags;
  if (!defined $self->{'sequence_tags'}) {
    my $FCSexperimentID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $seqth = $dbc->get_SequenceTagHandle;
    my $dbh = $self->{'database_handle'};
    my $query = qq(SELECT sequence_tagID1,sequence_tagID2
                   FROM FCSexperiment
                   WHERE experimentID  = ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($FCSexperimentID);
    while (my @row = $sth->fetchrow_array()) {
	  foreach my $i(0..$#row) {
	  	if (defined($row[$i])) {
	      my $seq_tag = $seqth->get_by_id($row[$i]);
	      push @{$self->{'sequence_tags'}},$seq_tag;
	  	}
	  }
    }
  }
  
  return @{$self->{'sequence_tags'}};
}

=head2 measurements

 Arg: optional, list of Mitocheck::Measurement
 Description: Gets/sets measurements made in the FCS experiment
 Returntype: list of Mitocheck::Measurement

=cut

sub measurements {

  my ($self,@measurements) = @_;
  @{$self->{'measurements'}} = @measurements if @measurements;
  if (!defined $self->{'measurements'}) {
    my $FCSexperimentID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $mh = $dbc->get_MeasurementHandle;
    my $dbh = $self->{'database_handle'};
    my $query = qq(SELECT DISTINCT measurementID
                   FROM measurement
                   WHERE FCSexperimentID  = ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($FCSexperimentID);
    while (my ($id) = $sth->fetchrow_array()) {
	  if (defined($id)) {
	    my $measurement = $mh->get_by_id($id);
	    push @{$self->{'measurements'}},$measurement if $measurement;
	  }
    }
  }
  
  return @{$self->{'measurements'}} if ($self->{'measurements'});
}

=head2 get_localizations

 Arg: none
 Description: Gets localization occurrences corresponding to measurements
              made in this experiment
 Returntype: list of Mitocheck::Localization objects

=cut

sub get_localizations {

  my $self = shift;
  if (!defined($self->{'localizations'})) {
  	my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $loch = $dbc->get_LocalizationHandle();
    my $sth = $dbh->prepare("SELECT DISTINCT lohm.localization_occurrenceID
                             FROM localization_occurrence_has_measurement lohm,
                                  measurement m
                             WHERE m.FCSexperimentID = ? 
                             AND m.measurementID = lohm.measurementID");
    $sth->execute($self->{'ID'});
    while (my ($locID) = $sth->fetchrow_array()) {
      my $loc = $loch->get_by_id($locID) if ($locID);
      push @{$self->{'localizations'}}, $loc if ($loc);
    }
  }
  return @{$self->{'localizations'}} if ($self->{'localizations'});
}

sub DESTROY {

  my $self = shift;
  $self->_decr_exp_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
