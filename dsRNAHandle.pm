# Author: jkh1
# 2005-05-31
#

=head1 NAME

 Mitocheck::dsRNAHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $dsRNA_handle = $dbc->get_dsRNAHandle();

 my $dsRNA = $dsRNA_handle->get_by_id('MCO_0000001');

=head1 DESCRIPTION

 Enables retrieval of Mitocheck dsRNA objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::dsRNAHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::dsRNA;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new dsRNA object handle
 Returntype: Mitocheck::dsRNAHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: dsRNA ID
 Description: Gets dsRNA with given ID
 Returntype: Mitocheck::dsRNA object

=cut

sub get_by_id {

  my ($self,$dsRNAID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # check if dsRNA exists in database
  my $query = qq(SELECT COUNT(*) FROM oligo_pair WHERE oligo_pairID= ?);
  my $sth= $dbh->prepare($query);
  $sth->execute($dsRNAID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::dsRNA($dbc,$dsRNAID);

}

=head2 get_by_external_id

 Arg: dsRNA ID
 Description: Gets dsRNA with given ID
 Returntype: Mitocheck::dsRNA object

=cut

sub get_by_external_id {

  my ($self,$dsRNAID) = @_;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $query = qq(SELECT oligo_pairID FROM oligo_pair WHERE external_ID= ?);
  my $sth= $dbh->prepare($query);
  $sth->execute($dsRNAID);
  my ($ID) = $sth->fetchrow_array();

  return undef if (!defined $ID);

  return $self->get_by_id($ID);

}


=head2 get_all_by_transcript

 Arg: Mitocheck::Transcript or transcript ID
 Description: Gets all dsRNAs that map to given transcript
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub get_all_by_transcript {

  my ($self,$transcript) = @_;
  my $transcriptID = ref($transcript) ? $transcript->ID(): $transcript;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $sth = $dbh->prepare("SELECT drmi.oligo_pairID
                           FROM dsRNA_map_info drmi
                           WHERE drmi.transcriptID = ? ");
  $sth->execute($transcriptID);
  my @dsRNAs;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@dsRNAs,$self->get_by_id($id));
  }
  $sth->finish();

  return @dsRNAs;

}

=head2 get_all_by_gene

 Arg1: Mitocheck::Gene or gene ID
 Arg2: (optional) Mitocheck::Source or source ID
 Description: Gets all dsRNAs that map to given gene.
              If a source is specified, limits to dsRNAs used
              in the corresponding screen/experiment
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub get_all_by_gene {

  my ($self,$gene,$source) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $sth;
  unless ($sourceID) {
    $sth = $dbh->prepare("SELECT DISTINCT drmi.oligo_pairID
                          FROM dsRNA_map_info drmi
                          WHERE drmi.geneID = ? ");
  }
  else {
    # assumes there is at least one image set per dsRNA in the screen
    $sth = $dbh->prepare("SELECT DISTINCT drmi.oligo_pairID
                          FROM dsRNA_map_info drmi, dsRNA_has_image_set dhis, image_set ims
                          WHERE drmi.geneID = ?
                          AND drmi.oligo_pairID = dhis.oligo_pairID
                          AND dhis.image_setID = ims.image_setID
                          AND ims.sourceID = '$sourceID' ");
  }
  $sth->execute($geneID);
  my @dsRNAs;
  while (my ($id)=$sth->fetchrow_array()) {
    my $dsRNA = $self->get_by_id($id);
    push (@dsRNAs,$dsRNA) if (defined($dsRNA));
  }
  $sth->finish();

  return @dsRNAs;

}

=head2 get_by_supplier

 Arg: optional, Mitocheck::Source or source ID
 Description: Gets all dsRNAs from the given supplier source object.
              If no source is specified, gets all dsRNAs in the database.
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub get_by_supplier {

  my ($self,$source) = @_;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $query;
  if ($sourceID) {
    $query = qq(SELECT DISTINCT oligo_pairID
                FROM oligo_pair
                WHERE sourceID = '$sourceID');
  }
  else {
    $query = qq(SELECT DISTINCT oligo_pairID FROM oligo_pair);
  }
  my $sth = $dbh->prepare($query);
  $sth->execute();
  my @dsRNAs;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@dsRNAs,$self->get_by_id($id));
  }
  $sth->finish();

  return @dsRNAs;
}


=head2 get_library

 Arg: optional, Mitocheck::Source or source ID
 Description: Gets all dsRNAs from the given experiment/screen source object.
              If no source is specified, gets all dsRNAs in the database.
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub get_library {

  my ($self,$source) = @_;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $query;
  if ($sourceID) {
    # assume there is at least one image set per dsRNA in the screen
    # currently, if a dsRNA has no image set assume it hasn't been used
    $query = qq(SELECT DISTINCT oligo_pairID
                FROM image_set
                WHERE sourceID = '$sourceID');
  }
  else {
    $query = qq(SELECT DISTINCT oligo_pairID FROM oligo_pair);
  }
  my $sth = $dbh->prepare($query);
  $sth->execute();
  my @dsRNAs;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@dsRNAs,$self->get_by_id($id));
  }
  $sth->finish();

  return @dsRNAs;
}

=head2 get_all_by_antisense

 Arg1: string, a sequence
 Description: Gets all dsRNAs that have the given antisense oligo.
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub get_all_by_antisense {

  my ($self,$seq) = @_;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $sth;
  $sth = $dbh->prepare("SELECT DISTINCT oligo_pairID
                        FROM oligo_pair
                        WHERE seq_oligo2 = ?");
  $sth->execute($seq);
  my @dsRNAs;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@dsRNAs,$self->get_by_id($id));
  }
  $sth->finish();

  return @dsRNAs;

}

=head2 get_all_by_sense

 Arg1: string, a sequence
 Description: Gets all dsRNAs that have the given sense oligo.
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub get_all_by_sense {

  my ($self,$seq) = @_;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $sth;
  $sth = $dbh->prepare("SELECT DISTINCT oligo_pairID
                        FROM oligo_pair
                        WHERE seq_oligo1 = ?");
  $sth->execute($seq);
  my @dsRNAs;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@dsRNAs,$self->get_by_id($id));
  }
  $sth->finish();

  return @dsRNAs;

}

=head2 new_dsRNA

 Arg (required): -seq1=> sense sequence
 Arg (required): -seq2=> anti-sense sequence
 Arg (required): -supplier=> Mitocheck::Source object
 Description: creates a new dsRNA object
 Returntype: Mitocheck::dsRNA

=cut

sub new_dsRNA {

  my $self = shift;
  my %dsRNA = @_;
  unless ($dsRNA{'-seq1'} && $dsRNA{'-seq2'}) {
    croak "Both sense and anti-sense sequences are required to create a new dsRNA.";
  }
  unless (defined($dsRNA{'-supplier'})) {
    croak "Supplier attribute is required to create a new dsRNA.";
  }
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $dsRNA = new Mitocheck::dsRNA($dbc);
  $dsRNA->sense_sequence($dsRNA{'-seq1'});
  $dsRNA->antisense_sequence($dsRNA{'-seq2'});
  $dsRNA->external_ID($dsRNA{'-ext_ID'});
  $dsRNA->supplier($dsRNA{'-supplier'});
  $dsRNA->intended_target_ID($dsRNA{'-intended_target_ID'});

  return $dsRNA;

}

=head2 store

 Arg: Mitocheck::dsRNA
 Description: Enters dsRNA and associated mapping data in Mitocheck
              database.
 Returntype: Mitocheck::dsRNA, same as Arg

=cut

sub store {

  my ($self,$dsRNA) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $dsRNAID;
  my $seq1 = $dsRNA->sense_sequence();
  my $seq2 = $dsRNA->antisense_sequence();
  unless ($seq2) {
    croak "Anti-sense sequence required. Can't store dsRNA in database";
  }
  unless ($seq1) {
    croak "Sense sequence required. Can't store dsRNA in database";
  }
  my $ext_ID = $dsRNA->external_ID();
  my $sourceID = $dsRNA->supplier->ID if ($dsRNA->supplier);
  unless ($sourceID) {
    croak "Supplier (source) required. Can't store dsRNA in database";
  }
  my $intended_targetID = $dsRNA->intended_target_ID || '';
  # check if dsRNA already in database using antisense sequence
  my $external_ID;
  my $query=qq(SELECT oligo_pairID,external_ID
               FROM   oligo_pair
               WHERE  seq_oligo2='$seq2'
               AND sourceID='$sourceID'
               AND external_ID = '$ext_ID');
  my $sth= $dbh->prepare($query);
  $sth->execute();
  ($dsRNAID,$external_ID)=$sth->fetchrow_array();
  $sth->finish();
  if (!$dsRNAID) {
    $dsRNAID = $dsRNA->ID();
    # add to database
    $query=qq(INSERT INTO oligo_pair (oligo_pairID,seq_oligo1,seq_oligo2,external_ID,sourceID,intended_target) VALUES ('$dsRNAID','$seq1','$seq2','$ext_ID','$sourceID','$intended_targetID'));
    my $rows= $dbh->do ($query);
  }
  else {
    if ($ext_ID ne $external_ID) {
      carp "WARNING: dsRNA sequence (external ID: $ext_ID) already in database (ID: $dsRNAID, external ID: $external_ID)\n";
    }
    $dsRNA->ID($dsRNAID);
  }
  # add mapping information if any
  if ($dsRNA->{'mapping_info'}) {
    foreach my $transcript ($dsRNA->target_transcripts()) {
      my $transcriptID = $transcript->ID();
      my $geneID = $transcript->gene->ID();
      my $proteinID ="";
      if ($transcript->protein()) {
	    $proteinID = $transcript->protein()->ID();
      }
      my ($chromosome,$start1,$end1,$start2,$end2) = @{$dsRNA->{'coordinates'}{$transcriptID}};
      $start2 ||="";
      $end2 ||="";
      my $mismatch = $dsRNA->mismatch($transcript);
      my $alignment = $dsRNA->alignment($transcript);
      my $query = qq(INSERT IGNORE INTO dsRNA_map_info (geneID,oligo_pairID,transcriptID,proteinID,chromosome,start1,end1,start2,end2,mismatch,alignment) VALUES('$geneID','$dsRNAID','$transcriptID','$proteinID','$chromosome','$start1','$end1','$start2','$end2','$mismatch','$alignment'));
      my $rows= $dbh->do($query);
    }
  }
  if ($dsRNA->{'image_sets'}) {
    my $ish = $dbc->get_ImageSetHandle();
  	foreach my $set(@{$dsRNA->{'image_sets'}}) {
  	  $ish->store($set);
  	  my $setID = $set->ID;
  	  my $query = qq(INSERT INTO dsRNA_has_image_set (oligo_pairID,image_setID)
  	                 VALUES ('$dsRNAID','$setID'));
  	  my $rows = $dbh->do($query);
  	}
  }
  $dsRNA->_decr_newID_count() if $dsRNA->{'is_new'};
  $dsRNA->{'is_new'} = undef;

  return $dsRNA;
}

1;
