# Author: jkh1
# 2005-05-31
#

=head1 NAME

 Mitocheck::dsRNA

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $dsRNA_handle = $dbc->get_dsRNAHandle();

 my $dsRNA = $dsRNA_handle->get_by_id('DSR00000001');

 my $external_id = $dsRNA->external_ID();
 my $sense_seq = $dsRNA->sense_sequence();
 my $antisense_seq = $dsRNA->antisense_sequence();
 my @phenotypes = $dsRNA->phenotypes();
 my @image_sets = $dsRNA->image_sets();
 my @target_transcripts = $dsRNA->target_transcripts();
 my @target_genes = $dsRNA->target_genes();
 $dsRNA->mapping_info(-transcript => $transcriptID,
		      -mismatch => $mismatch,
		      -alignment => $alignement,
		      -chromosome => $chromosome,
		      -start1 => $sorted_coords[0],
		      -end1 => $sorted_coords[1],
		      -start2 => $sorted_coords[2],
		      -end2 => $sorted_coords[3]);


=head1 DESCRIPTION

 Representation of a Mitocheck dsRNA.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::dsRNA;


use strict;
use warnings;
use Carp;
use Scalar::Util qw(weaken);

{
  my $_dsRNA_count = 0;
  my $_newID_count = 0;

  sub get_dsRNA_count {
    return $_dsRNA_count;
  }
  sub _incr_dsRNA_count {
    return ++$_dsRNA_count;
  }
  sub _decr_dsRNA_count {
    return --$_dsRNA_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, dsRNA ID
 Description: Creates a new dsRNA object, creates Mitocheck ID if none given.
 Returntype: Mitocheck::dsRNA

=cut

sub new {

  my ($class,$dbc,$dsRNAID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_dsRNA_count();
  if (!defined $self->{'ID'} && !$dsRNAID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT oligo_pairID
                 FROM oligo_pair
                 ORDER BY oligo_pairID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }

    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $dsRNAID="DSR"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $dsRNAID;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck dsRNA ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 external_ID

 Arg: optional, dsRNA external ID
 Description: Gets/sets external ID
 Returntype: string

=cut

sub external_ID {

  my $self = shift;
  $self->{'external_ID'} = shift if @_;
  if (!defined $self->{'external_ID'}) {
    my $dsRNAID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT op.external_ID FROM oligo_pair op
                             WHERE op.oligo_pairID = ? ");
    $sth->execute($dsRNAID);
    ($self->{'external_ID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'external_ID'};

}

=head2 sense_sequence

 Arg: optional, sequence
 Description: Gets/sets dsRNA sense sequence
 Returntype: string

=cut

sub sense_sequence {

  my $self = shift;
  $self->{'sense_sequence'} = shift if @_;
  if (!defined $self->{'sense_sequence'}) {
    my $dsRNAID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT op.seq_oligo1 FROM oligo_pair op
                             WHERE op.oligo_pairID = ? ");
    $sth->execute($dsRNAID);
    ($self->{'sense_sequence'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'sense_sequence'};

}

=head2 antisense_sequence

 Arg: optional, sequence
 Description: Gets/sets dsRNA antisense sequence
 Returntype: string

=cut


sub antisense_sequence {

  my $self = shift;
  $self->{'antisense_sequence'} = shift if @_;
  if (!defined $self->{'antisense_sequence'}) {
    my $dsRNAID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT op.seq_oligo2 FROM oligo_pair op
                             WHERE op.oligo_pairID = ? ");
    $sth->execute($dsRNAID);
    ($self->{'antisense_sequence'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'antisense_sequence'};

}

=head2 image_sets

 Arg: optional, list of Mitocheck::ImageSet objects
 Description: Gets/sets image sets associated with a dsRNA
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub image_sets {

  my ($self,@sets) = @_;
  @{$self->{'image_sets'}} = @sets if @sets;
  if (!defined $self->{'image_sets'}) {
    my $dbc = $self->{'DBConnection'};
    my $ish = $dbc->get_ImageSetHandle();
    @{$self->{'image_sets'}} = $ish->get_all_by_dsRNA($self);
  }

  return @{$self->{'image_sets'}} if (defined($self->{'image_sets'}));

}

=head2 target_genes

 Arg: (optional), integer, maximum number of mismatches
 Description: Gets genes targeted by this dsRNA with less than the
              specified number of mismatches
 Returntype: list of Mitocheck::Gene objects

=cut

sub target_genes {

  my $self = shift;
  my $mismatch = shift if @_;
  my $dbc = $self->{'DBConnection'};
  @{$self->{'target_genes'}} = ();
  my $gh = $dbc->get_GeneHandle();
  @{$self->{'target_genes'}} = $gh->get_all_by_dsRNA($self);
  if (defined($mismatch)) {
   @{$self->{'target_genes'}} = grep { $self->mismatch($_)<=$mismatch } @{$self->{'target_genes'}}
  }

  return @{$self->{'target_genes'}};

}

=head2 target_transcripts

 Arg: optional, list of Mitocheck::Transcript objects
 Description: Gets/sets transcripts targeted by dsRNA
 Returntype: list of Mitocheck::Transcript objects

=cut

sub target_transcripts {

  my ($self,@transcripts) = @_;
  push (@{$self->{'target_transcripts'}},@transcripts) if @transcripts;
  if (!defined $self->{'target_transcripts'}) {
    my $dbc = $self->{'DBConnection'};
    my $th = $dbc->get_TranscriptHandle();
    @{$self->{'target_transcripts'}} = $th->get_all_by_dsRNA($self);
  }
  return @{$self->{'target_transcripts'}} if (defined($self->{'target_transcripts'}));

}

=head2 phenotypes

 Arg: optional, list of Mitocheck::Phenotype objects
 Description: Gets/sets phenotypes associated with dsRNA
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub phenotypes {

  my ($self,@phenotypes) = @_;
  @{$self->{'phenotypes'}} = @phenotypes if @phenotypes;
  if (!defined $self->{'phenotypes'}) {
    my $dbc = $self->{'DBConnection'};
    my $ph = $dbc->get_PhenotypeHandle();
    @{$self->{'phenotypes'}} = $ph->get_all_by_dsRNA($self);
  }

  return @{$self->{'phenotypes'}} if (defined($self->{'phenotypes'}));

}

=head2 mismatch

 Arg1: Mitocheck::Gene or Mitocheck::Transcript or gene ID or
       transcript ID
 Arg2: optional, number of mismatches
 Description: Gets/sets number of mismatches between dsRNA and
              given transcript. If Arg1 is a gene, returns the
              maximum value of all transcripts for that gene.
 Returntype: integer

=cut

sub mismatch {

  my ($self,$target,$mismatch) = @_;
  my $targetID = ref($target) ? $target->ID(): $target;
  $self->{'mismatch'}{$targetID} = $mismatch if defined $mismatch;
  if (!defined $self->{'mismatch'}{$targetID}) {
    my $dsRNAID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $query;
    if ($targetID=~/^TRN\d{7}/i) {
      $query = "SELECT drmi.mismatch FROM dsRNA_map_info drmi
                WHERE drmi.oligo_pairID = ?
                AND drmi.transcriptID = ?
                ORDER BY drmi.mismatch";
    }
    elsif ($targetID=~/^GEN\d{7}/i) {
      # keep the lowest value per transcript but the highest for the gene
      $query = "SELECT MIN(mismatch)
                FROM dsRNA_map_info
                WHERE oligo_pairID = ?
                AND geneID = ?
                GROUP BY transcriptID
                ORDER BY mismatch
                DESC
                LIMIT 1";
    }
    else { return undef }

    my $sth = $dbh->prepare($query);
    $sth->execute($dsRNAID,$targetID);
    ($self->{'mismatch'}{$targetID}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'mismatch'}{$targetID};

}

=head2 alignment

 Arg1: Mitocheck::Transcript or transcript ID
 Arg2: optional, preformated alignment
 Description: Gets/sets alignment of dsRNA to transcript
 Returntype: string

=cut

sub alignment {

  my ($self,$target,$alignment) = @_;
  my $targetID = ref($target) ? $target->ID(): $target;
  $self->{'alignment'}{$targetID} = $alignment if $alignment;
  if (!defined $self->{'alignment'}{$targetID}) {
    my $dsRNAID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $query;
    if ($targetID=~/^TRN\d{7}/i) {
      $query = "SELECT drmi.alignment FROM dsRNA_map_info drmi
                WHERE drmi.oligo_pairID = ?
                AND drmi.transcriptID = ?
                ORDER BY drmi.mismatch";
    }
    else { return undef }

    my $sth = $dbh->prepare($query);
    $sth->execute($dsRNAID,$targetID);
    ($self->{'alignment'}{$targetID}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'alignment'}{$targetID};

}

=head2 intended_target_ID

 Arg: optional, a gene identifier
 Description: Gets/sets ID of the gene the dsRNA is supposed
              to target
 Returntype: string

=cut

sub intended_target_ID {

  my ($self,$target) = @_;
  $self->{'intended_target'} = $target if $target;
  if (!defined $self->{'intended_target'}) {
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $ID = $self->{'ID'};
    my $query = qq(SELECT intended_target
                   FROM oligo_pair
                   WHERE oligo_pairID= ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($ID);
    ($self->{'intended_target'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'intended_target'};
}

=head2 mapping_info

 Args: key=>value pairs
 Description: Sets mapping information for dsRNA
 Returntype: 1

=cut

sub mapping_info {

  my $self = shift;
  my %mapping = @_;
  my $transcript = $mapping{'-transcript'};
  unless (ref ($transcript) eq 'Mitocheck::Transcript') {
    my $dbc = $self->{'DBConnection'};
    my $th = $dbc->get_TranscriptHandle();
    if ($transcript=~/^ENST\d{11}/) {
      $transcript = $th->get_by_EnsemblID($transcript);
    }
    elsif ($transcript=~/^TRN\d{7}/) {
      $transcript = $th->get_by_id($transcript);
    }
    else {
      croak "Error: $transcript is not a valid transcript. You must provide either a Mitocheck::Transcript object or a Mitocheck transcript ID or an Ensembl transcript ID";
    }
  }
  if (!defined $transcript) {
    croak "WARNING: Transcript $mapping{'-transcript'} not found in database";
  }
  $self->target_transcripts($transcript);
  $self->mismatch($transcript,$mapping{'-mismatch'});
  $self->alignment($transcript,$mapping{'-alignment'});
  my $chromosome = $mapping{'-chromosome'};
  my $start1 = $mapping{'-start1'} || $mapping{'-start'};
  my $end1 = $mapping{'-end1'} || $mapping{'-end'};
  my ($start2,$end2) = ($mapping{'-start2'},$mapping{'-end2'});
  $self->{'coordinates'}{$transcript->ID}=[$chromosome,$start1,$end1,$start2,$end2];

  return ++$self->{'mapping_info'};

}

=head2 coordinates

 Arg: Mitocheck::Transcript or Mitocheck::Gene
 Description: Gets genomic coordinates of dsRNA on the given
              gene or transcript.
 Returntype: list: chromosome,start1,end1,start2,end2

=cut

sub coordinates {
  my $self = shift;
  my $target = shift if @_;
  unless (defined $target) {
    croak "WARNING: Target gene or transcript required";
  }
  my $dsRNAID = $self->ID();
  my $targetID = $target->ID();
  my $query;
  if (ref($target) eq 'Mitocheck::Gene') {
    $query = qq(SELECT chromosome,start1,end1,start2,end2
                FROM dsRNA_map_info
                WHERE oligo_pairID = '$dsRNAID'
                AND geneID = '$targetID');
  }
  else {
    $query = qq(SELECT chromosome,start1,end1,start2,end2
                FROM dsRNA_map_info
                WHERE oligo_pairID = '$dsRNAID'
                AND transcriptID = '$targetID');
  }
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare($query);
  $sth->execute();
  @{$self->{'coordinates'}} = $sth->fetchrow_array();
  $sth->finish();

  return @{$self->{'coordinates'}};
}

=head2 supplier

 Arg:(optional) Mitocheck::Source object
 Description: Gets/sets the source (i.e. supplier) of this dsRNA.
 Returntype: Mitocheck::Source

=cut

sub supplier {

  my ($self,$source) = @_;
  $self->{'source'} = $source if defined($source);
  if (!defined $self->{'source'}) {
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $ID = $self->{'ID'};
    my $query = qq(SELECT sourceID
                   FROM oligo_pair
                   WHERE oligo_pairID= ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($ID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $sh = $dbc->get_SourceHandle;
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};

}

=head2 type

 Arg:(optional) string
 Description: Gets/sets the type of this dsRNA.
 Returntype: string

=cut

sub type {

  my ($self,$type) = @_;
  $self->{'type'} = $type if defined($type);
  if (!defined $self->{'source'}) {
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $ID = $self->{'ID'};
    my $query = qq(SELECT type
                   FROM oligo_pair
                   WHERE oligo_pairID= ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($ID);
    ($type) = $sth->fetchrow_array();
    $sth->finish();
    $self->{'type'} = $type if (defined($type));
  }
  return $self->{'type'};

}

=head2 replicates

 Arg: Mitocheck::Source object
 Description: Gets the number of times this dsRNA was used in the given screen
 Returntype: integer

=cut

sub replicates {

  my ($self,$source) = @_;
  if (!$source || ref($source) ne 'Mitocheck::Source') {
    croak "\nERROR: Source object required as argument";
  }
  my $sourceID = $source->ID;
  if (!defined ${$self->{'replicates'}}{$sourceID}) {
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $ID = $self->{'ID'};
    my $query = qq(SELECT a.value
                   FROM attribute a, dsRNA_has_attribute da
                   WHERE da.oligo_pairID = ?
                   AND da.attributeID = a.attributeID
                   AND a.sourceID = ?
                   AND a.name = 'replicates');
    my $sth = $dbh->prepare($query);
    $sth->execute($ID,$sourceID);
    my ($n) = $sth->fetchrow_array();
    ${$self->{'replicates'}}{$sourceID} = $n || 0;
    $sth->finish();
  }

  return ${$self->{'replicates'}}{$sourceID};

}

=head2 is_used_in_screen

 Arg: Mitocheck::Source object
 Description: Test whether the dsRNA was used in the given screen
 Returntype: number of times this dsRNA was used in the given screen

=cut

sub is_used_in_screen {

  my ($self,$source) = @_;

  return $self->replicates($source);

}

=head2 store_attributes

 Args: key=>value pairs
 Description: Sets arbitrary attributes for dsRNA and stores them
              in the database.
              One of the attributes must be a source=>source object pair.
 Returntype: 1

=cut

sub store_attributes {

  my $self = shift;
  my %attribute = @_;
  my $source = $attribute{'source'} || $attribute{'-source'};
  if (!defined($source) || ref($source) ne 'Mitocheck::Source') {
    croak " Source object required.";
  }
  my $sourceID = $source->ID;
  delete($attribute{'source'}) if (defined($attribute{'source'}));
  delete($attribute{'-source'}) if (defined($attribute{'-source'}));
  my $dbh = $self->{'database_handle'};
  foreach my $key(keys %attribute) {
    $key =~s/^-//; # remove leading - if any
    $self->{$key} = $attribute{$key};
    my $query = "SELECT attributeID FROM attribute WHERE name= ? AND sourceID= ? AND value= ?";
    my $sth= $dbh->prepare($query);
    $sth->execute($key,$sourceID,$attribute{$key});
    my ($ID) = $sth->fetchrow_array();
    if (!$ID) {
      # find last ID used
      my $query=qq(SELECT attributeID
                   FROM attribute
                   ORDER BY attributeID
                   DESC
                   LIMIT 1);
      my $sth= $dbh->prepare ($query);
      $sth->execute();
      my ($lastID) = $sth->fetchrow_array();
      $ID = ++$lastID;
      $query = "INSERT INTO attribute (attributeID,name,value,sourceID) VALUES('$ID','$key','$attribute{$key}','$sourceID')";
      my $rows= $dbh->do($query);
    }
    my $dsRNAID = $self->ID;
    $query = "INSERT IGNORE INTO dsRNA_has_attribute (oligo_pairID,attributeID) VALUES('$dsRNAID','$ID')";
    my $rows= $dbh->do($query);
  }
  return 1;
}

=head2 AUTOLOAD

 Arg1: Mitocheck::Source object
 Arg2: (optional) attribute value
 Description: Retrieves value of any attribute stored in the database
 Returntype: any

=cut

sub AUTOLOAD {

  my $self = shift;
  my $dsRNAID = $self->ID;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  my $source = shift if @_;
  croak "\nERROR: source object required\n" unless ($source);
  $self->{$attribute} = shift if @_;
  if (!defined $self->{$attribute}) {
    my $oligo_pairID = $self->{'ID'};
    my $sourceID = $source->ID;
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT a.value
                             FROM dsRNA_has_attribute drha, attribute a
                             WHERE drha.oligo_pairID = ?
                             AND drha.attributeID = a.attributeID
                             AND a.name = ?
                             AND a.sourceID = ?");
    $sth->execute($dsRNAID,$attribute,$sourceID);
    ($self->{$attribute})=$sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{$attribute};

}

sub DESTROY {

  my $self = shift;
  $self->_decr_dsRNA_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
