# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::Protein

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new( -database => 'mitocheck2_0',
					 -host     => 'www.mitocheck.org',
					 -user     => 'anonymous',
					 -password => '');

 my $protein_handle = $dbc->get_ProteinHandle();

 my $protein = $protein_handle->get_by_id('PRT00000001');

 my $uniprot = $protein->uniprot_acc();

=head1 DESCRIPTION

 Representation of Mitocheck proteins.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Protein;


use strict;
use warnings;
use Carp;
use Scalar::Util qw(weaken);

{
  my $_prot_count = 0;
  my $_newID_count = 0;

  sub get_protein_count {
    return $_prot_count;
  }
  sub _incr_prot_count {
    return ++$_prot_count;
  }
  sub _decr_prot_count {
    return --$_prot_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }

}



=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, protein ID
 Description: Creates a new protein object.
 Returntype: Mitocheck::Protein

=cut

sub new {

  my ($class,$dbc,$proteinID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbc->{'database_handle'};
  $class->_incr_prot_count();
  if (!defined $self->{'ID'} && !$proteinID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   proteinID
                 FROM     protein
                 ORDER BY proteinID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $proteinID = "PRT"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $proteinID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck protein ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 uniprot_acc

 Arg:  optional, uniprot accession number
 Description: Gets/sets protein's uniprot accession number
 Returntype: string

=cut

sub uniprot_acc {

  my $self = shift;
  $self->{'uniprot-acc'} = shift if @_;
  if (!defined $self->{'uniprot-acc'}) {
    my $proteinID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT Uniprot_acc FROM protein p WHERE p.proteinID = ? ");
    $sth->execute($proteinID);
    ($self->{'uniprot-acc'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'uniprot-acc'};

}

=head2 EnsemblID

 Arg:  optional, Ensembl ID
 Description: Gets/sets protein's ID in Ensembl
 Returntype: string

=cut

sub EnsemblID {

  my $self = shift;
  $self->{'EnsemblID'} = shift if @_;
  if (!defined $self->{'EnsemblID'}) {
    my $proteinID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT p.EnsemblID FROM protein p WHERE p.proteinID = ? ");
    $sth->execute($proteinID);
    ($self->{'EnsemblID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'EnsemblID'};

}

=head2 localizations

 Arg: optional, list of Mitocheck::Localization objects
 Description: Gets/sets protein subcellular localizations
 Returntype: list of Mitocheck::Localization objects

=cut

sub localizations {

  my ($self,@localizations) = @_;
  @{$self->{'localizations'}} = @localizations if @localizations;
  if (!defined $self->{'localizations'}) {
    my $dbc = $self->{'DBConnection'};
    my $lh = $dbc->get_LocalizationHandle();
    @{$self->{'localizations'}} = $lh->get_all_by_protein($self);
  }
  if (defined $self->{'localizations'}){
  	return @{$self->{'localizations'}};
  }
  else {
  	return undef;
  }
}

=head2 interactors

 Arg: optional, list of Mitocheck::Protein objects
 Description: Gets/sets protein interactors
 Returntype: list of Mitocheck::Protein objects

=cut

sub interactors {

  my ($self,@interactors) = @_;
  @{$self->{'interactors'}} = @interactors if @interactors;
  if (!defined $self->{'interactors'}) {
    my $proteinID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $prh = $dbc->get_ProteinHandle();
    my $sth = $dbh->prepare("SELECT DISTINCT p.proteinID, pi.interaction_type
                             FROM protein p, protein_interaction pi
                             WHERE (pi.interactorID= ?
                             AND pi.proteinID = p.proteinID)
                             UNION
                             SELECT DISTINCT p.proteinID, pi.interaction_type
                             FROM protein p, protein_interaction pi
                             WHERE (pi.proteinID = ?
                             AND pi.interactorID = p.proteinID) ");
    $sth->execute($proteinID,$proteinID);
    my %seen=();
    while (my ($id,$int_type)=$sth->fetchrow_array()) {
      next if $seen{$id}++;
      my $interactor = $prh->get_by_id($id) if ($id);
      $interactor->interaction_type($int_type) if ($int_type);
      push (@{$self->{'interactors'}},$interactor) if ($interactor);
    }
  }
  if (defined $self->{'interactors'}) {
  	return @{$self->{'interactors'}};
  }	
  else {
  	return undef;
  }
}

=head2 gene

 Arg: none
 Description: Gets gene the protein originates from
 Returntype: Mitocheck::Gene object

=cut

sub gene {

  my $self = shift;
  $self->{'gene'} = "";
  my $dbc = $self->{'DBConnection'};
  my $gh = $dbc->get_GeneHandle();
  $self->{'gene'} = $gh->get_by_protein($self);

  return $self->{'gene'};

}

=head2 distance

 Arg: target Mitocheck::Protein object
 Description: Gets the number of interactors between current
              protein and target protein. Currently using breadth
              first search.
 Returntype: integer

=cut

sub distance {

  my $self = shift;
  my $target = shift;
  my $proteinID = $self->ID();
  my $targetID = $target->ID();
  # get all interactions in memory (too slow otherwise)
  my %interactors;
  my $query = qq( SELECT * FROM protein_interaction );
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->get_DatabaseHandle();
  my $sth = $dbh->prepare($query);
  $sth->execute();
  while (my ($id1,$id2)=$sth->fetchrow_array()) {
    push @{$interactors{$id1}},$id2;
    push @{$interactors{$id2}},$id1;
  }
  my @queue = @{$interactors{$proteinID}};
  push @queue,'level';
  my %seen;
  $seen{'level'} = 0;
  while(my $protID=shift(@queue)) {
    if ($protID eq 'level') {
      return undef if (!@queue); # prevents infinite loop if proteins are not connected
      push @queue,'level';
      $seen{'level'}++;
      next;
    }
    if ($protID eq $targetID) {
      return $seen{'level'};
    }
    next if ($seen{$protID}++);
    push @queue, @{$interactors{$protID}};
  }
  return undef;
}


=head2 neighbourhood

 Arg1: integer, distance (in number of interactions) to travel
       from the protein. Currently limited to 1 or 2.
 Arg2: optional, interaction type
 Arg3: optional, source
 Description: Gets proteins interacting with current protein up
              to specified level of interaction. Currenly limited
              to 2 levels of interactions.
 Returntype: list of Mitocheck::Interaction objects


=cut

sub neighbourhood {

  my $self = shift;
  my @interactors = $self->interactors();
  return undef unless(@interactors);
  my $level = shift || 1;
  # because can't currently cope with display of large network
  $level = 2 if $level>2;
  my $int_type = shift || qq('AC','IP','Y2H','FCCS','unknown');
  my $source = shift || '';
  my $sourceID = ref($source) ? $source->ID() : $source;
  my @interactions;
  my $dbc = $self->{'DBConnection'};
  my $inth = $dbc->get_InteractionHandle();
  if ($level==1) {
    @interactions = $inth->get_all_by_protein($self,$int_type);
    return @interactions;
  }
  if ($int_type && $int_type!~/^'.+'$/) {
    $int_type = qq('$int_type');
  }
  else {
    $int_type=qq('AC','IP','Y2H','FCCS','unknown');
  }
  my $dbh = $dbc->get_DatabaseHandle();
  my @neighbourhood;
   $level--;
  # get all interactions in memory (too slow otherwise)
  my %interactors;
  my $query;
  if ($source) {
    $query = qq( SELECT pi.proteinID,pi.interactorID
                 FROM protein_interaction pi, interaction_occurrence io
                 WHERE pi.interaction_type IN ($int_type)
                 AND io.sourceID='$sourceID'
                 AND io.interactionID=pi.interactionID);
  }
  else {
    $query = qq( SELECT proteinID,interactorID
                 FROM protein_interaction
                 WHERE interaction_type IN ($int_type) );
  }
  my $ph = $dbc->get_ProteinHandle();
  my $sth = $dbh->prepare($query);
  $sth->execute();
  while (my ($id1,$id2)=$sth->fetchrow_array()) {
    push @{$interactors{$id1}},$id2;
    push @{$interactors{$id2}},$id1;
  }
  # stop here if protein has no interaction of selected type
  return @interactions unless (defined($interactors{$self->{'ID'}}));
  my @queue = @{$interactors{$self->{'ID'}}};
  push @queue,'level';
  my %seen;
  $seen{'level'} = 0;
  while(my $protID=shift(@queue)) {
    if ($protID eq 'level') {
      if (!@queue) { # prevents infinite loop
	    my %seen = ();
	    foreach my $prot(@neighbourhood) {
	      push @interactions,$inth->get_all_by_protein($prot,$int_type,$source);
	    }
	    return grep {!$seen{$_->ID}++ } @interactions;
      }
      push @queue,'level';
      $seen{'level'}++;
      next;
    }
    if ($seen{'level'}>=$level) {
      my %seen = ();
      foreach my $prot(@neighbourhood) {
	    push @interactions,$inth->get_all_by_protein($prot,$int_type,$source);
      }
      return grep {!$seen{$_->ID}++ } @interactions;
    }
    next if ($seen{$protID}++);
    push @queue, @{$interactors{$protID}};
    my $protein = $ph->get_by_id($protID);
    push @neighbourhood, $protein;
  }
  return @interactions;
}

=head2 seq_length

 Arg:  optional, integer
 Description: Gets/sets this protein's length in number of amino-acids
 Returntype: integer

=cut

sub seq_length {

  my $self = shift;
  $self->{'seq_length'} = shift if @_;
  if (!defined $self->{'seq_length'}) {
    my $proteinID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT p.seq_length FROM protein p WHERE p.proteinID = ? ");
    $sth->execute($proteinID);
    ($self->{'seq_length'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'seq_length'};

}

=head2 mass

 Arg:  optional, double
 Description: Gets/sets this protein's molecular weight in kDa
 Returntype: double

=cut

sub mass {

  my $self = shift;
  $self->{'mass'} = shift if @_;
  if (!defined $self->{'mass'}) {
    my $proteinID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT p.mass FROM protein p WHERE p.proteinID = ? ");
    $sth->execute($proteinID);
    ($self->{'mass'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'mass'};

}

=head2 get_MSexperiments

 Arg: (optional) 1 to get only experiments where protein is the bait
 Description: Gets all mass spectrometry experiments where peptides
              from this protein appear or experiments where this protein is
              used as bait.
 Returntype: list of Mitocheck::MSexperiment objects

=cut

sub get_MSexperiments {

  my $self = shift;
  my $is_bait = shift;

  my $dbh = $self->{'DBConnection'}->get_DatabaseHandle();
  my $exph = $self->{'DBConnection'}->get_MSexperimentHandle();
  my $query;
  my @experiments;
  if ($is_bait) {
    $query = qq(SELECT distinct experimentID
                FROM peptide_occurrence
                WHERE baitID = ?);
  }
  else {
    $query = qq(SELECT distinct po.experimentID
                FROM peptide_occurrence po, peptide_map_info pmi
                WHERE pmi.proteinID = ?
                AND pmi.peptideID = po.peptideID);
  }
  my $sth = $dbh->prepare($query);
  $sth->execute($self->ID);
  while (my ($expID) = $sth->fetchrow_array()) {
    push @experiments,$exph->get_by_id($expID) if (defined($expID));
  }

  return @experiments;
}

=head2 get_peptides

 Arg: (optional) Mitocheck::MSexperiment
 Description: Gets all peptides that map to the given protein,
              optionally limited to the peptides recovered in the given experiment
 Returntype: list of Mitocheck::Peptide objects


=cut

sub get_peptides {

  my $self = shift;
#  my $bait = shift;
  my $exp = shift;
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID : $exp;

  if (!defined($self->{'peptides'}{$expID})) {
    my $dbc = $self->{'DBConnection'};
    my $peph = $dbc->get_PeptideHandle();
    my @peptides = $peph->get_all_by_protein($self,$exp);
    $self->{'peptides'}{$expID} = \@peptides if (@peptides);
  }

  return @{$self->{'peptides'}{$expID}} if (defined($self->{'peptides'}{$expID}));
}

=head2 coverage

 Arg1: Mitocheck::MSexperiment object
 Arg2: (optional) coverage as %
 Description: Gets/sets coverage of protein (as % of length) in the given MS experiment.
 Returntype: double


=cut

sub coverage {

  my $self = shift;
  my $exp = shift;
  if (!defined($exp) || !$exp->isa('Mitocheck::MSexperiment')) {
    croak "\nERROR: Mitocheck::MSexperiment object required as argument to method get_coverage";
  }
  my $expID = $exp->ID;
  $self->{'coverage'}{$expID} = shift if @_;
  if (!defined $self->{'coverage'}{$expID}) {
    my $dbc = $self->{'DBConnection'};
    my $peph = $dbc->get_PeptideHandle();
    my @peptides = $peph->get_all_by_protein($self,$exp,1); # 1 = get only highest scoring occurrence of each peptide sequence
    my $seq_length = $self->seq_length();
    my $seq = '0' x $seq_length; # mock sequence
    foreach my $peptide(@peptides) {
      my ($pstart,$pend,undef) = $peptide->coordinates($self);
      if (defined($pstart) && defined($pend)) {
	    # Substitute 1s in mock sequence where peptide maps
	    my $l = $pend-$pstart+1;
	    substr($seq,$pstart-1,$l) = '1' x $l;
      }
    }
    # Count number of 1s to get coverage
    $self->{'coverage'}{$expID} = sprintf("%.1f",(($seq=~tr/1//)/$seq_length)*100);
  }
  return $self->{'coverage'}{$expID};
}

=head2 score

 Arg1: Mitocheck::MSexperiment object or MS experiment ID
 Arg2: (optional) score
 Description: Gets/sets score of protein (as sum of unique peptides scores) in the
              given MS experiment
 Returntype: double


=cut

sub score {

  my $self = shift;
  my $exp = shift;
  my $expID = ref($exp) ? $exp->ID : $exp;
  $self->{'score'}{$expID} = shift if @_;
  if (!defined $self->{'score'}{$expID}) {
  	my $proteinID = $self->ID;
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
  	my $query = qq(SELECT SUM(S.maxscore) FROM 
  		             (SELECT MAX(po.score) AS maxscore
                   	  FROM peptide_occurrence po, peptide_map_info pmi
                      WHERE pmi.proteinID = '$proteinID'
                      AND pmi.peptideID = po.peptideID
                      AND po.experimentID = '$expID'
                      GROUP BY po.peptideID) AS S);
    my $sth = $dbh->prepare($query);                   
    $sth->execute();
    $self->{'score'}{$expID} = $sth->fetchrow_array();
    $sth->finish;
  }
  return $self->{'score'}{$expID};
}

=head2 get_all_copurifying_proteins

 Description: Gets all proteins found in the same purifications as this protein
 Returntype: list of Mitocheck::Protein objects


=cut

sub get_all_copurifying_proteins {

  my $self = shift;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->get_DatabaseHandle();
  my $ph = $dbc->get_ProteinHandle();
  my %seen;
  foreach my $exp($self->get_MSexperiments) {
    my @prots = $exp->get_proteins;
    foreach my $protein(@prots) {
      $seen{$protein->ID}++;
    }
  }
  my @proteins;
  foreach my $ID(keys %seen) {
    push @proteins,$ph->get_by_id($ID) if (defined($ID));
  }
  return @proteins;

}

=head2 stochiometry

 Arg1: Mitocheck::Complex or Mitocheck complex ID
 Arg2: (optional) stochiometry value
 Description: Gets/sets stochiometry of this protein in the given complex
 Returntype: double


=cut

sub stochiometry {

  my $self = shift;
  my $complex = shift;
  my $complexID = ref($complex) ? $complex->ID : $complex;
  $self->{'stochiometry'}{$complexID} = shift if @_;
  if (!defined $self->{'stochiometry'}{$complexID}) {
    my $proteinID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT stochiometry
          					 FROM complex_membership
          					 WHERE proteinID = ? 
          					 AND complexID = ? ");
    $sth->execute($proteinID,$complexID);
    my ($n) = $sth->fetchrow_array();
    if (defined($n)) {
      $self->{'stochiometry'}{$complexID} = $n;
    }
    $sth->finish();
  }
  return $self->{'stochiometry'}{$complexID};
  
}

sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  return $self->{$attribute};
}

sub DESTROY {

  my $self = shift;
  $self->_decr_prot_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
