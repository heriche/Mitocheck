# Author: jkh1
# 2012-09-24

=head1 NAME

 Mitocheck::InteractionHandle

=head1 SYNOPSIS



=head1 DESCRIPTION

Enables retrieval of Mitocheck Interaction objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::InteractionHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Interaction;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new interaction object handle
 Returntype: Mitocheck::InteractionHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: interaction ID
 Description: Gets interaction occurrence with given ID
 Returntype: Mitocheck::Interaction object

=cut

sub get_by_id {

  my ($self,$intID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $query = qq(SELECT count(*) FROM interaction_occurrence WHERE interaction_occurrenceID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($intID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::Interaction->new($dbc,$intID);

}

=head2 get_all_by_protein

 Arg1: Mitocheck::Protein or protein ID
 Arg2: optional, interaction type (AC, IP , Y2H or unknown, default is all)
 Arg3: optional, Mitocheck::Source or source ID
 Description: Gets all interactions involving given protein
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_by_protein {

  my ($self,$protein,$int_type,$source) = @_;
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  if (defined($int_type) && $int_type!~/^'.+'$/) {
    $int_type = qq('$int_type');
  }
  else {
    $int_type=qq('AC','IP','Y2H','FCCS','unknown');
  }
  my $query;
  if ($source) {
    $query = qq( SELECT DISTINCT interaction_occurrenceID
                 FROM protein_interaction pi, interaction_occurrence io
                 WHERE (pi.proteinID= ? OR pi.interactorID= ?)
                 AND pi.interaction_type IN ($int_type)
                 AND io.sourceID='$sourceID'
                 AND pi.interactionID = io.interactionID );
  }
  else {
    $query = qq( SELECT DISTINCT interaction_occurrenceID
                 FROM protein_interaction pi, interaction_occurrence io
                 WHERE (pi.proteinID= ? OR pi.interactorID= ?)
                 AND pi.interaction_type IN ($int_type)
                 AND pi.interactionID = io.interactionID );
  }
  my $sth = $dbh->prepare($query);
  $sth->execute($proteinID,$proteinID);
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;

}

=head2 get_all_by_gene

 Arg1: Mitocheck::Gene or gene ID
 Arg2: optional, Mitocheck::Source or source ID
 Description: Gets all interactions involving given gene
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_by_gene {

  my ($self,$gene,$source) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $query;
  if ($source) {
    $query = qq( SELECT DISTINCT io.interaction_occurrenceID
                 FROM transcript t, protein_interaction pi, interaction_occurrence io
                 WHERE t.geneID = ?
                 AND (t.proteinID = pi.proteinID OR t.proteinID = pi.interactorID)
                 AND io.sourceID='$sourceID'
                 AND pi.interactionID = io.interactionID );
  }
  else {
    $query = qq( SELECT DISTINCT io.interaction_occurrenceID
                 FROM transcript t, protein_interaction pi, interaction_occurrence io
                 WHERE t.geneID = ?
                 AND (t.proteinID = pi.proteinID OR t.proteinID = pi.interactorID)
                 AND pi.interactionID = io.interactionID );
  }
  my $sth = $dbh->prepare($query);
  $sth->execute($geneID);
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;

}

=head2 get_all_by_bait

 Arg1: Mitocheck::Protein or protein ID
 Arg2: optional, Mitocheck::Source or source ID
 Description: Gets all interactions from the given bait.
              Only works for interactions from mass spectrometry experiments.
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_by_bait {

  my ($self,$bait,$source) = @_;
  my $baitID = ref($bait) ? $bait->ID() : $bait;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $query;
  # By convention, baits are in the first column (i.e. proteinID)
  # in the protein_interaction table (when it is known which partner
  # is the bait)
  if ($source) {
    $query = qq( SELECT interaction_occurrenceID
                 FROM protein_interaction pi, peptide_occurrence po, interaction_occurrence io
                 WHERE pi.proteinID= ?
                 AND io.sourceID='$sourceID'
                 AND pi.proteinID = po.baitID
                 AND pi.interactionID = io.interactionID );
  }
  else {
    $query = qq( SELECT interaction_occurrenceID
                 FROM protein_interaction pi, peptide_occurrence po, interaction_occurrence io
                 WHERE pi.proteinID= ?
                 AND pi.proteinID = po.baitID
                 AND pi.interactionID = io.interactionID );
  }
  my $sth = $dbh->prepare($query);
  $sth->execute($baitID);
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;

}

=head2 get_all_by_source

 Arg: Mitocheck::Source object or source ID
 Description: Gets all interactions from given source
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_by_source {

  my ($self,$source) = @_;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT interaction_occurrenceID
                           FROM interaction_occurrence
                           WHERE sourceID= ?");
  $sth->execute($sourceID);
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;
}

=head2 get_all_by_cellular_component

 Arg: Mitocheck::CellularComponent object or cellular component ID
 Description: Gets all interactions from given cellular component
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_by_cellular_component {

  my ($self,$cellular_component) = @_;
  my $ccID = ref($cellular_component) ? $cellular_component->ID() : $cellular_component;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT interaction_occurrenceID
                           FROM interaction_occurrence
                           WHERE cellular_componentID= ?");
  $sth->execute($ccID);
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;
}

=head2 get_all_by_event

 Arg: Mitocheck::Event object or event ID
 Description: Gets all interactions from given event
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_by_event {

  my ($self,$event) = @_;
  my $eventID = ref($event) ? $event->ID() : $event;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT interaction_occurrenceID
                           FROM interaction_occurrence
                           WHERE eventID= ?");
  $sth->execute($eventID);
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;
}

=head2 get_all_by_occurrence

 Arg1: Mitocheck::Event object or event ID
 Arg2: Mitocheck::CellularComponent object or cellular component ID
 Description: Gets all interactions from given event and cellular component
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_by_occurrence {

  my ($self,$event,$cellular_component) = @_;
  my $eventID = ref($event) ? $event->ID() : $event;
  my $ccID = ref($cellular_component) ? $cellular_component->ID : $cellular_component;
  if (!$eventID || !$ccID) {
    croak "\nERROR: Event and cellular component required\n";
  }
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT interaction_occurrenceID
                           FROM interaction_occurrence
                           WHERE eventID= ?
                           AND cellular_componentID= ?");
  $sth->execute($eventID,$ccID);
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;
}

=head2 get_all_interactions

 Arg: none
 Description: Gets all occurrences of interaction currently in the database
 Returntype: list of Mitocheck::Interaction objects

=cut

sub get_all_interactions {

  my $self = shift;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT interaction_occurrenceID
                           FROM interaction_occurrence");
  $sth->execute();
  my @interactions;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@interactions,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @interactions;
}

=head2 new_interaction

 Arg: key => value pairs
 Description: creates a new interaction object
 Returntype: Mitocheck::Interaction

=cut

sub new_interaction {

  my $self = shift;
  my %interaction = @_;
  my $dbc = $self->{'DBConnection'};
  unless ($interaction{'-partner1'} && $interaction{'-partner2'}) {
    croak "Interaction requires two partners";
  }
  unless (defined $interaction{'-event'}) {
    croak "Error: -event argument required for interaction";
  }
  unless (defined $interaction{'-cellular_component'}) {
    croak "Error: -cellular_component argument required for interaction";
  }
  unless (defined $interaction{'-source'}) {
    croak "Error: -source argument required for interaction";
  }
  
  $interaction{'-type'} ||='unknown';

  my $interaction = Mitocheck::Interaction->new($dbc,undef,$interaction{'-partner1'},$interaction{'-partner2'},$interaction{'-type'},
  											  $interaction{'-cellular_component'},$interaction{'-event'},$interaction{'-source'});

  return $interaction;

}

sub store {

  my ($self,$interaction) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  unless ($interaction->{'partner1'} && $interaction->{'partner2'}) {
    croak "Interaction requires two partners";
  }
  my $p1 = $interaction->{'partner1'};
  my $p2 = $interaction->{'partner2'};
  my $p1id = $p1->ID();
  my $p2id = $p2->ID();
  my $type = $interaction->type() || 'unknown';
  my $qtype = $dbh->quote($type);
  my $source = $interaction->source;

  # Check if interaction already in database
  # We care about interaction directionality because
  # p1 is the bait by convention in Mitocheck and MitoSys experiments
  my $query = qq(SELECT interactionID
                 FROM protein_interaction
                 WHERE proteinID = '$p1id' AND interactorID = '$p2id'
                 AND interaction_type = '$type');
  my $sth = $dbh->prepare($query);
  $sth->execute();
  my ($interactionID) = $sth->fetchrow_array();
  $sth->finish();
  # Add interaction if it's new
  if (!$interactionID) {
    # Find last ID used in interaction table
    # because interaction object ID is an interaction_occurrence ID
    my $query=qq(SELECT   interactionID
                 FROM     protein_interaction
                 ORDER BY interactionID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array() || (0);
    $interactionID = $lastID+1;
    # Update interaction table
    $query = qq(INSERT IGNORE INTO protein_interaction (interactionID,proteinID,interactorID,interaction_type) VALUES ('$interactionID','$p1id','$p2id','$type'));
    my $rows= $dbh->do ($query);
  }
  # Check if interaction occurrence already in database
  my $sourceID = $source->ID();
  my $cellular_component = $interaction->cellular_component();
  my $ccID = $cellular_component->ID || '';
  my $event = $interaction->event;
  my $eventID = $event->ID || '';
  $query = qq(SELECT interaction_occurrenceID
              FROM interaction_occurrence
              WHERE interactionID = ?
              AND cellular_componentID = ?
              AND eventID = ?
              AND sourceID = ? );
  $sth = $dbh->prepare($query);
  $sth->execute($interactionID,$ccID,$eventID,$sourceID);
  my ($ID) = $sth->fetchrow_array();
  $sth->finish();
  if ($ID) {
    $interaction->ID($ID);
  }
  else {
    # Record occurrence
    my $interaction_occurrenceID = $interaction->ID;
    my $query = qq(INSERT IGNORE INTO interaction_occurrence (interaction_occurrenceID,interactionID,cellular_componentID,eventID,sourceID)
                   VALUES ('$interaction_occurrenceID','$interactionID','$ccID','$eventID','$sourceID'));
    my $rows= $dbh->do ($query);
  }
  $interaction->_decr_newID_count() if $interaction->{'is_new'};
  $interaction->{'is_new'} = undef;

  return $interaction;
}

1;
