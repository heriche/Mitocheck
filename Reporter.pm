# Author: jkh1
# 2016-04-06

=head1 NAME

  Mitocheck::Reporter

=head1 SYNOPSIS



=head1 DESCRIPTION

 Representation of a reporter. A reporter is what is being visualized or
 measured on an image, e.g. an antibody against a protein.

=head1 CONTACT

 heriche@embl.de


=head1 COPYRIGHT AND LICENSE

Copyright (C) 2016 by Jean-Karim Heriche

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.0 or,
at your option, any later version of Perl 5 you may have available.

=cut

=head1 METHODS

=cut

package Mitocheck::Reporter;

our $VERSION = '0.01';
use 5.006;
use strict;
use Carp;
use Scalar::Util qw(blessed);

{
 my $_reporter_count = 0;
  my $_newID_count = 0;

  sub get_reporter_count {
    return $_reporter_count;
  }
  sub _incr_reporter_count {
    return ++$_reporter_count;
  }
  sub _decr_reporter_count {
    return --$_reporter_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: (optional) reporter ID
 Description: Creates a new reporter object.
 Returntype: Mitocheck::Reporter

=cut

sub new {

  my ($class,$dbc,$reporterID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $class->_incr_reporter_count();
  if (!defined $self->{'ID'} && !$reporterID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   reporterID
                 FROM     reporter
                 ORDER BY reporterID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $reporterID = "REP"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $reporterID;

  bless ($self, $class);

  return $self;
}

=head2 ID

 Arg: optional, integer
 Description: Gets/sets reporter ID
 Returntype: integer

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};
}

=head2 external_ID

 Arg: (optional) string
 Description: Gets/sets sample ID in reference database
 Returntype: string

=cut

sub external_ID {

  my $self = shift;
  $self->{'external_ID'} = shift if @_;
  if (!defined $self->{'external_ID'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT externalID FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'external_ID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'external_ID'};
}

=head2 reference_database

 Arg: (optional) string
 Description: Gets/sets database in which the externalID is valid.
 Returntype: string

=cut

sub reference_database {

  my $self = shift;
  $self->{'refDB'} = shift if @_;
  if (!defined $self->{'refDB'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT refDB FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'refDB'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'refDB'};
}

=head2 source

 Arg: (optional), Mitocheck::Source object
 Description: Gets/sets the source (i.e. supplier) of this reporter
 Returntype: Mitocheck::Source object

=cut

sub source {

  my $self = shift;
  $self->{'source'} = shift if @_;
  if (!defined $self->{'source'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sourceID FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $sh = $dbc->get_SourceHandle;
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};
}

=head2 type

 Arg: (optional) string
 Description: Gets/sets reporter type, e.g. compound, antibody...
 Returntype: string

=cut

sub type {

  my $self = shift;
  $self->{'type'} = shift if @_;
  if (!defined $self->{'type'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT type FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'type'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'type'};
}

=head2 name

 Arg: (optional) string
 Description: Gets/sets reporter name
 Returntype: string

=cut

sub name {

  my $self = shift;
  $self->{'name'} = shift if @_;
  if (!defined $self->{'name'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT name FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'name'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'name'};
}


=head2 description

 Arg: (optional) string
 Description: Gets/sets reporter description
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT description FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};
}

=head2 EFOID

 Arg: (optional) string
 Description: Gets/sets ID of the Experimental Factor Ontology term related
              to this experimental reporter
 Returntype: string

=cut

sub EFOID {

  my $self = shift;
  $self->{'EFOID'} = shift if @_;
  if (!defined $self->{'EFOID'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT EFOID FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'EFOID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'EFOID'};
}

=head2 EFOterm

 Arg: (optional) string
 Description: Gets/sets the Experimental Factor Ontology term related to this
              experimental reporter
 Returntype: string

=cut

sub EFOterm {

  my $self = shift;
  $self->{'EFOterm'} = shift if @_;
  if (!defined $self->{'EFOterm'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT EFOterm FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'EFOterm'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'EFOterm'};
}

=head2 url

 Arg: (optional) string
 Description: Gets/sets a URL at which more info on the reporter can be had.
 Returntype: string

=cut

sub url {

  my $self = shift;
  $self->{'url'} = shift if @_;
  if (!defined $self->{'url'}) {
    my $reporterID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT URL FROM reporter WHERE reporterID = ?");
    $sth->execute($reporterID);
    ($self->{'url'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'url'};
}

=head2 channel

 Arg: optional, string
 Description: Gets/sets the channel/color used to visualize the reporter
 Returntype: string

=cut

sub channel {

  my $self = shift;
  $self->{'channel'} = shift if @_;
  return $self->{'channel'};
}

sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  return $self->{$attribute};
}

sub DESTROY {

  my $self = shift;
  $self->_decr_reporter_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
