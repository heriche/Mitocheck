# Author: jkh1
# 2011-05-10
#

=head1 NAME

 Mitocheck::Event

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $event_handle = $dbc->get_EventHandle();

 my $event = $event_handle->get_by_id('EV00000034');

 my $description = $event->description();

=head1 DESCRIPTION

 Representation of events. An event can be any kind of time information
 e.g. anaphase or 30 h post-transfection. An event can be associated with an
 ontology term and/or a sequence of events.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Event;

use strict;
use warnings;
use Scalar::Util qw(weaken);


{
  my $_event_count = 0;
  my $_newID_count = 0;

  sub get_event_count {
    return $_event_count;
  }
  sub _incr_event_count {
    return ++$_event_count;
  }
  sub _decr_event_count {
    return --$_event_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, event ID
 Description: Creates a new event object.
 Returntype: Mitocheck::Event

=cut

sub new {

  my ($class,$dbc,$eventID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_event_count();
  if (!defined $self->{'ID'} && !$eventID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   eventID
                 FROM     event
                 ORDER BY eventID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $eventID = "EVN"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $eventID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck event ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 name

 Arg: optional, text
 Description: Gets/sets event name
 Returntype: string

=cut

sub name {

  my $self = shift;
  $self->{'name'} = shift if @_;
  if (!defined $self->{'name'}) {
    my $eventID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT name FROM event
                             WHERE eventID = ?");
    $sth->execute($eventID);
    $self->{'name'} = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'name'};

}

=head2 description

 Arg: optional, text
 Description: Gets/sets event description
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $eventID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT description FROM event
                             WHERE eventID = ?");
    $sth->execute($eventID);
    $self->{'description'} = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 order_in_sequence

 Arg: optional, text
 Description: Gets/sets position of event in the sequence it belongs to
 Returntype: string

=cut

sub order_in_sequence {

  my $self = shift;
  $self->{'order'} = shift if @_;
  if (!defined $self->{'order'}) {
    my $eventID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT position FROM event_sequence
                             WHERE eventID = ?");
    $sth->execute($eventID);
    $self->{'order'} = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'order'};

}

=head2 GOID

 Arg: optional, GO term ID
 Description: Gets/sets biological process GO term ID associated
              with this event
 Returntype: string

=cut

sub GOID {

  my $self = shift;
  $self->{'GOID'} = shift if @_;
  if (!defined $self->{'GOID'}) {
    my $ID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT GOID FROM event
                             WHERE eventID = ?");
    $sth->execute($ID);
    $self->{'GOID'} = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'GOID'};

}

=head2 GOterm

 Arg: optional, GO term
 Description: Gets/sets biological process GO term associated
              with this event
 Returntype: string

=cut

sub GOterm {

  my $self = shift;
  $self->{'GOterm'} = shift if @_;
  if (!defined $self->{'GOterm'}) {
    my $ID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT GOterm FROM event
                             WHERE eventID = ?");
    $sth->execute($ID);
    $self->{'GOterm'} = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'GOterm'};

}

sub DESTROY {

  my $self = shift;
  $self->_decr_event_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
