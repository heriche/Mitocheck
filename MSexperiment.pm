# Author: jkh1
# 2008-06-16
#

=head1 NAME

 Mitocheck::MSexperiment

=head1 SYNOPSIS


=head1 DESCRIPTION

 Representation of Mitocheck mass spectrometry experiments

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::MSexperiment;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_exp_count = 0;
  my $_newID_count = 0;

  sub get_exp_count {
    return $_exp_count;
  }
  sub _incr_exp_count {
    return ++$_exp_count;
  }
  sub _decr_exp_count {
    return --$_exp_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }

}



=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, MS experiment ID
 Description: Creates a new MS experiment object.
 Returntype: Mitocheck::MSexperiment

=cut

sub new {

  my ($class,$dbc,$ID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbc->{'database_handle'};
  $class->_incr_exp_count();
  if (!defined $self->{'ID'} && !$ID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   experimentID
                 FROM     MSexperiment
                 ORDER BY experimentID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $ID = "MSE"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $ID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, MS experiment ID
 Description: Gets/sets MS experiment ID
 Returntype: integer

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 sequence_tag

 Arg: (optional) SequenceTag object
 Description: Gets/sets the sequence tag used for identifying the bait.
 Returntype: SequenceTag object

=cut

sub sequence_tag {

  my ($self,$seqTag) = @_;
  $self->{'sequence_tag'} = $seqTag if $seqTag;
  if (!defined $self->{'sequence_tag'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sequence_tagID
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    my ($seqTagID) = $sth->fetchrow_array();
    $sth->finish();
    if ($seqTagID) {
      my $seqTagh = $self->{'DBConnection'}->get_SequenceTagHandle();
      $self->{'sequence_tag'} = $seqTagh->get_by_id($seqTagID);
    }
  }
  return $self->{'sequence_tag'};

}

=head2 name

 Arg: (optional) string
 Description: Gets/sets this experiment's name, usually some
              sort of ID given by the experimenter.
 Returntype: string

=cut

sub name {

  my ($self,$name) = @_;
  $self->{'name'} = $name if $name;
  if (!defined $self->{'name'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT name
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    ($self->{'name'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'name'};

}

=head2 bait

 Arg: (optional) Mitocheck::Protein object
 Description: Gets/sets the bait used in this MS experiment.
              This is always a human protein regardless of the species
              of the actual bait.
 Returntype: Mitocheck::Protein object

=cut

sub bait {

  my ($self,$bait) = @_;
  $self->{'bait'} = $bait if $bait;
  if (!defined $self->{'bait'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT baitID
                             FROM peptide_occurrence
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
     my ($baitID) = $sth->fetchrow_array();
    $sth->finish();
    if ($baitID) {
      my $ph = $self->{'DBConnection'}->get_ProteinHandle();
      $self->{'bait'} = $ph->get_by_id($baitID);
    }
  }
  return $self->{'bait'};
}

=head2 actual_bait

 Arg: (optional) listref of strings: [$EnsemblID,$species]
 Description: Gets/sets the bait actually used in the experiment.
              This is not a Mitocheck::Protein object because it can be
              a non-human protein.
 Returntype: listref of strings: [$EnsemblID,$species]

=cut

sub actual_bait {

  my ($self,$actual_bait) = @_;
  $self->{'actual_bait'} = $actual_bait if $actual_bait;
  if (!defined $self->{'actual_bait'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT st.geneID,st.species
                             FROM sequence_tag st, MSexperiment mse
                             WHERE mse.experimentID = ?
                             AND mse.sequence_tagID = st.sequence_tagID");
    $sth->execute($self->ID);
    my ($EnsemblID,$species) = $sth->fetchrow_array();
    $sth->finish();
    $self->{'actual_bait'} = [$EnsemblID,$species] if ($EnsemblID);
  }
  return $self->{'actual_bait'};

}

=head2 actual_bait_score

 Arg: (optional) double
 Description: Gets/sets score for the bait actually used in the experiment.
 Returntype: double

=cut

sub actual_bait_score {

  my ($self,$score) = @_;
  $self->{'actual_bait_score'} = $score if (defined($score));
  if (!defined $self->{'actual_bait_score'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT bait_score
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    ($self->{'actual_bait_score'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'actual_bait_score'};

}

=head2 actual_bait_coverage

 Arg: (optional) double
 Description: Gets/sets coverage for the bait actually used in the experiment.
 Returntype: double

=cut

sub actual_bait_coverage {

  my ($self,$cov) = @_;
  $self->{'actual_bait_coverage'} = $cov if (defined($cov));
  if (!defined $self->{'actual_bait_coverage'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT bait_coverage
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    ($self->{'actual_bait_coverage'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'actual_bait_coverage'};

}

=head2 actual_bait_length

 Arg: (optional) double
 Description: Gets/sets sequence length of the bait actually used in the experiment.
 Returntype: double

=cut

sub actual_bait_length {

  my ($self,$len) = @_;
  $self->{'actual_bait_length'} = $len if (defined($len));
  if (!defined $self->{'actual_bait_length'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT bait_seq_length
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    ($self->{'actual_bait_length'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'actual_bait_length'};

}

=head2 actual_bait_mass

 Arg: (optional) double
 Description: Gets/sets mass (in kDa) of the bait actually used in the experiment.
 Returntype: double

=cut

sub actual_bait_mass {

  my ($self,$mass) = @_;
  $self->{'actual_bait_mass'} = $mass if (defined($mass));
  if (!defined $self->{'actual_bait_mass'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT bait_mass
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    ($self->{'actual_bait_mass'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'actual_bait_mass'};

}

=head2 actual_bait_species

 Arg: (optional) double
 Description: Gets/sets species of the bait actually used in the experiment.
 Returntype: double

=cut

sub actual_bait_species {

  my ($self,$species) = @_;
  $self->{'actual_bait_species'} = $species if (defined($species));
  if (!defined $self->{'actual_bait_species'}) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT st.species
                             FROM sequence_tag st, MSexperiment mse
                             WHERE mse.experimentID = ?
                             AND mse.sequence_tagID = st.sequence_tagID");
    $sth->execute($self->ID);
    ($self->{'actual_bait_species'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'actual_bait_species'};

}

=head2 get_proteins

 Arg: (optional) set to 1 to remove contaminants
 Description: Gets the proteins recovered in this MS experiment.
              Note that this is NOT the minimal protein list (see method
              get_minimal_gene_list)
 Returntype: list of Mitocheck::Protein objects

=cut

sub get_proteins {

  my $self = shift;
  my $rm_contaminants = shift;

  if (!defined($self->{'proteins'})) {
    my $dbh = $self->{'database_handle'};
    my $ph = $self->{'DBConnection'}->get_ProteinHandle();
    my $query;
    if ($rm_contaminants) {
#      # Query too slow
#      $query = qq(SELECT DISTINCT pmi.proteinID
#                  FROM peptide_map_info pmi, peptide_occurrence po, transcript t,
#peptide_gene_assignment pga
#                  WHERE po.experimentID = ?
#                  AND po.peptideID = pmi.peptideID
#                  AND pmi.proteinID = t.proteinID
#                  AND t.geneID = pga.geneID
#                  AND pga.is_contaminant is null);

      # Use of temporary table is ~100x faster than single query with large peptide_gene_assignment table
      my $create_tmp_table = qq(CREATE TEMPORARY TABLE IF NOT EXISTS
                                  non_contaminants (geneID varchar(20), index (geneID))
      			                AS SELECT DISTINCT geneID
      			                   FROM peptide_gene_assignment
      			                   WHERE is_contaminant is null);
      $dbh->do($create_tmp_table); 
      $query = qq(SELECT DISTINCT pmi.proteinID
                  FROM peptide_map_info pmi, peptide_occurrence po, transcript t, non_contaminants nc
                  WHERE po.experimentID = ?
                  AND po.peptideID = pmi.peptideID
                  AND pmi.proteinID = t.proteinID
                  AND t.geneID = nc.geneID);
    }
    else {
      $query = qq(SELECT DISTINCT pmi.proteinID
                  FROM peptide_map_info pmi, peptide_occurrence po
                  WHERE po.experimentID = ?
                  AND po.peptideID = pmi.peptideID);
    }
    my $sth = $dbh->prepare($query);
    $sth->execute($self->ID);
    while (my ($protID) = $sth->fetchrow_array()) {
      if ($protID) {
	my $prot = $ph->get_by_id($protID);
	push @{$self->{'proteins'}},$prot;
      }
    }
  }
  return @{$self->{'proteins'}} if defined($self->{'proteins'});

}

=head2 peptides

 Arg: (optional) list of Mitocheck::Peptide objects
 Description: Gets/sets the peptides recovered in this MS experiment
 Returntype: list of Mitocheck::Peptide objects

=cut

sub peptides {

  my $self = shift;
  @{$self->{'peptides'}} = @_ if @_;
  if (!defined($self->{'peptides'})) {
    my $dbh = $self->{'database_handle'};
    my $ph = $self->{'DBConnection'}->get_PeptideHandle();
    my $sth = $dbh->prepare("SELECT DISTINCT occurrenceID
                             FROM peptide_occurrence
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    while (my ($ID) = $sth->fetchrow_array()) {
      if ($ID) {
	push @{$self->{'peptides'}},$ph->get_by_id($ID);
      }
    }
    $sth->finish();
  }
  return @{$self->{'peptides'}} if defined($self->{'peptides'});

}

=head2 gel_image

 Arg: (optional) path to file
 Description: Gets/sets the path to the file that contains an image of the gel
              associated with this purification
 Returntype: string

=cut

sub gel_image {

  my $self = shift;
  $self->{'gel_image'} = shift if @_;
  if (!defined($self->{'gel_image'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT gel_image
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    my ($file) = $sth->fetchrow_array();
    if ($file) {
      $self->{'gel_image'} = $file;
    }
    $sth->finish();
  }
  return $self->{'gel_image'};

}

=head2 gel_lane

 Arg: (optional) number of the lane where the purification is on the gel
 Description: Gets/sets the position of the lane that contains this purification
              on the gel associated with this purification
 Returntype: integer

=cut

sub gel_lane {

  my $self = shift;
  $self->{'gel_lane'} = shift if @_;
  if (!defined($self->{'gel_lane'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT gel_lane
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->ID);
    my ($lane) = $sth->fetchrow_array();
    if ($lane) {
      $self->{'gel_lane'} = $lane;
    }
    $sth->finish();
  }
  return $self->{'gel_lane'};

}

=head2 vector_name

 Arg: (optional) name of the vector used for tagging the bait
 Description: Gets/sets the name of the vector carrying the tagged bait
 Returntype: string

=cut

sub vector_name {

  my $self = shift;
  $self->{'vector_name'} = shift if @_;
  if (!defined($self->{'vector_name'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT st.vector_name
                             FROM sequence_tag st, MSexperiment mse
                             WHERE mse.experimentID = ?
                             AND mse.sequence_tagID = st.sequence_tagID");
    $sth->execute($self->ID);
    my ($vector) = $sth->fetchrow_array();
    if ($vector) {
      $self->{'vector_name'} = $vector;
    }
    $sth->finish();
  }
  return $self->{'vector_name'};

}

=head2 get_minimal_gene_list

 Arg: (optional) set to 1 to remove contaminants
 Description: Gets the genes obtained by most parsimonious assignment of
              peptides found in this MS experiment.
              Sets gene attribute is_contaminant if a gene is considered
              a contaminant in the experiment.
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_minimal_gene_list {

  my $self = shift;
  my $rm_contaminants = shift;

  if (!defined($self->{'genes'})) {
    my $dbh = $self->{'database_handle'};
    my $gh = $self->{'DBConnection'}->get_GeneHandle();
    my $query;
    if ($rm_contaminants) {
      $query = qq(SELECT DISTINCT pga.geneID,pga.is_contaminant
                  FROM peptide_gene_assignment pga
                  WHERE pga.experimentID = ?
                  AND pga.is_contaminant is null);
    }
    else {
      $query = qq(SELECT DISTINCT pga.geneID,pga.is_contaminant
                  FROM peptide_gene_assignment pga
                  WHERE pga.experimentID = ?);
    }
    my $sth = $dbh->prepare($query);
    $sth->execute($self->ID);
    while (my ($geneID,$is_contaminant) = $sth->fetchrow_array()) {
      if ($geneID) {
	my $gene = $gh->get_by_id($geneID);
	$gene->is_contaminant($is_contaminant);
	push @{$self->{'genes'}},$gene;
      }
    }
  }
  return @{$self->{'genes'}} if defined($self->{'genes'});

}

=head2 get_proteins_from_minimal_gene_list

 Arg: (optional) 1 to remove contaminants
 Description: Gets the proteins that have a peptide in this MS experiment and
              are from the genes obtained by most parsimonious assignment of
              peptides found in this MS experiment.
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_proteins_from_minimal_gene_list {

  my $self = shift;
  my $rm_contaminants = shift;

  if (!defined($self->{'proteins'})) {
    my $dbh = $self->{'database_handle'};
    my $ph = $self->{'DBConnection'}->get_ProteinHandle();
    my $query;
    if ($rm_contaminants) {
      $query = qq(SELECT DISTINCT t.proteinID
                  FROM transcript t, peptide_map_info pmi, peptide_occurrence po, peptide_gene_assignment pga
                  WHERE pga.experimentID= ?
                  AND pga.is_contaminant is null
                  AND pga.geneID=t.geneID
                  AND t.proteinID=pmi.proteinID
                  AND pmi.peptideID=po.peptideID
                  AND po.experimentID= ?);
    }
    else {
      $query = qq(SELECT DISTINCT t.proteinID
                  FROM transcript t, peptide_map_info pmi, peptide_occurrence po, peptide_gene_assignment pga
                  WHERE pga.experimentID= ?
                  AND pga.geneID=t.geneID
                  AND t.proteinID=pmi.proteinID
                  AND pmi.peptideID=po.peptideID
                  AND po.experimentID= ?);
    }
    my $sth = $dbh->prepare($query);
    $sth->execute($self->ID,$self->ID);
    while (my ($proteinID) = $sth->fetchrow_array()) {
      if ($proteinID) {
	    my $protein = $ph->get_by_id($proteinID);
		push @{$self->{'proteins'}},$protein;
      }
    }
  }
  return @{$self->{'proteins'}} if defined($self->{'proteins'});

}

=head2 get_summary_table

 Arg: (optional) set to 1 to remove contaminants
 Description: Gets a summary table of the experiment where each row consists of
              a gene and its highest scoring protein in the experiment.
              Rows are ordered by score.
              Note that genes sharing the same set of peptides appear on
              distinct rows
 Returntype: array of array

=cut

sub get_summary_table {

  my $self = shift;
  my $rm_contaminants = shift;

  if (!defined($self->{'summary'})) {
    my $dbh = $self->{'database_handle'};
    my $ph = $self->{'DBConnection'}->get_ProteinHandle();
    my $gh = $self->{'DBConnection'}->get_GeneHandle();
    my $query;
    if ($rm_contaminants) {
      $query = qq(SELECT DISTINCT t.geneID, t.proteinID, max(s.score) as maxscore, pga.is_contaminant
                  FROM peptide_gene_assignment pga, transcript t JOIN
                    (SELECT pmi.proteinID, sum(p.maxscore) as score
                     FROM peptide_map_info pmi JOIN
                       (SELECT peptideID, max(score) as maxscore
                        FROM peptide_occurrence
                        WHERE experimentID = ?
                        GROUP BY peptideID) AS p
                     ON pmi.peptideID = p.peptideID
                     GROUP BY pmi.proteinID) AS s
                  ON s.proteinID = t.proteinID
                  WHERE t.geneID = pga.geneID
                  AND pga.is_contaminant IS NULL
                  AND pga.experimentID = ?
                  GROUP BY t.geneID
                  ORDER by maxscore DESC);
    }
    else {
      $query = qq(SELECT DISTINCT t.geneID, t.proteinID, max(s.score) as maxscore, pga.is_contaminant
                  FROM peptide_gene_assignment pga, transcript t JOIN
                    (SELECT pmi.proteinID, sum(p.maxscore) as score
                     FROM peptide_map_info pmi JOIN
                       (SELECT peptideID, max(score) as maxscore
                        FROM peptide_occurrence
                        WHERE experimentID = ?
                        GROUP BY peptideID) AS p
                     ON pmi.peptideID = p.peptideID
                     GROUP BY pmi.proteinID) AS s
                  ON s.proteinID = t.proteinID
                  WHERE t.geneID = pga.geneID
                  AND pga.experimentID = ?
                  GROUP BY t.geneID
                  ORDER by maxscore DESC);
    }
    my $sth = $dbh->prepare($query);
    $sth->execute($self->ID,$self->ID);
    while (my ($geneID,$proteinID,$score,$is_contaminant) = $sth->fetchrow_array()) {
      my ($gene,$protein);
      if ($proteinID) {
	    $protein = $ph->get_by_id($proteinID);
	    $protein->score($self->ID,$score);
      }
      if ($geneID) {
      	$gene = $gh->get_by_id($geneID);
      	$gene->is_contaminant($is_contaminant) if ($is_contaminant);
      }
      if ($gene && $protein) {
      	push @{$self->{'summary'}}, [$gene,$protein];
      }
    }
  }
  return @{$self->{'summary'}} if defined($self->{'summary'});

}

=head2 cell_line

 Arg: optional, Mitocheck::CellLine
 Description: Gets/sets cell line used in the experiment
 Returntype: Mitocheck::CellLine

=cut

sub cell_line {

  my ($self,$cell_line) = @_;
  $self->{'cell_line'} = $cell_line if defined($cell_line);
  if(!defined($self->{'cell_line'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT cell_lineID
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->{'ID'});
    my ($cell_lineID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $clh = $dbc->get_CellLineHandle();
    $self->{'cell_line'} = $clh->get_by_id($cell_lineID);
  }
  return $self->{'cell_line'};
}

=head2 cellular_component

 Arg: optional, Mitocheck::CellularComponent
 Description: Gets/sets where in the cell the material for this experiment
              came from
 Returntype: Mitocheck::CellularComponent

=cut

sub cellular_component {

  my ($self,$cellular_component) = @_;
  $self->{'cellular_component'} = $cellular_component if defined($cellular_component);
  if(!defined($self->{'cellular_component'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT cellular_componentID
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->{'ID'});
    my ($cellular_componentID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $cch = $dbc->get_CellularComponentHandle();
    $self->{'cellular_component'} = $cch->get_by_id($cellular_componentID);
  }
  return $self->{'cellular_component'};
}

=head2 event

 Arg: optional, Mitocheck::Event
 Description: Gets/sets time information for this experiment
 Returntype: Mitocheck::Event

=cut

sub event {

  my ($self,$event) = @_;
  $self->{'event'} = $event if defined($event);
  if(!defined($self->{'event'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT eventID
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->{'ID'});
    my ($eventID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $eh = $dbc->get_EventHandle();
    $self->{'event'} = $eh->get_by_id($eventID);
  }
  return $self->{'event'};
}

=head2 source

 Arg: optional, Mitocheck::Source
 Description: Gets/sets origin of this experiment
 Returntype: Mitocheck::Source

=cut

sub source {

  my ($self,$source) = @_;
  $self->{'source'} = $source if defined($source);
  if(!defined($self->{'source'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sourceID
                             FROM MSexperiment
                             WHERE experimentID = ?");
    $sth->execute($self->{'ID'});
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};
}

sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  return $self->{$attribute};
}

sub DESTROY {

  my $self = shift;
  $self->_decr_exp_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
