# Author: jkh1
# 2013-04-19
#

=head1 NAME

 Mitocheck::CellularComponent

=head1 SYNOPSIS


=head1 DESCRIPTION

 Representation of a Gene Ontology cellular component.
 Negation of terms is allowed resulting in GO IDs of the form 'NOT GO:xxxxxxx'
 and GO terms starting with not.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::CellularComponent;


use strict;
use warnings;
use Scalar::Util qw(weaken);


{
  my $_cc_count = 0;
  my $_newID_count = 0;

  sub get_cc_count {
    return $_cc_count;
  }
  sub _incr_cc_count {
    return ++$_cc_count;
  }
  sub _decr_cc_count {
    return --$_cc_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, cellular component ID
 Description: Creates a new cellular component object.
 Returntype: Mitocheck::CellularComponent

=cut

sub new {

  my ($class,$dbc,$ccID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_cc_count();
  if (!defined $self->{'ID'} && !$ccID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   cellular_componentID
                 FROM     cellular_component
                 ORDER BY cellular_componentID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $ccID = "CLC"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $ccID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck cellular component ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 description

 Arg: optional, text
 Description: Gets/sets cellular component description
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $ccID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT description
    			     FROM cellular_component
                             WHERE cellular_componentID = ?");
    $sth->execute($ccID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 GOID

 Arg: optional, GO term ID
 Description: Gets/sets cellular component GO term ID.
 			  Expected to be of the form GO:xxxxxxx.
              Can start with NOT in the case of a negated term. 
 Returntype: string

=cut

sub GOID {

  my $self = shift;
  $self->{'GOID'} = shift if @_;
  if (!defined $self->{'GOID'}) {
    my $ccID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT GOID
    			     FROM cellular_component cc
    			     WHERE cellular_componentID = ?");
    $sth->execute($ccID);
    ($self->{'GOID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'GOID'};

}

=head2 GOterm

 Arg: optional, GO term
 Description: Gets/sets cellular component GO term.
 	      Starts with not in the case of negated terms.
 Returntype: string

=cut

sub GOterm {

  my $self = shift;
  $self->{'GOterm'} = shift if @_;
  if (!defined $self->{'GOterm'}) {
    my $ccID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT GOterm
    			     FROM cellular_component
    			     WHERE cellular_componentID = ?");
    $sth->execute($ccID);
    ($self->{'GOterm'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'GOterm'};

}

######################################################################
# GO-based queries require GO tables term, term2term and graph_path

=head2 get_ancestors

 Description: Gets all GO terms this component's term is a child of
 Returntype: list of strings (GO term accessions)

=cut

sub get_ancestors {

  my $self = shift;
  my $GOID = $self->{'GOID'};
  my $dbh = $self->{'database_handle'};
  my @ancestors;
  my $query = qq(SELECT DISTINCT ancestor.acc
                 FROM term
                 INNER JOIN graph_path ON (term.id=graph_path.term2_id)
                 INNER JOIN term AS ancestor ON (ancestor.id=graph_path.term1_id)
                 WHERE term.acc= ?);

  my $sth = $dbh->prepare($query);
  $sth->execute($GOID);
  while (my ($anc_acc) = $sth->fetchrow_array()) {
    push @ancestors,$anc_acc;
  }

  return @ancestors;

}

=head2 get_descendants

 Description: Gets all GO terms below this component's term
 Returntype: list of strings (GO term accessions)

=cut

sub get_descendants {

  my $self = shift;
  my $GOID = $self->{'GOID'};
  my $dbh = $self->{'database_handle'};
  my @children;
  my $query = qq(SELECT DISTINCT child.acc
                 FROM term as ancestor, graph_path, term as child
                 WHERE ancestor.id=graph_path.term1_id
                 AND child.id=graph_path.term2_id
                 AND graph_path.distance>0
                 AND ancestor.acc= ?);

  my $sth = $dbh->prepare($query);
  $sth->execute($GOID);
  while (my ($acc) = $sth->fetchrow_array()) {
    push @children,$acc;
  }

  return @children;

}


sub DESTROY {

  my $self = shift;
  $self->_decr_cc_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
