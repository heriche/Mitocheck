# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::Gene

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $gene_handle = $dbc->get_GeneHandle();

 my $gene = $gene_handle->get_by_id('GEN00000001');

 my $symbol= $gene->symbol();
 my $EnsemblID = $gene->EnsemblID();
 my @names = $gene->names();
 my @transcripts = $gene->transcripts();
 my @proteins = $gene->proteins();
 my @phenotypes = $gene->phenotypes();
 my @dsRNAs = $gene->dsRNAs();


=head1 DESCRIPTION

 Representation of a Mitocheck gene.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Gene;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_gene_count = 0;
  my $_newID_count = 0;

  sub get_gene_count {
    return $_gene_count;
  }
  sub _incr_gene_count {
    return ++$_gene_count;
  }
  sub _decr_gene_count {
    return --$_gene_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, gene ID
 Description: Creates a new gene object.
 Returntype: Mitocheck::Gene

=cut

sub new {

  my ($class,$dbc,$geneID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $class->_incr_gene_count();
  if (!defined $self->{'ID'} && !$geneID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   geneID
                 FROM     gene
                 ORDER BY geneID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }

    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $geneID = "GEN"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $geneID;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck gene ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 symbol

 Arg: optional, gene symbol
 Description: Gets/sets gene's symbol
 Returntype: string

=cut

sub symbol {

  my $self = shift;
  $self->{'symbol'} = shift if @_;
  if (!defined $self->{'symbol'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT g.symbol FROM gene g WHERE g.geneID = ?");
    $sth->execute($geneID);
    ($self->{'symbol'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'symbol'};

}

=head2 EnsemblID

 Arg: optional, Ensembl gene ID
 Description: Gets/sets gene's Ensembl ID
 Returntype: string

=cut

sub EnsemblID {

  my $self = shift;
  $self->{'EnsemblID'} = shift if @_;
  if (!defined $self->{'EnsemblID'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT g.EnsemblID FROM gene g WHERE g.geneID = ? ");
    $sth->execute($geneID);
    ($self->{'EnsemblID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'EnsemblID'};

}

=head2 description

 Arg: optional, text description of gene function
 Description Gets/sets description of gene function
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT g.description FROM gene g WHERE g.geneID = ? ");
    $sth->execute($geneID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 names

 Arg: optional, list of names
 Description: Gets/sets the gene's names
 Returntype: list of strings

=cut

sub names {

  my ($self,@names) = @_;
  push (@{$self->{'names'}},@names) if @names;
  if (!defined $self->{'names'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT DISTINCT n.name
                             FROM name n, gene_has_name ghn
                             WHERE ghn.geneID = ?
                             AND ghn.nameID = n.nameID
			     AND n.display>0");
    $sth->execute($geneID);
    while (my $n=$sth->fetchrow_array()) {
      push (@{$self->{'names'}},$n);
    }
    $sth->finish();
  }
  return @{$self->{'names'}} if (defined($self->{'names'}));

}

=head2 transcripts

 Arg: optional, list of Mitocheck::Transcript objects
 Description: Gets/sets transcripts associated with gene
 Returntype: list of Mitocheck::Transcript objects

=cut

sub transcripts {

  my ($self,@transcripts) = @_;
  push (@{$self->{'transcripts'}},@transcripts) if @transcripts;
  if (!defined $self->{'transcripts'}) {
    my $dbc = $self->{'DBConnection'};
    my $trh = $dbc->get_TranscriptHandle();
    @{$self->{'transcripts'}} = $trh->get_all_by_gene($self);
  }
  return @{$self->{'transcripts'}} if (defined($self->{'transcripts'}));

}

=head2 proteins

 Arg: optional, list of Mitocheck::Protein objects
 Description: Gets/sets proteins associated with gene
 Returntype: list of Mitocheck::Protein objects

=cut

sub proteins {

  my ($self,@proteins) = @_;
  @{$self->{'proteins'}} = @proteins if @proteins;
  if (!defined $self->{'proteins'}) {
    my $dbc = $self->{'DBConnection'};
    my $prh = $dbc->get_ProteinHandle();
    @{$self->{'proteins'}} = $prh->get_all_by_gene($self);
  }
  return @{$self->{'proteins'}} if (defined($self->{'proteins'}));

}

=head2 phenotypes

 Arg: none
 Description: Gets all phenotypes associated with gene
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub phenotypes {

  my $self = shift;
  @{$self->{'phenotypes'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $ph = $dbc->get_PhenotypeHandle();
  @{$self->{'phenotypes'}} = $ph->get_all_by_gene($self);

  return @{$self->{'phenotypes'}};

}

=head2 phenoclasses

 Arg: optional, 'id' flag to return class IDs
 Description: Gets all phenotype classes the gene belongs to
 Returntype: list

=cut

sub phenoclasses {

  my $self = shift;
  my $ids = shift;
  @{$self->{'phenoclasses'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $geneID = $self->{'ID'};
  if (lc($ids) eq 'id') {
    my $sth = $dbh->prepare("SELECT DISTINCT phc.phenoclassID
                             FROM gene_has_phenotype ghp, phenotype_has_phenoclass phc
                             WHERE ghp.geneID = ?
                             AND ghp.phenotypeID = phc.phenotypeID
                             ORDER BY phenoclassID");
    $sth->execute($geneID);
    while (my ($pcID)=$sth->fetchrow_array()) {
      push @{$self->{'phenoclasses'}},$pcID;
    }
  }
  else {
    my $sth = $dbh->prepare("SELECT DISTINCT pc.name
                             FROM gene_has_phenotype ghp, phenotype_has_phenoclass phc, phenoclass pc
                             WHERE ghp.geneID = ?
                             AND ghp.phenotypeID = phc.phenotypeID
                             AND phc.phenoclassID=pc.phenoclassID
                             ORDER BY pc.phenoclassID");
    $sth->execute($geneID);
    while (my ($pc)=$sth->fetchrow_array()) {
      push @{$self->{'phenoclasses'}},$pc;
    }
  }
  return @{$self->{'phenoclasses'}};
}

=head2 localizations

 Arg: none
 Description: Gets all subcellular localizations associated with gene
 Returntype: list of Mitocheck::Localization objects

=cut

sub localizations {

  my $self = shift;
  @{$self->{'localizations'}} = ();
  my $dbc = $self->{'DBConnection'};
  my @proteins = $self->proteins;
  foreach my $protein(@proteins) {
    my @localizations = $protein->localizations();
    push @{$self->{'localizations'}}, @localizations if (@localizations && $localizations[0]);
  }
  return @{$self->{'localizations'}};
}

=head2 domains

 Arg: optional, list of Mitocheck::Domain objects
 Description: Gets/sets domains that are part of the proteins
              produced by the gene
 Returntype: list of Mitocheck::Domain objects

=cut

sub domains {

  my ($self,@domains) = shift;
  @{$self->{'domains'}} = @domains if @domains;
  if (!defined $self->{'domains'}) {
    my $dh = $self->{'DBConnection'}->get_DomainHandle;
    @{$self->{'domains'}} = $dh->get_all_by_gene($self);
  }
  return @{$self->{'domains'}} if (defined($self->{'domains'}));

}

=head2 dsRNAs

 Arg: (optional) Mitocheck::Source object
 Description: Gets all dsRNAs that map to gene.
              If a source is specified, limits to dsRNAs used
              in the corresponding screen/experiment
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub dsRNAs {

  my $self = shift;
  my $source = shift if @_;
  @{$self->{'dsRNAs'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $drh = $dbc->get_dsRNAHandle();
  @{$self->{'dsRNAs'}} = $drh->get_all_by_gene($self,$source);

  return @{$self->{'dsRNAs'}};

}

=head2 image_sets

 Arg: (optional) class, currently 'phenotype' or 'localization'
 Description: Gets image sets of selected class associated with
              the gene. If no class is specified then gets all
              image sets for the gene.
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub image_sets {

  my ($self,$class) = @_;
  @{$self->{'image_sets'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $ish = $dbc->get_ImageSetHandle();
  @{$self->{'image_sets'}} = $ish->get_all_by_gene($self,$class);

  return @{$self->{'image_sets'}};

}


=head2 chromosome

 Arg: none
 Description: Gets chromosome the gene maps to
 Returntype: string

=cut

sub chromosome {

  my $self = shift;
  $self->{'chromosome'} = "";
  my $geneID = $self->{'ID'};
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # retrieves chromosome where a dsRNA targeting the gene maps
  my $sth = $dbh->prepare("SELECT drmi.chromosome
                           FROM dsRNA_map_info drmi
                           WHERE drmi.geneID = ?
                           LIMIT 1 ");
  $sth->execute($geneID);
  ($self->{'chromosome'}) = $sth->fetchrow_array();
  $sth->finish();

  return $self->{'chromosome'};

}

=head2 TreefamAC

 Arg: none
 Description : Gets Treefam accession number for this gene's family.
 Returntype: string

=cut

sub TreefamAC {

  my $self = shift;
  $self->{'TreefamAC'} = shift if @_;
  if (!defined $self->{'TreefamAC'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT g.TreefamAC FROM gene g WHERE g.geneID = ?");
    $sth->execute($geneID);
    ($self->{'TreefamAC'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'TreefamAC'};

}

=head2 orthologs

 Arg: optional, species name
 Description : Gets orthologs of gene, limit to given species
               if any.
 Returntype: list of Mitocheck::Ortholog objects

=cut

sub orthologs {

  my $self = shift;
  my $species = shift;
  @{$self->{'orthologs'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $oh = $dbc->get_OrthologHandle();
  @{$self->{'orthologs'}} = $oh->get_all_by_gene($self,$species);

  return @{$self->{'orthologs'}};

}

=head2 paralogs

 Arg: none
 Description : Gets paralogs of gene.
 Returntype: list of Mitocheck::Gene objects

=cut

sub paralogs {

  my $self = shift;

  @{$self->{'paralogs'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $geneID = $self->{'ID'};
  my $gh = $dbc->get_GeneHandle();
  my $sth = $dbh->prepare("SELECT DISTINCT g.geneID
                           FROM gene g, paralog p
                           WHERE (p.geneID= ?
                           AND p.paralogID = g.geneID)
                           UNION
                           SELECT DISTINCT g.geneID
                           FROM gene g, paralog p
                           WHERE (p.paralogID = ?
                           AND p.geneID = g.geneID) ");
  $sth->execute($geneID,$geneID);
  my %seen=();
  while (my ($id)=$sth->fetchrow_array()) {
    next if $seen{$id}++;
    push (@{$self->{'paralogs'}},$gh->get_by_id($id)) if ($id);
  }

  return @{$self->{'paralogs'}};
}

=head2 get_peptides

 Arg1: Mitocheck::MSexperiment object
 Arg2: (optional) 1 to get only one occurrence of each peptide.
       This selects the highest scoring occurrence of each peptide
       that maps to the gene's proteins
 Description : Gets all peptides assigned to the gene in the given MS experiment
 Returntype: list of Mitocheck::Peptide objects

=cut

sub get_peptides {

  my $self = shift;
  my $exp = shift;
  my $uniq = shift if @_;
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID : $exp;
  my $dbc = $self->{'DBConnection'};

  if (!defined($self->{'peptides'}{$expID})) {
    my $dbc = $self->{'DBConnection'};
    my $peph = $dbc->get_PeptideHandle();
    my @peptides = $peph->get_all_by_gene($self,$exp,$uniq);
    $self->{'peptides'}{$expID} = \@peptides if (@peptides);
  }

  return @{$self->{'peptides'}{$expID}} if (defined($self->{'peptides'}{$expID}));

}

=head2 get_complexes

 Arg: none
 Description: Gets all complexes involving this gene
 Returntype: list of Mitocheck::Complex objects

=cut

sub get_complexes {

  my $self = shift;
  @{$self->{'phenotypes'}} = ();
  my $dbc = $self->{'DBConnection'};
  my $ch = $dbc->get_ComplexHandle();
  @{$self->{'complexes'}} = $ch->get_all_by_gene($self);

  return @{$self->{'complexes'}};

}

=head2 is_contaminant

 Arg: none
 Description: Gets/sets whether the gene is considered a contaminant in mass spectrometry experiments
 Returntype: 1 if contaminant

=cut

sub is_contaminant {

  my $self = shift;
  $self->{'is_contaminant'} = shift if @_;
  if (!defined($self->{'is_contaminant'})) {
  	my $dbc = $self->{'DBConnection'};
  	my $dbh = $dbc->{'database_handle'};
  	my $geneID = $self->{'ID'};
  	my $sth = $dbh->prepare("SELECT DISTINCT is_contaminant
                             FROM peptide_gene_assignment
                             WHERE geneID = ? ");
  	$sth->execute($geneID);
  	($self->{'is_contaminant'}) = $sth->fetchrow_array();
  }

  return $self->{'is_contaminant'};

}

sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  return $self->{$attribute};
}

sub DESTROY {

  my $self = shift;
  $self->_decr_gene_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}


1;
