# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::PhenotypeHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $phenotype_handle = $dbc->get_PhenotypeHandle();

 my $phenotype = $phenotype_handle->get_by_id('PHN00000001');


=head1 DESCRIPTION

 Enables retrieval of Mitocheck phenotype objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::PhenotypeHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Phenotype;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new Phenotype object handle.
 Returntype: Mitocheck::PhenotypeHandle

=cut

sub new {

  my ($class,$dbc) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;

  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: phenotype ID
 Description: Gets phenotype with given ID
 Returntype: Mitocheck::Phenotype object

=cut

sub get_by_id {

  my ($self,$phenotypeID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if phenotype exists in database
  my $query = qq(SELECT COUNT(*) FROM phenotype WHERE phenotypeID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($phenotypeID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::Phenotype($dbc,$phenotypeID);

}

=head2 get_all_phenotypes

 Description: Gets all phenotypes listed in the database
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub get_all_phenotypes {

  my ($self) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my @phenotypes;
  my $query = qq(SELECT DISTINCT phenotypeID FROM phenotype);
  my $sth= $dbh->prepare($query);
  $sth->execute();
  while (my ($phenotypeID) = $sth->fetchrow_array()) {
  	if ($phenotypeID) {
      push @phenotypes,Mitocheck::Phenotype->new($dbc,$phenotypeID);
  	}
  }

  return @phenotypes;

}


=head2 get_by_description

 Arg: string, phenotype description
 Description: Gets phenotype with given description
 Returntype: Mitocheck::Phenotype object

=cut

sub get_by_description {

  my ($self,$description) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT phenotypeID FROM phenotype
                 WHERE description = ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($description);
  my ($phenotypeID) = $sth->fetchrow_array();
  return undef unless $phenotypeID;

  return new Mitocheck::Phenotype($dbc,$phenotypeID);

}


=head2 get_all_by_gene

 Arg: Mitocheck::Gene or geneID
 Description: Gets phenotypes of given gene
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub get_all_by_gene {

  my ($self,$gene) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $sth = $dbh->prepare("SELECT DISTINCT ghp.phenotypeID
                           FROM gene_has_phenotype ghp
                           WHERE ghp.geneID = ?
                           ORDER BY phenotypeID");
  $sth->execute($geneID);
  my @phenotypes;
  while (my ($id)=$sth->fetchrow_array()) {
    my $phenotype = $self->get_by_id($id);
    $phenotype->{'caller'} = $gene;
    push (@phenotypes,$phenotype);
  }
  $sth->finish();

  return @phenotypes;

}

=head2 get_all_by_dsRNA

 Arg: Mitocheck::dsRNA or dsRNAID
 Description: Gets phenotypes obtained with given dsRNA
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub get_all_by_dsRNA {

  my ($self,$dsRNA) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $dsRNAID = ref($dsRNA) ? $dsRNA->ID() : $dsRNA;
  my $sth = $dbh->prepare("SELECT DISTINCT drgp.phenotypeID
                           FROM dsRNA_gives_phenotype drgp
                           WHERE drgp.oligo_pairID = ? ");
  $sth->execute($dsRNAID);
  my @phenotypes;
  while (my ($id)=$sth->fetchrow_array()) {
    my $phenotype = $self->get_by_id($id);
    $phenotype->{'caller'} = $dsRNA;
    push (@phenotypes,$phenotype);
  }
  $sth->finish();

  return @phenotypes;
}

=head2 get_all_by_image_set

 Arg: Mitocheck::ImageSet
 Description: Gets phenotypes associated with given image set
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub get_all_by_image_set {

  my ($self,$set) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $setID = $set->ID();
  my $sth = $dbh->prepare("SELECT DISTINCT phenotypeID
                           FROM image_set_has_phenotype
                           WHERE image_setID= ?");
  $sth->execute($setID);
  my @phenotypes;
  while (my ($id)=$sth->fetchrow_array()) {
    my $phenotype = $self->get_by_id($id);
    $phenotype->{'caller'} = $set;
    push (@phenotypes,$phenotype);
  }
  $sth->finish();

  return @phenotypes;
}

=head2 get_all_by_screen

 Arg1: Mitocheck::Source
 Arg2: (optional) 1 to retrieve only phenotypes that have
        been used to annotate dsRNAs in the screen.
 Description: Gets phenotypes associated with given screen
 Returntype: list of Mitocheck::Phenotype objects

=cut

sub get_all_by_screen {

  my ($self,$source,$used) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $sourceID = $source->ID();
  my $sth;
  unless($used) {
    $sth = $dbh->prepare("SELECT DISTINCT phenotypeID
                          FROM phenotype
                          WHERE sourceID = ?");
  }
  else {
    $sth = $dbh->prepare("SELECT DISTINCT p.phenotypeID
                          FROM phenotype p, dsRNA_gives_phenotype dgp
                          WHERE p.sourceID = ?
                          AND p.phenotypeID = dgp.phenotypeID");
  }
  $sth->execute($sourceID);
  my @phenotypes;
  while (my ($id)=$sth->fetchrow_array()) {
    my $phenotype = $self->get_by_id($id);
    push (@phenotypes,$phenotype);
  }
  $sth->finish();

  return @phenotypes;
}

=head2 new_phenotype

 Arg(required): -description => text
 Arg(required): -source => Mitocheck::Source object or sourceID
 Arg(required): -annotation_type => 'manual' or 'automatic'
 Arg(required): -origin => method used to obtain the phenotype,
                e.g. dsRNA
 Arg(optional): -image_sets => list of Mitocheck::ImageSet objects
 Arg(optional): -dsRNAs => list of Mitocheck::dsRNA objects
 Description: creates a new phenotype object
 Returntype: Mitocheck::Phenotype

=cut

sub new_phenotype {

  my $self = shift;
  my %phenotype = @_;
  # required attributes = ('-source','-description','-annotation_type','-origin');
  unless (defined($phenotype{'-description'}) && defined($phenotype{'-source'}) && defined($phenotype{'-annotation_type'}) && defined($phenotype{'-origin'})) {
    croak "WARNING: Phenotype requires at least the following attributes: -description, -source, -annotation_type, -origin";
  }
  my $dbc = $self->{'DBConnection'};

  my $phenotype = Mitocheck::Phenotype->new($dbc);
  $phenotype->description($phenotype{'-description'});
  delete($phenotype{'-description'});
  $phenotype->origin($phenotype{'-origin'});
  delete($phenotype{'-origin'});
  $phenotype->annotation_type($phenotype{'-annotation_type'});
  delete($phenotype{'-annotation_type'});
  my $source = ref($phenotype{'-source'}) eq 'Mitocheck::Source'? $phenotype{'-source'}: $dbc->get_SourceHandle()->get_by_id($phenotype{'-source'});
  $phenotype->source($source);
  delete($phenotype{'-source'});
  if ($phenotype{'-image_sets'}) {
    $phenotype->image_sets(@{$phenotype{'-image_sets'}});
    delete($phenotype{'-image_sets'});
  }
  if ($phenotype{'-dsRNAs'}) {
    $phenotype->dsRNAs(@{$phenotype{'-dsRNAs'}});
    delete($phenotype{'-dsRNAs'});
  }

  return $phenotype;

}

=head2 store

 Arg: Mitocheck::Phenotype
 Description: Enters phenotype and associated data in Mitocheck
              database
 Returntype: Mitocheck::Phenotype, normally same as Arg

=cut

sub store {

  my ($self,$phenotype) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $phenotypeID;
  # check if phenotype already in database
  # two phenotypes are the same if they have
  # same description, source, origin and
  # annotation type
  my $query = qq(SELECT phenotypeID
                 FROM phenotype
                 WHERE description= ?
                 AND sourceID = ?
                 AND origin = ?
                 AND annotation_type= ? );
  my $sth = $dbh->prepare($query);
  $sth->execute($phenotype->description,$phenotype->source->ID,$phenotype->origin,$phenotype->annotation_type);
  my ($phID) = $sth->fetchrow_array();
  $sth->finish();
  if ($phID) {
    $phenotype->ID($phID);
  }
  else {
    my $phID = $phenotype->ID;
    my $desc = $phenotype->description;
    my $ori = $phenotype->origin;
    my $annot = $phenotype->annotation_type;
    my $sourceID = $phenotype->source->ID;

    my $query = qq(INSERT INTO phenotype (phenotypeID,description,sourceID,origin,annotation_type) VALUES ('$phID','$desc','$sourceID','$ori','$annot'));
    my $rows = $dbh->do($query);
  }
  $phenotypeID = $phenotype->ID();
  my @image_set = $phenotype->image_sets();
  if (@image_set) {
    foreach my $set(@image_set) {
      next unless $set;
      my $image_setID = $set->ID();
      if (defined($image_setID)) {
	my $query = "INSERT IGNORE INTO image_set_has_phenotype (image_setID,phenotypeID) VALUES('$image_setID','$phenotypeID')";
	my $rows = $dbh->do($query);
      }
    }
  }
  else {
    carp "\tWARNING: No image set\n";
  }
  if($phenotype->dsRNAs()) {
    foreach my $dsRNA ($phenotype->dsRNAs()) {
      next unless $dsRNA;
      my $dsRNAID = $dsRNA->ID();
      if (defined($dsRNAID)) {
	my $reproducibility = $phenotype->reproducibility($dsRNA);
	my $query = "INSERT IGNORE INTO dsRNA_gives_phenotype (oligo_pairID,phenotypeID,reproducibility) VALUES('$dsRNAID','$phenotypeID','$reproducibility')";
	my $rows = $dbh->do($query);
      }
    }
  }
  $phenotype->_decr_newID_count() if $phenotype->{'is_new'};
  $phenotype->{'is_new'} = undef;

  return $phenotype;
}

1;

