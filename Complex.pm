# Author: jkh1
# 2009-07-30

=head1 NAME

 Mitocheck::Complex

=head1 SYNOPSIS


=head1 DESCRIPTION

 Representation of a Mitocheck protein complex.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Complex;


use strict;
use warnings;
use Carp;

use Scalar::Util qw(weaken);

{
  my $_complex_count = 0;
  my $_newID_count = 0;

  sub get_complex_count {
    return $_complex_count;
  }
  sub _incr_complex_count {
    return ++$_complex_count;
  }
  sub _decr_complex_count {
    return --$_complex_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}



=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, complex ID
 Description: Creates a new complex object.
 Returntype: Mitocheck::Complex

=cut

sub new {

  my ($class,$dbc,$complexID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $class->_incr_complex_count();
  if (!defined $self->{'ID'} && !$complexID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   complexID
                 FROM     complex
                 ORDER BY complexID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }

    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $complexID = "CMP"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $complexID;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: (optional) Mitocheck complex ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 name

 Arg: (optional), complex name
 Description: Gets/sets the complex name
 Returntype: string

=cut

sub name {

  my $self = shift;
  $self->{'name'} = shift if @_;
  if (!defined $self->{'name'}) {
    my $complexID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT name FROM complex WHERE complexID = ?");
    $sth->execute($complexID);
    ($self->{'name'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'name'};

}

=head2 inference

 Arg: optional, string
 Description: Gets/sets how the complex was inferred
              i.e. by curation, prediction method...
 Returntype: string

=cut

sub inference {

  my $self = shift;
  $self->{'inference'} = shift if @_;
  if (!defined $self->{'inference'}) {
    my $complexID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT inference FROM complex WHERE complexID = ?");
    $sth->execute($complexID);
    ($self->{'inference'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'inference'};

}

=head2 cellular_component

 Arg: optional, Mitocheck::CellularComponent object
 Description: Gets/sets cellular component where the complex is found
 Returntype: Mitocheck::CellularComponent object

=cut

sub cellular_component {

  my $self = shift;
  $self->{'cellular_component'} = shift if @_;
  if (!defined $self->{'cellular_component'}) {
    my $complexID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT cellular_componentID FROM complex WHERE complexID = ?");
    $sth->execute($complexID);
    my ($ccID) = $sth->fetchrow_array();
    $sth->finish();
    my $cch = $dbc->get_CellularComponentHandle();
    $self->{'cellular_component'} = $cch->get_by_id($ccID);
  }

  return $self->{'cellular_component'};

}

=head2 event

 Arg: optional, Mitocheck::Event
 Description: Gets/sets when this complex is found
 Returntype: Mitocheck::Event

=cut

sub event {

  my ($self,$event) = @_;
  $self->{'event'} = $event if defined($event);
  if(!defined($self->{'event'})) {
  	my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT eventID
                             FROM complex
                             WHERE complexID = ?");
    $sth->execute($self->{'ID'});
    my ($eventID) = $sth->fetchrow_array();
    $sth->finish();
    my $eh = $dbc->get_EventHandle();
    $self->{'event'} = $eh->get_by_id($eventID);
  }
  return $self->{'event'};
}

=head2 description

 Arg: optional, text description of the complex
 Description Gets/sets description of the complex
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $geneID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT description FROM complex WHERE complexID = ? ");
    $sth->execute($geneID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 source

 Arg: (optional) Mitocheck::Source
 Description: Gets/sets origin of the complex
 Returntype: Mitocheck::Source object

=cut

sub source {

  my ($self,$source) = @_;

  if (defined $source) {
    if (ref($source) ne 'Mitocheck::Source') {
      croak "Argument must be a Mitocheck::Source object";
    }
    else { $self->{'source'} = $source }
  }

  if (!defined $self->{'source'}) {
    my $complexID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sourceID
                             FROM complex
                             WHERE complexID = ? ");
    $sth->execute($complexID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};

}

=head2 proteins

 Arg: (optional) list of Mitocheck::Protein objects
 Description: Gets/sets components of the complex
 Returntype: list of Mitocheck::Protein objects

=cut

sub proteins {

  my ($self,@proteins) = @_;

  if (@proteins) {
    @{$self->{'proteins'}} = @proteins;
  }

  if (!defined $self->{'proteins'}) {
    my $complexID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $ph = $dbc->get_ProteinHandle();
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT proteinID, stochiometry
                             FROM complex_membership
                             WHERE complexID = ?");
    $sth->execute($complexID);
    while (my ($proteinID,$n)=$sth->fetchrow_array()) {
      my $protein = $ph->get_by_id($proteinID);
      $protein->stochiometry($complexID,$n) if (defined($n));
      push @{$self->{'proteins'}},$protein if ($protein);
    }
    $sth->finish();
  }
  return @{$self->{'proteins'}} if defined($self->{'proteins'});

}

=head2 members

 Arg: (optional) list of Mitocheck::Protein objects
 Description: Gets/sets components of the complex
 Returntype: list of Mitocheck::Protein objects

=cut

sub members {

  my $self = shift;
  return $self->proteins(@_);

}

=head2 interactions

 Arg: (optional) list of Mitocheck::Interaction objects
 Description: Gets/sets interactions between complex members
 Returntype: list of Mitocheck::Interaction objects

=cut

sub interactions {

  my ($self,@interactions) = @_;

  if (@interactions) {
    @{$self->{'interactions'}} = @interactions;
  }

  if (!defined $self->{'interactions'}) {
    my $complexID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $ih = $dbc->get_InteractionHandle();
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT DISTINCT io.interaction_occurrenceID
                             FROM protein_interaction pi,
                                  complex_membership cm,
                                  interaction_occurrence io,
                                  complex c
                             WHERE c.complexID= ?
                             AND c.sourceID = io.sourceID
                             AND c.eventID = io.eventID
                             AND c.cellular_componentID = io.cellular_componentID
                             AND c.complexID = cm.complexID
                             AND io.interactionID = pi.interactionID
                             AND pi.proteinID IN (SELECT proteinID 
                                                  FROM complex_membership
                                                  WHERE complexID = ?)
                             AND pi.interactorID IN (SELECT proteinID 
                                                     FROM complex_membership
                                                     WHERE complexID = ?)");
    $sth->execute($complexID,$complexID,$complexID);
    while (my ($interaction_occurrenceID)=$sth->fetchrow_array()) {
      my $interaction = $ih->get_by_id($interaction_occurrenceID);
      push @{$self->{'interactions'}},$interaction if ($interaction);
    }
    $sth->finish();
  }
  return @{$self->{'interactions'}} if defined($self->{'interactions'});

}

sub DESTROY {

  my $self = shift;
  $self->_decr_complex_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
