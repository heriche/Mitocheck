# Author: jkh1
# 2012-09-27
#

=head1 NAME

 Mitocheck::MeasurementHandle

=head1 SYNOPSIS



=head1 DESCRIPTION

 Enables retrieval of Mitocheck measurement objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::MeasurementHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Measurement;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new measurement object handle
 Returntype: Mitocheck::MeasurementHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: experiment ID
 Description: Gets measurement with given ID
 Returntype: Mitocheck::Measurement object

=cut

sub get_by_id {

  my ($self,$ID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # Check if measurement exists in database
  my $query = qq(SELECT COUNT(*) FROM measurement WHERE measurementID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($ID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::Measurement->new($dbc,$ID);

}

=head2 get_all_by_name

 Arg: string, measurement name
 Description: Gets all measurements with given name
 Returntype: list of Mitocheck::Measurement objects

=cut

sub get_all_by_name {

  my ($self,$name) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $query = qq(SELECT DISTINCT measurementID FROM measurement WHERE name = ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($name);
  my @measurements;
  while (my ($measurementID) = $sth->fetchrow_array()) {
  	if (defined($measurementID)) {
  	  push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	}
  }
  return @measurements;
}

=head2 get_all_by_protein

 Arg: Mitocheck::Protein object or protein ID
 Description: Gets all measurements related to the given protein
 Returntype: list of Mitocheck::Measurement objects

=cut

sub get_all_by_protein {

  my ($self,$protein) = @_;
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $query = qq(SELECT DISTINCT me.measurementID
  		 FROM measurement me, localization_occurrence_has_measurement lohm, protein_localization_occurrence plo
  		 WHERE plo.proteinID = ?
  		 AND plo.localization_occurrenceID = lohm.localization_occurrenceID
  		 AND lohm.measurementID = me.measurementID);
  my $sth= $dbh->prepare ($query);
  $sth->execute($proteinID);
  my @measurements;
  while (my ($measurementID) = $sth->fetchrow_array()) {
  	if (defined($measurementID)) {
  	  push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	}
  }
  return @measurements;
}

=head2 get_all_by_localization

 Arg1: Mitocheck::Localization object or localization occurrence ID
 Arg2: (optional) Mitocheck::FCSexperiment object or FCS experiment ID
 Description: Gets all measurements related to the given localization occurrence,
              optionally limited to the given experiment
 Returntype: list of Mitocheck::Measurement objects

=cut

sub get_all_by_localization {

  my ($self,$localization,$FCSexp) = @_;
  my $localizationID = ref($localization) ? $localization->ID() : $localization;
  my $FCSexpID = ref($FCSexp) ? $FCSexp->ID : $FCSexp;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my @measurements;
  if ($FCSexpID) {
    my $query = qq(SELECT DISTINCT m.measurementID
   	           FROM localization_occurrence_has_measurement lohm, measurement m
  		   WHERE lohm.localization_occurrenceID = ?
  		   AND lohm.measurmentID = m.measurementID
  		   AND m.FCSexperimentID = ?);
    my $sth= $dbh->prepare ($query);
    $sth->execute($localizationID,$FCSexpID);
    while (my ($measurementID) = $sth->fetchrow_array()) {
  	  if (defined($measurementID)) {
  	    push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	  }
    }
  }
  else {
    my $query = qq(SELECT DISTINCT measurementID
   	           FROM localization_occurrence_has_measurement
  		   WHERE localization_occurrenceID = ? );
    my $sth= $dbh->prepare ($query);
    $sth->execute($localizationID);
    while (my ($measurementID) = $sth->fetchrow_array()) {
  	  if (defined($measurementID)) {
  	    push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	  }
    }
  }

  return @measurements;

}

=head2 get_all_by_event

 Arg1: Mitocheck::Event object or event ID
 Arg2: (optional) Mitocheck::FCSexperiment object or FCS experiment ID
 Description: Gets all measurements related to the given event,
              optionally limited to the given experiment
 Returntype: list of Mitocheck::Measurement objects

=cut

sub get_all_by_event {

  my ($self,$event,$FCSexp) = @_;
  my $eventID = ref($event) ? $event->ID() : $event;
  my $FCSexpID = ref($FCSexp) ? $FCSexp->ID : $FCSexp;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my @measurements;
  if ($FCSexpID) {
    my $query = qq(SELECT DISTINCT m.measurementID
   	           FROM protein_localization_occurrence plo, localization_occurrence_has_measurement lohm, measurement m
  		   WHERE plo.eventID = ?
  		   AND lohm.localization_occurrenceID = plo.localization_occurrenceID
  		   AND lohm.measurmentID = m.measurementID
  		   AND m.FCSexperimentID = ?);
    my $sth= $dbh->prepare ($query);
    $sth->execute($eventID,$FCSexpID);
    while (my ($measurementID) = $sth->fetchrow_array()) {
  	  if (defined($measurementID)) {
  	    push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	  }
    }
  }
  else {
    my $query = qq(SELECT DISTINCT lohm.measurementID
   	           FROM protein_localization_occurrence plo, localization_occurrence_has_measurement lohm
  		   WHERE plo.eventID = ?
  		   AND lohm.localization_occurrenceID = plo.localization_occurrenceID);
    my $sth= $dbh->prepare ($query);
    $sth->execute($eventID);
    while (my ($measurementID) = $sth->fetchrow_array()) {
  	  if (defined($measurementID)) {
  	    push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	  }
    }
  }

  return @measurements;

}

=head2 get_all_by_cellular_component

 Arg1: Mitocheck::CellularComponent object or cellular component ID
 Arg2: (optional) Mitocheck::FCSexperiment object or FCS experiment ID
 Description: Gets all measurements related to the given cellular component,
              optionally limited to the given experiment
 Returntype: list of Mitocheck::Measurement objects

=cut

sub get_all_by_cellular_component {

  my ($self,$cell_comp,$FCSexp) = @_;
  my $cell_compID = ref($cell_comp) ? $cell_comp->ID() : $cell_comp;
  my $FCSexpID = ref($FCSexp) ? $FCSexp->ID : $FCSexp;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my @measurements;
  if ($FCSexpID) {
    my $query = qq(SELECT DISTINCT m.measurementID
   	           FROM protein_localization_occurrence plo, localization_occurrence_has_measurement lohm, measurement m
  		   WHERE plo.cellular_componentID = ?
  		   AND lohm.localization_occurrenceID = plo.localization_occurrenceID
  		   AND lohm.measurmentID = m.measurementID
  		   AND m.FCSexperimentID = ?);
    my $sth= $dbh->prepare ($query);
    $sth->execute($cell_compID,$FCSexpID);
    while (my ($measurementID) = $sth->fetchrow_array()) {
  	  if (defined($measurementID)) {
  	    push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	  }
    }
  }
  else {
    my $query = qq(SELECT DISTINCT lohm.measurementID
   	           FROM protein_localization_occurrence plo, localization_occurrence_has_measurement lohm
  		   WHERE plo.cellular_componentID = ?
  		   AND lohm.localization_occurrenceID = plo.localization_occurrenceID);
    my $sth= $dbh->prepare ($query);
    $sth->execute($cell_compID);
    while (my ($measurementID) = $sth->fetchrow_array()) {
  	  if (defined($measurementID)) {
  	    push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	  }
    }
  }

  return @measurements;

}

=head2 get_all_by_interaction

 Arg: Mitocheck::Interaction object or interaction occurence ID
 Description: Gets all measurements related to the given interaction occurrence
 Returntype: list of Mitocheck::Measurement objects

=cut

sub get_all_by_interaction {

  my ($self,$interaction) = @_;
  my $interactionID = ref($interaction) ? $interaction->ID() : $interaction;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $query = qq(SELECT DISTINCT measurementID
  		 FROM interaction_occurrence_has_measurement
  		 WHERE interaction_occurrenceID = ? );
  my $sth= $dbh->prepare ($query);
  $sth->execute($interactionID);
  my @measurements;
  while (my ($measurementID) = $sth->fetchrow_array()) {
  	if (defined($measurementID)) {
  	  push @measurements,Mitocheck::Measurement->new($dbc,$measurementID);
  	}
  }
  return @measurements;

}

=head2 new_measurement

 Arg: key=>value pairs
 Description: creates a new measurement object
 Returntype: Mitocheck::Measurement

=cut

sub new_measurement {

  my $self = shift;
  my %measurement = @_;
  my $dbc = $self->{'DBConnection'};

  unless (defined $measurement{'-name'}) {
    croak "Error: -name argument required for measurement";
  }
  unless (defined $measurement{'-statistics'}) {
    croak "Error: -statistics argument required for measurement";
  }
  unless (defined $measurement{'-localization'} || defined $measurement{'-interaction'}) {
    croak "Error: new measurement requires one of the two arguments -localization or -interaction";
  }
  $measurement{'-unit'} ||= "";
  my $measurement = Mitocheck::Measurement->new($dbc);
  $measurement->name($measurement{'-name'});
  $measurement->statistics($measurement{'-statistics'});
  $measurement->unit($measurement{'-unit'});
  $measurement->localization($measurement{'-localization'}) if ($measurement{'-localization'});
  $measurement->interaction($measurement{'-interaction'}) if ($measurement{'-interaction'});
  $measurement->FCSexperiment($measurement{'-FCSexperiment'});
  $measurement->description($measurement{'-description'});
  $measurement->component($measurement{'-component'});

  return $measurement;

}

=head2 store

 Arg: Mitocheck::Measurement
 Description: Enters measurement and associated data in Mitocheck database
 Returntype: Mitocheck::Measurement, normally same as Arg

=cut

sub store {

  my ($self,$measurement) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $stats = $measurement->statistics;
  my $statID;
  if ($stats) {  # Record values
    # Find last statID used
    my $query=qq(SELECT   statisticsID
                 FROM     statistics
                 ORDER BY statisticsID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array() || 0;
    $statID = $lastID + 1;
    my $mean = $stats->{'mean'};
    my $stddev = $stats->{'stddev'};
    $query = qq(INSERT INTO statistics (statisticsID,mean,stddev)
                VALUES ('$statID','$mean','$stddev'));
    my $rows = $dbh->do($query);
  }
  else {
  	croak "\nERROR: Measurement has no statistics";
  }

  # Check if measurement already in database:
  # two measurements are the same if they have
  # same name, statistics, component and experiment
  my $FCSexpID = $measurement->FCSexperiment->ID;
  my $name = $measurement->name;
  my $component = $measurement->component;
  my $query = qq(SELECT measurementID
                 FROM measurement
                 WHERE name= ?
                 AND statisticsID = ?
                 AND FCSexperimentID = ?
                 AND component = ?);
  my $sth = $dbh->prepare($query);
  $sth->execute($name,$statID,$FCSexpID,$component);
  my ($ID) = $sth->fetchrow_array();
  $sth->finish();
  if (defined($ID)) {
    $measurement->ID($ID);
  }
  else {
    my $measurementID = $measurement->ID;
    my $desc = $measurement->description || '';
    my $unit = $measurement->unit || '';
    my $query = qq(INSERT INTO measurement (measurementID,name,description,statisticsID,component,FCSexperimentID,unit)
                   VALUES ('$measurementID','$name','$desc','$statID','$component','$FCSexpID','$unit'));
    my $rows = $dbh->do($query);
  }
  # Deal with occurrence separately because the same measurement
  # could have been made in different circumstances in the same experiment
  my $localization = $measurement->localization();
  if ($localization) {
    my $locID = $localization->ID;
    my $measurementID = $measurement->ID;
    $query = qq(SELECT count(*)
                FROM localization_occurrence_has_measurement
                WHERE localization_occurrenceID = ?
                AND measurementID = ? );
    $sth = $dbh->prepare($query);
    $sth->execute($locID,$measurementID);
    my ($count) = $sth->fetchrow_array();
    if ($count==0) {
      my $query = qq(INSERT INTO localization_occurrence_has_measurement (localization_occurrenceID,measurementID)
                     VALUES ('$locID','$measurementID'));
      my $rows = $dbh->do($query);
    }
  }
  my $interaction = $measurement->interaction();
  if ($interaction) {
  	my $intID = $interaction->ID;
    my $measurementID = $measurement->ID;
    $query = qq(SELECT count(*)
                FROM interaction_occurrence_has_measurement
                WHERE interaction_occurrenceID = ?
                AND measurementID = ? );
    $sth = $dbh->prepare($query);
    $sth->execute($intID,$measurementID);
    my ($count) = $sth->fetchrow_array();
    if ($count==0) {
      my $query = qq(INSERT INTO interaction_occurrence_has_measurement (interaction_occurrenceID,measurementID)
                     VALUES ('$intID','$measurementID'));
      my $rows = $dbh->do($query);
    }
  }
  if ($measurement->{'data_files'}) {
  	my $measurementID = $measurement->ID;
  	my $dfh = $dbc->get_DataFileHandle();
  	foreach my $file(@{$measurement->{'data_files'}}) {
  	  $dfh->store($file);
  	  my $fileID = $file->ID;
  	  my $query = qq(INSERT INTO measurement_has_data_file (measurementID,data_fileID)
  	                 VALUES ('$measurementID','$fileID'));
  	  my $rows = $dbh->do($query);
  	}
  }

  $measurement->_decr_newID_count() if $measurement->{'is_new'};
  $measurement->{'is_new'} = undef;

  return $measurement;
}


1;
