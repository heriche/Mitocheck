# Author: jkh1
# 2008-05-12
#

=head1 NAME

 Mitocheck::PeptideHandle

=head1 SYNOPSIS



=head1 DESCRIPTION

 Enables retrieval of Mitocheck peptide objects.

=head1 CONTACT

heriche@embl.de


=cut

=head1 METHODS

=cut

package Mitocheck::PeptideHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Peptide;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new peptide object handle
 Returntype: Mitocheck::PeptideHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: peptide ID
 Description: Gets peptide with given occurrence ID
 Returntype: Mitocheck::Peptide object

=cut

sub get_by_id {

  my ($self,$occurrenceID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # check if peptide occurrence exists in database
  my $query = qq(SELECT COUNT(*) FROM peptide_occurrence WHERE occurrenceID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($occurrenceID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::Peptide($dbc,$occurrenceID);

}

=head2 get_all_by_protein

 Arg1: Mitocheck::Protein or protein ID
 Arg2: (optional) Mitocheck::MSexperiment or experiment ID
 Arg3: (optional) 1 to get only one occurrence of each peptide.
       This selects the highest scoring occurrence of each peptide
       that maps to the query protein
 Description: Gets all occurrences of peptides that map to the given protein,
              optionally limited to the peptides recovered in the given experiment.
 Returntype: list of Mitocheck::Peptide objects

=cut

sub get_all_by_protein {

  my ($self,$protein,$exp,$uniq) = @_;
  $protein ||= "";
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID : $exp;

  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  my $query;
  if ($expID) {
  	if ($uniq) {
  	  $query = qq(SELECT DISTINCT po.occurrenceID, MAX(score)
                  FROM peptide_occurrence po, peptide_map_info pmi
                  WHERE pmi.proteinID = '$proteinID'
                  AND pmi.peptideID = po.peptideID
                  AND po.experimentID = '$expID'
                  GROUP BY po.peptideID);
  	}
  	else {
  	  $query = qq(SELECT DISTINCT po.occurrenceID
                  FROM peptide_occurrence po, peptide_map_info pmi
                  WHERE pmi.proteinID = '$proteinID'
                  AND pmi.peptideID = po.peptideID
                  AND po.experimentID = '$expID');
  	}
  }
  else { # No experiment specified
  	if ($uniq) {
      $query = qq(SELECT DISTINCT po.occurrenceID, MAX(score)
                  FROM peptide_occurrence po, peptide_map_info pmi
                  WHERE pmi.proteinID = '$proteinID'
                  AND pmi.peptideID = po.peptideID
                  GROUP BY po.peptideID);
  	}
  	else {
  	  $query = qq(SELECT DISTINCT po.occurrenceID
                  FROM peptide_occurrence po, peptide_map_info pmi
                  WHERE pmi.proteinID = '$proteinID'
                  AND pmi.peptideID = po.peptideID);
  	}
  }
  $sth = $dbh->prepare($query);
  $sth->execute();

  my @peptides;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@peptides,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @peptides;
}

=head2 get_all_by_bait

 Arg1: Mitocheck::Protein or protein ID
 Arg2: (optional) Mitocheck::MSexperiment or  experiment ID
 Description: Gets all peptides recovered with the given bait protein,
              optionally limited to the given experiment
 Returntype: list of Mitocheck::Peptide objects

=cut

sub get_all_by_bait {

  my ($self,$bait,$exp) = @_;
  $bait ||= "";
  my $baitID = ref($bait) ? $bait->ID() : $bait;
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID() : $exp;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  if ($expID) {
    $sth = $dbh->prepare("SELECT DISTINCT po.occurrenceID
                          FROM peptide_occurrence po
                          WHERE po.baitID = ?
                          AND po.experimentID = ?");
    $sth->execute($baitID,$expID);
  }
  else {
    $sth = $dbh->prepare("SELECT DISTINCT po.occurrenceID
                          FROM peptide_occurrence po
                          WHERE po.baitID = ?");
    $sth->execute($baitID);
  }
  my @peptides;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@peptides,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @peptides;
}

=head2 get_all_by_experiment

 Arg: Mitocheck::MSexperiment or experiment ID
 Description: Gets all peptides recovered in the given experiment
 Returntype: list of Mitocheck::Peptide objects

=cut

sub get_all_by_experiment {

  my ($self,$exp) = @_;
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID() : $exp;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT po.occurrenceID
                           FROM peptide_occurrence po
                           WHERE po.experimentID = ?");
  $sth->execute($expID);
  my @peptides;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@peptides,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @peptides;
}

=head2 get_all_by_gene

 Arg1: Mitocheck::Gene or gene ID
 Arg2: Mitocheck::MSexperiment or experiment ID
 Description: Gets all peptides assigned to the given gene,
              limited to the given experiment if any
 Returntype: list of Mitocheck::Peptide objects

=cut

sub get_all_by_gene {

  my ($self,$gene,$exp) = @_;
  $gene ||= "";
  my $geneID = ref($gene) ? $gene->ID : $gene;
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID() : $exp;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  if ($expID) {
    $sth = $dbh->prepare("SELECT DISTINCT po.occurrenceID
                          FROM peptide_gene_assignment pga, peptide_occurrence po
                          WHERE pga.geneID = ?
                          AND pga.experimentID = ?
                          AND pga.peptideID = po.peptideID
                          AND pga.experimentID = po.experimentID");
    $sth->execute($geneID, $expID);
  }
  else {
    $sth = $dbh->prepare("SELECT DISTINCT po.occurrenceID
                          FROM peptide_gene_assignment pga, peptide_occurrence po
                          WHERE pga.geneID = ?
                          AND pga.peptideID = po.peptideID");
    $sth->execute($geneID);
  }
  my @peptides;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@peptides,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @peptides;
}


1;
