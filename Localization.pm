# Author: jkh1
# 2012-09-24
#

=head1 NAME

 Mitocheck::Localization

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');


 my $localization_handle = $dbc->get_LocalizationHandle();

 my $localization = $localization_handle->get_by_id('LOC00000023');

 my $description = $localization->description();

=head1 DESCRIPTION

 Representation of a particular occurrence of a protein subcellular
 localization. This means it is associated with a space-time localization,
 i.e. a combination of cellular component and event

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Localization;


use strict;
use warnings;
use Scalar::Util qw(weaken);


{
  my $_loc_count = 0;
  my $_newID_count = 0;

  sub get_localization_count {
    return $_loc_count;
  }
  sub _incr_loc_count {
    return ++$_loc_count;
  }
  sub _decr_loc_count {
    return --$_loc_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, localization ID
 Description: Creates a new localization object.
 Returntype: Mitocheck::Localization

=cut

sub new {

  my ($class,$dbc,$localizationID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_loc_count();
  if (!defined $self->{'ID'} && !$localizationID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   localization_occurrenceID
                 FROM     protein_localization_occurrence
                 ORDER BY localization_occurrenceID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $localizationID = "LOC"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $localizationID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck localization ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 cellular_component

 Arg: optional
 Description: Gets/sets the cellular component for this localization occurrence
 Returntype: Mitocheck::CellularComponent object

=cut

sub cellular_component {

  my $self = shift;
  $self->{'cellular_component'} = shift if @_;
  if (!defined $self->{'cellular_component'}) {
    my $localizationID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT cellular_componentID
    						 FROM protein_localization_occurrence
                             WHERE localization_occurrenceID = ? ");
    $sth->execute($localizationID);
    my ($ccID) = $sth->fetchrow_array();
    $sth->finish();
    $self->{'cellular_component'} = Mitocheck::CellularComponent->new($self->{'DBConnection'},$ccID);
  }
  return $self->{'cellular_component'};

}

=head2 event

 Arg: optional, Mitocheck::Event object
 Description: Gets/sets event (~time information) of localization
 Returntype: Mitocheck::Event object

=cut

sub event {

  my $self = shift;
  $self->{'event'} = shift if @_;
  if (!defined $self->{'event'}) {
    my $localizationID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT eventID
    						 FROM protein_localization_occurrence
                             WHERE localization_occurrenceID = ? ");
    $sth->execute($localizationID);
    my ($eventID) = $sth->fetchrow_array();
    my $dbc = $self->{'DBConnection'};
    my $eh = $dbc->get_EventHandle();
    $self->{'event'} = $eh->get_by_id($eventID);
    $sth->finish();
  }
  return $self->{'event'};

}

=head2 image_sets

 Arg: optional, list of  Mitocheck::ImageSet
 Description: Gets/sets image sets of localization
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub image_sets {

  my $self = shift;
  @{$self->{'image_sets'}} = @_ if @_;
  if (!defined $self->{'image_sets'}) {
    my $dbc = $self->{'DBConnection'};
    my $ish = $dbc->get_ImageSetHandle();
  	@{$self->{'image_sets'}} = $ish->get_all_by_localization($self);
  }
  return @{$self->{'image_sets'}} if (defined $self->{'image_sets'});
}

=head2 measurements

 Description: Gets/sets measurements associated with this localization
 Returntype: list of Mitocheck::Measurement

=cut

sub measurements {

  my ($self,@measurements) = @_;
  @{$self->{'measurements'}} = @measurements if @measurements;
  if (!defined $self->{'measurements'}) {
    my $dbc = $self->{'DBConnection'};
    my $mh = $dbc->get_MeasurementHandle();
    @{$self->{'measurements'}} = $mh->get_all_by_localization($self);
  }
  return @{$self->{'measurements'}} if (defined($self->{'measurements'}));
}

=head2 source

 Arg: optional, Mitocheck::Source
 Description: Gets/sets origin of the localization data
 Returntype: Mitocheck::Source

=cut

sub source {

  my ($self,$source) = @_;
  $self->{'source'} = $source if $source;
  if (!defined $self->{'source'}) {
    my $localizationID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth;
    $sth = $dbh->prepare("SELECT sourceID
    					  FROM protein_localization_occurrence
                          WHERE localization_occurrenceID = ? ");
    $sth->execute($localizationID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};
}

=head2 protein

 Arg: optional, Mitocheck::Protein object
 Description: Gets/sets protein for which this localization has been observed
 Returntype: Mitocheck::Protein object

=cut

sub protein {

  my ($self,$protein) = @_;
  $self->{'protein'} = $protein if $protein;
  if (!defined($self->{'protein'})) {
    my $ID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $query = qq(SELECT proteinID
                   FROM protein_localization_occurrence
                   WHERE localization_occurrenceID = ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($ID);
    my ($proteinID) = $sth->fetchrow_array();
    $sth->finish();
    my $prh = $dbc->get_ProteinHandle();
    my $protein = $prh->get_by_id($proteinID);
    $self->{'protein'} = $protein if $protein;
  }
  return $self->{'protein'};
}

=head2 sequence_tag

 Arg: optional, Mitocheck::SequenceTag object
 Description: Gets/sets sequence tag identifying the protein whose localization this is
 Returntype: Mitocheck::SequenceTag object

=cut

sub sequence_tag {

  my ($self,$tag) = @_;
  $self->{'seq_tag'} = $tag if $tag;
  if (!defined($self->{'seq_tag'})) {
    my $ID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $query = qq(SELECT sequence_tagID
                   FROM protein_localization_occurrence
                   WHERE localization_occurrenceID = ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($ID);
    my ($tagID) = $sth->fetchrow_array();
    $sth->finish();
    my $seqth = $dbc->get_SequenceTagHandle();
    my $sequence_tag = $seqth->get_by_id($tagID);
    $self->{'seq_tag'} = $sequence_tag if $sequence_tag;
  }
  return $self->{'seq_tag'};
}


sub DESTROY {

  my $self = shift;
  $self->_decr_loc_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
