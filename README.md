## Mitocheck perl API

### Description
Set of perl modules providing an object-oriented interface to the Mitocheck database.

### Installation
1. `git clone https://git.embl.de/heriche/Mitocheck`
2. Add the Mitocheck directory to PERL5LIB, e.g. in bash:
PERL5LIB=${PERL5LIB}:/path/to/Mitocheck

### Documentation
Documentation is in perl POD and can be read with the perldoc command, e.g.
    `perldoc Mitocheck::DBConnection`

