# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::TranscriptHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $transcript_handle = $dbc->get_TranscriptHandle();

 my $transcript = $transcript_handle->get_by_id('TRN00000012');

 my $protein = $transcript->protein();
 my $transcript_EnsemblID = $transcript->EnsemblID();


=head1 DESCRIPTION

 Enables retrieval of Mitocheck transcript objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::TranscriptHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Transcript;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new transcript object handle
 Returntype: Mitocheck::TranscriptHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});

  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: transcrip ID
 Description: Gets transcript with given ID
 Returntype: Mitocheck::Transcript object

=cut

sub get_by_id {

  my ($self,$transcriptID) = @_;

  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # check if exists in database first
  my $sth = $dbh->prepare("SELECT COUNT(*)
                           FROM transcript t
                           WHERE t.transcriptID = ? ");
  $sth->execute($transcriptID);
  my ($count) = $sth->fetchrow_array();
  $sth->finish();
  if ($count == 0) {
    return undef;
  }
  return new Mitocheck::Transcript($dbc,$transcriptID);

}

=head2 get_by_EnsemblID

 Arg: Ensembl transcript ID
 Description: Gets transcript with given ID
 Returntype: Mitocheck::Transcript object

=cut

sub get_by_EnsemblID {

  my ($self,$EnsemblID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT transcriptID FROM transcript
                            WHERE EnsemblID=?");
  $sth->execute($EnsemblID);
  my ($transcriptID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if (!defined($transcriptID));

  return $self->get_by_id($transcriptID);

}

=head2 get_all_by_gene

 Arg: string
 Description: Gets all transcripts for given gene
 Returntype: list of Mitocheck::Transcript objects

=cut

sub get_all_by_gene {

  my ($self,$gene) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT t.transcriptID
                           FROM transcript t
                           WHERE t.geneID = ? ");
  $sth->execute($geneID);
  my @transcripts;
  while (my $id=$sth->fetchrow_array()) {
    push (@transcripts,$self->get_by_id($id));
  }
  $sth->finish();

  return @transcripts;

}

=head2 get_all_by_dsRNA

 Arg: string
 Description: Gets all transcripts targeted by given dsRNA
 Returntype: list of Mitocheck::Transcript objects

=cut

sub get_all_by_dsRNA {

  my ($self,$dsRNA) = @_;
  my $dsRNAID = ref($dsRNA) ? $dsRNA->ID() : $dsRNA;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT drmi.transcriptID
                           FROM dsRNA_map_info drmi
                           WHERE drmi.oligo_pairID = ? ");
  $sth->execute($dsRNAID);
  my @transcripts;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@transcripts,$self->get_by_id($id));
  }
  $sth->finish();

  return @transcripts;
}

=head2 new_transcript

 Arg(required): -EnsemblID => $EnsemblID
 Arg(required): -gene => $gene
 Arg: -protein => $protein
 Description: creates a new transcript object with given attributes
 Returntype: Mitocheck::Transcript

=cut

sub new_transcript {

  my $self = shift;
  my $dbc = $self->{'DBConnection'};
  my %transcript = @_;
  # attributes = ('-EnsemblID','-gene','-protein');
  unless (defined($transcript{'-EnsemblID'}) && $transcript{'-EnsemblID'}=~/^ENS\w{0,4}T\d{11}/i) {
    croak "WARNING: Ensembl ID required for transcript";
  }
  if (!$transcript{'-gene'}) {
    croak "WARNING: Gene required for transcript";
  }
  my $EnsemblID = $transcript{'-EnsemblID'};
  my $gene = $transcript{'-gene'} if ($transcript{'-gene'});
  unless (ref $gene eq 'Mitocheck::Gene') {
    my $gh = $dbc->get_GeneHandle();
    if ($gene=~/^GEN\d{8}/) {
      $gene = $gh->get_by_id($gene);
    }
    elsif ($gene=~/^ENS\w{0,4}G\d{11}/) {
      $gene = $gh->get_by_EnsemblID($gene);
    }
    else {
      croak "Error: $gene is not a valid gene. You must provide either a Mitocheck::Gene object or a Mitocheck gene ID or an Ensembl gene ID";
    }
  }
  if (!defined $gene) {
    croak "WARNING: Gene $transcript{'-gene'} not found in database";
  }
  my $protein = $transcript{'-protein'} if ($transcript{'-protein'});
  if ($protein && ref($protein) ne 'Mitocheck::Protein') {
    my $ph = $dbc->get_ProteinHandle();
    if ($protein=~/^PRT\d{8}/) {
      $protein = $ph->get_by_id($protein);
    }
    elsif ($protein=~/^ENS\w{0,4}P\d{11}/) {
      $protein = $ph->get_by_EnsemblID($protein);
    }
    else {
      $protein = $ph->get_by_uniprot_acc($protein);
    }
  }
  if ($transcript{'-protein'} && !defined $protein) {
    croak "WARNING: Protein $transcript{'-protein'} not found in database";
  }

  my $transcript = new Mitocheck::Transcript($dbc);
  $transcript->gene($gene);
  $transcript->EnsemblID($EnsemblID);
  $transcript->protein($protein) if $protein;

  return $transcript;

}

=head2 store

 Arg: Mitocheck::Transcript
 Description: Enters transcript and associated data in Mitocheck
              database
 Returntype: Mitocheck::Transcript, normally same as Arg

=cut

sub store {

  my ($self,$transcript) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $transcriptID;
  my $gene = $transcript->gene();
  if (!$gene) {
    croak "Error: Gene required for transcript. Can't enter transcript in database";
  }
  my $geneID = $gene->ID();
  unless (defined($geneID) && $geneID=~/^GEN\d{8}/) {
    croak "Error: Bad gene ID. Can't enter transcript in database";
  }
  my $EnsemblID = $transcript->EnsemblID();
  unless (defined($EnsemblID) && $EnsemblID=~/^ENS\w{0,4}T\d{11}/) {
    croak "Error: Valid Ensembl ID required for transcript. Can't enter transcript in database";
  }
  # check if gene exists in database
  my $gh = $dbc->get_GeneHandle();
  my $gene_in_db = $gh->get_by_id($geneID);
  if (!defined $gene_in_db) {
    croak "Error: Gene associated with transcript not found in database";
  }
  # add associated protein if any
  my $protein = $transcript->protein();
  my $proteinID = "";
  if ($protein) {
    $proteinID = $protein->ID();
    my $prh = $dbc->get_ProteinHandle();
    my $protein = $prh->store($protein);
  }
  else {
    carp "WARNING: non-coding transcript.\n";
  }
  # check if transcript already associated with this gene
  # in database using Ensembl ID
  my $gID;
  my $query = qq(SELECT transcriptID, geneID FROM transcript
                 WHERE EnsemblID='$EnsemblID');
  my $sth= $dbh->prepare ($query);
  $sth->execute();
  ($transcriptID,$gID)=$sth->fetchrow_array();
  $sth->finish();
  if (!$transcriptID) {
    $transcriptID = $transcript->ID();
    # add to database
    $query=qq(INSERT INTO transcript (geneID,transcriptID,proteinID,EnsemblID) VALUES('$geneID','$transcriptID','$proteinID','$EnsemblID'));
    my $rows= $dbh->do ($query);
  }
  elsif ($gID ne $geneID) {
    # Reassign transcript to new gene
    $query=qq(UPDATE transcript SET geneID='$geneID' WHERE EnsemblID='$EnsemblID');
    my $rows= $dbh->do ($query);
  }
  else {
    $transcript->ID($transcriptID);
  }
  $transcript->_decr_newID_count() if $transcript->{'is_new'};
  $transcript->{'is_new'} = undef;

  return $transcript;

}

=head2 remove

 Arg: Mitocheck::Transcript or transcript ID or Ensembl
      transcript ID
 Description: removes transcript and associated mapping data
              from Mitocheck database.
              CAUTION: Doesn't remove gene if last transcript for
              that gene is removed. Doesn't remove associated protein.
 Returntype: ID of the transcript just removed

=cut

sub remove {

  my ($self,$transcript) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  if ($transcript=~/^ENS\w{0,4}T\d{11}/i) {
    $transcript = $self->get_by_EnsemblID($transcript);
  }
  elsif ($transcript=~/^TRN\d{7}/i) {
    $transcript = $self->get_by_id($transcript);
  }
  unless (ref $transcript eq 'Mitocheck::Transcript') {
    croak "Not a valid transcript";
  }
  my $transcriptID = $transcript->ID();
  # DELETE in multiple queries to allow updates when one of the tables has already been modified
  my $query = qq(DELETE FROM transcript WHERE transcriptID='$transcriptID');
  my $rows = $dbh->do($query);
  $query = qq(DELETE FROM dsRNA_map_info WHERE transcriptID='$transcriptID');
  $rows = $dbh->do($query);

  return $transcriptID;
}

1;
