# Author: jkh1
# 2009-07-30
#

=head1 NAME

 Mitocheck::ComplexHandle

=head1 SYNOPSIS


=head1 DESCRIPTION

 Enables retrieval of Mitocheck complex objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::ComplexHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Complex;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new complex object handle
 Returntype: Mitocheck::ComplexHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: complex ID
 Description: Gets complex with given ID
 Returntype: Mitocheck::Complex object

=cut

sub get_by_id {

  my ($self,$complexID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if complex exists in database
  my $query = qq(SELECT COUNT(*) FROM complex WHERE complexID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($complexID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::Complex->new($dbc,$complexID);

}

=head2 get_all_complexes

 Description: Gets all complexes
 Returntype: list of Mitocheck::Complex objects

=cut

sub get_all_complexes {

  my $self = shift;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT complexID
                           FROM complex");
  $sth->execute();
  my @complexes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@complexes,$self->get_by_id($id));
  }
  $sth->finish();
  return @complexes
}

=head2 get_all_by_source

 Arg: Mitocheck::Source or source ID
 Description: Gets complexes from the given source
 Returntype: list of Mitocheck::Complex object

=cut

sub get_all_by_source {

  my ($self,$source) = @_;
 
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT complexID
                           FROM complex
                           WHERE sourceID=?");
  $sth->execute($sourceID);
  my @complexes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@complexes,$self->get_by_id($id));
  }
  $sth->finish();
  return @complexes

}
=head2 get_all_by_gene

 Arg: Mitocheck::Gene or gene ID
 Description: Gets complexes containing proteins from the given gene
 Returntype: List of Mitocheck::Complex object

=cut

sub get_all_by_gene {

  my ($self,$gene) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my @proteins = map {$_->ID} $gene->proteins;
  my $query = "'".join("','",@proteins)."'";
  my $sth = $dbh->prepare("SELECT DISTINCT complexID
                           FROM complex_membership
                           WHERE proteinID IN ($query) ");
  $sth->execute();
  my @complexes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@complexes,$self->get_by_id($id));
  }
  $sth->finish();
  return @complexes

}

=head2 new_complex

 Args: key => value pairs
 Description: creates a new complex object
 Returntype: Mitocheck::Complex

=cut

sub new_complex {

  my $self = shift;
  my %complex = @_;
  my $dbc = $self->{'DBConnection'};
  my @required_attributes=('-proteins','-inference','-source','-cellular_component','-event');
  foreach my $attribute (@required_attributes) {
    unless ($complex{$attribute}) {
      croak "Error: Attribute $attribute required for complex";
    }
  }
  my $complex = Mitocheck::Complex->new($dbc);
  $complex->name($complex{'-name'});
  $complex->source($complex{'-source'});
  $complex->inference($complex{'-inference'});
  $complex->proteins(@{$complex{'-proteins'}});
  $complex->cellular_component($complex{'-cellular_component'});
  $complex->event($complex{'-event'});

  return $complex;

}

=head2 store

 Arg: Mitocheck::Complex
 Description: Enter complex and associated data in Mitocheck database.
              Proteins that are complex members must already be in the database.
 Returntype: Mitocheck::Complex, same as Arg

=cut

sub store {

  my ($self,$complex) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my @proteins = $complex->proteins;
  if (!@proteins || (!defined($proteins[0]) && !defined($proteins[1]))) {
    croak "Complex has no proteins.";
  }
  my $source = $complex->source();
  if (!$source) {
    croak "Source required.";
  }
  my $sourceID = $source->ID;
  # Check if complex already exists
  my %seen;
  foreach my $protein(@proteins) {
    my $query =qq(SELECT DISTINCT c.complexID,c.cellular_componentID,c.eventID
                  FROM complex_membership cm, complex c
    			  WHERE cm.proteinID=?
    			  AND cm.complexID=c.complexID);
    my $sth = $dbh->prepare($query);
    $sth->execute($protein->ID);
    while (my ($complexID,$ccID,$eventID)=$sth->fetchrow_array()) {
      $seen{"$complexID:$ccID:$eventID"}++ if $complexID;
    }
  }
  my $ok = 1;
  foreach my $cID (keys %seen) {
    if ($seen{$cID} eq scalar(@proteins)) {
      # These proteins are already in a complex with
      # the same cellular component and event
      carp "WARNING: A complex already exists with these proteins";
      $ok = 0;
      last;
    }
  }
  if ($ok) {
    # Store new complex
    my $ID = $complex->ID;
    my $name = $complex->name || '';
    my $description = $complex->description || '';
    my $inference = $complex->inference || '';
    my $cellular_componentID = $complex->cellular_component->ID;
    my $eventID = $complex->event->ID;
    my $query=qq(INSERT IGNORE INTO complex (complexID,name,description,sourceID,inference,cellular_componentID,eventID)
                 VALUES ('$ID','$name','$description','$sourceID','$inference','$cellular_componentID','$eventID'));
    $dbh->do($query);
    foreach my $protein(@proteins) {
      my $protID = $protein->ID;
      my $n = $protein->stochiometry($complex);
      my $query;
      if ($n) {
      	$query=qq(INSERT IGNORE INTO complex_membership (proteinID,complexID,stochiometry) VALUES ('$protID','$ID',$n));
      }
      else {
      	$query=qq(INSERT IGNORE INTO complex_membership (proteinID,complexID) VALUES ('$protID','$ID'));
      }
      $dbh->do($query);
    }
  }
  $complex->_decr_newID_count() if $complex->{'is_new'};
  $complex->{'is_new'} = undef;

  return $complex;

}

sub remove {

  my ($self,$complex) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $cID = $complex->ID;
  my $query=qq(DELETE FROM complex_membership WHERE complexID='$cID');
  $dbh->do($query);
  $query=qq(DELETE FROM complex WHERE complexID='$cID');
  $dbh->do($query);

  return $complex->ID;
}

1;
