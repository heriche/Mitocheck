# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::ProteinHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $protein_handle = $dbc->get_ProteinHandle();

 my $protein = $protein_handle->get_by_id('PRT00000001');

=head1 DESCRIPTION

 Enables retrieval of Mitocheck protein objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::ProteinHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Protein;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new protein object handle
 Returntype: Mitocheck::ProteinHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: protein ID
 Description: Gets protein with given ID
 Returntype: Mitocheck::Protein object

=cut

sub get_by_id {

  my ($self,$proteinID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # check if protein exists in database
  my $query = qq(SELECT COUNT(*) FROM protein WHERE proteinID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($proteinID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::Protein($dbc,$proteinID);

}

=head2 get_by_uniprot_acc

 Arg: uniprot accession number
 Description: Gets protein with given uniprot accession number
 Returntype: Mitocheck::Protein object

=cut

sub get_by_uniprot_acc {

  my ($self,$acc) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT p.proteinID
                           FROM protein p
                           WHERE p.Uniprot_acc = ? ");
  $sth->execute($acc);
  my ($proteinID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if (!defined $proteinID);

  return $self->get_by_id($proteinID);

}

=head2 get_by_EnsemblID

 Arg: Ensembl ID
 Description: Gets protein with given Ensembl ID
 Returntype: Mitocheck::Protein object

=cut

sub get_by_EnsemblID {

  my ($self,$EnsemblID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT p.proteinID
                           FROM protein p
                           WHERE p.EnsemblID = ? ");
  $sth->execute($EnsemblID);
  my ($proteinID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if (!defined $proteinID);

  return $self->get_by_id($proteinID);

}


=head2 get_all_by_gene

 Arg: Mitocheck::Gene or gene ID
 Description: Gets proteins produced by given gene
 Returntype: list of Mitocheck::Protein objects

=cut

sub get_all_by_gene {

  my ($self,$gene) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT t.proteinID
                           FROM transcript t
                           WHERE t.geneID = ? ");
  $sth->execute($geneID);
  my @proteins;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@proteins,$self->get_by_id($id))if $id;
  }
  $sth->finish();

  return @proteins;

}

=head2 get_by_transcript

 Arg: Mitocheck::Transcript or transcript ID
 Description: Gets protein produced by given transcript
 Returntype: Mitocheck::Protein object

=cut

sub get_by_transcript {

  my ($self,$transcript) = @_;
  my $protein = "";
  my $transcriptID = ref($transcript) ? $transcript->ID() : $transcript;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT t.proteinID
                           FROM transcript t
                           WHERE t.transcriptID = ? ");
  $sth->execute($transcriptID);
  my ($id) = $sth->fetchrow_array();
  $sth->finish();
  $protein = $self->get_by_id($id);

  return $protein;

}

=head2 new_protein

 Arg: key => value pairs
 Description: creates a new protein object
 Returntype: Mitocheck::Protein

=cut

sub new_protein {

  my $self = shift;
  my %protein = @_;
  my $dbc = $self->{'DBConnection'};
  unless ($protein{'-EnsemblID'} || $protein{'-Ensembl_ID'}) {
    croak "Ensembl ID required for protein";
  }
  unless ($protein{'-seq_length'}) {
    croak "Sequence length required for protein";
  }
  my $protein = new Mitocheck::Protein($dbc);
  $protein{'-EnsemblID'} ||= $protein{'-Ensembl_ID'};
  $protein->EnsemblID($protein{'-EnsemblID'});
  $protein->uniprot_acc($protein{'-uniprot_acc'}) if $protein{'-uniprot_acc'};
  $protein->seq_length($protein{'-seq_length'});
  $protein->mass($protein{'-mass'}) if $protein{'-mass'};

  return $protein;

}

=head2 store

 Arg: Mitocheck::Protein
 Description: Enters protein and associated data in Mitocheck database.
 Returntype: Mitocheck::Protein, same as Arg

=cut

sub store {

  my ($self,$protein) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $uniprot = $protein->uniprot_acc() || "";
  my $EnsemblID = $protein->EnsemblID();
  my $seq_length = $protein->seq_length;
  my $mass = $protein->mass;
  if (!$EnsemblID) {
    croak "Ensembl ID required for protein";
  }
  if (!$seq_length) {
    croak "Sequence length required for protein $EnsemblID";
  }
  if (!defined($mass)) {
    croak "Mass required for protein $EnsemblID";
  }
  # Check if protein already in database
  my $query = qq(SELECT proteinID FROM protein WHERE EnsemblID='$EnsemblID');
  my $sth = $dbh->prepare($query);
  $sth->execute();
  my ($proteinID) = $sth->fetchrow_array();
  $sth->finish();
  if (!$proteinID) {
    $proteinID = $protein->ID();
    # Update protein table
    $query = qq(INSERT INTO protein (proteinID,EnsemblID,Uniprot_acc,seq_length,mass) VALUES ('$proteinID','$EnsemblID','$uniprot','$seq_length','$mass'));
    my $rows= $dbh->do ($query);
  }
  else { 
    $protein->ID($proteinID);
  }
  $protein->_decr_newID_count() if $protein->{'is_new'};
  $protein->{'is_new'} = undef;

  return $protein;
}

=head2 remove

 Arg: Mitocheck::Protein or protein ID or Ensembl protein ID
 Description: Removes protein and associated data from Mitocheck
              database.
 Returntype: ID of the protein just removed

=cut

sub remove {

  my ($self,$proteinID) = @_;
  unless (defined($proteinID)) {
    croak "Method requires a protein as argument";
  }
  my $protein;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  if ($proteinID=~/^ENS\w{0,4}P\d{11}/i) {
    $protein = $self->get_by_EnsemblID($proteinID);
  }
  elsif ($proteinID=~/^PRT\d{7}/i) {
    $protein = $self->get_by_id($proteinID);
  }
  unless (ref($proteinID) && $proteinID->isa('Mitocheck::Protein')) {
    croak "Not a valid protein ($proteinID)";
  }
  else {
    $protein = $proteinID;
  }
  $proteinID = $protein->ID();
  my $query = qq(UPDATE transcript SET proteinID='' WHERE proteinID = '$proteinID');
  my $rows = $dbh->do($query);
  $query = qq(DELETE FROM protein WHERE proteinID = '$proteinID');
  $rows = $dbh->do($query);
  $query = qq(DELETE FROM protein_has_domain WHERE proteinID = '$proteinID');
  $rows = $dbh->do($query);
  $query = qq(DELETE FROM protein_has_localization WHERE proteinID = '$proteinID');
  $rows = $dbh->do($query);
  $query = qq(DELETE FROM protein_interaction WHERE proteinID = '$proteinID');
  $rows = $dbh->do($query);
  $query = qq(DELETE FROM protein_interaction WHERE interactorID = '$proteinID');
  $rows = $dbh->do($query);
  $query = qq(DELETE FROM peptide_map_info WHERE proteinID = '$proteinID');
  $rows = $dbh->do($query);

  return $proteinID;

}

1;
