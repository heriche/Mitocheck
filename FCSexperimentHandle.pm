# Author: jkh1
# 2012-09-21
#

=head1 NAME

 Mitocheck::FCSexperimentHandle

=head1 SYNOPSIS



=head1 DESCRIPTION

 Enables retrieval of Mitocheck fluorescence spectroscopy experiment objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::FCSexperimentHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::FCSexperiment;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new FCS experiment object handle
 Returntype: Mitocheck::FCSexperimentHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: experiment ID
 Description: Gets FCS experiment with given ID
 Returntype: Mitocheck::FCSexperiment object

=cut

sub get_by_id {

  my ($self,$ID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # check if experiment exists in database
  my $query = qq(SELECT COUNT(*) FROM FCSexperiment WHERE experimentID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($ID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::FCSexperiment->new($dbc,$ID);

}

=head2 get_all_experiments

 Arg: (optional) Mitocheck::Source or source ID
 Description: Gets all FCS experiments (from given source if any)
 Returntype: list of Mitocheck::FCSexperiment objects

=cut

sub get_all_experiments {

  my $self = shift;
  my $source = shift if @_;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my @experiments;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $query;
  if ($sourceID) {
    $query = qq(SELECT DISTINCT experimentID FROM FCSexperiment
                WHERE sourceID='$sourceID');
  }
  else {
    $query = qq(SELECT DISTINCT experimentID FROM FCSexperiment);
  }
  my $sth= $dbh->prepare($query);
  $sth->execute();
  while (my ($id) = $sth->fetchrow_array()) {
    push (@experiments,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @experiments;
}

=head2 get_all_by_protein

 Arg: Mitocheck::Protein or protein ID
 Description: Gets all FCS experiments relating to the given protein
 Returntype: list of Mitocheck::FCSexperiment objects

=cut

sub get_all_by_protein {

  my ($self,$protein) = @_;
  $protein ||= "";
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  $sth = $dbh->prepare("SELECT DISTINCT fcse.experimentID
                        FROM FCSexperiment fcse, tag_map_info tmi
                        WHERE tmi.proteinID = ? 
                        AND (tmi.sequence_tagID = fcse.sequence_tagID1
                        	 OR tmi.sequence_tagID = fcse.sequence_tagID2)");
  $sth->execute($proteinID);
  my @experiments;
  while (my ($id) = $sth->fetchrow_array()) {
    push (@experiments,$self->get_by_id($id)) if $id;
  }
  $sth->finish();

  return @experiments;
}

=head2 get_all_by_gene

 Arg: Mitocheck::Gene or gene ID
 Description: Gets all FCS experiments relating to a protein from the given gene.
 Returntype: list of Mitocheck::FCSexperiment objects

=cut

sub get_all_by_gene {

  my ($self,$gene) = @_;
  $gene ||= "";
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  $sth = $dbh->prepare("SELECT DISTINCT fcse.experimentID
                        FROM FCSexperiment fcse, tag_map_info tmi
                        WHERE tmi.geneID = ? 
                        AND (tmi.sequence_tagID = fcse.sequence_tagID1
                        	 OR tmi.sequence_tagID = fcse.sequence_tagID2)");
  $sth->execute($geneID);
  my @experiments;
  while (my ($id) = $sth->fetchrow_array()) {
    if ($id) {
      push (@experiments,$self->get_by_id($id));
    }
  }
  $sth->finish();

  return @experiments;
}

=head2 new_FCSexperiment

 Args (required): -cell_line => Mitocheck::CellLine, -sequence_tag1 => Mitocheck::SequenceTag
 Args (optional): -sequence_tag2 => Mitocheck::SequenceTag, -source => Mitocheck::Source
 Description: creates a new FCS experiment object
 Returntype: Mitocheck::FCSexperiment

=cut

sub new_FCSexperiment {

  my $self = shift;
  my %exp = @_;
  # required attributes = ('-cell_line','-sequence_tag1');
  unless (defined($exp{'-cell_line'})) {
  	print STDERR "keys: ",join(",",keys %exp),"\n";
    croak "ERROR: FCS experiment requires the following attribute: -cell_line";
  }
   unless (defined($exp{'-sequence_tag1'})) {
    croak "ERROR: FCS experiment requires the following attribute: -sequence_tag1";
  }
  my $dbc = $self->{'DBConnection'};

  my $FCSexp = Mitocheck::FCSexperiment->new($dbc);
  $FCSexp->cell_line($exp{'-cell_line'});
  $FCSexp->sequence_tags($exp{'-sequence_tag1'},$exp{'-sequence_tag2'});
  $FCSexp->source($exp{'-source'});
 
  return $FCSexp;

}

=head2 store

 Arg: Mitocheck::FCSexperiment
 Description: Enters FCS experiment and associated data in Mitocheck database
 Returntype: Mitocheck::FCSexperiment, normally same as Arg

=cut

sub store {

  my ($self,$exp) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
	
  # Always allow creation of a new experiment because more than one
  # can be done under the same circumstances
  my $expID = $exp->ID;
  my @seq_tags = $exp->sequence_tags;
  my $seq_tagID1 = $seq_tags[0]->ID;
  my $seq_tagID2 = "";
  if ($seq_tags[1]) {
    $seq_tagID2 = $seq_tags[1]->ID;
  }
  my $cell_lineID = $exp->cell_line->ID;
  my $sourceID = $exp->source->ID;
  my $query = qq(INSERT INTO FCSexperiment (experimentID,sourceID,sequence_tagID1,sequence_tagID2,cell_lineID)
                 VALUES ('$expID','$sourceID','$seq_tagID1','$seq_tagID2','$cell_lineID'));
  my $rows = $dbh->do($query);
  if ($exp->{'image_sets'}) {
  	my $ish = $dbc->get_ImageSetHandle();
  	foreach my $set(@{$exp->{'image_sets'}}) {
  	  $ish->store($set);
  	  my $setID = $set->ID;
  	  my $query = qq(INSERT INTO FCSexperiment_has_image_set (FCSexperimentID,image_setID)
  	                 VALUES ('$expID','$setID'));
  	  my $rows = $dbh->do($query);   
  	}
  }
  if ($exp->{'data_files'}) {
  	my $dfh = $dbc->get_DataFileHandle();
  	foreach my $file(@{$exp->{'data_files'}}) {
  	  $dfh->store($file);
  	  my $fileID = $file->ID;
  	  my $query = qq(INSERT INTO FCSexperiment_has_data_file (FCSexperimentID,data_fileID)
  	                 VALUES ('$expID','$fileID'));
  	  my $rows = $dbh->do($query);   
  	}
  }
  $exp->_decr_newID_count() if $exp->{'is_new'};
  $exp->{'is_new'} = undef;

  return $exp;
}

1;



