# Author: jkh1
# 2013-07-03
#

=head1 NAME

 Mitocheck::DataFileHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $dataFile_handle = $dbc->get_DataFileHandle();

 my $file = $dataFile_handle->get_by_id('DTF00000123');


=head1 DESCRIPTION

 Enables retrieval of Mitocheck data file objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::DataFileHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::DataFile;
use Scalar::Util qw(weaken);

=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new DataFile object handle.
 Returntype: Mitocheck::DataFileHandle

=cut

sub new {

  my ($class,$dbc) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;

  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: data file ID
 Description: Gets data file with given ID
 Returntype: Mitocheck::DataFile object

=cut

sub get_by_id {

  my ($self,$ID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if data file exists in database
  my $query = qq(SELECT COUNT(*) FROM data_file WHERE data_fileID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($ID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::DataFile->new($dbc,$ID);

}

=head2 get_by_filename

 Arg: filename
 Description: Gets data file with given filename (assumes filename is unique).
 Returntype: Mitocheck::DataFile object

=cut

sub get_by_filename {

  my ($self,$filename) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT data_fileID FROM data_file WHERE filename = ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($filename);
  my ($ID) = $sth->fetchrow_array();
  if ($ID) {
  	return Mitocheck::DataFile->new($dbc,$ID);
  }
}

=head2 get_all_by_measurement

 Arg: Mitocheck::Measurement object or measurement ID
 Description: Gets data files associated with given measurement
 Returntype: list of Mitocheck::DataFile object

=cut

sub get_all_by_measurement {

  my ($self,$measurement) = @_;
  my $measurementID = ref($measurement) ? $measurement->ID(): $measurement;
  my @files;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT data_fileID
                           FROM measurement_has_data_file
                           WHERE measurementID = ? ");
  $sth->execute($measurementID);
  while (my ($ID) = $sth->fetchrow_array()) {
    push (@files,$self->get_by_id($ID));
  }
  return @files;
}

=head2 get_all_by_FCSexperiment

 Arg: Mitocheck::FCSexperiment object or FCS experiment ID
 Description: Gets data files associated with given FCS experiment.
              Note that this doesn't get the files associated with 
              measurements made in this experiment.
 Returntype: list of Mitocheck::DataFile object

=cut

sub get_all_by_FCSexperiment {

  my ($self,$exp) = @_;
  my $expID = ref($exp) ? $exp->ID(): $exp;
  my @files;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT data_fileID
                           FROM FCSexperiment_has_data_file
                           WHERE FCSexperimentID = ? ");
  $sth->execute($expID);
  while (my ($ID) = $sth->fetchrow_array()) {
    push (@files,$self->get_by_id($ID));
  }
  return @files;
}

=head2 new_data_file

 Args (required): -filename => string, -type => string, -format => string, -data_name => string
 Arg: (optional): -spot_position => string
 Description: creates a new data file object
 Returntype: Mitocheck::DataFile

=cut

sub new_data_file {

  my $self = shift;
  my %file = @_;
  my @required = qw(-filename -type -format -data_name);
  foreach my $attribute(@required) {
  	unless (defined($file{$attribute})) {
  		croak "ERROR: Data file requires the following attribute: $attribute";
  	}
  }
  my $dbc = $self->{'DBConnection'};

  my $DataFile = Mitocheck::DataFile->new($dbc);
  $DataFile->filename($file{'-filename'});
  $DataFile->type($file{'-type'});
  $DataFile->format($file{'-format'});
  $DataFile->data_name($file{'-data_name'});
  $DataFile->spot_position($file{'-spot_position'}) if (defined ($file{'-spot_position'}));
 
  return $DataFile;

}

=head2 store

 Arg: Mitocheck::DataFile
 Description: Enters data file in Mitocheck database
 Returntype: Mitocheck::DataFile, normally same as Arg

=cut

sub store {

  my ($self,$file) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $ID = $file->ID;
  my $filename = $file->filename;
  my $qfilename = $dbh->quote($filename);
  my $type = $file->type;
  my $format = $file->format;
  my $data_name = $file->data_name;
  my $qdata_name = $dbh->quote($data_name);
  my $spot_position = $file->spot_position;
  my $qspot_position = $dbh->quote($spot_position);

  my $query = qq(INSERT INTO data_file (data_fileID, filename, type, data_name, format,spot_position)
                 VALUES ('$ID',$qfilename,'$type',$qdata_name,'$format',$qspot_position));
  my $rows = $dbh->do($query);

  $file->_decr_newID_count() if $file->{'is_new'};
  $file->{'is_new'} = undef;

  return $file;
}


1;
