# Author: jkh1
# 2012-09-24
#

=head1 NAME

 Mitocheck::SequenceTagHandle

=head1 SYNOPSIS


=head1 DESCRIPTION

 Enables retrieval of Mitocheck sequence tag objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::SequenceTagHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::SequenceTag;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new sequence tag object handle
 Returntype: Mitocheck::SequenceTagHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: sequence tag id
 Description: Gets sequence tag with given id
 Returntype: Mitocheck::SequenceTag object

=cut

sub get_by_id {

  my ($self,$ID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};

  my $query = qq(SELECT sequence_tagID
                 FROM sequence_tag
                 WHERE sequence_tagID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($ID);
  my ($seq_tagID) = $sth->fetchrow_array();
  if ($seq_tagID) {
    return Mitocheck::SequenceTag->new($dbc,$seq_tagID);
  }
  else {
    return undef;
  }
}

=head2 get_all_by_gene

 Arg: Mitocheck::Gene or gene ID
 Description: Gets all sequence tags that map to the given gene.
 Returntype: list of Mitocheck::SequenceTag objects

=cut

sub get_all_by_gene {

  my ($self,$gene) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT sequence_tagID
                           FROM tag_map_info
                           WHERE geneID = ? ");
  $sth->execute($geneID);
  my @tags;
  while (my ($id)=$sth->fetchrow_array()) {
    my $tag = $self->get_by_id($id);
    push (@tags,$tag) if (defined($tag));
  }
  $sth->finish();

  return @tags;

}

=head2 get_all_by_transcript

 Arg: Mitocheck::Transcript or transcript ID
 Description: Gets all sequence tags that map to the given transcript.
 Returntype: list of Mitocheck::SequenceTag objects

=cut

sub get_all_by_transcript {

  my ($self,$transcript) = @_;
  my $transcriptID = ref($transcript) ? $transcript->ID() : $transcript;
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT sequence_tagID
                           FROM tag_map_info
                           WHERE transcriptID = ? ");
  $sth->execute($transcriptID);
  my @tags;
  while (my ($id)=$sth->fetchrow_array()) {
    my $tag = $self->get_by_id($id);
    push (@tags,$tag) if (defined($tag));
  }
  $sth->finish();

  return @tags;

}

=head2 new_sequence_tag

 Arg (required): -sequence => sequence
 Arg (required): -species => latin name of the species the sequence is from
 Arg (required): -vector_name => name of vector carrying the sequence
 Arg (required): -vector_type => type of vector carrying the sequence
 Arg (required): -EnsemblID => Ensembl ID of the gene the sequence originally comes from
 Arg (required): -is_overexpressed => 0 or 1 if the sequence is overexpressed
 Description: creates a new sequence tag object
 Returntype: Mitocheck::SequenceTag

=cut

sub new_sequence_tag {

  my $self = shift;
  my %tag = @_;
  unless ($tag{'-sequence'}) {
    croak("Sequence tag requires a nucleotide sequence.");
  }
  unless (defined($tag{'-species'})) {
    croak("Species required for sequence tag.");
  }
  unless (defined($tag{'-vector_name'})) {
    croak("Vector name required for sequence tag.");
  }
  unless (defined($tag{'-vector_type'})) {
    croak("Vector type required for sequence tag.");
  }
  unless (defined($tag{'-is_overexpressed'})) {
    croak("Argument -is_overexpressed required for sequence tag.");
  }

  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $seq_tag = Mitocheck::SequenceTag->new($dbc);
  $seq_tag->sequence($tag{'-sequence'});
  $seq_tag->species($tag{'-species'});
  $seq_tag->vector_name($tag{'-vector_name'});
  $seq_tag->vector_type($tag{'-vector_type'});
  $seq_tag->is_overexpressed($tag{'-is_overexpressed'});
  $seq_tag->external_ID($tag{'-external_ID'}) if ($tag{'-external_ID'});
  $seq_tag->original_geneID($tag{'-EnsemblID'}) if ($tag{'-EnsemblID'});

  return $seq_tag;

}

=head2 store

 Arg: Mitocheck::SequenceTag
 Description: Enter sequence tag and associated data in Mitocheck database.
 Returntype: Mitocheck::SequenceTag, same as Arg

=cut

sub store {

  my ($self,$seq_tag) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $sequence = $seq_tag->sequence;
  my $species = $seq_tag->species;
  my $vector_name = $seq_tag->vector_name;
  my $vector_type = $seq_tag->vector_type;
  my $original_geneID = $seq_tag->original_geneID || '';
  my $external_ID = $seq_tag->external_ID() || '';
  my $is_overexpressed = $seq_tag->is_overexpressed;

  # Check if already in database
  # Sequence tags are identical if they have the same sequence, species and vector name
  my $query =qq(SELECT sequence_tagID
                FROM sequence_tag
                WHERE sequence = ?
                AND species = ?
                AND vector_name = ?);
  my $sth = $dbh->prepare($query);
  $sth->execute($sequence,$species,$vector_name);
  my ($tagID)=$sth->fetchrow_array();
  $sth->finish();
  if (!$tagID) {
    $tagID = $seq_tag->ID();
    # Add to database
    $query=qq(INSERT INTO sequence_tag (sequence_tagID,sequence,species,vector_name,vector_type,geneID,is_overexpressed,external_ID)
    	 	  VALUES ('$tagID','$sequence','$species','$vector_name','$vector_type','$original_geneID','$is_overexpressed','$external_ID'));
    $dbh->do($query);
   }
   else {
   	$seq_tag->ID($tagID);
   }
   # Add mapping information if any
   if ($seq_tag->{'mapping_info'}) {
     foreach my $transcript ($seq_tag->transcripts()) {
       my $transcriptID = $transcript->ID();
       my $geneID = $transcript->gene->ID();
       my $proteinID ="";
       if ($transcript->protein()) {
	      $proteinID = $transcript->protein()->ID();
       }
       my $mismatch = $seq_tag->mismatch($transcript);
       my $alignment = $seq_tag->alignment($transcript);
       my $query = qq(INSERT IGNORE INTO tag_map_info (sequence_tagID,geneID,transcriptID,proteinID,mismatch,alignment)
	       			  VALUES('$tagID','$geneID','$transcriptID','$proteinID','$mismatch','$alignment'));
       my $rows= $dbh->do($query);
     }
   }

  $seq_tag->_decr_newID_count() if $seq_tag->{'is_new'};
  $seq_tag->{'is_new'} = undef;

  return $seq_tag;
}

1;
