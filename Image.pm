# Author: jkh1
# 2005-05-31
#

=head1 NAME

 Mitocheck::Image

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $image_handle = $dbc->get_ImageHandle();

 my $image = $image_handle->get_by_id('IMG00000001');

 my $filename = $image->filename();
 my $comments = $image->comments();
 my $type = $image->file_type();
 my $class = $image->class();
 my @reporters = $image->reporters();
 my $dsRNA = $image->dsRNA();
 my $protein = $image->protein();
 my $source = $image->source();

=head1 DESCRIPTION

 Representation of Mitocheck images.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Image;


use strict;
use warnings;
use Carp;
use Scalar::Util qw(weaken);


{
  my $_image_count = 0;
  my $_newID_count = 0;

  sub get_image_count {
    return $_image_count;
  }
  sub _incr_image_count {
    return ++$_image_count;
  }
  sub _decr_image_count {
    return --$_image_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, image ID
 Description: Creates a new image object.
 Returntype: Mitocheck::Image

=cut

sub new {

  my ($class,$dbc,$imageID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_image_count();
  if (!defined $self->{'ID'} && !$imageID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   i.imageID
                 FROM     image i
                 ORDER BY i.imageID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $imageID = "IMG"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $imageID;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck image ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 setID

 Arg: optional, integer
 Description: Gets/sets ID of the set the image is part of
 Returntype: integer

=cut

sub setID {

  my $self = shift;
  $self->{'setID'} = shift if @_;
  if (!defined $self->{'setID'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT image_setID FROM image
                             WHERE imageID = ? ");
    $sth->execute($imageID);
    ($self->{'setID'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'setID'};

}

=head2 filename

 Arg: optional, file name
 Description: Gets/sets image file name
 Returntype: string

=cut

sub filename {

  my $self = shift;
  $self->{'filename'} = shift if @_;
  if (!defined $self->{'filename'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT i.filename FROM image i
                             WHERE i.imageID = ? ");
    $sth->execute($imageID);
    ($self->{'filename'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'filename'};

}

=head2 comments

 Arg: optional, text
 Description: Gets/sets comments on the image
 Returntype: string

=cut

sub comments {

  my $self = shift;
  $self->{'comments'} = shift if @_;
  if (!defined $self->{'comments'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT i.comments
                             FROM image i
                             WHERE i.imageID = ? ");
    $sth->execute($imageID);
    ($self->{'comments'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'comments'};

}

=head2 reporters

 Arg: optional, list of Mitocheck::Reporter objects
 Description: Gets/sets reporters/markers used in the image
 Returntype: list of Mitocheck::Reporter objects

=cut

sub reporters {

  my ($self,@reporters) = @_;
  push (@{$self->{'reporters'}},@reporters) if @reporters;
  my $dbc = $self->{'DBConnection'};
  my $rh = $dbc->get_ReporterHandle();
  @{$self->{'reporters'}} = $rh->get_all_by_image($self);

  return @{$self->{'reporters'}};

}

=head2 cell_line

 Arg: optional, Mitocheck::CellLine object
 Description: Gets/sets cell line used in the image
 Returntype: Mitocheck::CellLine object

=cut

sub cell_line {

  my ($self,$cell_line) = @_;
  $self->{'cell_line'} = $cell_line if $cell_line;
  if (!defined $self->{'cell_line'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT cell_lineID
                             FROM image
                             WHERE imageID = ? ");
    $sth->execute($imageID);
    my ($lineID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $clh = $dbc->get_CellLineHandle();
    $self->{'cell_line'} = $clh->get_by_id($lineID);
  }

  return $self->{'cell_line'};

}

=head2 file_type

 Arg: optional, type of image
 Description: Gets/sets type of image file (i.e. still or video)
 Returntype: string

=cut

sub file_type {

  my $self = shift;
  $self->{'type'} = shift if @_;
  if (!defined $self->{'type'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT i.file_type
                             FROM image i
                             WHERE i.imageID = ? ");
    $sth->execute($imageID);
    ($self->{'type'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'type'};

}

=head2 dsRNA

 Arg: optional, Mitocheck::dsRNA object
 Description: Gets/sets dsRNA used to obtain the image
 Returntype: Mitocheck::dsRNA object

=cut

sub dsRNA {

  my ($self,$dsRNA) = @_;
  carp "WARNING: Image method dsRNA is deprecated, use Image->dsRNAs instead.\n";
  if (defined $dsRNA) {
    if (ref($dsRNA) ne 'Mitocheck::dsRNA') {
      croak "Argument must be a Mitocheck::dsRNA object";
    }
    else { push @{$self->{'dsRNAs'}},$dsRNA }
  }

  if (!defined $self->{'dsRNAs'}) {
    $self->dsRNAs();
  }
  return $self->{'dsRNAs'}->[0] if (defined($self->{'dsRNAs'}));

}

=head2 dsRNAs

 Arg: optional, list of Mitocheck::dsRNA objects
 Description: Gets/sets dsRNAs used to obtain the image
 Returntype: list of Mitocheck::dsRNA objects

=cut

sub dsRNAs {

  my ($self,@dsRNAs) = @_;

  if (@dsRNAs) {
    if (ref($dsRNAs[0]) ne 'Mitocheck::dsRNA') {
      croak "Argument must be a list of Mitocheck::dsRNA objects";
    }
    else { @{$self->{'dsRNAs'}} = @dsRNAs }
  }

  if (!defined $self->{'dsRNAs'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $dbc = $self->{'DBConnection'};
    my $dsrh = $dbc->get_dsRNAHandle();
    my $sth = $dbh->prepare("SELECT drhis.oligo_pairID
                             FROM dsRNA_has_image_set drhis, image i
                             WHERE i.imageID = ?");
    $sth->execute($imageID);
    while (my ($dsRNAID) = $sth->fetchrow_array()) {
      my $dsRNA = $dsrh->get_by_id($dsRNAID);
      push @{$self->{'dsRNAs'}}, $dsRNA if $dsRNA;
    }
    $sth->finish();
  }
  return @{$self->{'dsRNAs'}} if (defined($self->{'dsRNAs'}));
}

=head2 proteins

 Arg: optional, list of Mitocheck::Protein objects
 Description: Gets/sets proteins whose localization is shown in
              the image
 Returntype: list of Mitocheck::Protein objects

=cut

sub proteins {

  my ($self,@proteins) = @_;

  if (!@proteins) {
    if (ref($proteins[0]) ne 'Mitocheck::Protein') {
      croak "Argument must be a list of Mitocheck::Protein objects";
    }
    else { @{$self->{'proteins'}} = @proteins }
  }

  if (!defined $self->{'proteins'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $dbc = $self->{'DBConnection'};
    my $ph = $dbc->get_ProteinHandle();
    my $sth = $dbh->prepare("SELECT DISTINCT plo.proteinID
                             FROM image i,
                                  localization_occurrence_has_image_set lohis,
                                  protein_localization_occurrence plo
                             WHERE i.imageID = ?
                             AND i.image_setID = lohis.image_setID
                             AND lohis.localization_occurrenceID = lo.localization_occurrenceID");
    $sth->execute($imageID);
    while (my ($proteinID) = $sth->fetchrow_array()) {
      push @{$self->{'proteins'}}, $ph->get_by_id($proteinID);
    }
  }
  return @{$self->{'proteins'}} if (defined $self->{'proteins'});

}

=head2 source

 Arg: optional, Mitocheck::Source
 Description: Gets/sets origin of the image
 Returntype: Mitocheck::Source object

=cut

sub source {

  my ($self,$source) = @_;

  if (defined $source) {
    if (ref($source) ne 'Mitocheck::Source') {
      croak "Argument must be a Mitocheck::Source object";
    }
    else { $self->{'source'} = $source }
  }

  if (!defined $self->{'source'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT ims.sourceID
                             FROM image i, image_set ims
                             WHERE i.imageID = ?
                             AND i.image_setID=ims.image_setID");
    $sth->execute($imageID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};

}

=head2 is_representative

 Arg: optional, 0 or 1
 Description: Gets/sets flag to indicate whether the image has
              been chosen as a representative example
 Returntype: 0 or 1

=cut

sub is_representative {

  my ($self,$flag) = @_;
  $self->{'is_representative'} = $flag if (defined $flag);
  if (!defined $self->{'is_representative'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT is_representative
                             FROM image i
                             WHERE i.imageID = ? ");
    $sth->execute($imageID);
    ($self->{'is_representative'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'is_representative'};
}

=head2 spot_location

 Arg: (optional) string
 Description: Gets/sets the position of the spot used to generate
              the image
 Returntype: string

=cut

sub spot_location {

  my ($self,$sploc) = @_;
  $self->{'spot_location'} = $sploc if (defined $sploc);
  my $dbc = $self->{'DBConnection'};
  if (!defined $self->{'spot_location'}) {
    my $imageID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT ims.spot_location
                             FROM image i, image_set ims
                             WHERE i.imageID = ?
                             AND i.image_setID = ims.image_setID");
    $sth->execute($imageID);
    ($self->{'spot_location'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'spot_location'};
}

=head2 get_set

 Description: Gets the set the image is part of
 Returntype: Mitocheck::ImageSet

=cut

sub get_set {

  my $self = shift;

  my $setID = $self->setID;
  my $ish = $self->{'DBConnection'}->get_ImageSetHandle();
  my $set = $ish->get_by_id($setID);

  return $set;
}

=head2 display_order

 Arg: (optional) integer
 Description: Gets/sets the order in which the image is displayed in a set
 Returntype: integer

=cut

sub display_order {

  my ($self,$order) = @_;
  $self->{'display_order'} = $order if $order;
  if (!defined $self->{'display_order'}) {
    my $ID = $self->ID;
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT display_order
                             FROM image
                             WHERE imageID = ? ");
    $sth->execute($ID);
    my ($ord) = $sth->fetchrow_array();
    $sth->finish();
    $self->{'display_order'} = $ord;
  }
  return $self->{'display_order'};
}

sub DESTROY {

  my $self = shift;
  $self->_decr_image_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
