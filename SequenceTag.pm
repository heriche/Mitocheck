# Author: jkh1
# 2012-09-24
#

=head1 NAME

 Mitocheck::SequenceTag

=head1 SYNOPSIS



=head1 DESCRIPTION

 Representation of a sequence fragment identifying a gene product. This is
 mapped to the reference genome annotation in order to infer the associated
 gene. This is safer than relying on ID conversion.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::SequenceTag;


use strict;
use warnings;
use Carp;
use Scalar::Util qw(weaken);

{
  my $_tag_count = 0;
  my $_newID_count = 0;

  sub get_tag_count {
    return $_tag_count;
  }
  sub _incr_tag_count {
    return ++$_tag_count;
  }
  sub _decr_tag_count {
    return --$_tag_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, sequence tag ID
 Description: Creates a new sequence tag object.
 Returntype: Mitocheck::SequenceTag

=cut

sub new {

  my ($class,$dbc,$tagID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $class->_incr_tag_count();
  if (!defined $self->{'ID'} && !$tagID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   sequence_tagID
                 FROM     sequence_tag
                 ORDER BY sequence_tagID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $tagID = "SQT"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $tagID;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck sequence tag ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 external_ID

 Arg: optional, ID of the sequence in an external database
 Description: Gets/sets external database ID
 Returntype: string

=cut

sub external_ID {

  my $self = shift;
  $self->{'external_ID'} = shift if @_;
  if (!defined $self->{'external_ID'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT external_ID FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'external_ID'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'external_ID'};

}

=head2 sequence

 Arg: optional, nucleic acid sequence
 Description: Gets/sets sequence
 Returntype: string

=cut

sub sequence {

  my $self = shift;
  $self->{'sequence'} = shift if @_;
  if (!defined $self->{'sequence'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sequence FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'sequence'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'sequence'};

}

=head2 species

 Arg: optional, species name
 Description: Gets/sets species the sequence is from
 Returntype: string

=cut

sub species {

  my $self = shift;
  $self->{'species'} = shift if @_;
  if (!defined $self->{'species'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT species FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'species'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'species'};

}

=head2 vector_name

 Arg: optional, name of the vector used
 Description: Gets/sets vector carrying the sequence
 Returntype: string

=cut

sub vector_name {

  my $self = shift;
  $self->{'vector_name'} = shift if @_;
  if (!defined $self->{'vector_name'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT vector_name FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'vector_name'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'vector_name'};

}

=head2 vector_type

 Arg: optional, type of vector used
 Description: Gets/sets type of vector carrying the sequence
 Returntype: string

=cut

sub vector_type {

  my $self = shift;
  $self->{'vector_type'} = shift if @_;
  if (!defined $self->{'vector_type'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT vector_type FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'vector_type'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'vector_type'};

}

=head2 original_geneID

 Arg: optional, gene ID
 Description: Gets/sets the gene ID this sequence was originally derived from.
 			  This is not a Mitocheck::Gene object as this gene can be from any species.
 Returntype: string

=cut

sub original_geneID {

  my $self = shift;
  $self->{'original_geneID'} = shift if @_;
  if (!defined $self->{'original_geneID'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT geneID FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'original_geneID'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'original_geneID'};

}

=head2 is_overexpressed

 Arg: optional, 0 or 1
 Description: Gets/sets whether the sequence is overexpressed
 Returntype: 0/1

=cut

sub is_overexpressed {

  my $self = shift;
  $self->{'is_overexpressed'} = shift if @_;
  if (!defined $self->{'is_overexpressed'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT is_overexpressed FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'is_overexpressed'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'is_overexpressed'};

}

=head2 tag_name

 Arg: (optional) tag name
 Description: Gets/sets the name of the tag
 Returntype: string

=cut

sub tag_name {

  my $self = shift;
  $self->{'tag_name'} = shift if @_;
  if (!defined $self->{'tag_name'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT tag_name FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'tag_name'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'tag_name'};

}

=head2 

 Arg: (optional) label
 Description: Gets/sets the name of the label used to visualize the tag (e.g. GFP)
 Returntype: string

=cut

sub label {

  my $self = shift;
  $self->{'label'} = shift if @_;
  if (!defined $self->{'label'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT tag_name FROM sequence_tag WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    ($self->{'label'}) = $sth->fetchrow_array();
    $sth->finish();
  }

  return $self->{'label'};

}

=head2 genes

 Arg: none
 Description: Gets the gene(s) this sequence maps to
 Returntype: list of Mitocheck::Gene objects

=cut

sub genes {

  my $self = shift;

  if (!defined $self->{'genes'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $gh = $dbc->get_GeneHandle;
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT DISTINCT geneID FROM tag_map_info WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    while ( my ($geneID) = $sth->fetchrow_array()) {
      my $gene = $gh->get_by_id($geneID);
      if (defined($gene)) {
        push @{$self->{'genes'}}, $gene;
      }
    }
  }
  if ($self->{'genes'}) {
    return @{$self->{'genes'}};
  }
  else {
    return undef;
  }

}

=head2 proteins

 Arg: none
 Description: Gets the protein(s) this sequence maps to
 Returntype: list of Mitocheck::Protein objects

=cut

sub proteins {

  my $self = shift;

  if (!defined $self->{'proteins'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $ph = $dbc->get_ProteinHandle;
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT DISTINCT proteinID FROM tag_map_info WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    while ( my ($proteinID) = $sth->fetchrow_array()) {
      my $protein = $ph->get_by_id($proteinID);
      if (defined($protein)) {
        push @{$self->{'proteins'}}, $protein;
      }
    }
  }

  return @{$self->{'proteins'}};

}

=head2 transcripts

 Arg: (optional) list of Transcript object
 Description: Gets/sets the transcript(s) this sequence maps to
 Returntype: list of Mitocheck::Transcript objects

=cut

sub transcripts {

  my ($self,@transcripts) = @_;
  push (@{$self->{'transcripts'}},@transcripts) if @transcripts;
  if (!defined $self->{'transcripts'}) {
    my $tagID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $th = $dbc->get_TranscriptHandle;
    my $dbh = $dbc->{'database_handle'};
    my $sth = $dbh->prepare("SELECT DISTINCT transcriptID FROM tag_map_info WHERE sequence_tagID = ?");
    $sth->execute($tagID);
    while (my ($transcriptID) = $sth->fetchrow_array()) {
      my $transcript = $th->get_by_id($transcriptID);
      if (defined($transcript)) {
        push @{$self->{'transcripts'}}, $transcript;
      }
    }
  }
  return @{$self->{'transcripts'}} if $self->{'transcripts'};

}

=head2 mapping_info

 Args: key=>value pairs
 Description: Sets mapping information of sequence tag relative to a transcript 
 Returntype: 1

=cut

sub mapping_info {

  my $self = shift;
  my %mapping = @_;
  my $transcript = $mapping{'-transcript'};
  unless (ref ($transcript) eq 'Mitocheck::Transcript') {
    my $dbc = $self->{'DBConnection'};
    my $th = $dbc->get_TranscriptHandle();
    if ($transcript=~/^ENST\d{11}/) {
      $transcript = $th->get_by_EnsemblID($transcript);
    }
    elsif ($transcript=~/^TRN\d{7}/) {
      $transcript = $th->get_by_id($transcript);
    }
    else {
      croak("Error: $transcript is not a valid transcript. You must provide either a Mitocheck::Transcript object or a Mitocheck transcript ID or an Ensembl transcript ID");
    }
  }
  if (!defined $transcript) {
    croak("WARNING: Transcript $mapping{'-transcript'} not found in database");
  }
  $self->transcripts($transcript);
  $self->mismatch($transcript,$mapping{'-mismatch'});
  $self->alignment($transcript,$mapping{'-alignment'});

  return ++$self->{'mapping_info'};

}

=head2 mismatch

 Arg1: Mitocheck::Gene or Mitocheck::Transcript or gene ID or
       transcript ID
 Arg2: optional, number of mismatches
 Description: Gets/sets number of mismatches between sequence tag and
              given transcript. If Arg1 is a gene, returns the
              maximum value of all transcripts for that gene.
 Returntype: integer

=cut

sub mismatch {

  my ($self,$target,$mismatch) = @_;
  my $targetID = ref($target) ? $target->ID(): $target;
  $self->{'mismatch'}{$targetID} = $mismatch if defined $mismatch;
  if (!defined $self->{'mismatch'}{$targetID}) {
    my $tagID = $self->{'ID'};
    my $dbh = $self->{'DBConnection'}->{'database_handle'};
    my $query;
    if ($targetID=~/^TRN\d{8}/i) {
      $query = "SELECT tmi.mismatch FROM tag_map_info tmi
                WHERE tmi.sequence_tagID = ?
                AND tmi.transcriptID = ?
                ORDER BY tmi.mismatch";
    }
    elsif ($targetID=~/^GEN\d{8}/i) {
      # keep the lowest value per transcript but the highest for the gene
      $query = "SELECT MIN(mismatch)
                FROM tag_map_info
                WHERE sequence_tagID = ?
                AND geneID = ?
                GROUP BY transcriptID
                ORDER BY mismatch
                DESC
                LIMIT 1";
    }
    else { return undef }

    my $sth = $dbh->prepare($query);
    $sth->execute($tagID,$targetID);
    ($self->{'mismatch'}{$targetID}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'mismatch'}{$targetID};

}

=head2 alignment

 Arg1: Mitocheck::Transcript or transcript ID
 Arg2: optional, preformated alignment
 Description: Gets/sets alignment of sequence to a transcript
 Returntype: string

=cut

sub alignment {

  my ($self,$target,$alignment) = @_;
  my $targetID = ref($target) ? $target->ID(): $target;
  $self->{'alignment'}{$targetID} = $alignment if $alignment;
  if (!defined $self->{'alignment'}{$targetID}) {
    my $tagID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $query;
    if ($targetID=~/^TRN\d{7}/i) {
      $query = "SELECT tmi.alignment FROM tag_map_info tmi
                WHERE tmi.sequence_tagID = ?
                AND tmi.transcriptID = ?
                ORDER BY tmi.mismatch";
    }
    else { return undef }

    my $sth = $dbh->prepare($query);
    $sth->execute($tagID,$targetID);
    ($self->{'alignment'}{$targetID}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'alignment'}{$targetID};

}

sub DESTROY {

  my $self = shift;
  $self->_decr_tag_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
