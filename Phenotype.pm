# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::Phenotype

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new( -database => 'mitocheck2_0',
					 -host     => 'www.mitocheck.org',
					 -user     => 'anonymous',
					 -password => '');


 my $phenotype_handle = $dbc->get_PhenotypeHandle();

 my $phenotype = $phenotype_handle->get_by_id('PHN00000001');

 my $description = $phenotype->description();
 my $class = $phenotype->class();

=head1 DESCRIPTION

 Representation of Mitocheck phenotypes.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Phenotype;


use strict;
use warnings;
use Scalar::Util qw(weaken);


{
  my $_phen_count = 0;
  my $_newID_count = 0;

  sub get_phenotype_count {
    return $_phen_count;
  }
  sub _incr_phen_count {
    return ++$_phen_count;
  }
  sub _decr_phen_count {
    return --$_phen_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, phenotype ID
 Description: Creates a new phenotype object.
 Returntype: Mitocheck::Phenotype

=cut

sub new {

  my ($class,$dbc,$phenotypeID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_phen_count();
  if (!defined $self->{'ID'} && !$phenotypeID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   phenotypeID
                 FROM     phenotype
                 ORDER BY phenotypeID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $phenotypeID = "PHN"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $phenotypeID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck phenotype ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 description

 Arg: optional, text
 Description: Gets/sets description of phenotype
 Returntype: string

=cut

sub description {

  my $self = shift;
  $self->{'description'} = shift if @_;
  if (!defined $self->{'description'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT description
                             FROM phenotype
                             WHERE phenotypeID  = ? ");
    $sth->execute($phenotypeID);
    ($self->{'description'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'description'};

}

=head2 reproducibility

 Arg1: (optional) Mitocheck::dsRNA
 Arg2: (optional) string, "number of replicates giving the
       phenotype/total number of replicates"
 Description: Gets/sets number of replicates yielding the
              phenotypes and total number of replicates using
              a given dsRNA
 Returntype: string

=cut

sub reproducibility {

  my $self = shift;
  my ($dsRNA,$repro);
  ($dsRNA,$repro) = @_ if @_;
  unless (defined($dsRNA)) {
  	$dsRNA = $self->{'caller'};
  }
  else {
  	$self->{'caller'} = $dsRNA;
  }
  $self->{'reproducibility'} = $repro if defined($repro);
  if(ref($dsRNA) ne 'Mitocheck::dsRNA') {
    die "ERROR: Reproducibility is only defined for phenotypes associated with a dsRNA: $!\n";
  }
  if (!defined($self->{'reproducibility'})) {
    my $phenotypeID = $self->{'ID'};
    my $oligo_pairID = $dsRNA->ID();
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT reproducibility FROM dsRNA_gives_phenotype
                             WHERE oligo_pairID = ? AND phenotypeID = ?");
    $sth->execute($oligo_pairID,$phenotypeID);
    ($self->{'reproducibility'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'reproducibility'};
}

=head2 dsRNA_index

 Arg1: Mitocheck::Gene
 Arg2: optional, string, "number of reproducible dsRNAs targeting the
       specified gene that give the phenotype/total number
       of dsRNAs targeting the gene in the experiment"
 Description: Gets/sets number of dsRNAs giving the phenotypes
              and total number of dsRNAs for a given gene.
 Returntype: string

=cut

sub dsRNA_index {

  my $self = shift;
  my ($gene,$index) = @_;
  unless (defined($gene)) {
  	$gene = $self->{'caller'};
  }
  else {
  	$self->{'caller'} = $gene;	
  }
  $self->{'dsRNA_index'} = $index if defined($index);
  if(ref($gene) ne 'Mitocheck::Gene') {
    die "dsRNA_index is only defined for phenotypes associated with a gene\n";
  }
  if (!defined $self->{'dsRNA_index'}) {
    my $phenotypeID = $self->{'ID'};
    my $geneID = $gene->ID();
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT dsRNA_index FROM gene_has_phenotype
                             WHERE geneID= ? AND phenotypeID = ?");
    $sth->execute($geneID,$phenotypeID);
    ($self->{'dsRNA_index'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'dsRNA_index'};
}

=head2 classes

 Arg: optional, list of phenotype classes
 Description: Gets/sets classes the phenotype belongs to
 Returntype: list of strings

=cut

sub classes {

  my ($self,@classes) = @_;
  @{$self->{'class'}} = @classes if @classes;
  if (!defined $self->{'class'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT c.name
                             FROM phenotype_class c, phenotype_has_class phc
                             WHERE phc.phenotypeID  = ?
                             AND phc.classID = c.classID");
    $sth->execute($phenotypeID);
    while (my $pc=$sth->fetchrow_array()) {
      push (@{$self->{'class'}},$pc);
    }
    $sth->finish();
  }
  if (defined $self->{'class'}) {
    return @{$self->{'class'}};
  }
  else {
    return undef;
  }
}


=head2 source

 Arg: optional, Mitocheck::Source
 Description: Gets/sets source of the phenotype data
              (e.g. Mitocheck primary screen), see also origin
 Returntype: Mitocheck::Source

=cut

sub source {

  my ($self,$source) = @_;
  $self->{'source'} = $source if $source;
  if (!defined $self->{'source'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth;
    $sth = $dbh->prepare("SELECT sourceID
                          FROM phenotype
                          WHERE phenotypeID  = ?");
    $sth->execute($phenotypeID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};

}

=head2 origin

 Arg: optional, string
 Description: Gets/sets cause of the phenotype
              (e.g. dsRNA, mutation...)
 Returntype: string

=cut

sub origin {

  my ($self,$origin) = @_;
  $self->{'origin'} = $origin if $origin;
  if (!defined $self->{'origin'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth;
    $sth = $dbh->prepare("SELECT origin
                          FROM phenotype
                          WHERE phenotypeID  = ?");
    $sth->execute($phenotypeID);
    ($self->{'origin'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'origin'};

}


=head2 image_sets

 Arg: optional,list of Mitocheck::ImageSet
 Description: Gets/sets list of image sets associated with
              caller (i.e. gene or dsRNA) showing the phenotype
 Returntype: list of Mitocheck::ImageSet

=cut

sub image_sets {

  my ($self,@sets) = @_;
  @{$self->{'image_sets'}} = @sets if @sets;
  if (!defined($self->{'image_sets'})) {
    my $phenotypeID = $self->ID;
    my $caller = $self->{'caller'} || '';
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $query;
    my $callerID = ref($caller)? $caller->ID():$caller;
    if ($callerID=~/^GEN\d+/i) {
      $query = "SELECT DISTINCT ims.image_setID
                FROM image_set_has_phenotype ishp, image_set ims, dsRNA_map_info drmi
                WHERE     drmi.geneID  = '$callerID'
                AND     drmi.mismatch  = 0
                AND drmi.oligo_pairID  = ims.oligo_pairID
                AND   ims.image_setID  = ishp.image_setID
                AND   ishp.phenotypeID = ?";
    }
    elsif ($callerID=~/^DSR\d+/i) {
      $query = "SELECT DISTINCT ims.image_setID
                FROM image_set ims, image_set_has_phenotype ishp
                WHERE  ims.oligo_pairID = '$callerID'
                AND     ims.image_setID = ishp.image_setID
                AND    ishp.phenotypeID = ? ";
    }
    else {
      $query = "SELECT DISTINCT image_setID
                FROM image_set_has_phenotype
                WHERE phenotypeID= ?";
    }
    my $sth = $dbh->prepare($query);
    $sth->execute($phenotypeID);
    while (my ($setID) = $sth->fetchrow_array()) {
      my $ish = $dbc->get_ImageSetHandle();
      my $set = $ish->get_by_id($setID);
      push @{$self->{'image_sets'}},$set if $set;
    }
  }
  return @{$self->{'image_sets'}} if (defined($self->{'image_sets'}));
}

=head2 dsRNAs

 Arg: optional,list of Mitocheck::dsRNAs objects
 Description: Gets/sets list of dsRNAs for which the phenotype
              has been observed
 Returntype: list of Mitocheck::dsRNAs objects

=cut

sub dsRNAs {

  my ($self,@dsRNAs) = @_;
  @{$self->{'dsRNAs'}} = @dsRNAs if @dsRNAs;
  if (!defined($self->{'dsRNAs'})) {
    my $phenotypeID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $query = qq(SELECT DISTINCT oligo_pairID
                   FROM dsRNA_gives_phenotype
                   WHERE phenotypeID = ?);
    my $sth = $dbh->prepare($query);
    $sth->execute($phenotypeID);
    while (my ($dsRNAID) = $sth->fetchrow_array()) {
      my $dsRNAh = $dbc->get_dsRNAHandle();
      my $dsRNA = $dsRNAh->get_by_id($dsRNAID);
      push @{$self->{'dsRNAs'}},$dsRNA if $dsRNA;
    }
  }
  return @{$self->{'dsRNAs'}} if (defined($self->{'dsRNAs'}));
}

=head2 examples

 Arg: optional,list of Mitocheck::Image
 Description: Gets/sets list of examples of the phenotype
 Returntype: list of Mitocheck::Image

=cut

sub examples {

  my ($self,@images) = @_;
  @{$self->{'examples'}} = @images if @images;
  if (!defined $self->{'examples'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbc = $self->{'DBConnection'};
    my $dbh = $self->{'database_handle'};
    my $query = qq(SELECT filename, file_type, reporter1, reporter2, reporter3, comments, sourceID
                   FROM example e, phenotype_has_example phe
                   WHERE phe.phenotypeID  = ?
                   AND phe.exampleID = e.exampleID );
    my $sth = $dbh->prepare($query);
    $sth->execute($phenotypeID);
    while (my @row = $sth->fetchrow_array()) {
      if (@row) {
	    my $image = Mitocheck::Image->new($dbc);
	    $image->filename($row[0]);
	    $image->file_type($row[1]);
	    $image->reporters(@row[2,3,4]);
	    $image->comments($row[5]);
	    my $sh = $dbc->get_SourceHandle;
	    $image->source($sh->get_by_id($row[6]));
	    push @{$self->{'examples'}},$image;
      }
    }
  }
  return () unless(defined($self->{'examples'}));

  return @{$self->{'examples'}};
}

=head2 annotation_type

 Arg: string
 Description: Gets/sets type of annotation for this phenotype
              (e.g. manual or automatic)
 Returntype: string

=cut

sub annotation_type {

  my $self = shift;
  $self->{'annotation_type'} = shift if @_;
  if (!defined $self->{'annotation_type'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT annotation_type
                             FROM phenotype
                             WHERE phenotypeID  = ? ");
    $sth->execute($phenotypeID);
    ($self->{'annotation_type'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'annotation_type'};

}

=head2 CMPOterm

 Arg: string
 Description: Gets/sets the Cellular Microscopy Phenotype Ontology term for this phenotype
 Returntype: string

=cut

sub CMPOterm {

  my $self = shift;
  $self->{'CMPOterm'} = shift if @_;
  if (!defined $self->{'CMPOterm'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT CMPOterm
                             FROM phenotype
                             WHERE phenotypeID  = ? ");
    $sth->execute($phenotypeID);
    ($self->{'CMPOterm'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'CMPOterm'};

}

=head2 CMPOID

 Arg: string
 Description: Gets/sets the Cellular Microscopy Phenotype Ontology term ID for this phenotype
 Returntype: string

=cut

sub CMPOID {

  my $self = shift;
  $self->{'CMPOID'} = shift if @_;
  if (!defined $self->{'CMPOID'}) {
    my $phenotypeID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT CMPOID
                             FROM phenotype
                             WHERE phenotypeID  = ? ");
    $sth->execute($phenotypeID);
    ($self->{'CMPOID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'CMPOID'};

}

sub AUTOLOAD {

  my $self = shift;
  my $attribute = our $AUTOLOAD;
  $attribute =~s/.*:://;
  $self->{$attribute} = shift if @_;
  if (!defined $self->{$attribute}) {
    my $phenotypeID = $self->{'ID'};
    my $caller = $self->{'caller'} || '';
    my $dbh = $self->{'database_handle'};
    if (ref($caller) eq 'Mitocheck::ImageSet') {
      my $callerID = $caller->ID;
      # image phenotype
      my $sth = $dbh->prepare("SELECT a.value
                               FROM image_set_has_attribute isha, attribute a, image i
                               WHERE isha.image_setID = ?
                               AND isha.attributeID = a.attributeID
                               AND a.name = ? ");
      $sth->execute($callerID,$attribute);
      ($self->{$attribute})=$sth->fetchrow_array();
      $sth->finish();
    }
    elsif (ref($caller) eq 'Mitocheck::dsRNA') {
      my $callerID = $caller->ID;
      # dsRNA phenotype
      my $sth = $dbh->prepare("SELECT a.value
                               FROM dsRNA_has_attribute dha, attribute a
                               WHERE dha.oligo_pairID = ?
                               AND dha.attributeID = a.attributeID
                               AND a.name = ? ");
      $sth->execute($callerID,$attribute);
      ($self->{$attribute})=$sth->fetchrow_array();
      $sth->finish();
    }
    else {
      # implement gene phenotypes
    }
  }
  return $self->{$attribute};

}


sub DESTROY {

  my $self = shift;
  $self->_decr_phen_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}



1;
