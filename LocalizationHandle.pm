# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::LocalizationHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $localization_handle = $dbc->get_LocalizationHandle();

 my $localization = $localization_handle->get_by_id('LOC00000023');


=head1 DESCRIPTION

 Enables retrieval of Mitocheck subcellular localization objects.

=head1 CONTACT

heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::LocalizationHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Localization;
use Scalar::Util qw(weaken);



=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new Localization object handle
 Returntype: Mitocheck::LocalizationHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  $self->{'database_handle'} = $self->{'DBConnection'}->{'database_handle'};
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: gene ID
 Description: Gets Localization with given ID
 Returntype: Mitocheck::Localization object

=cut

sub get_by_id {

  my ($self,$localizationID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  #  Check if localization exists in database
  my $query = qq(SELECT COUNT(*) FROM protein_localization_occurrence WHERE localization_occurrenceID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($localizationID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::Localization->new($dbc,$localizationID);

}

=head2 get_all_by_protein

 Arg: Mitocheck::Protein or protein ID
 Description: Gets localizations of given protein
 Returntype: list of Mitocheck::Localization objects

=cut

sub get_all_by_protein {

  my ($self,$protein) = @_;
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT localization_occurrenceID
                           FROM protein_localization_occurrence
                           WHERE proteinID = ?");
  $sth->execute($proteinID);
  my @localizations;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@localizations,$self->get_by_id($id));
  }
  $sth->finish();

  return @localizations;

}

=head2 get_all_by_source

 Arg: Mitocheck::Source or source ID
 Description: Gets localization occurrences from a given source
 Returntype: list of Mitocheck::Localization objects

=cut

sub get_all_by_source {

  my ($self,$source) = @_;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT localization_occurrenceID
                           FROM protein_localization_occurrence
                           WHERE sourceID = ?");
  $sth->execute($sourceID);
  my @localizations;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@localizations,$self->get_by_id($id));
  }
  $sth->finish();

  return @localizations;

}

=head2 new_localization

 Arg: key=>value pairs
 Description: creates a new localization object
 Returntype: Mitocheck::Localization

=cut

sub new_localization {

  my $self = shift;
  my %localization = @_;
  my $dbc = $self->{'DBConnection'};
  unless (defined $localization{'-protein'}) {
    croak "Error: -protein argument required for localization";
  }
  unless (defined $localization{'-event'}) {
    croak "Error: -event argument required for localization";
  }
  unless (defined $localization{'-cellular_component'}) {
    croak "Error: -cellular_component argument required for localization";
  }
  unless (defined $localization{'-source'}) {
    croak "Error: -source argument required for localization";
  }
 
  my $localization = Mitocheck::Localization->new($dbc);
  $localization->protein($localization{'-protein'});
  $localization->sequence_tag($localization{'-sequence_tag'});
  $localization->event($localization{'-event'});
  $localization->source($localization{'-source'});
  $localization->cellular_component($localization{'-cellular_component'});
  $localization->image_sets(@{$localization{'-image_sets'}}) if ($localization{'-image_sets'});

  return $localization;

}

=head2 store

 Arg: Mitocheck::Localization
 Description: Enters localization and associated data in Mitocheck database
 Returntype: Mitocheck::Localization, normally same as Arg

=cut

sub store {

  my ($self,$localization) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  # Check if localization occurrence already in database.
  # Two occurrences are the same if they have same protein,
  # same cellular component, same event and same source
  my $proteinID = $localization->protein->ID;
  my $eventID = $localization->event->ID;
  my $cellular_componentID = $localization->cellular_component->ID;
  my $sequence_tag = $localization->sequence_tag;
  my $seqTagID = $sequence_tag ? $sequence_tag->ID : "";
  my $sourceID = $localization->source->ID;
  my $query = qq(SELECT localization_occurrenceID
                 FROM protein_localization_occurrence
                 WHERE proteinID= ?
                 AND cellular_componentID = ?
                 AND eventID = ?
                 AND sequence_tagID = ?
                 AND sourceID = ? );
  my $sth = $dbh->prepare($query);
  $sth->execute($proteinID,$cellular_componentID,$eventID,$seqTagID,$sourceID);
  my ($locID) = $sth->fetchrow_array();
  $sth->finish();
  if ($locID) {
    $localization->ID($locID);
  }
  else {
    my $sourceID = $localization->source->ID;
    my $locID = $localization->ID;
    my $query = qq(INSERT INTO protein_localization_occurrence (localization_occurrenceID,proteinID,cellular_componentID,eventID,sequence_tagID,sourceID)
                VALUES ('$locID','$proteinID','$cellular_componentID','$eventID','$seqTagID','$sourceID'));
    my $rows = $dbh->do($query);
  }
  if ($localization->{'image_sets'}) { # Deal with associated images
  	my $ish = $dbc->get_ImageSetHandle();
  	foreach my $set(@{$localization->{'image_sets'}}) {
  	  $ish->store($set);
  	  my $locID = $localization->ID;
  	  my $setID = $set->ID;
  	  my $query = qq(INSERT IGNORE INTO localization_occurrence_has_image_set (localization_occurrenceID,image_setID)
  	                 VALUES ('$locID','$setID'));
  	  my $rows = $dbh->do($query);
  	}
  }

  $localization->_decr_newID_count() if $localization->{'is_new'};
  $localization->{'is_new'} = undef;

  return $localization;
}

1;
