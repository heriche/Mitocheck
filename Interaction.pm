# Author: jkh1
# 2012-09-24
#

=head1 NAME

 Mitocheck::Interaction

=head1 SYNOPSIS



=head1 DESCRIPTION

 Represents a particular occurrence of a protein-protein interaction

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Interaction;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_interaction_count = 0;
  my $_newID_count = 0;

  sub get_interaction_count {
    return $_interaction_count;
  }
  sub _incr_interaction_count {
    return ++$_interaction_count;
  }
  sub _decr_interaction_count {
    return --$_interaction_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }

}



=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: interaction (occurrence) ID
 Arg3: optional, Mitocheck::Protein
 Arg4: optional, Mitocheck::Protein
 Arg5: optional, interaction type
 Arg6: optional, Mitocheck::CellularComponent
 Arg7: optional, Mitocheck::Event
 Arg8: optional, Mitocheck::Source
 Description: Creates a new interaction object.
 Returntype: Mitocheck::Interaction

=cut

sub new {

  my ($class,$dbc,$id,$partner1,$partner2,$int_type,$cellular_component,$event,$source) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbc->{'database_handle'};
  $class->_incr_interaction_count();
  if (!defined $self->{'ID'} && !$id) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   interaction_occurrenceID
                 FROM     interaction_occurrence
                 ORDER BY interaction_occurrenceID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $id = "INT"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $id;
  $self->{'partner1'} = $partner1 if $partner1;
  $self->{'partner2'} = $partner2 if $partner2;
  $self->{'type'} = $int_type if $int_type;
  $self->{'cellular_component'} = $cellular_component if $cellular_component;
  $self->{'event'} = $event if $event;
  $self->{'source'} = $source if $source;

  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, interaction ID
 Description: Gets/sets ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 type

 Arg: optional, interaction type
 Description: Gets/sets type of interaction
 Returntype: string

=cut

sub type {

  my $self = shift;
  $self->{'type'} = shift if @_;
  if (!defined ($self->{'type'})) {
  	my $interactionID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth;
    $sth = $dbh->prepare("SELECT pi.interaction_type
                          FROM protein_interaction pi, interaction_occurrence io
                          WHERE io.interaction_occurrenceID = ?
                          AND io.interactionID = pi.interactionID");
    $sth->execute($interactionID);
    my ($interaction_type) = $sth->fetchrow_array();
    $sth->finish();
    $self->{'type'} = $interaction_type;
  }
  return $self->{'type'};

}

=head2 partners

 Arg: optional, pair of interacting Protein objects
 Description: Gets/sets pair of protein that are part of this interaction
 Returntype: list of Protein objects

=cut

sub partners {

  my $self = shift;
  $self->{'partner1'} = shift if @_;
  $self->{'partner2'} = shift if @_;
  if (!defined ($self->{'partner1'}) || !defined ($self->{'partner2'}) ) {
    my $interactionID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth;
    $sth = $dbh->prepare("SELECT pi.proteinID,pi.interactorID
                          FROM protein_interaction pi, interaction_occurrence io
                          WHERE io.interaction_occurrenceID = ?
                          AND io.interactionID = pi.interactionID");
    $sth->execute($interactionID);
    my ($prot1ID,$prot2ID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $ph = $dbc->get_ProteinHandle();
    if ($prot1ID && $prot2ID) {
      $self->{'partner1'} = $ph->get_by_id($prot1ID);
      $self->{'partner2'} = $ph->get_by_id($prot2ID);
    }
  }

  return $self->{'partner1'},$self->{'partner2'};

}

=head2 source

 Arg: optional, Mitocheck::Source
 Description: Gets/sets source of the interaction
 Returntype: Mitocheck::Source

=cut

sub source {

  my ($self,$source) = @_;
  $self->{'source'} = $source if $source;
  if (!defined $self->{'source'}) {
    my $interactionID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT sourceID
                             FROM interaction_occurrence
                             WHERE interaction_occurrenceID = ?");
    $sth->execute($interactionID);
    my ($sourceID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $sh = $dbc->get_SourceHandle();
    $self->{'source'} = $sh->get_by_id($sourceID);
  }
  return $self->{'source'};
}

=head2 bait

 Arg: optional, Mitocheck::Protein
 Description: Gets/sets protein used as bait to recover this interaction
              Only defined for MitoCheck or MitoSys data.
 Returntype: Mitocheck::Protein

=cut

sub bait {

  my ($self,$bait) = @_;
  $self->{'bait'} = $bait if $bait;
  if (!defined $self->{'bait'}) {
    my $source_name = $self->source->name;
    # By convention, partner1 is the bait for MitoCheck and MitoSys data
    if ($source_name =~/MitoCheck|MitoSys/i) {
      ($self->{'bait'},undef) = $self->partners;
    }
  }
  return $self->{'bait'};
}

=head2 cellular_component

 Arg: optional, Mitocheck::CellularComponent
 Description: Gets/sets where in the cell this interaction was detected
 Returntype: Mitocheck::CellularComponent

=cut

sub cellular_component {

  my ($self,$cellular_component) = @_;
  $self->{'cellular_component'} = $cellular_component if defined($cellular_component);
  if(!defined($self->{'cellular_component'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT cellular_componentID
                             FROM interaction_occurrence
                             WHERE interaction_occurrenceID = ?");
    $sth->execute($self->{'ID'});
    my ($cellular_componentID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $cch = $dbc->get_CellularComponentHandle();
    $self->{'cellular_component'} = $cch->get_by_id($cellular_componentID);
  }
  return $self->{'cellular_component'};
}

=head2 event

 Arg: optional, Mitocheck::Event
 Description: Gets/sets when this interaction was detected
 Returntype: Mitocheck::Event

=cut

sub event {

  my ($self,$event) = @_;
  $self->{'event'} = $event if defined($event);
  if(!defined($self->{'event'})) {
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT eventID
                             FROM interaction_occurrence
                             WHERE interaction_occurrenceID = ?");
    $sth->execute($self->{'ID'});
    my ($eventID) = $sth->fetchrow_array();
    $sth->finish();
    my $dbc = $self->{'DBConnection'};
    my $eh = $dbc->get_EventHandle();
    $self->{'event'} = $eh->get_by_id($eventID);
  }
  return $self->{'event'};
}

=head2 measurements

 Description: Gets/sets measurements associated with this interaction
 Returntype: list of Mitocheck::Measurement

=cut

sub measurements {

  my ($self,@measurements) = @_;
  @{$self->{'measurements'}} = @measurements if @measurements;
  if (!defined $self->{'measurements'}) {
    my $dbc = $self->{'DBConnection'};
    my $mh = $dbc->get_MeasurementHandle();
    @{$self->{'measurements'}} = $mh->get_all_by_interaction($self);
  }
  return @{$self->{'measurements'}} if (defined($self->{'measurements'}));
}

sub DESTROY {

  my $self = shift;
  $self->_decr_interaction_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
