# Author: jkh1
# 2008-09-03
#

=head1 NAME

 Mitocheck::ImageSetHandle

=head1 SYNOPSIS



=head1 DESCRIPTION

 Enables retrieval of Mitocheck image objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::ImageSetHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::ImageSet;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new image set object handle
 Returntype: Mitocheck::ImageSetHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: image ID
 Description: Gets image set with given ID
 Returntype: Mitocheck::ImageSet object

=cut

sub get_by_id {

  my ($self,$setID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if image set exists in database
  my $query = qq(SELECT COUNT(*) FROM image_set WHERE image_setID= ?);
  my $sth= $dbh->prepare($query);
  $sth->execute($setID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::ImageSet($dbc,$setID);

}

=head2 get_by_spot

 Arg1: spot location, usually in the form plateID/spot_number
       (e.g. LT00123/284)
 Arg2: source of the image set as Mitocheck::Source object
 Description: Gets image set for the corresponding plate position
 Returntype: Mitocheck::ImageSet object

=cut

sub get_by_spot {

  my ($self,$spot,$source) = @_;
  if (!defined($source)) {
    croak "Need source of image set.\n";
  }
  my $sourceID = ref($source)? $source->ID:$source;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT image_setID
                 FROM image_set
                 WHERE spot_location=?
                 AND sourceID=?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($spot,$sourceID);
  my ($setID) = $sth->fetchrow_array();
  my $set = $self->get_by_id($setID);
  $sth->finish();

  return $set;

}


=head2 get_all_by_dsRNA

 Arg: Mitocheck::dsRNA or dsRNA ID
 Description: Gets image sets obtained using given dsRNA
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub get_all_by_dsRNA {

  my ($self,$dsRNA) = @_;
  my $dsRNAID = ref($dsRNA) ? $dsRNA->ID() : $dsRNA;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT image_setID FROM dsRNA_has_image_set
                           WHERE oligo_pairID = ? ");
  $sth->execute($dsRNAID);
  my @sets;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@sets,$self->get_by_id($id));
  }
  $sth->finish();

  return @sets;

}

=head2 get_all_by_protein

 Arg: Mitocheck::Protein or protein ID
 Description: Gets image sets showing localization of given protein
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub get_all_by_protein {

  my ($self,$protein) = @_;
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT lohis.image_setID
                           FROM localization_occurrence_has_image_set lohis,
                                protein_localization_occurrence plo
                           WHERE plo.proteinID = ?
                           AND plo.localization_occurrenceID = lohis.localization_occurrenceID");
  $sth->execute($proteinID);
  my @sets;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@sets,$self->get_by_id($id));
  }
  $sth->finish();

  return @sets;

}

=head2 get_all_by_gene

 Arg1: Mitocheck::Gene or gene ID
 Arg2: (optional) image class, currently 'phenotype' or 'localization'
 Description: Gets image sets of selected class associated with
              given gene. If no class selected then gets all
              image sets for given gene
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub get_all_by_gene {

  my ($self,$gene,$class) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbh = $self->{'database_handle'};
  my ($query,$sth);
  $class ||= "";
  if ($class eq 'phenotype') {
    $query = "SELECT drhis.image_setID
              FROM dsRNA_has_image_set drhis, dsRNA_map_info drmi
              WHERE drhis.oligo_pairID = drmi.oligo_pairID
              AND drmi.geneID = ?
              AND drmi.mismatch = 0";
    $sth = $dbh->prepare($query);
    $sth->execute($geneID);
  }
  elsif ($class eq 'localization') {
    $query = "SELECT lohis.image_setID
              FROM transcript t, localization_occurrence_has_image_set lohis,
                   protein_localization_occurrence plo
              WHERE t.geneID = ?
              AND t.proteinID = plo.proteinID
              AND plo.localization_occurrenceID = lohis.localization_occurrenceID";
    $sth = $dbh->prepare($query);
    $sth->execute($geneID);
  }
  else {
    $query = "(SELECT drhis.image_setID
              FROM dsRNA_has_image_set drhis, dsRNA_map_info drmi
              WHERE drhis.oligo_pairID = drmi.oligo_pairID
              AND drmi.geneID = ?
              AND drmi.mismatch = 0)
              UNION
              (SELECT lohis.image_setID
              FROM transcript t, localization_occurrence_has_image_set lohis,
                   protein_localization_occurrence plo
              WHERE t.geneID = ?
              AND t.proteinID = plo.proteinID
              AND plo.localization_occurrenceID = lohis.localization_occurrenceID)";
    $sth = $dbh->prepare($query);
    $sth->execute($geneID,$geneID);
  }
  my @sets;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@sets,$self->get_by_id($id));
  }
  $sth->finish();

  return @sets;

}

=head2 get_all_by_source

 Arg: Mitocheck::Source or source ID
 Description: Gets image sets from the given source
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub get_all_by_source {

  my ($self,$source) = @_;
  my $sourceID = ref($source) ? $source->ID() : $source;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT image_setID FROM image_set
                           WHERE sourceID = ? ");
  $sth->execute($sourceID);
  my @sets;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@sets,$self->get_by_id($id));
  }
  $sth->finish();

  return @sets;

}

=head2 get_all_by_FCSexperiment

 Arg: Mitocheck::FCSexperiment or FCS experiment ID
 Description: Gets image sets from the given FCS experiment
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub get_all_by_FCSexperiment {

  my ($self,$exp) = @_;
  my $expID = ref($exp) ? $exp->ID() : $exp;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT image_setID
                           FROM FCSexperiment_has_image_set
                           WHERE FCSexperimentID  = ?");
  $sth->execute($expID);
  my @sets;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@sets,$self->get_by_id($id));
  }
  $sth->finish();

  return @sets;

}

=head2 get_all_by_localization

 Arg: Mitocheck::Localization object or protein localization occurrence ID
 Description: Gets image sets associated with the given protein localization occurrence
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub get_all_by_localization {

  my ($self,$loc) = @_;
  my $locID = ref($loc) ? $loc->ID() : $loc;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT image_setID
    					   FROM localization_occurrence_has_image_set
                           WHERE localization_occurenceID = ?");
  $sth->execute($locID);
  my @sets;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@sets,$self->get_by_id($id));
  }
  $sth->finish();

  return @sets;

}

=head2 get_all_by_cellular_component

 Arg: Mitocheck::CellularComponent object or cellular component ID
 Description: Gets image sets associated with the given cellular component
 Returntype: list of Mitocheck::ImageSet objects

=cut

sub get_all_by_cellular_component {

  my ($self,$cc) = @_;
  my $ccID = ref($cc) ? $cc->ID() : $cc;
  my $dbh = $self->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT lohims.image_setID
    					   FROM localization_occurrence_has_image_set lohims,
    					        protein_localization_occurrence plo
                           WHERE plo.cellular_componentID = ?
                           AND plo.localization_occurrenceID = lohims.localization_occurrenceID");
  $sth->execute($ccID);
  my @sets;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@sets,$self->get_by_id($id));
  }
  $sth->finish();

  return @sets;

}

=head2 new_set

 Args: key => value pairs
 Description: creates a new image set object
 Returntype: Mitocheck::ImageSet

=cut

sub new_set {

  my $self = shift;
  my %set = @_;
  my $dbc = $self->{'DBConnection'};
  my @required_attributes=('-images','-class','-dsRNA','-dsRNAs','-proteins','-source');
  # Require protein(s) for localization and dsRNA(s) for phenotype
  if ($set{'-class'} eq 'phenotype' && !$set{'-dsRNA'} && !$set{'-dsRNAs'}) {
    croak "Error: Image set of class 'phenotype' requires the '-dsRNAs' argument.";
  }
  if ($set{'-class'} eq 'localization' && !$set{'-proteins'}) {
    croak "Error: Image set of class 'localization' requires the '-proteins' argument.";
  }
  my $set = Mitocheck::ImageSet->new($dbc);
  foreach my $image(@{$set{'-images'}}) {
  	$image->setID($set->ID);
  }
  $set->images(@{$set{'-images'}});
  $set->class($set{'-class'});
  $set->source($set{'-source'});
  $set->dsRNA($set{'-dsRNA'}) if $set{'-dsRNA'};
  $set->dsRNAs(@{$set{'-dsRNAs'}}) if $set{'-dsRNAs'};
  $set->proteins(@{$set{'-proteins'}}) if $set{'-proteins'};
  $set->control($set{'-control'}) if ($set{'-control'});
  $set->spot_location($set{'-spot_location'}) if $set{'-spot_location'};
  $set->comments($set{'-comments'}) if $set{'-comments'};

  return $set;

}

=head2 store

 Arg: Mitocheck::ImageSet
 Description: Enters image set and associated data in Mitocheck database.
 Returntype: Mitocheck::ImageSet, same as Arg

=cut

sub store {

  my ($self,$set) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my @images = $set->images;
  if (!@images) {
    croak "Set has no images.";
  }
  my $class = $set->class();
  if (!$class) {
    croak "Class required.";
  }
  my $sourceID = $set->source()->ID();
  if (!$sourceID) {
    croak "Source required.";
  }
  my $comments = $set->comments() || "";
  my $control = $set->control();
  my $controlID ="";
  if ($control) {
    $controlID = ref($control)? $control->ID():$control;
  }
  my $spot_location = $set->spot_location();
  $spot_location ||="";
  my $setID = $set->ID();
  # Check if set already exists
  my %seen;
  foreach my $image(@images) {
    my $qfilename = $dbh->quote($image->filename);
    my $query =qq(SELECT image_setID FROM image WHERE filename=$qfilename);
    my $sth = $dbh->prepare($query);
    $sth->execute();
    my ($setID)=$sth->fetchrow_array();
    $sth->finish();
    $seen{$setID}++ if $setID;
  }
  if (scalar(keys %seen)== 1) {
    # These images are already in a set
    carp "WARNING: A set already exists with these images";
    my @IDs = keys %seen;
    $set->ID($IDs[0]);
  }
  elsif (scalar(keys %seen)>1) {
    croak "\nERROR: Images are already in different sets";
  }
  else {
    # Store new set
    my $ih = $dbc->get_ImageHandle();
    foreach my $image(@images) {
      $image->setID($setID);
      $ih->store($image);
      my $imageID = $image->ID;
    }
    my $query=qq(INSERT IGNORE INTO image_set (image_setID,sourceID,controlID,spot_location,class)
                 VALUES ('$setID','$sourceID','$controlID','$spot_location','$class'));
    $dbh->do($query);
  }
  $set->_decr_newID_count() if $set->{'is_new'};
  $set->{'is_new'} = undef;

  return $set;

}

sub remove {

  my ($self,$set) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $ih = $dbc->get_ImageHandle();
  foreach my $image($set->images) {
    $ih->remove($image);
  }

  return $set->ID;
}


1;
