# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::Transcript

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $transcript_handle = $dbc->get_TranscriptHandle();

 my $transcript = $transcript_handle->get_by_id('TRN00000012');

 my $protein = $transcript->protein();
 my $transcript_EnsemblID = $transcript->EnsemblID();

=head1 DESCRIPTION

 Representation of Mitocheck transcripts.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::Transcript;


use strict;
use warnings;
use Scalar::Util qw(weaken);

{
  my $_transcript_count = 0;
  my $_newID_count = 0;

  sub get_transcript_count {
    return $_transcript_count;
  }
  sub _incr_transcript_count {
    return ++$_transcript_count;
  }
  sub _decr_transcript_count {
    return --$_transcript_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, transcript ID
 Description: Creates a new transcript object.
 Returntype: Mitocheck::Transcript

=cut

sub new {

  my ($class,$dbc,$transcriptID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbc->{'database_handle'};
  $class->_incr_transcript_count();
  if (!defined $self->{'ID'} && !$transcriptID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   transcriptID
                 FROM     transcript
                 ORDER BY transcriptID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i=substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $transcriptID = "TRN"."0"x(8-length $i).$i;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $transcriptID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck transcript ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 EnsemblID

 Arg: optional, Ensembl transcript ID
 Description: Gets/sets transcript's Ensembl ID
 Returntype: string

=cut

sub EnsemblID {

  my $self = shift;
  $self->{'EnsemblID'} = shift if @_;
  if (!defined $self->{'EnsemblID'}) {
    my $transcriptID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT t.EnsemblID
                             FROM transcript t
                             WHERE t.transcriptID = ? ");
    $sth->execute($transcriptID);
    ($self->{'EnsemblID'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'EnsemblID'};

}

=head2 protein

 Arg: optional, Mitocheck::Protein
 Description: Gets/sets protein produced by the transcript
 Returntype: Mitocheck::Protein

=cut

sub protein {

  my ($self,$protein) = @_;
  $self->{'protein'} = $protein if $protein;
  if (!defined $self->{'protein'}) {
    my $dbc = $self->{'DBConnection'};
    my $prh = $dbc->get_ProteinHandle();
    $self->{'protein'} = $prh->get_by_transcript($self);
  }
  return $self->{'protein'};

}

=head2 chromosome

 Arg: optional, chromosome
 Description: Gets/sets chromosome the transcript maps to
 Returntype: string

=cut

sub chromosome {

  my $self = shift;
  $self->{'chromosome'} = shift if @_;
  if (!defined $self->{'chromosome'}) {
    my $transcriptID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT DISTINCT drmi.chromosome
                             FROM dsRNA_map_info drmi
                             WHERE drmi.transcriptID = ? ");
    $sth->execute($transcriptID);
    ($self->{'chromosome'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'chromosome'};

}

=head2 gene

 Arg: optional, Mitocheck::Gene
 Description: Gets/sets gene the transcript is a product of
 Returntype: Mitocheck::Gene

=cut

sub gene {

  my ($self,$gene) = @_;
  $self->{'gene'} = $gene if $gene;
  if (!defined($self->{'gene'})) {
    my $dbc = $self->{'DBConnection'};
    my $gh = $dbc->get_GeneHandle();
    $self->{'gene'} = $gh->get_by_transcript($self);
  }
  return $self->{'gene'};

}


sub DESTROY {

  my $self = shift;
  $self->_decr_transcript_count();
  $self->_decr_newID_count() if $self->{'is_new'};

}

1;
