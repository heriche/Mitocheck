# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::GeneHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $gene_handle = $dbc->get_GeneHandle();

 my $gene1 = $gene_handle->get_by_id('GEN00000001');

 my $gene2 = $gene_handle->new_gene(-EnsemblID => $EnsemblID,
			            -symbol => $symbol,
			            -transcripts => [@transcripts],
                                    -names => [@names]);


=head1 DESCRIPTION

 Enables retrieval of Mitocheck gene objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::GeneHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Gene;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new gene object handle
 Returntype: Mitocheck::GeneHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: gene ID
 Description: Gets gene with given ID
 Returntype: Mitocheck::Gene object

=cut

sub get_by_id {

  my ($self,$geneID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  # check if gene exists in database
  my $query = qq( SELECT COUNT(*) FROM gene WHERE geneID= ? );
  my $sth= $dbh->prepare ($query);
  $sth->execute($geneID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::Gene($dbc,$geneID);

}

=head2 get_by_symbol

 Arg: gene symbol
 Description: Gets gene with given symbol
 Returntype: Mitocheck::Gene object

=cut

sub get_by_symbol {

  my ($self,$symbol) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT geneID FROM gene
                            WHERE symbol=?");
  $sth->execute($symbol);
  my ($geneID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $geneID );

  return $self->get_by_id($geneID);

}

=head2 get_by_EnsemblID

 Arg: Ensembl gene ID
 Description: Gets gene with given Ensembl ID
 Returntype: Mitocheck::Gene object

=cut

sub get_by_EnsemblID {

  my ($self,$EnsemblID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT geneID FROM gene
                           WHERE EnsemblID=?");
  $sth->execute($EnsemblID);
  my ($geneID) = $sth->fetchrow_array();
  $sth->finish();

  return undef if( !defined $geneID );

  return $self->get_by_id($geneID);

}

=head2 get_all_by_name

 Arg: string
 Description: Gets genes with names matching given string
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_all_by_name {

  my ($self,$name) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT g.geneID
		           FROM gene g,
		                gene_has_name ghn,
		                name n
	                   WHERE MATCH (n.name) AGAINST ( ? IN BOOLEAN MODE)
                           AND n.nameID   = ghn.nameID
		           AND ghn.geneID = g.geneID
                           ORDER BY symbol");
  $sth->execute($name);
  my @genes=();
  while (my ($g)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($g));
  }
  $sth->finish();

  return @genes;

}

=head2 get_all_by_dsRNA

 Arg: Mitocheck::dsRNA or dsRNA ID
 Description: Gets genes targeted by given dsRNA
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_all_by_dsRNA {

  my ($self,$dsRNA) = @_;
  my $dsRNAID = ref($dsRNA) ? $dsRNA->ID() : $dsRNA;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT drmi.geneID
                           FROM dsRNA_map_info drmi
                           WHERE drmi.oligo_pairID = ? ");
  $sth->execute($dsRNAID);
  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;
}

=head2 get_by_protein

 Arg: Mitocheck::Protein or protein ID
 Description: Gets gene producing given protein
 Returntype: Mitocheck::Gene object

=cut

sub get_by_protein {

  my ($self,$protein) = @_;
  my $proteinID = ref($protein) ? $protein->ID() : $protein;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT t.geneID
                           FROM transcript t
                           WHERE t.proteinID = ? ");
  $sth->execute($proteinID);
  my ($id) = $sth->fetchrow_array();
  $sth->finish();

  return $self->get_by_id($id);

}

=head2 get_by_transcript

 Arg: Mitocheck::Transcript or transcript ID
 Description: Gets gene producing given transcript
 Returntype: Mitocheck::Gene object

=cut

sub get_by_transcript {

  my ($self,$transcript) = @_;
  my $transcriptID = ref($transcript) ? $transcript->ID() : $transcript;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT t.geneID
                           FROM transcript t
                           WHERE t.transcriptID = ? ");
  $sth->execute($transcriptID);
  my ($id) = $sth->fetchrow_array();
  $sth->finish();
  return $self->get_by_id($id);

}

=head2 get_all_by_phenotype

 Arg: Mitocheck::Phenotype or phenotype ID
 Description: Gets genes with given phenotype
 Returntype: list of Mitocheck::Gene object

=cut

sub get_all_by_phenotype {

  my ($self,$phenotype) = @_;
  my $phenotypeID = ref($phenotype) ? $phenotype->ID() : $phenotype;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $sth = $dbh->prepare("SELECT DISTINCT geneID
                           FROM gene_has_phenotype
                           WHERE phenotypeID = ?");
  $sth->execute($phenotypeID);

  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;

}

=head2 get_all_by_phenotype_desc

 Arg: string
 Description: Gets genes with phenotype description matching the given string
 Returntype: list of Mitocheck::Gene object

=cut

sub get_all_by_phenotype_desc {

  my ($self,$query) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $qquery = $dbh->quote($query);
  my $sth = $dbh->prepare("SELECT DISTINCT ghp.geneID
		           FROM gene_has_phenotype ghp, phenotype ph
	                   WHERE MATCH (ph.description) AGAINST ($qquery IN BOOLEAN MODE)
 	                   AND  ph.phenotypeID = ghp.phenotypeID");
  $sth->execute();

  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;

}

=head2 get_all_by_localization_desc

 Arg: string
 Description: Gets genes with subcellular localization description
              matching the given string
 Returntype: list of Mitocheck::Gene object

=cut

sub get_all_by_localization_desc {

  my ($self,$query) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $qquery = $dbh->quote($query);
  my $sth = $dbh->prepare("SELECT DISTINCT t.geneID
		           FROM cellular_component cc, protein_localization_occurrence plo, transcript t
	                   WHERE MATCH (cc.description) AGAINST ($qquery IN BOOLEAN MODE)
 	                   AND  cc.cellular_componentID = plo.cellular_componentID
                           AND plo.proteinID = t.proteinID");
  $sth->execute();

  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;

}

=head2 get_all_genes

 Description: Gets all the genes in the database
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_all_genes {

  my $self = shift;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT geneID
                           FROM gene");
  $sth->execute();
  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;

}

=head2 get_random_genes

 Arg: number of genes to return
 Description: Gets a given number of genes at random
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_random_genes {

  my $self = shift;
  my $n = shift;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT geneID
                           FROM gene
                           ORDER by RAND() LIMIT $n");
  $sth->execute();
  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;

}

=head2 get_all_by_peptides

 Arg1: reference to list of Mitocheck::Peptide objects
 Arg2: Mitocheck::MSexperiment
 Description: Gets all the genes that have been assigned the given set
              of peptides in the given experiment
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_all_by_peptides {

  my $self = shift;
  my ($peptref,$exp) = @_;
  my @peptides = grep {defined($_) && $_} @{$peptref};
  unless (@peptides) {
    croak "Error: Peptides required";
  }
  my @oIDs = map {$_->ID} @peptides;
  my $oIDs = join("','",@oIDs);
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID : $exp;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  my @genes;
  if ($expID) {
    # The query peptide IDs are actually occurrence IDs
    my @pIDs;
    $sth = $dbh->prepare("SELECT DISTINCT peptideID
                          FROM peptide_occurrence
                          WHERE occurrenceID IN ('$oIDs')
                          AND experimentID='$expID'");
    $sth->execute();
    while (my ($id) = $sth->fetchrow_array()) {
      push @pIDs,$id;
    }
    my $pIDs = join("','",@pIDs);
    # This query gets all genes that share the whole list of peptides
    $sth = $dbh->prepare("SELECT DISTINCT pga2.geneID
                          FROM peptide_gene_assignment pga2
                          LEFT JOIN
                            (SELECT DISTINCT pga1.geneID
                             FROM peptide_gene_assignment pga1
                             WHERE pga1.peptideID NOT IN ('$pIDs')
                             AND pga1.experimentID='$expID') as notwanted
                          ON pga2.geneID = notwanted.geneID
                          WHERE notwanted.geneID is null
                          AND pga2.experimentID='$expID'");
    $sth->execute();
    while (my ($id)=$sth->fetchrow_array()) {
      push (@genes,$self->get_by_id($id));
    }
    $sth->finish();
  }
  return @genes;

}

=head2 get_all_by_shared_peptides

 Arg1: Mitocheck::Gene object or gene ID
 Arg2: Mitocheck::MSexperiment or MS experiment ID
 Description: Gets all the genes that share the same set of peptides as the given gene
              in the given experiment
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_all_by_shared_peptides {
  my $self = shift;
  my ($gene,$exp) = @_;	
  $gene ||= "";
  my $geneID = ref($gene) ? $gene->ID : $gene;
  $exp ||= "";
  my $expID = ref($exp) ? $exp->ID() : $exp;
  my @genes;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth = $dbh->prepare("SELECT DISTINCT pga2.geneID
  						   FROM peptide_gene_assignment pga2
  						   LEFT JOIN 
  						     (SELECT DISTINCT pga1.geneID
  						      FROM peptide_gene_assignment pga1
  						      WHERE pga1.peptideID NOT IN 
  						        (SELECT p1.peptideID
  						         FROM 
  						           (SELECT DISTINCT po.peptideID
  						            FROM peptide_gene_assignment pga, peptide_occurrence po
  						            WHERE pga.geneID = ?
  						            AND pga.experimentID = ?
  						            AND pga.peptideID = po.peptideID
  						            AND pga.experimentID = po.experimentID) AS p1)
  						      AND pga1.experimentID= ? ) AS notwanted
  						   ON pga2.geneID = notwanted.geneID 
  						   WHERE notwanted.geneID IS NULL
  						   AND pga2.experimentID= ?");
  $sth->execute($geneID,$expID,$expID,$expID);
  while (my ($id) = $sth->fetchrow_array()) {
    push @genes,$self->get_by_id($id) if ($id);
  }
  return @genes;
  
}

=head2 get_all_by_copurification

 Arg1: Mitocheck::Gene or gene ID
 Arg2: (optional) 1 to remove contaminants
 Description: Gets genes found in MS experiments where the given gene was also found
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_all_by_copurification {

  my ($self,$gene,$no_contaminants) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $sth;
  unless ($no_contaminants) {
	$sth = $dbh->prepare("SELECT DISTINCT t1.geneID
						  FROM peptide_gene_assignment t1
						  WHERE t1.experimentID IN 
						  	(SELECT DISTINCT t2.experimentID 
						  	 FROM (SELECT distinct pga.experimentID
						  		   FROM peptide_gene_assignment pga 
						  		   WHERE pga.geneID= ?) as t2");
  }
  else {
  		$sth = $dbh->prepare("SELECT DISTINCT t1.geneID
						  FROM peptide_gene_assignment t1
						  WHERE t1.experimentID IN 
						  	(SELECT DISTINCT t2.experimentID 
						  	 FROM (SELECT distinct pga.experimentID
						  		   FROM peptide_gene_assignment pga 
						  		   WHERE pga.geneID= ?) as t2)
						  AND t1.is_contaminant is null");  
  }
  $sth->execute($geneID);

  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;

}

=head2 get_all_by_interaction

 Arg: Mitocheck::Gene or gene ID
 Description: Gets genes whose proteins interact with proteins of the query gene
 Returntype: list of Mitocheck::Gene objects

=cut

sub get_all_by_interaction {

  my ($self,$gene) = @_;
  my $geneID = ref($gene) ? $gene->ID() : $gene;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $sth = $dbh->prepare("SELECT DISTINCT t.geneID
                           FROM transcript t
                           WHERE t.proteinID in
                            (SELECT p2.* FROM
                              (SELECT DISTINCT interactorID
                               FROM protein_interaction
                               WHERE proteinID IN
                                 (SELECT p3.proteinID FROM
                                    (SELECT proteinID FROM transcript WHERE geneID= ?) as p3
                                 )
                              UNION
                              (SELECT DISTINCT proteinID
                               FROM protein_interaction
                               WHERE interactorID IN
                               (SELECT p4.proteinID FROM
                                  (SELECT proteinID FROM transcript WHERE geneID= ?) as p4)
                              )
                              ) as p2)");
  $sth->execute($geneID,$geneID);

  my @genes;
  while (my ($id)=$sth->fetchrow_array()) {
    push (@genes,$self->get_by_id($id));
  }
  $sth->finish();

  return @genes;

}

=head2 new_gene

 Arg(required): -EnsemblID => $ID
 Arg(required): -transcripts => [@transcripts]
 Arg: -symbol => $symbol
 Arg: -names => [@names]
 Description: creates a new gene object with given attributes
 Returntype: Mitocheck::Gene

=cut

sub new_gene {

  my $self = shift;
  my %gene=@_;
  # attributes = ('-EnsemblID','-transcripts','-symbol','-names');
  unless (defined($gene{'-EnsemblID'}) && $gene{'-EnsemblID'}=~/^ENS\w{0,4}G\d{11}/i) {
    croak "Error: Ensembl ID required for gene";
  }
  unless ($gene{'-transcripts'} && scalar(@{$gene{'-transcripts'}})>0) {
    croak "Error: Gene must have some transcripts";
  }
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $gene = new Mitocheck::Gene($dbc);
  my $EnsemblID = $gene->EnsemblID($gene{'-EnsemblID'});
  unless ($gene{'-symbol'}) {
    $gene{'-symbol'} = $EnsemblID;
  }
  my $symbol = $gene->symbol($gene{'-symbol'});
  my $description = $gene{'-description'}? $gene->($gene{'-description'}):"";
  my @transcripts = @{$gene{'-transcripts'}};
  foreach my $transcript (@transcripts) {
    unless (ref $transcript eq 'Mitocheck::Transcript') {
      my $trh = $dbc->get_TranscriptHandle();
      if ($transcript=~/^TRN\d{7}/) {
	my $ID = $transcript;
	$transcript = $trh->get_by_id($ID);
	if (!defined($transcript)) {
	  croak "Error: Transcript $ID not found in database";
	}
      }
      elsif ($transcript=~/^ENS\w{0,4}T\d{11}/) {
	my $ID = $transcript;
	$transcript = $trh->get_by_EnsemblID($ID);
	if (!defined($transcript)) {
	  my $trh = $dbc->get_TranscriptHandle();
	  $transcript = $trh->new_transcript( -EnsemblID => $ID,
					      -gene => $gene );
	}
      }
      else {
	croak "Error: Transcript $transcript is not a valid transcript";
      }
    }
  }
  $gene->transcripts(@transcripts);
  $gene->names(@{$gene{'-names'}}) if ($gene{-'names'});

  return $gene;

}

=head2 store

 Arg: Mitocheck::Gene
 Description: Enters gene and associated data in Mitocheck database.
 Returntype: Mitocheck::Gene, same as Arg

=cut

sub store {

  my ($self,$gene) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  my $geneID = "";
  my $HUGOID = $gene->symbol();
  my $EnsemblID = $gene->EnsemblID() || croak "Error: Gene must have Ensembl ID";
  if (!$gene->transcripts()) {
    croak "Error: Gene must have some transcripts. Can't store gene";
  }
  # check if gene already in database
  my $query=qq(SELECT geneID
               FROM   gene
               WHERE  EnsemblID = '$EnsemblID');
  my $sth= $dbh->prepare ($query);
  $sth->execute();
  ($geneID)=$sth->fetchrow_array();
  $sth->finish();
  if (!$geneID) {
    $geneID = $gene->ID();
    # update gene table
    my $query = qq(INSERT INTO gene (geneID,symbol,EnsemblID) VALUES ('$geneID','$HUGOID','$EnsemblID'));
    my $rows= $dbh->do ($query);
  }
  else { $gene->ID($geneID) }
  # update transcript table
  foreach my $transcript ($gene->transcripts()) {
    my $trh = $dbc->get_TranscriptHandle();
    $trh->store($transcript);
  }
  # update name table
  my @names = $gene->names();
  foreach my $name (@names) {
    my $nameID;
    my $quoted_name=$dbh->quote($name);
    # check if name already in name table
    my $query = qq(SELECT nameID FROM name WHERE name=$quoted_name);
    $sth = $dbh->prepare($query);
    $sth->execute();
    ($nameID) = $sth->fetchrow_array();
    $sth->finish();
    if (!$nameID) {
      # get last name ID used
      $query = qq(SELECT nameID FROM name ORDER BY nameID DESC LIMIT 1);
      $sth = $dbh->prepare($query);
      $sth->execute();
      my ($lastID) = $sth->fetchrow_array();
      $sth->finish();
      #set new name ID
      $nameID= ++$lastID;
      # add new name to name table
      $query=qq(INSERT INTO name (nameID,name) VALUES ('$nameID',$quoted_name));
      my $rows= $dbh->do ($query);
    }
    # check if name associated with gene in gene_has_name table
    $query = qq(SELECT geneID FROM gene_has_name WHERE nameID='$nameID' AND geneID='$geneID');
    $sth = $dbh->prepare($query);
    $sth->execute();
    my ($found) = $sth->fetchrow_array();
    $sth->finish();
    if (!$found) {
      # update gene_has_name table
      my $query=qq(INSERT INTO gene_has_name (nameID,geneID) VALUES ('$nameID','$geneID'));
      my $rows= $dbh->do ($query);
    }
  }
  $gene->_decr_newID_count() if $gene->{'is_new'};
  $gene->{'is_new'} = undef;
  return $gene;
}

=head2 remove

 Arg: Mitocheck::Gene or gene ID or Ensembl gene ID
 Description: removes gene and associated transcripts and
              associated mapping data from Mitocheck database.
 Returntype: ID of the gene just removed

=cut

sub remove {

  my ($self,$gene) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  if ($gene=~/^ENS\w{0,4}G\d{11}/i) {
    $gene = $self->get_by_EnsemblID($gene);
  }
  elsif ($gene=~/^GEN\d{8}/i) {
    $gene = $self->get_by_id($gene);
  }
  unless (ref $gene eq 'Mitocheck::Gene') {
    croak "Not a valid gene";
  }
  # delete transcripts
  my @transcripts = $gene->transcripts();
  my $trh = $dbc->get_TranscriptHandle();
  foreach my $transcript(@transcripts) {
    $trh->remove($transcript);
  }
  my $geneID = $gene->ID();
  # delete gene
  my $query = qq(DELETE FROM gene WHERE geneID='$geneID');
  my $rows = $dbh->do($query);
  # DELETE split into 2 queries to allow updates when one of the tables has already been modified
  $query = qq(DELETE FROM gene_has_name WHERE geneID='$geneID');
  $rows = $dbh->do($query);
  return $geneID;

}


1;
