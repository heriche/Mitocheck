# Author: jkh1
# 2011-01-14
#

=head1 NAME

 Mitocheck::User

=head1 SYNOPSIS




=head1 DESCRIPTION

 Representation of a user. A user represents who is authorised to view data
 from some sources. If a userID is set for a source, then data from this
 source can only be viewed by the corresponding user.
 NOTE: This is not implemented yet (and may never be).

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::User;


use strict;
use warnings;
use Mitocheck::DBConnection;
use Scalar::Util qw(weaken);
use Digest::MD5;

=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, user ID as string
 Description: Creates a new user object.
 Returntype: Mitocheck::User

=cut

sub new {

  my ($class,$dbc,$userID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_source_count();
  if (!defined $self->{'ID'} && !$userID) {
    # issue new ID
    $userID = (Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$)));
  }
  $self->{'ID'} = $userID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck user ID
 Description: Gets/sets Mitocheck user ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 name

 Arg:  optional, user's name
 Description: Gets/sets user's name
 Returntype: string

=cut

sub name {

  my $self = shift;
  $self->{'name'} = shift if @_;
  if (!defined $self->{'name'}) {
    my $userID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT u.name FROM user u WHERE u.userID = ? ");
    $sth->execute($userID);
    ($self->{'name'}) = $sth->fetchrow_array() || "";
    $sth->finish();
  }
  return $self->{'name'};

}

=head2 login

 Arg:  optional, user's login name
 Description: Gets/sets user's login name
 Returntype: string

=cut

sub login {

  my $self = shift;
  $self->{'login'} = shift if @_;
  if (!defined $self->{'login'}) {
    my $userID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT u.login FROM user u WHERE u.userID = ? ");
    $sth->execute($userID);
    ($self->{'login'}) = $sth->fetchrow_array() || "";
    $sth->finish();
  }
  return $self->{'login'};

}

=head2 password

 Arg:  optional, user's password
 Description: Gets/sets user's password
 Returntype: string

=cut

sub password {

  my $self = shift;
  $self->{'password'} = shift if @_;
  if (!defined $self->{'password'}) {
    my $userID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT u.password FROM user u WHERE u.userID = ? ");
    $sth->execute($userID);
    ($self->{'password'}) = $sth->fetchrow_array() || "";
    $sth->finish();
  }
  return $self->{'password'};

}

1;
