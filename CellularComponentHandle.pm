# Author: jkh1
# 2013-04-19
#

=head1 NAME

 Mitocheck::CellularComponentHandle

=head1 SYNOPSIS


=head1 DESCRIPTION

 Enables retrieval of Mitocheck cellular component objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::CellularComponentHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::CellularComponent;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new cellular component object handle
 Returntype: Mitocheck::CellularComponentHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: cellular component ID
 Description: Gets cellular component with given ID
 Returntype: Mitocheck::CellularComponent object

=cut

sub get_by_id {

  my ($self,$ccID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if exists in database
  my $query = qq(SELECT COUNT(*) FROM cellular_component WHERE cellular_componentID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($ccID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return Mitocheck::CellularComponent->new($dbc,$ccID);

}

=head2 get_by_GOID

 Arg: cellular component GO ID
 Description: Gets cellular component with given ID
 Returntype: Mitocheck::CellularComponent object

=cut

sub get_by_GOID {

  my ($self,$ID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT cellular_componentID FROM cellular_component WHERE GOID= ?);
  my $sth= $dbh->prepare($query);
  $sth->execute($ID);
  my ($ccID) = $sth->fetchrow_array();

  return undef if (!defined $ccID);

  return $self->get_by_id($ccID);

}

=head2 get_by_GOterm

 Arg: cellular component GO term
 Description: Gets cellular component with given term
 Returntype: Mitocheck::CellularComponent object

=cut

sub get_by_GOterm {

  my ($self,$term) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT cellular_componentID FROM cellular_component WHERE GOterm= ?);
  my $sth= $dbh->prepare($query);
  $sth->execute($term);
  my ($ccID) = $sth->fetchrow_array();

  return undef if (!defined $ccID);

  return $self->get_by_id($ccID);

}

=head2 new_cellular_component

 Args: key => value pairs
 Description: creates a new cellular component object
 Returntype: Mitocheck::CellularComponent

=cut

sub new_cellular_component {

  my $self = shift;
  my %cellular_component = @_;
  my $dbc = $self->{'DBConnection'};
  my @required_attributes=('-GOID','-GOterm','-description');
  foreach my $attribute (@required_attributes) {
    unless ($cellular_component{$attribute}) {
      croak "Error: Attribute $attribute required for cellular component";
    }
  }
  my $cellular_component = Mitocheck::CellularComponent->new($dbc);
  $cellular_component->GOID($cellular_component{'-GOID'});
  $cellular_component->GOTerm($cellular_component{'-GOterm'});
  $cellular_component->description($cellular_component{'-description'});

  return $cellular_component;

}

=head2 store

 Arg: Mitocheck::CellularComponent
 Description: Enter cellular component and associated data in Mitocheck database.
 Returntype: Mitocheck::CellularComponent, same as Arg

=cut

sub store {

  my ($self,$cellular_component) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  # Check if already exists
  my $GOID = $cellular_component->GOID;
  my $query = qq(SELECT cellular_componentID
                 FROM cellular_component
                 WHERE GOID= ? );
  my $sth = $dbh->prepare($query);
  $sth->execute($GOID);
  my ($ccID) = $sth->fetchrow_array();
  $sth->finish();
  if ($ccID) {
    $cellular_component->ID($ccID);
  }
  else {
  	my $ccID = $cellular_component->ID;
	my $GOterm = $cellular_component->GOTerm;
	my $desc = $cellular_component->description;
	my $query = qq(INSERT INTO cellular_component (cellular_componentID,GOID,GOterm,description)
                   VALUES ('$ccID','$GOID','$GOterm','$desc'));
    my $rows = $dbh->do($query);
  }
  $cellular_component->_decr_newID_count() if $cellular_component->{'is_new'};
  $cellular_component->{'is_new'} = undef;

  return $cellular_component;
}


sub remove {

  my ($self,$cellular_component) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};

  my $ccID = $cellular_component->ID;
  my $query=qq(DELETE FROM cellular_component WHERE cellular_componentID='$ccID');
  $dbh->do($query);

  return $cellular_component->ID;
}

1;
