# Author: jkh1
# 2013-07-03
#

=head1 NAME

 Mitocheck::DataFile

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $dfh = $dbc->get_DataFileHandle();

 my $file = $dfh->get_by_id('DTF00000123');

 my $filename = $file->filename();

=head1 DESCRIPTION

 Representation of Mitocheck data files.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::DataFile;

use strict;
use warnings;
use Scalar::Util qw(weaken);


{
  my $_file_count = 0;
  my $_newID_count = 0;

  sub get_file_count {
    return $_file_count;
  }
  sub _incr_file_count {
    return ++$_file_count;
  }
  sub _decr_file_count {
    return --$_file_count;
  }
  sub get_newID_count {
    return $_newID_count;
  }
  sub _incr_newID_count {
    return ++$_newID_count;
  }
  sub _decr_newID_count {
    return --$_newID_count;
  }
}


=head2 new

 Arg1: Mitocheck::DBConnection
 Arg2: optional, data file ID
 Description: Creates a new data file object.
 Returntype: Mitocheck::DataFile

=cut

sub new {

  my ($class,$dbc,$ID) = @_;
  my $self = {};
  $self->{'DBConnection'} = $dbc;
  weaken($self->{'DBConnection'});
  my $dbh = $dbc->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  $class->_incr_file_count();
  if (!defined $self->{'ID'} && !$ID) {
    # find last ID used
    my $i;
    my $query=qq(SELECT   data_fileID
                 FROM     data_file
                 ORDER BY data_fileID
                 DESC
                 LIMIT 1);
    my $sth= $dbh->prepare ($query);
    $sth->execute();
    my ($lastID) = $sth->fetchrow_array();
    if (defined $lastID) { $i = substr($lastID,3,8) }
    # issue new ID
    $class->_incr_newID_count();
    $i += $class->get_newID_count();
    $ID = "DTF"."0"x(8-length $i).$i;;
    $self->{'is_new'} = 1;
  }
  $self->{'ID'} = $ID;
  bless ($self, $class);

  return $self;

}

=head2 ID

 Arg: optional, Mitocheck data file ID
 Description: Gets/sets Mitocheck ID
 Returntype: string

=cut

sub ID {

  my $self = shift;
  $self->{'ID'} = shift if @_;
  return $self->{'ID'};

}

=head2 filename

 Arg: optional, file name
 Description: Gets/sets data file name
 Returntype: string

=cut

sub filename {

  my $self = shift;
  $self->{'filename'} = shift if @_;
  if (!defined $self->{'filename'}) {
    my $ID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT filename FROM data_file
                             WHERE data_fileID = ? ");
    $sth->execute($ID);
    ($self->{'filename'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'filename'};

}

=head2 type

 Arg: optional, type
 Description: Gets/sets data type
 Returntype: string

=cut

sub type {

  my $self = shift;
  $self->{'type'} = shift if @_;
  if (!defined $self->{'type'}) {
    my $ID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT type FROM data_file
                             WHERE data_fileID = ? ");
    $sth->execute($ID);
    ($self->{'type'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'type'};

}

=head2 data_name

 Arg: optional, data name
 Description: Gets/sets name of the data contained in the file
 Returntype: string

=cut

sub data_name {

  my $self = shift;
  $self->{'data_name'} = shift if @_;
  if (!defined $self->{'data_name'}) {
    my $ID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT data_name FROM data_file
                             WHERE data_fileID = ? ");
    $sth->execute($ID);
    ($self->{'data_name'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'data_name'};

}

=head2 format

 Arg: optional, file format
 Description: Gets/sets format of the file
 Returntype: string

=cut

sub format {

  my $self = shift;
  $self->{'format'} = shift if @_;
  if (!defined $self->{'format'}) {
    my $ID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT format FROM data_file
                             WHERE data_fileID = ? ");
    $sth->execute($ID);
    ($self->{'format'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'format'};

}

=head2 spot_position

 Arg: optional, string
 Description: Gets/sets position of the spot where the data has been acquired
 Returntype: string

=cut

sub spot_position {

  my $self = shift;
  $self->{'spot_position'} = shift if @_;
  if (!defined $self->{'spot_position'}) {
    my $ID = $self->{'ID'};
    my $dbh = $self->{'database_handle'};
    my $sth = $dbh->prepare("SELECT spot_position FROM data_file
                             WHERE data_fileID = ? ");
    $sth->execute($ID);
    ($self->{'spot_position'}) = $sth->fetchrow_array();
    $sth->finish();
  }
  return $self->{'spot_position'};

}

sub DESTROY {

  my $self = shift;
  $self->_decr_file_count();
  $self->_decr_newID_count() if $self->{'is_new'};
}

1;
