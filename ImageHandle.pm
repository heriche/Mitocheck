# Author: jkh1
# 2005-05-26
#

=head1 NAME

 Mitocheck::ImageHandle

=head1 SYNOPSIS

 use Mitocheck::DBConnection;

 my $dbc = Mitocheck::DBConnection->new(
                                        -database => 'mitocheck2_0',
			                -host     => 'www.mitocheck.org',
			                -user     => 'anonymous',
			                -password => '');

 my $image_handle = $dbc->get_ImageHandle();

 my $image = $image_handle->get_by_id('IMG00000001');

 my $new_movie = $image_handle->new_image( -filename => 'new_movie.avi',
                                           -file_type => 'movie',
                                           -class => 'phenotype',
                                           -dsRNA => $dsRNA,
                                           -reporters => ['Histone-GFP','Rhodamine-Phalloidin'],
                                           -source => $source)


=head1 DESCRIPTION

 Enables retrieval of Mitocheck image objects.

=head1 CONTACT

 heriche@embl.de

=cut

=head1 METHODS

=cut

package Mitocheck::ImageHandle;


use strict;
use warnings;
use Carp;
use Mitocheck::Image;
use Scalar::Util qw(weaken);


=head2 new

 Arg: Mitocheck::DBConnection
 Description: Creates a new image object handle
 Returntype: Mitocheck::ImageHandle

=cut

sub new {

  my $class = shift;
  my $self = {};
  $self->{'DBConnection'} = shift;
  weaken($self->{'DBConnection'});
  my $dbh = $self->{'DBConnection'}->{'database_handle'};
  $self->{'database_handle'} = $dbh;
  bless ($self, $class);

  return $self;

}

=head2 get_by_id

 Arg: image ID
 Description: Gets image with given ID
 Returntype: Mitocheck::Image object

=cut

sub get_by_id {

  my ($self,$imageID) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  # check if image exists in database
  my $query = qq(SELECT COUNT(*) FROM image WHERE imageID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($imageID);
  my ($count) = $sth->fetchrow_array();
  if ($count==0) {
    return undef;
  }

  return new Mitocheck::Image($dbc,$imageID);

}

=head2 get_by_filename

 Arg1: string
 Description: Gets image with given file name
 Returntype: Mitocheck::Image object

=cut

sub get_by_filename {

  my ($self,$filename) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $qfilename = $dbh->quote($filename);
  my $query = qq(SELECT imageID FROM image WHERE filename=$qfilename);
  my $sth= $dbh->prepare ($query);
  $sth->execute();
  my ($imageID) = $sth->fetchrow_array();
  if (!$imageID) {
    return undef;
  }

  return new Mitocheck::Image($dbc,$imageID);

}

=head2 get_all_by_setid

 Arg: integer, image set ID
 Description: Gets all images in a set
 Returntype: list of Mitocheck::Image object

=cut

sub get_all_by_setid {

  my ($self,$image_setID) = @_;
  my @set;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my $query = qq(SELECT imageID FROM image WHERE image_setID= ?);
  my $sth= $dbh->prepare ($query);
  $sth->execute($image_setID);
  while ( my ($imageID) = $sth->fetchrow_array()) {
    push @set,$self->get_by_id($imageID) if ($imageID);
  }

  return @set;

}

=head2 get_all_by_spot

 Arg1: spot location, usually in the form plateID/spot_number
       (e.g. LT00123/284)
 Description: Gets images for the corresponding plate position
 Returntype: list of Mitocheck::Image object

=cut

sub get_all_by_spot {

  my ($self,$spot) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my @images;
  my $query = qq(SELECT i.imageID
                 FROM image i, image_set ims
                 WHERE ims.spot_location=?
                 AND ims.image_setID=i.image_setID);
  my $sth= $dbh->prepare ($query);
  $sth->execute($spot);
  while (my ($imageID) = $sth->fetchrow_array()) {
    push @images,$self->get_by_id($imageID);
  }
  $sth->finish();

  return @images;

}

=head2 new_image

 Args: key => value pairs
 Description: creates a new image object
 Returntype: Mitocheck::Image

=cut

sub new_image {

  my $self = shift;
  my %image = @_;
  my $dbc = $self->{'DBConnection'};
  my @required_attributes=('-filename','-file_type','-reporters','-cell_line');
  foreach my $attribute (@required_attributes) {
    unless ($image{$attribute}) {
      croak "Error: Attribute $attribute required for image/movie";
    }
  }
  my @reporters = @{$image{'-reporters'}};
  my $image = Mitocheck::Image->new($dbc);
  $image->filename($image{'-filename'});
  $image->file_type($image{'-file_type'});
  $image->reporters(@reporters);
  $image->cell_line($image{'-cell_line'});
  $image->display_order($image{'-display_order'});
  $image->comments($image{'-comments'});
  $image->setID($image{'-setID'}) if $image{'-setID'};;

  return $image;

}

=head2 store

 Arg: Mitocheck::Image
 Description: Enters image and associated data in Mitocheck database.
 Returntype: Mitocheck::Image, same as Arg

=cut

sub store {

  my ($self,$image) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $self->{'database_handle'};
  my @required_attributes=('-filename','-type','-reporters','-cell_line');
  foreach my $attribute (@required_attributes) {
    unless ($image->{$attribute}) {
      croak "Error: Attribute $attribute required for image/movie";
    }
  }
  my $imageID;

  my $qfilename = $dbh->quote($image->filename());
  my $file_type = $image->file_type();
  my @reporters = $image->reporters();
  my $comments = $image->comments() || "";
  my $setID = $image->setID() || "";
  my $disp_order = $image->display_order() || "";
  my $cell_line = $image->cell_line;
  my $cell_lineID = $cell_line->ID;

  # Check if already in database using file name
  my $query =qq(SELECT imageID FROM image WHERE filename=$qfilename);
  my $sth = $dbh->prepare($query);
  $sth->execute();
  ($imageID)=$sth->fetchrow_array();
  $sth->finish();
  if (!$imageID) {
    $imageID = $image->ID();
    # add to database
    $query=qq(INSERT INTO image (imageID,image_setID,filename,file_type,comments,display_order,cell_lineID)
              VALUES ('$imageID','$setID',$qfilename,'$file_type','$comments','$disp_order','$cell_lineID'));
    $dbh->do($query);
    my $rh = $dbc->get_ReporterHandle();
    foreach my $reporter(@reporters) {
      $reporter = $rh->store($reporter);
      my $reporterID = $reporter->ID;
      my $query = qq(INSERT INTO image_has_reporter (imageID,reporterID)
  	             VALUES ('$imageID','$reporterID'));
      my $rows = $dbh->do($query);
    }
  }
  else { $image->ID($imageID) }

  $image->_decr_newID_count() if $image->{'is_new'};
  $image->{'is_new'} = undef;

  return $image;

}

sub remove {

  my ($self,$image) = @_;
  my $dbc = $self->{'DBConnection'};
  my $dbh = $dbc->{'database_handle'};
  my $imageID = ref($image) eq 'Mitocheck::Image' ? $image->ID() : $image;
  my $setID = $image->setID;
  unless ($imageID=~/^IMG\d{8}/i) {
    croak "Not a valid image";
  }
  my $query = qq(DELETE FROM image WHERE imageID='$imageID');
  my $rows = $dbh->do($query);
  $query = qq(DELETE FROM image_has_reporter WHERE imageID='$imageID');
  $rows = $dbh->do($query);
  # was it the last image in the set ?
  $query = qq(SELECT imageID FROM image WHERE image_setID= ?);
  my $sth = $dbh->prepare($query);
  $sth->execute($imageID);
  my ($id)=$sth->fetchrow_array();
  $sth->finish;
  if (!$id) { # this was the last image in the set
    $query = qq(DELETE FROM image_set WHERE image_setID='$setID');
    $rows = $dbh->do($query);
    $query = qq(DELETE FROM image_set_has_phenotype WHERE image_setID='$setID');
    $rows = $dbh->do($query);
    $query = qq(DELETE FROM image_set_has_attribute WHERE image_setID='$setID');
    $rows = $dbh->do($query);
    $query = qq(DELETE FROM localization_occurrence_has_image_set WHERE image_setID='$setID');
    $rows = $dbh->do($query);
    $query = qq(DELETE FROM dsRNA_has_image_set WHERE image_setID='$setID');
    $rows = $dbh->do($query);
  }

  return $imageID;

}

1;
